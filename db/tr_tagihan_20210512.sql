-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 12, 2021 at 12:12 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apa_ditlala`
--

-- --------------------------------------------------------

--
-- Table structure for table `tr_tagihan`
--

CREATE TABLE `tr_tagihan` (
  `id` int(5) NOT NULL,
  `id_kontrak` int(5) DEFAULT NULL,
  `id_detail_kegiatan` int(5) DEFAULT NULL,
  `nomor_tagihan` varchar(20) NOT NULL,
  `jenis_tagihan` enum('kontrak','non_kontrak') NOT NULL,
  `nilai_tagihan` int(5) NOT NULL,
  `potongan_pajak_ppn` int(5) NOT NULL,
  `jenis_pajak_pph` varchar(5) NOT NULL,
  `potongan_pajak_pph` int(5) NOT NULL,
  `uraian_tagihan` varchar(225) NOT NULL,
  `file_surat_permohonan` varchar(100) NOT NULL,
  `file_invoice` varchar(100) NOT NULL,
  `file_kwitansi` varchar(100) NOT NULL,
  `file_faktur_pajak` varchar(100) NOT NULL,
  `file_bap` varchar(100) NOT NULL,
  `file_bapp` varchar(100) NOT NULL,
  `file_bast` varchar(100) NOT NULL,
  `file_laporan` varchar(100) NOT NULL,
  `file_foto_kegiatan` varchar(100) NOT NULL,
  `status_tagihan` enum('verifikasi bendahara','ppspm','terbit spm','') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tr_tagihan`
--

INSERT INTO `tr_tagihan` (`id`, `id_kontrak`, `id_detail_kegiatan`, `nomor_tagihan`, `jenis_tagihan`, `nilai_tagihan`, `potongan_pajak_ppn`, `jenis_pajak_pph`, `potongan_pajak_pph`, `uraian_tagihan`, `file_surat_permohonan`, `file_invoice`, `file_kwitansi`, `file_faktur_pajak`, `file_bap`, `file_bapp`, `file_bast`, `file_laporan`, `file_foto_kegiatan`, `status_tagihan`, `created_at`, `updated_at`, `deleted_at`) VALUES
(50, NULL, 7, 'CGV-752-FAST', 'non_kontrak', 78200000, 0, 'pph 1', 0, '', 'surat-permohonan-cgv-752-fast-20210512161303.jpg', 'invoice-cgv-752-fast-20210512161303.jpg', 'kwitansi-cgv-752-fast-20210512161303.jpg', 'faktur-pajak-cgv-752-fast-20210512161303.jpg', 'bap-cgv-752-fast-20210512161303.jpg', 'bapp-cgv-752-fast-20210512161303.jpg', 'bast-cgv-752-fast-20210512161303.jpg', 'laporan-cgv-752-fast-20210512161303.jpg', 'foto-kegiatan-cgv-752-fast-20210512161303.jpg', 'verifikasi bendahara', '0000-00-00 00:00:00', '2021-05-12 09:13:03', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tr_tagihan`
--
ALTER TABLE `tr_tagihan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tr_tagihan`
--
ALTER TABLE `tr_tagihan`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
