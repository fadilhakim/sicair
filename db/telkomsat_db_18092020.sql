-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2020 at 03:31 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `telkomsat_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_groups`
--

CREATE TABLE `m_groups` (
  `id` int(11) NOT NULL,
  `group_name` varchar(255) NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_groups`
--

INSERT INTO `m_groups` (`id`, `group_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '0000-00-00 00:00:00', '2020-09-14 15:01:55'),
(2, 'SD', '0000-00-00 00:00:00', '2020-09-14 15:01:55');

-- --------------------------------------------------------

--
-- Table structure for table `m_menu`
--

CREATE TABLE `m_menu` (
  `id` int(11) NOT NULL,
  `menu_name` varchar(50) NOT NULL,
  `menu_key` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_menu`
--

INSERT INTO `m_menu` (`id`, `menu_name`, `menu_key`, `created_at`, `updated_at`) VALUES
(1, 'Dashboard', 'dashboard', '2020-09-16 00:00:00', '2020-09-16 16:35:31'),
(2, 'Project Update', 'project_update', '0000-00-00 00:00:00', '2020-09-16 16:35:31'),
(3, 'Project History', 'project_history', '2020-09-16 00:00:00', '2020-09-16 16:35:31'),
(4, 'User Setting', 'user_setting', '2020-09-16 00:00:00', '2020-09-16 16:36:16'),
(5, 'Group Setting', 'group_setting', '2020-09-16 00:00:00', '2020-09-16 16:36:16');

-- --------------------------------------------------------

--
-- Table structure for table `m_users`
--

CREATE TABLE `m_users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `fullname` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `group_id` int(11) NOT NULL DEFAULT 0,
  `status_id` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_users`
--

INSERT INTO `m_users` (`id`, `username`, `password`, `fullname`, `email`, `group_id`, `status_id`, `created_at`, `updated_at`) VALUES
(1, 'admin', '52c69e3a57331081823331c4e69d3f2e', 'Aries Dimas Yudhistira', 'alhusna901@gmail.com', 1, 1, '2020-09-08 00:00:00', '2020-09-08 07:28:29'),
(2, 'Rian', '52c69e3a57331081823331c4e69d3f2e', 'Rian Dikison', 'rian@gmail.com', 2, 1, '2020-09-08 00:00:00', '2020-09-08 07:28:32'),
(4, 'fadil', '52c69e3a57331081823331c4e69d3f2e', 'fadilhakim', 'fadilhakim@gmail.com', 2, 1, '0000-00-00 00:00:00', '2020-09-08 10:23:48');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(225) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`id`, `title`, `description`, `created_at`) VALUES
(1, 'Lorem Ipsum', 'Sit Dolor Amet', '2020-08-31 15:15:56');

-- --------------------------------------------------------

--
-- Table structure for table `tr_group_menu`
--

CREATE TABLE `tr_group_menu` (
  `id` int(5) NOT NULL,
  `group_id` int(3) NOT NULL,
  `menu_id` int(3) NOT NULL,
  `authorized` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tr_group_menu`
--

INSERT INTO `tr_group_menu` (`id`, `group_id`, `menu_id`, `authorized`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2020-09-16 00:00:00', '2020-09-16 16:51:30'),
(2, 1, 2, 1, '2020-09-16 00:00:00', '2020-09-16 16:51:30'),
(3, 1, 3, 1, '2020-09-16 00:00:00', '2020-09-16 16:51:30'),
(4, 1, 4, 1, '2020-09-16 00:00:00', '2020-09-16 16:51:30'),
(5, 1, 5, 1, '2020-09-16 00:00:00', '2020-09-16 16:51:30'),
(11, 2, 1, 1, '2020-09-17 00:00:00', '2020-09-17 13:53:48'),
(12, 2, 2, 1, '2020-09-17 00:00:00', '2020-09-17 13:53:56'),
(13, 2, 3, 1, '2020-09-17 00:00:00', '2020-09-17 13:54:03'),
(14, 2, 4, 0, '2020-09-17 00:00:00', '2020-09-17 13:54:13'),
(15, 2, 5, 0, '2020-09-17 00:00:00', '2020-09-17 13:54:23');

-- --------------------------------------------------------

--
-- Table structure for table `tr_user_login_records`
--

CREATE TABLE `tr_user_login_records` (
  `id` int(11) NOT NULL,
  `user_id` int(5) NOT NULL,
  `ip_address` varchar(20) NOT NULL,
  `location` varchar(225) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tr_user_login_records`
--

INSERT INTO `tr_user_login_records` (`id`, `user_id`, `ip_address`, `location`, `created_at`) VALUES
(1, 1, '127.0.0.1', 'Indonesia/DKI Jakarta/jakarta', '2020-09-17 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_groups`
--
ALTER TABLE `m_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_menu`
--
ALTER TABLE `m_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_users`
--
ALTER TABLE `m_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group fk` (`group_id`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_group_menu`
--
ALTER TABLE `tr_group_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`) USING BTREE,
  ADD KEY `menu_id` (`menu_id`);

--
-- Indexes for table `tr_user_login_records`
--
ALTER TABLE `tr_user_login_records`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_groups`
--
ALTER TABLE `m_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `m_menu`
--
ALTER TABLE `m_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `m_users`
--
ALTER TABLE `m_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tr_group_menu`
--
ALTER TABLE `tr_group_menu`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tr_user_login_records`
--
ALTER TABLE `tr_user_login_records`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `m_users`
--
ALTER TABLE `m_users`
  ADD CONSTRAINT `m_users_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `m_groups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tr_group_menu`
--
ALTER TABLE `tr_group_menu`
  ADD CONSTRAINT `tr_group_menu_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `m_groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tr_group_menu_ibfk_2` FOREIGN KEY (`menu_id`) REFERENCES `m_menu` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tr_user_login_records`
--
ALTER TABLE `tr_user_login_records`
  ADD CONSTRAINT `tr_user_login_records_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `m_users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
