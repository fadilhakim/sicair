-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Sep 20, 2020 at 01:51 AM
-- Server version: 5.7.25
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `telkomsat_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tr_group_menu`
--

CREATE TABLE `tr_group_menu` (
  `id` int(5) NOT NULL,
  `group_id` int(3) NOT NULL,
  `menu_id` int(3) NOT NULL,
  `authorized` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tr_group_menu`
--

INSERT INTO `tr_group_menu` (`id`, `group_id`, `menu_id`, `authorized`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2020-09-16 00:00:00', '2020-09-16 16:51:30'),
(2, 1, 2, 1, '2020-09-16 00:00:00', '2020-09-16 16:51:30'),
(3, 1, 3, 1, '2020-09-16 00:00:00', '2020-09-16 16:51:30'),
(4, 1, 4, 1, '2020-09-16 00:00:00', '2020-09-16 16:51:30'),
(5, 1, 5, 1, '2020-09-16 00:00:00', '2020-09-16 16:51:30'),
(11, 2, 1, 1, '2020-09-17 00:00:00', '2020-09-17 13:53:48'),
(12, 2, 2, 1, '2020-09-17 00:00:00', '2020-09-17 13:53:56'),
(13, 2, 3, 1, '2020-09-17 00:00:00', '2020-09-17 13:54:03'),
(14, 2, 4, 0, '2020-09-17 00:00:00', '2020-09-17 13:54:13'),
(15, 2, 5, 0, '2020-09-17 00:00:00', '2020-09-17 13:54:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tr_group_menu`
--
ALTER TABLE `tr_group_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`) USING BTREE,
  ADD KEY `menu_id` (`menu_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tr_group_menu`
--
ALTER TABLE `tr_group_menu`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
