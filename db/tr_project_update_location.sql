-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 27, 2020 at 06:27 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `telkomsat_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tr_project_update_location`
--

CREATE TABLE `tr_project_update_location` (
  `id` int(11) NOT NULL,
  `no_provisioning` varchar(150) NOT NULL,
  `sid` varchar(150) NOT NULL,
  `project_update_id` int(11) NOT NULL,
  `h_kontrak_nodelink_id` int(11) NOT NULL,
  `BW` int(11) NOT NULL,
  `alamat` varchar(225) NOT NULL,
  `kota` int(11) NOT NULL,
  `provinsi` int(11) NOT NULL,
  `PIC` varchar(150) NOT NULL,
  `pic_phone_number` varchar(30) NOT NULL,
  `teknisi` varchar(150) NOT NULL,
  `teknisi_phone_number` varchar(50) NOT NULL,
  `tgl_mulai_progress` date NOT NULL,
  `TOL` varchar(100) NOT NULL,
  `status` int(5) NOT NULL,
  `keterangan` varchar(225) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tr_project_update_location`
--

INSERT INTO `tr_project_update_location` (`id`, `no_provisioning`, `sid`, `project_update_id`, `h_kontrak_nodelink_id`, `BW`, `alamat`, `kota`, `provinsi`, `PIC`, `pic_phone_number`, `teknisi`, `teknisi_phone_number`, `tgl_mulai_progress`, `TOL`, `status`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, '', '3020001030001', 1, 0, 25, 'DESA MANIS MATA', 0, 0, 'Fadil', '089632541778', 'Dimas', '081456325669', '0000-00-00', '', 3, 'Hello word', '2020-09-27 05:34:29', '2020-09-27 04:12:06'),
(2, '', '5480001690001', 4, 23, 0, 'Jl. Rukan Emerald Blok UA No. 2, Kelurahan Harapan Mulya, Kecamatan Medan Satria , Kabupaten Bekasi\r\n', 0, 0, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:45', '2020-09-27 04:09:45'),
(3, '', '5480001690002', 4, 24, 2, 'Jl. KH. Sholeh Iskandar KM.6,6 Kelurahan Kedung Badak, Kota Bogor\r\n', 0, 0, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:45', '2020-09-27 04:09:45'),
(4, '', '5480001690003', 4, 25, 0, 'Jl. Raya industri tegal gede RT. 009 /013, Desa pasir sari, Kec. Cikarang Selatan, Kab. Bekasi\r\n', 0, 0, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:45', '2020-09-27 04:09:45'),
(5, '', '5480001690004', 4, 26, 0, 'Jl. PLN Duren Tiga No. 101, Kel. Duren Tiga, Kec. Pancoran, Jakarta Selatan\r\n', 0, 0, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:45', '2020-09-27 04:09:45'),
(6, '', '5480001690005', 4, 27, 0, 'Jl. Tole iskandar Rt. 04 Rw. 04, Kel. Sukmajaya, Kec. Sukmajaya. Kota Depok\r\n', 0, 0, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:45', '2020-09-27 04:09:45'),
(7, '', '5480001690006', 4, 28, 0, 'Jl. Raya Perjuangan No. 9B, Kebon Jeruk, Jakarta Barat (Samping Bank BJB)\r\n', 0, 0, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:45', '2020-09-27 04:09:45'),
(8, '', '5480001690007', 4, 29, 0, 'Jl. Raya Boulevard Barat komplek Plaza kelapa gading Blok C No. 58-59, Kel. Kelapa gading barat, Kec. Kelapa gading. Jakarta Utara', 0, 0, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:45', '2020-09-27 04:09:45'),
(9, '', '5480001690008', 4, 30, 0, 'Gedung Bank Mandiri Lantai 3, Jl. Matraman Raya No. 31, Jakarta Timur\r\n', 0, 0, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:45', '2020-09-27 04:09:45'),
(10, '', '5480001690009', 4, 31, 0, 'Jl. KH. A. Fatah Hasan Blok A 4, Sumur Pecung - Kota Serang\r\n', 0, 0, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:45', '2020-09-27 04:09:45'),
(11, '', '5480001690010', 4, 32, 0, 'Jl. Hartono raya (Arah jalur masuk) Ruko modernland Arcade no. 16 - 17, Tangerang\r\n', 0, 0, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:45', '2020-09-27 04:09:45'),
(12, '', '5480001690011', 4, 33, 0, 'Jl. BKR No. 190 Rt. 004 /05, Kel Ciatuel, Kec. Regol, Bandung, Jawa Barat\r\n', 0, 0, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:45', '2020-09-27 04:09:45'),
(13, '', '5480001690012', 4, 34, 0, 'Jl. KH Abdullah Bin Noeh no. 5 RT/RW 003/010, Kel. Sawah Gede, Kec. Cianjur, Cianjur\r\n', 0, 0, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:45', '2020-09-27 04:09:45'),
(14, '', '5480001690013', 4, 35, 0, 'Jl. Raya Jatsari, Balonggadu, Karawang, Jawa Barat\r\n', 0, 0, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:45', '2020-09-27 04:09:45'),
(15, '', '5480001690014', 4, 36, 0, 'Jl. Jend. H Amir Machmud no. 864 C-D, Kel. Padasuka, Kec. Cimahi Tengah, Cimahi, Jawa Barat.\r\n', 0, 0, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:45', '2020-09-27 04:09:45'),
(16, '', '5480001690015', 4, 37, 0, 'Ruko Gold Sunset No. 12A dan 12B, Cirebon Super Blok, Kelurahan Pekiringan, Kecamatan Kesambi Cirebon\r\n', 0, 0, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:45', '2020-09-27 04:09:45'),
(17, '', '5480001690016', 4, 38, 0, 'Ruko Grand Taruma Blok A, jl interchange tol karawang barat (sebelah bank mandiri)\r\n', 0, 0, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:45', '2020-09-27 04:09:45'),
(18, '', '5480001690017', 4, 39, 0, 'Jl. Otto Iskandar Dinata 101, RT/RW 084/13, Desa Karang Anyar, Kec. Subang, Jawa Barat\r\n', 0, 0, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:45', '2020-09-27 04:09:45'),
(19, '', '5480001690018', 4, 40, 0, 'Jl. Brawijaya Rt. 02 Rw. 21, Kel. Sriwidari, Kec. Gunung puyuh, Kota sukabumi\r\n', 0, 0, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:46', '2020-09-27 04:09:46'),
(20, '', '5480001690019', 4, 41, 0, 'Tasik Indah Plaza No.4-5, Jl. KHZ Mustofa, Tasikmalaya\r\n', 0, 0, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:46', '2020-09-27 04:09:46'),
(21, '', '5480001690020', 4, 42, 0, 'Jl. Jend. Sudirman No. 220 Rt. 01/RW 03, Desa Tumpangkrasak, Kec. Jati, Kab. Kudus, Jawa Tengah\r\n', 0, 0, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:46', '2020-09-27 04:09:46'),
(22, '', '5480001690021', 4, 43, 0, 'Jl Diponegoro No.56, Magelang\r\n', 0, 0, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:46', '2020-09-27 04:09:46'),
(23, '', '5480001690022', 4, 44, 0, 'Jl. Ahmad Yani, Kel. Bener, Kec. Wiradesa, Kab. Pekalongan. Jawa Tengah\r\n', 0, 0, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:46', '2020-09-27 04:09:46'),
(24, '', '5480001690023', 4, 45, 0, 'Jl S Parman No 18 Purwokerto\r\n', 0, 0, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:46', '2020-09-27 04:09:46'),
(25, '', '5480001690024', 4, 46, 0, 'Ruko Platinum No. 10 F Jl. Soekarno Hatta Rt. 005/ 001, Kel. Tlogosari Kulon, Kec. Pedurungan, Semarang, Jawa Tengah\r\n', 0, 0, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:46', '2020-09-27 04:09:46'),
(26, '', '5480001690025', 4, 47, 0, 'Jl. Solo Baru, Rukan the Park Blok B23A dan B25  Desa Madegondo, Kecamatan Grogol, Kabupaten Sukoharjo\r\n', 216, 13, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:46', '2020-09-27 04:09:46'),
(27, '', '5480001690026', 4, 48, 0, 'Jl. Gajah Mada No. 26 RT/RW 004/001 Kelurahan Kraton Tegal\r\n', 0, 0, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:46', '2020-09-27 04:09:46'),
(28, '', '5480001690027', 4, 49, 0, 'Jl. Mayjen Sutoyo 20 B, Kel. Mantrijeron, Kec. Mantrijeron, Yogyakarta\r\n', 224, 14, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:46', '2020-09-27 04:09:46'),
(29, '', '5480001690028', 4, 50, 0, 'Jl. Kelud No. 101, Kepanjen Lor, Blitar, Jawa Timur\r\n', 236, 15, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:46', '2020-09-27 04:09:46'),
(30, '', '5480001690029', 4, 51, 0, 'Jl. Melati 3 No. 174 , Kel. Jember Kidul, Kec. Kaliwates, Kab. Jember,\r\n', 258, 15, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:46', '2020-09-27 04:09:46'),
(31, '', '5480001690030', 4, 52, 0, 'Jl. Soekarno Hatta No. 19-23 Dusun Tepus Desa Sukorejo Kec. Ngasem Kab. Kediri\r\n', 246, 15, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:46', '2020-09-27 04:09:46'),
(32, '', '5480001690031', 4, 53, 0, 'Jl. Basuki Rahmat No. 20A-C, Kel. Sukosari, Kec. Kartoharjo, Madiun, Jawa Timur\r\n', 240, 15, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:46', '2020-09-27 04:09:46'),
(33, '', '5480001690032', 4, 54, 0, 'Jl. Laksamana Agung Sucipto No.94, Kel. Blimbing, Kec. Blimbing, Kab. Malang, Jatim\r\n', 244, 15, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:46', '2020-09-27 04:09:46'),
(34, '', '5480001690033', 4, 55, 0, 'Jl. Jayanegara no. 197, mojokerto, jawa timur (dekat bundaran uks)\r\n', 239, 15, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:46', '2020-09-27 04:09:46'),
(35, '', '5480001690034', 4, 56, 0, 'Perumahan Pondok Mutiara Blok A19-A19A-A19B & A19B, Desa Jati, Sidoarjo, Jawa Timur\r\n', 261, 15, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:46', '2020-09-27 04:09:46'),
(36, '', '5480001690035', 4, 57, 0, 'merr ruko pandugo, jl sukarno 464, Surabaya, Jawa Timur\r\n', 241, 15, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:46', '2020-09-27 04:09:46'),
(37, '', '5480001690036', 4, 58, 0, 'Komp. Ruko Golden Palace, Jl. HR. Mohammad 85 V, Blok B 20-21, Surabaya\r\n', 241, 15, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:46', '2020-09-27 04:09:46'),
(38, '', '5480001690037', 4, 59, 0, 'Jl. Gatsu Timur, Desa Kesiman Kertalangu, Kec. Denpasar Timur, Kodya Denpasar\r\n', 275, 17, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:47', '2020-09-27 04:09:47'),
(39, '', '5480001690038', 4, 60, 0, 'Jl. MT Haryono No. 88, Balikpapan, Kalimantan Timur\r\n', 359, 23, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:47', '2020-09-27 04:09:47'),
(40, '', '5480001690039', 4, 61, 0, 'Jl. Gatot Subroto NO.4, Kel. Kuripan, Kec. Banjar Timur, Banjarmasin, Kal-Sel\r\n', 345, 22, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:47', '2020-09-27 04:09:47'),
(41, '', '5480001690040', 4, 62, 0, 'Jl. Ahmad Yani, Kel. Desa Sungai Pinang Dalam, Kec. Samarinda Hilir, Kodya. Samarinda, Kalimantan Timur (Dekat Summit Otto Finance)\r\n', 358, 23, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:47', '2020-09-27 04:09:47'),
(42, '', '5480001690041', 4, 63, 0, 'Jl. Brigen. M Joenoes, No. 117, Bypass Kendari, Kel. Bende, Kec Kadia, Kodya Kendari, Sulteng\r\n', 427, 28, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:47', '2020-09-27 04:09:47'),
(43, '', '5480001690042', 4, 64, 0, 'Jl. Petarani, Ruko Business Center No. 34 - 35, Makasar, Sulawesi Selatan\r\n', 403, 27, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:47', '2020-09-27 04:09:47'),
(44, '', '5480001690043', 4, 65, 0, 'Jl. Bau Massepe No. 179, Pare pare, Sulawesi Selatan\r\n', 405, 27, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:47', '2020-09-27 04:09:47'),
(45, '', '5480001690044', 4, 66, 0, 'Jl. Tengku ImumLueng Bata No. 9-10, Komp. Pertokoan Lueng bata, Gampong Blangcut, Kec. Lueng Bata, Kota Banda Aceh\r\n', 5, 1, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:47', '2020-09-27 04:09:47'),
(46, '', '5480001690045', 4, 67, 0, 'Jl. Laksamana Bintan, Komp. Gading Mas Blok AB, No. 07-09, Sei Panas, Kota Batam, Riau\r\n', 149, 10, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:47', '2020-09-27 04:09:47'),
(47, '', '5480001690046', 4, 68, 0, 'Jl. Hayam Wuruk No. 33, RT.01, Cempaka Putih. Jelutung, Kota Jambi, Sumatera\r\n', 89, 5, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:47', '2020-09-27 04:09:47'),
(48, '', '5480001690047', 4, 69, 0, 'Jl. Pangeran Antasari 125 F-G, Kel. Tanjung Baru, Kec. Kedamaian, Kota Lampung\r\n', 129, 8, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:47', '2020-09-27 04:09:47'),
(49, '', '5480001690048', 4, 70, 0, 'Jl. Adam Malik No. 167 A-B, Medan\r\n', 34, 2, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:47', '2020-09-27 04:09:47'),
(50, '', '5480001690049', 4, 71, 0, 'Jl. Residen Abdul Rozal No. 8-9, Bukit Sangkal, Kalidoni, Palembang , Sumatera Selatan\r\n', 102, 6, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:47', '2020-09-27 04:09:47'),
(51, '', '5480001690050', 4, 72, 0, 'Jl. Arifin Ahmad Kav.2, Kel. Tangkerang Tengah, Kec. Marpoyan Darman, Pekanbaru Riau\r\n', 78, 4, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:47', '2020-09-27 04:09:47'),
(52, '', '5480001690051', 4, 73, 0, 'Komp. Ruko IBC C-23 RT/RW 00/02, Kel. Pakuwon, Kec. Garut Kota\r\n', 180, 12, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:47', '2020-09-27 04:09:47'),
(53, '', '5480001690052', 4, 74, 0, 'Jl. Kelud No. 101, Kepanjen Lor, Blitar, Jawa Timur\r\n', 236, 15, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:47', '2020-09-27 04:09:47'),
(54, '', '5480001690053', 4, 75, 0, 'Ruko Green Garden Regency A5 No 31 Dahanrejo, Kebomas, Gresik\r\n', 231, 15, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:47', '2020-09-27 04:09:47'),
(55, '', '5480001690054', 4, 76, 0, 'Jl. Jokotole no. 29, Kel. Barurambat Kota, Kec. Pamekasan, Kab. Pamekasan, Jawa Timur\r\n', 234, 15, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:47', '2020-09-27 04:09:47'),
(56, '', '5480001690055', 4, 77, 0, 'Jl. Imam Bonjol No. 103 Kel. Mangunharjo Kec. Mayangan, Kota. Probolinggo, Jawa Timur\r\n', 237, 15, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:48', '2020-09-27 04:09:48'),
(57, '', '5480001690056', 4, 79, 0, 'Gedung Sentra Rempoa I, Komplek Griya Mandiri Rempoa, Jl Ir H Juanda, Rempoa, Ciputat, Tangerang Selatan', 266, 16, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:48', '2020-09-27 04:09:48'),
(58, '', '5480001690057', 4, 80, 0, 'Jl.Raya Bukit Bali Lakarsantri, Lakarsantri, Kec. Lakarsantri, Kota SBY, Jawa Timur 60211\r\n', 241, 15, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:48', '2020-09-27 04:09:48'),
(59, '', '5360001040001', 2, 2, 14, 'Jl. M.H Thamrin, Kavlin No. B-3 Lippo Cikarang, Bekasi', 174, 12, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:56', '2020-09-27 04:09:56'),
(60, '', '5360001030001', 2, 3, 22, 'Jl. M.H Thamrin, Kavlin No. B-3 Lippo Cikarang, Bekasi', 174, 12, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:56', '2020-09-27 04:09:56'),
(61, '', '5360001690001', 2, 4, 2, 'Jl. M.H Thamrin, Kavlin No. B-3 Lippo Cikarang, Bekasi', 174, 12, '-', '-', '', '', '0000-00-00', '', 0, '', '2020-09-27 06:09:56', '2020-09-27 04:09:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tr_project_update_location`
--
ALTER TABLE `tr_project_update_location`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tr_project_update_location`
--
ALTER TABLE `tr_project_update_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
