-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 12, 2021 at 12:13 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apa_ditlala`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_anggaran`
--

CREATE TABLE `m_anggaran` (
  `id` int(11) NOT NULL,
  `nomor_dipa` varchar(255) NOT NULL,
  `tanggal_dipa` date NOT NULL,
  `pagu_anggaran` int(100) NOT NULL,
  `tahun_anggaran` int(11) NOT NULL,
  `kode_satker` varchar(255) NOT NULL,
  `nama_satker` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_anggaran`
--

INSERT INTO `m_anggaran` (`id`, `nomor_dipa`, `tanggal_dipa`, `pagu_anggaran`, `tahun_anggaran`, `kode_satker`, `nama_satker`, `created_at`, `deleted_at`) VALUES
(1, 'DPA01', '2021-04-16', 10000000, 2021, 'KD-02-9901', 'Bantuan Angkutan Laut Satu 1', '2021-04-16 06:20:58', '0000-00-00 00:00:00'),
(2, 'DPA-1', '2021-04-07', 19000, 2021, 'KD-21', '2021', '2021-04-19 09:29:00', '0000-00-00 00:00:00'),
(3, 'DPA-1', '2021-04-15', 19000, 2021, 'KD-21', '2021', '2021-04-19 09:29:54', '0000-00-00 00:00:00'),
(4, 'DPA-1', '2021-04-10', 19000, 2021, 'KD-21', '2021', '2021-04-19 09:31:40', '0000-00-00 00:00:00'),
(5, 'DPA-1', '2021-04-15', 19000, 2021, 'KD-21', '2021', '2021-04-19 09:56:55', '0000-00-00 00:00:00'),
(6, 'DPA-1', '2021-04-16', 19000, 2021, 'KD-21', '2021', '2021-04-22 12:04:12', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `m_groups`
--

CREATE TABLE `m_groups` (
  `id` int(11) NOT NULL,
  `group_name` varchar(255) NOT NULL DEFAULT '',
  `division_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_groups`
--

INSERT INTO `m_groups` (`id`, `group_name`, `division_id`) VALUES
(1, 'Admin', 1),
(2, 'PPK', 0),
(3, 'Bendahara Pembantu', 0),
(4, 'Bendahara Pengeluaran', 0),
(5, 'PPSPM', 0),
(6, 'Direktur', 0);

-- --------------------------------------------------------

--
-- Table structure for table `m_menu`
--

CREATE TABLE `m_menu` (
  `id` int(11) NOT NULL,
  `menu_name` varchar(50) NOT NULL,
  `menu_key` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_menu`
--

INSERT INTO `m_menu` (`id`, `menu_name`, `menu_key`, `created_at`, `updated_at`) VALUES
(1, 'Dashboard', 'dashboard', '2020-09-16 00:00:00', '2020-09-16 16:35:31'),
(2, 'Daftar Anggaran', 'daftar_anggaran', '0000-00-00 00:00:00', '2021-04-19 04:39:52'),
(3, 'Daftar Kegiatan', 'daftar_kegiatan', '2020-09-16 00:00:00', '2021-04-19 04:40:08'),
(4, 'Daftar Kontrak', 'daftar_kontrak', '2020-09-16 00:00:00', '2021-04-19 04:40:42'),
(5, 'Pencairan Anggaran', 'pencairan_anggaran', '2020-09-16 00:00:00', '2021-04-19 04:41:06');

-- --------------------------------------------------------

--
-- Table structure for table `m_users`
--

CREATE TABLE `m_users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `fullname` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `division_id` int(11) NOT NULL DEFAULT 0,
  `group_id` int(11) NOT NULL DEFAULT 0,
  `signature_name` varchar(255) DEFAULT NULL,
  `status_id` int(11) NOT NULL DEFAULT 1,
  `jabatan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_users`
--

INSERT INTO `m_users` (`id`, `username`, `password`, `fullname`, `email`, `division_id`, `group_id`, `signature_name`, `status_id`, `jabatan`) VALUES
(1, 'via', '21232f297a57a5a743894a0e4a801fc3', 'Via Jupita', 'via@valen.net', 1, 2, NULL, 1, ''),
(2, 'admin', '52c69e3a57331081823331c4e69d3f2e', 'admin', 'admin@gmail.com', 1, 1, '1', 1, ''),
(3, 'deno', 'e10adc3949ba59abbe56e057f20f883e', 'deno putra', 'deno@gmail.com', 0, 1, NULL, 1, ''),
(4, 'fadil', 'e10adc3949ba59abbe56e057f20f883e', 'fadilSD', 'fadil.nylon@gmail.com', 0, 2, NULL, 1, ''),
(5, 'sitNextToMe', '21232f297a57a5a743894a0e4a801fc3', 'sitNextToMe', 'sitNextToMe@gmail.com', 0, 2, NULL, 1, ''),
(6, '916031', '', 'yoman aja xx', 'yoman@gmail.com', 1, 2, NULL, 1, ''),
(9, 'denny', 'e10adc3949ba59abbe56e057f20f883e', 'deny s', 'denypspm@gmail.com', 0, 4, NULL, 1, ''),
(10, 'ppk', '21232f297a57a5a743894a0e4a801fc3', 'ppk', 'ppk@gmail.com', 0, 2, NULL, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `tr_detail_kegiatan`
--

CREATE TABLE `tr_detail_kegiatan` (
  `id` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `kode_mak` int(11) NOT NULL,
  `detail_kegiatan` text NOT NULL,
  `pagu_kegiatan` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tr_detail_kegiatan`
--

INSERT INTO `tr_detail_kegiatan` (`id`, `id_kegiatan`, `kode_mak`, `detail_kegiatan`, `pagu_kegiatan`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 523121, 'Belanja Pemeliharaan Peralatan dan Mesin', 631780000, '2021-04-27 04:54:07', NULL, NULL),
(2, 2, 521219, 'Belanja Barang Non Operasional Lainnya', 5995000, '2021-04-27 21:34:58', NULL, NULL),
(3, 2, 523121, 'Belanja Pemeliharaan Peralatan dan Mesin', 441619420, '2021-04-27 04:56:13', NULL, NULL),
(4, 3, 521219, 'Belanja Barang Non Operasional Lainnya', 43600000, '2021-04-27 05:24:23', NULL, NULL),
(5, 3, 523121, 'Belanja Biaya Pemeliharaan Peralatan dan Mesin', 20000000000, '2021-04-27 05:24:23', NULL, NULL),
(6, 3, 524111, 'Belanja Perjalanan Dinas Biasa', 462156000, '2021-04-27 05:26:31', NULL, NULL),
(7, 3, 522131, 'Belanja Jasa Konsultan', 560000000, '2021-04-27 05:26:31', NULL, NULL),
(8, 4, 522191, 'Belanja Jasa Lainnya', 570000000, '2021-04-27 05:29:29', NULL, NULL),
(9, 4, 524111, 'Belanja Perjalanan Dinas Biasa', 80000000, '2021-04-27 05:29:29', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tr_detail_lampiran`
--

CREATE TABLE `tr_detail_lampiran` (
  `id` int(11) NOT NULL,
  `id_tagihan` int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tr_group_menu`
--

CREATE TABLE `tr_group_menu` (
  `id` int(5) NOT NULL,
  `group_id` int(3) NOT NULL,
  `menu_id` int(3) NOT NULL,
  `authorized` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tr_group_menu`
--

INSERT INTO `tr_group_menu` (`id`, `group_id`, `menu_id`, `authorized`, `created_at`, `updated_at`) VALUES
(71, 1, 1, 1, '2021-04-19 13:01:46', '2021-04-19 06:09:06'),
(72, 1, 2, 1, '2021-04-19 13:01:46', '2021-04-19 06:09:06'),
(73, 1, 3, 1, '2021-04-19 13:01:46', '2021-04-19 06:09:06'),
(74, 1, 4, 0, '2021-04-19 13:01:46', '2021-04-19 06:01:46'),
(75, 1, 5, 0, '2021-04-19 13:01:46', '2021-04-19 06:01:46'),
(76, 2, 1, 1, '2021-04-19 13:09:56', '2021-04-19 06:09:56'),
(77, 2, 2, 0, '2021-04-19 13:09:56', '2021-04-19 06:09:56'),
(78, 2, 3, 0, '2021-04-19 13:09:56', '2021-04-19 06:09:56'),
(79, 2, 4, 0, '2021-04-19 13:09:56', '2021-04-19 06:09:56'),
(80, 2, 5, 0, '2021-04-19 13:09:56', '2021-04-19 06:09:56'),
(81, 3, 1, 1, '2021-04-19 13:10:13', '2021-04-19 06:10:13'),
(82, 3, 2, 0, '2021-04-19 13:10:13', '2021-04-19 06:10:13'),
(83, 3, 3, 0, '2021-04-19 13:10:13', '2021-04-19 06:10:13'),
(84, 3, 4, 0, '2021-04-19 13:10:13', '2021-04-19 06:10:13'),
(85, 3, 5, 0, '2021-04-19 13:10:13', '2021-04-19 06:10:13'),
(86, 4, 1, 1, '2021-04-19 13:10:26', '2021-04-19 06:10:26'),
(87, 4, 2, 0, '2021-04-19 13:10:26', '2021-04-19 06:10:26'),
(88, 4, 3, 0, '2021-04-19 13:10:26', '2021-04-19 06:10:26'),
(89, 4, 4, 0, '2021-04-19 13:10:26', '2021-04-19 06:10:26'),
(90, 4, 5, 0, '2021-04-19 13:10:26', '2021-04-19 06:10:26'),
(91, 5, 1, 1, '2021-04-19 13:10:35', '2021-04-19 06:10:35'),
(92, 5, 2, 0, '2021-04-19 13:10:35', '2021-04-19 06:10:35'),
(93, 5, 3, 0, '2021-04-19 13:10:35', '2021-04-19 06:10:35'),
(94, 5, 4, 0, '2021-04-19 13:10:35', '2021-04-19 06:10:35'),
(95, 5, 5, 0, '2021-04-19 13:10:35', '2021-04-19 06:10:35'),
(96, 6, 1, 1, '2021-04-20 14:34:50', '2021-04-20 07:35:32'),
(97, 6, 2, 1, '2021-04-20 14:34:50', '2021-04-20 07:35:32'),
(98, 6, 3, 0, '2021-04-20 14:34:50', '2021-04-20 07:34:50'),
(99, 6, 4, 0, '2021-04-20 14:34:50', '2021-04-20 07:34:50'),
(100, 6, 5, 0, '2021-04-20 14:34:50', '2021-04-20 07:34:50');

-- --------------------------------------------------------

--
-- Table structure for table `tr_kegiatan`
--

CREATE TABLE `tr_kegiatan` (
  `id` int(11) NOT NULL,
  `kode_aktivitas` varchar(255) NOT NULL,
  `kro` varchar(255) NOT NULL,
  `ro` varchar(255) NOT NULL,
  `nama_kegiatan` text NOT NULL,
  `detail_kegiatan` text NOT NULL,
  `id_user` int(11) NOT NULL,
  `prioritas` int(11) NOT NULL,
  `id_anggaran` int(11) NOT NULL,
  `pagu_kegiatan` varchar(255) NOT NULL,
  `komponen` varchar(255) NOT NULL,
  `subkomponen` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tr_kegiatan`
--

INSERT INTO `tr_kegiatan` (`id`, `kode_aktivitas`, `kro`, `ro`, `nama_kegiatan`, `detail_kegiatan`, `id_user`, `prioritas`, `id_anggaran`, `pagu_kegiatan`, `komponen`, `subkomponen`, `created_at`, `deleted_at`) VALUES
(1, '4659', 'CCE', '001', 'DOCKING DAN SUPERVISI KAPAL PERINTIS KM. SABUK NUSANTARA 45 (200 DWT) KONTRAK TAHUN JAMAK (MYC)', '', 1, 1, 1, '', '051', 'A', '2021-04-27 05:28:14', '0000-00-00 00:00:00'),
(2, '4659', 'CCE', '001', 'DOCKING KHUSUS 11 UNIT KAPAL PERINTIS MILIK NEGARA KONDISI RUSAK BERAT', '', 1, 1, 1, '', '051', 'A', '2021-04-27 05:28:17', '0000-00-00 00:00:00'),
(3, '4659', 'CCE', '001', 'DOCKING DAN SUPERVISI KAPAL TOL LAUT', '', 1, 1, 1, '', '052', 'A', '2021-04-27 05:28:19', '0000-00-00 00:00:00'),
(4, '4661', 'AEA', '002', 'PENINGKATAN DAN PENGEMBANGAN SDM PENGELOLAAN KAPAL PERINTIS MILIK NEGARA', '', 1, 1, 1, '', '052', 'F', '2021-04-27 05:27:59', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tr_kontrak`
--

CREATE TABLE `tr_kontrak` (
  `id` int(11) NOT NULL,
  `id_detail_kegiatan` int(11) NOT NULL,
  `no_kontrak` varchar(255) DEFAULT NULL,
  `tanggal_kontrak` date DEFAULT NULL,
  `uraian` text DEFAULT NULL,
  `status` enum('Verfikasi Bendahara','Terdaftar','Rejected','') DEFAULT NULL,
  `jenis_kontrak` int(11) DEFAULT NULL,
  `nomor_addendum` varchar(255) DEFAULT NULL,
  `tanggal_addendum` date DEFAULT NULL,
  `nama_kontraktor` varchar(255) DEFAULT NULL,
  `alamat_kontraktor` text DEFAULT NULL,
  `npwp_kontraktor` varchar(255) DEFAULT NULL,
  `rekening_kontraktor` varchar(20) DEFAULT NULL,
  `nama_rekening` varchar(255) DEFAULT NULL,
  `nilai_kontrak` bigint(20) DEFAULT NULL,
  `cara_pembayaran` enum('sekaligus','termin') DEFAULT NULL,
  `waktu_penyelesaian` int(11) DEFAULT NULL,
  `waktu_pemeliharaan` int(11) DEFAULT NULL,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `file_kontrak` varchar(255) DEFAULT NULL,
  `file_addendum` varchar(255) DEFAULT NULL,
  `file_myx` varchar(255) DEFAULT NULL,
  `jumlah_termin` int(11) DEFAULT NULL,
  `uang_muka` int(100) DEFAULT NULL,
  `subdit` enum('Subdit 1','Subdit 2','Subdit 3','Subdit 4','Subdit 5','Subbag TU') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tr_kontrak`
--

INSERT INTO `tr_kontrak` (`id`, `id_detail_kegiatan`, `no_kontrak`, `tanggal_kontrak`, `uraian`, `status`, `jenis_kontrak`, `nomor_addendum`, `tanggal_addendum`, `nama_kontraktor`, `alamat_kontraktor`, `npwp_kontraktor`, `rekening_kontraktor`, `nama_rekening`, `nilai_kontrak`, `cara_pembayaran`, `waktu_penyelesaian`, `waktu_pemeliharaan`, `tanggal_mulai`, `tanggal_selesai`, `file_kontrak`, `file_addendum`, `file_myx`, `jumlah_termin`, `uang_muka`, `subdit`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'CON/123/111', '2021-04-25', 'jadi begini isi kontraknya ', '', 1, NULL, NULL, 'Aries Dimas Yudhistira', 'jl. warakas 4', '8560.0256.0.55485', '345243534', 'Aries Dimas', 80250000, '', 1, 1, '2021-04-22', '2021-04-30', 'kontrak-con/123/111-20210502124738.', '', 'sdfsdf', NULL, 70000000, 'Subdit 4', '2021-04-25 22:08:56', '2021-05-10 09:10:05', '2021-04-25 22:08:56'),
(2, 2, 'CTR/11/11', '2021-04-21', 'fderyayfdhggfgh', NULL, 0, 'AD-111-wer', '2021-05-12', 'dimas', 'warakas', '123/123/123', '2147483647', 'dimas', 78979800, '', NULL, NULL, NULL, NULL, NULL, 'addendum-ctr1111-20210510160532.jpg', NULL, NULL, 8000000, NULL, '2021-05-01 07:45:48', '2021-05-10 13:52:35', '2021-05-01 07:45:48'),
(4, 7, 'CTR/11/11', '2021-05-19', '67hgjdgfhsfghfsgh', NULL, 0, 'AF-567-ADD', '2021-05-20', 'dimas', 'warakas', '123/123/123', '45645634', 'dimas', 78979800, '', NULL, NULL, NULL, NULL, NULL, 'addendum-ctr1111-20210510211903.docx', NULL, NULL, 0, NULL, '2021-05-01 08:42:18', '2021-05-10 14:19:03', '2021-05-01 08:42:18'),
(28, 1, 'CTR-67-67', '2021-05-02', 'kontrak ini adalah pengadaan 10 PC ', NULL, 1, NULL, NULL, 'Aries Dimas Yudhistira', 'jl. warakas 4', '8560.0256.0.55485', '5670123478', 'Aries Dimas', 14000000000, 'sekaligus', 1, 1, '2021-05-10', '2021-05-31', 'kontrak-ctr-67-67-20210512153633.jpg', NULL, 'myx-ctr-67-67-20210512153633.jpg', NULL, 2000000000, 'Subdit 5', '2021-05-02 12:42:48', '2021-05-12 08:36:33', '2021-05-02 12:42:48');

-- --------------------------------------------------------

--
-- Table structure for table `tr_kontrak_termin`
--

CREATE TABLE `tr_kontrak_termin` (
  `id` int(11) NOT NULL,
  `id_kontrak` int(11) NOT NULL,
  `termin_phase` int(11) NOT NULL,
  `harga_termin` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tr_tagihan`
--

CREATE TABLE `tr_tagihan` (
  `id` int(5) NOT NULL,
  `id_kontrak` int(5) DEFAULT NULL,
  `id_detail_kegiatan` int(5) DEFAULT NULL,
  `nomor_tagihan` varchar(20) NOT NULL,
  `jenis_tagihan` enum('kontrak','non_kontrak') NOT NULL,
  `nilai_tagihan` int(5) NOT NULL,
  `potongan_pajak_ppn` int(5) NOT NULL,
  `jenis_pajak_pph` varchar(5) NOT NULL,
  `potongan_pajak_pph` int(5) NOT NULL,
  `uraian_tagihan` varchar(225) NOT NULL,
  `file_surat_permohonan` varchar(100) NOT NULL,
  `file_invoice` varchar(100) NOT NULL,
  `file_kwitansi` varchar(100) NOT NULL,
  `file_faktur_pajak` varchar(100) NOT NULL,
  `file_bap` varchar(100) NOT NULL,
  `file_bapp` varchar(100) NOT NULL,
  `file_bast` varchar(100) NOT NULL,
  `file_laporan` varchar(100) NOT NULL,
  `file_foto_kegiatan` varchar(100) NOT NULL,
  `status_tagihan` enum('verifikasi bendahara','ppspm','terbit spm','') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tr_tagihan`
--

INSERT INTO `tr_tagihan` (`id`, `id_kontrak`, `id_detail_kegiatan`, `nomor_tagihan`, `jenis_tagihan`, `nilai_tagihan`, `potongan_pajak_ppn`, `jenis_pajak_pph`, `potongan_pajak_pph`, `uraian_tagihan`, `file_surat_permohonan`, `file_invoice`, `file_kwitansi`, `file_faktur_pajak`, `file_bap`, `file_bapp`, `file_bast`, `file_laporan`, `file_foto_kegiatan`, `status_tagihan`, `created_at`, `updated_at`, `deleted_at`) VALUES
(50, NULL, 7, 'CGV-752-FAST', 'non_kontrak', 78200000, 0, 'pph 1', 0, '', 'surat-permohonan-cgv-752-fast-20210512161303.jpg', 'invoice-cgv-752-fast-20210512161303.jpg', 'kwitansi-cgv-752-fast-20210512161303.jpg', 'faktur-pajak-cgv-752-fast-20210512161303.jpg', 'bap-cgv-752-fast-20210512161303.jpg', 'bapp-cgv-752-fast-20210512161303.jpg', 'bast-cgv-752-fast-20210512161303.jpg', 'laporan-cgv-752-fast-20210512161303.jpg', 'foto-kegiatan-cgv-752-fast-20210512161303.jpg', 'verifikasi bendahara', '0000-00-00 00:00:00', '2021-05-12 09:13:03', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tr_user_login_records`
--

CREATE TABLE `tr_user_login_records` (
  `id` int(11) NOT NULL,
  `user_id` int(5) NOT NULL,
  `session_id` varchar(150) NOT NULL,
  `username` varchar(100) NOT NULL,
  `ip_address` varchar(20) NOT NULL,
  `location` varchar(225) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tr_user_login_records`
--

INSERT INTO `tr_user_login_records` (`id`, `user_id`, `session_id`, `username`, `ip_address`, `location`, `created_at`) VALUES
(2, 1, 'ckfn437s3m651o7sl2fpsan99dshqt5j', 'admin', '::1', 'Jakarta', '2020-11-03 23:43:38'),
(3, 1, 'a917753d9bca1700785cda303c1f9e18', 'admin', '::1', 'Jakarta', '2020-11-04 00:24:26'),
(4, 1, '09b5c06a787faa76ae784fe48e69b1ac', 'admin', '::1', 'Jakarta', '2020-11-04 07:26:56'),
(5, 1, '37ccd9e83866ed0dff1fe8be85b5c9ae', 'admin', '::1', 'Jakarta', '2020-11-04 07:26:57'),
(6, 1, '330c5b662e5258ae496b36d482d49bc3', 'admin', '::1', 'South Tangerang', '2020-11-04 10:06:51'),
(7, 1, 'bd6b9360b506fc8dda6447f767e12e8f', 'admin', '::1', 'South Tangerang', '2020-11-04 10:06:52'),
(8, 1, 'a863f00135bca54a504da93894f57e14', 'admin', '::1', 'South Tangerang', '2020-11-04 12:57:01'),
(9, 6, 'a4ee7bedf00b21bb5cba81c5852ebfb1', '916031', '::1', 'South Tangerang', '2020-11-04 22:03:50'),
(10, 6, '6e4bd119c0c9339ece08269e2b45ebe5', '916031', '::1', 'South Tangerang', '2020-11-04 22:04:44'),
(11, 6, 'e1d44f184fcb0fd70bbe908a36a1b1de', '916031', '::1', 'South Tangerang', '2020-11-04 22:06:16'),
(12, 6, '2ff438d1c4380ecf60761c8f23ce4dc3', '916031', '::1', 'South Tangerang', '2020-11-05 04:48:57'),
(13, 1, '450edbd451e90dd98c3b4e4fe1f4febd', 'admin', '::1', 'South Tangerang', '2020-11-06 20:15:01'),
(14, 1, 'bdde95fd4a70f0ddf01ee7b95b9f34f4', 'admin', '::1', 'South Tangerang', '2020-11-06 20:15:02'),
(15, 1, '9a5d740b99b6ac5c7ee96a72556084dd', 'admin', '::1', 'South Tangerang', '2020-11-08 07:51:50'),
(16, 1, 'd2d1d55253cd71dc43953ce7dde9cca8', 'admin', '::1', 'Depok', '2020-11-08 20:10:30'),
(17, 1, '36bf633920c0138fa1ff97bf2d71d57d', 'admin', '::1', 'Depok', '2020-11-08 20:10:31'),
(18, 1, 'c3343ade4d9b5818cc0bd008bac7d104', 'admin', '::1', 'Depok', '2020-11-09 06:56:47'),
(19, 1, '38dcddf03f20daab2bbdda1d6349728d', 'admin', '::1', 'Depok', '2020-11-09 06:56:48'),
(20, 1, 'fdfd0d06d12d028c1e3d2819bb1c5ae1', 'admin', '::1', 'Depok', '2020-11-09 09:39:34'),
(21, 1, '5caee34040ef3d63bfa090793ff18c96', 'admin', '::1', 'Depok', '2020-11-09 19:03:42'),
(22, 1, 'ce38bfa648a33af0b836f063964d85de', 'admin', '::1', 'Depok', '2020-11-10 13:00:52'),
(23, 1, 'ea6ed9174a13af81b0a7c676b67729b7', 'admin', '::1', 'Depok', '2020-11-10 13:00:53'),
(24, 1, '40b78736faecaf3fd20a4b8da53ac678', 'admin', '::1', 'Depok', '2020-11-10 20:14:52'),
(25, 1, 'd70a45e7bbc8d459a5c003032296a687', 'admin', '::1', 'Depok', '2020-11-10 20:14:52'),
(26, 1, 'ee7b803fcd592b5d1d2803809f51c71f', 'admin', '::1', 'Depok', '2020-11-12 08:39:51'),
(27, 1, '9c6468646f9127b293fc19402c7d4fb9', 'admin', '::1', 'Depok', '2020-11-13 08:00:25'),
(28, 1, '2d0f20cb43366ed82b5c72fb0c8b81dd', 'admin', '::1', 'Depok', '2020-11-13 14:36:05'),
(29, 1, '7b932d7b9797672fafab26763ae3cc4e', 'admin', '::1', 'Depok', '2020-11-14 06:23:30'),
(30, 1, '2930f395f4b3ec073e590859046b63be', 'admin', '::1', 'Depok', '2020-11-14 06:23:30'),
(31, 1, 'e33fabd6cf1fd908db2cc9452cd57698', 'admin', '::1', 'Depok', '2020-11-14 13:44:55'),
(32, 1, '8edd56c087d2735563b34ca0d615b80a', 'admin', '::1', 'Depok', '2020-11-14 13:44:57'),
(33, 1, 'be8a7feac9f79da6b44d5edc67eb1bc2', 'admin', '::1', 'Depok', '2020-11-14 18:36:39'),
(34, 1, '9a09831880fbbce1682234368dcc7adc', 'admin', '::1', 'Depok', '2020-11-14 18:36:40'),
(35, 1, 'daa4838d8b77ea0c5e08768264267364', 'admin', '::1', 'Depok', '2020-11-15 09:37:50'),
(36, 1, 'f4afd0fd331c0c896344d9d6e4ab547d', 'admin', '::1', 'Depok', '2020-11-15 10:42:24'),
(37, 1, '4bd25ef5c158cb945d1fd37476b03cff', 'admin', '::1', 'Depok', '2020-11-15 15:31:56'),
(38, 1, '2744a3fa3bb2f76e2574696209cd0bc0', 'admin', '::1', 'Depok', '2020-11-15 15:31:57'),
(39, 1, 'd9effbb4ce895c93f8f05b828f671b15', 'admin', '::1', 'Depok', '2020-11-15 16:10:30'),
(40, 1, 'a8505a39af51c3326f6209bde4d93e88', 'admin', '::1', 'Depok', '2020-11-16 07:30:50'),
(41, 1, '106cbc3e60235d455fa83a9611d71780', 'admin', '::1', 'Depok', '2020-11-16 07:30:51'),
(42, 1, 'ef955f4a0d5a9fe749e2c943b720762f', 'admin', '::1', 'Depok', '2020-11-16 11:48:23'),
(43, 1, '4a96dd624af5016850d41404eee2a681', 'admin', '::1', 'Depok', '2020-11-16 11:48:35'),
(44, 1, 'de241e3108925efbf9ebeb526814471c', 'admin', '::1', 'Depok', '2020-11-16 11:48:36'),
(45, 1, 'b4816747ae61bc54c02241265dbd0bbe', 'admin', '::1', 'Depok', '2020-11-17 10:16:07'),
(46, 1, '28370cdb1deaea119d54f769077d8c28', 'admin', '::1', 'Depok', '2020-11-17 10:16:08'),
(47, 1, 'e990f60b1c00c2169e860b89b4ce2599', 'admin', '::1', 'Depok', '2020-11-17 15:07:14'),
(48, 1, 'da8420d4b1d80782e6250f8119b81459', 'admin', '::1', 'Depok', '2020-11-17 15:13:13'),
(49, 1, '225bb202eecdf8719b355007c8ff705f', 'admin', '::1', 'Depok', '2020-11-17 15:13:14'),
(50, 1, 'bb878a2d148397fd148ac10c6b2b4b44', 'admin', '::1', 'Depok', '2020-11-17 15:47:20'),
(51, 6, 'f2e9062a271dc680c74d28015e680869', '916031', '::1', 'Depok', '2020-11-17 15:48:03'),
(52, 1, '5b8672741f7a6f5be8be112a0f6f5599', 'admin', '::1', 'Depok', '2020-11-17 16:09:50'),
(53, 1, '0cc7b3f1f61d5c061cfe647f3cf43822', 'admin', '::1', 'Depok', '2020-11-17 16:09:51'),
(54, 1, 'fd71f57899dc560e9a3c9a7c577fe2df', 'admin', '::1', 'Depok', '2020-11-17 20:14:37'),
(55, 1, '0d23397fe19b834e3edbe05b8b3a08f7', 'admin', '::1', 'Depok', '2020-11-18 10:25:56'),
(56, 1, '1536cc5f51a82026021c623be1d623d3', 'admin', '::1', 'Depok', '2020-11-18 14:06:52'),
(57, 1, 'a06a09ad39540bc9ff5c6b49753e3e58', 'admin', '::1', 'Depok', '2020-11-19 10:39:16'),
(58, 1, '13678090ecf2d01565f0ed69ef392e49', 'admin', '::1', 'Depok', '2020-11-19 21:19:37'),
(59, 1, 'c844a6294757b0f69fc7b6863ce2ece8', 'admin', '::1', 'Depok', '2020-11-19 21:19:38'),
(60, 1, 'b5e358995093dd9c4cc8c1007e6e0b05', 'admin', '::1', 'Depok', '2020-11-20 08:02:26'),
(61, 1, 'cea5576c509b50e0ecdf5dda6c45314d', 'admin', '::1', 'Depok', '2020-11-20 08:02:26'),
(62, 1, '33c58d03a75c7955d3f59fba62154126', 'admin', '::1', 'Depok', '2020-11-20 16:48:30'),
(63, 1, 'e165cb67c15d3733982464414e470e46', 'admin', '::1', 'Depok', '2020-11-20 16:48:31'),
(64, 1, '3df68b72bd44c7b14b1e540f3fe72efe', 'admin', '::1', 'Depok', '2020-11-20 19:15:56'),
(65, 1, '42317b9bf43ec5c42394f99a3d856f4e', 'admin', '::1', 'Depok', '2020-11-20 19:15:57'),
(66, 1, 'b5410800b9909ecdaceffc69dd6c2f1d', 'admin', '::1', 'Depok', '2020-11-21 13:25:39'),
(67, 1, 'f47bdf7b3a9cecb7e722b0e55dee32b6', 'admin', '::1', 'Depok', '2020-11-21 13:25:40'),
(68, 1, 'a5f5526a2a5bcd25a7e8df4e7a570a96', 'admin', '::1', 'Depok', '2020-11-21 18:09:03'),
(69, 1, '07feb1f92de12bd772af6dfe3a204fb2', 'admin', '::1', 'Depok', '2020-11-21 21:40:13'),
(70, 1, '40768e81cd7589da3cc1bca4d9299ef6', 'admin', '::1', 'Depok', '2020-11-22 08:28:07'),
(71, 1, 'e1037a9a347d30db31b949068226110a', 'admin', '::1', 'Depok', '2020-11-22 13:42:56'),
(72, 1, '9bbfd1a0693c00e2d9ffba0577c21f20', 'admin', '::1', 'Depok', '2020-11-22 19:39:11'),
(73, 1, '9267e986a15cfd5521b21d59894e9565', 'admin', '::1', 'Depok', '2020-11-22 20:40:27'),
(74, 1, '92438a776b3751ee17e57718aaea4fbe', 'admin', '::1', 'Depok', '2020-11-22 22:14:33'),
(75, 1, '18b36e83e409e7ec20f71bdff9ffe356', 'admin', '::1', 'Depok', '2020-11-23 14:27:24'),
(76, 1, '09233ff9fde7b8c70ca7e25636c98f4e', 'admin', '::1', 'Depok', '2020-11-23 14:27:25'),
(77, 1, '3366fd9f6cc47a7246e5c62e081bc810', 'admin', '::1', 'Depok', '2020-11-23 20:53:43'),
(78, 1, 'c067a64a8edb5b4d684a5c801d652e5c', 'admin', '::1', 'Depok', '2020-11-23 20:53:43'),
(79, 1, 'd2a149720533e8d8d981e244f9812ab9', 'admin', '::1', 'Depok', '2020-11-24 05:57:24'),
(80, 1, '63ef1ab056e18ae4a66ea1f5d1fd04ad', 'admin', '::1', 'Depok', '2020-11-24 05:57:25'),
(81, 1, 'c4a147284ad9686a64538f34a4f7fe38', 'admin', '::1', 'Depok', '2020-11-25 20:18:13'),
(82, 1, '3a0596386b650f73844aee8fe3e7994f', 'admin', '::1', 'Depok', '2020-11-26 10:17:11'),
(83, 1, 'e416776a15a85721c1e93111fe7ebbb1', 'admin', '::1', 'Depok', '2020-11-26 13:44:33'),
(84, 1, '4c10f5dc066b1562b0284647df94b9c9', 'admin', '::1', 'Depok', '2020-11-27 14:44:03'),
(85, 1, '81f6c4272bafdb49c2b83aaedbdc7b64', 'admin', '::1', 'Depok', '2020-11-28 14:19:56'),
(86, 1, '32f722901a5249f5f5ca69524a7fca1d', 'admin', '::1', 'Depok', '2020-11-28 21:50:49'),
(87, 1, 'ad1273ed121c04cf17c08b3ab960067f', 'admin', '::1', 'Depok', '2020-11-29 05:17:25'),
(88, 1, '7f5e3e93637deb53cc379b0c71a5dbf4', 'admin', '::1', 'Depok', '2020-11-29 18:15:33'),
(89, 1, '1c6f2d3dcc87db8d902b3b145b39d72a', 'admin', '::1', 'Depok', '2020-11-30 05:10:09'),
(90, 1, 'f97edc3b9bb358b344afc309d46d3ad7', 'admin', '::1', 'Depok', '2020-11-30 09:25:42'),
(91, 1, 'a8a2eed501f9a834ff8d6939ed084823', 'admin', '::1', 'Depok', '2020-12-01 06:36:51'),
(92, 1, '8ad0ca4edebaccbffec03f66dc3d91b2', 'admin', '::1', 'Depok', '2020-12-01 06:36:52'),
(93, 1, '91f52fe8ebbef026c3ddd19eb654ed71', 'admin', '::1', 'Depok', '2020-12-01 11:33:27'),
(94, 1, '8f064d77875717955db5558cffabc493', 'admin', '::1', 'Depok', '2020-12-01 11:33:29'),
(95, 1, '22b25b9b068ed2ad24156dc0415bf83c', 'admin', '::1', 'Depok', '2020-12-02 10:28:50'),
(96, 1, '810fb2c95534bda65ca6b04df69f27fc', 'admin', '::1', 'Depok', '2020-12-02 10:28:52'),
(97, 1, '074556117b72cc9b662ba20d29db022c', 'admin', '::1', 'Depok', '2020-12-02 14:43:43'),
(98, 1, '6061bae7ca503561eda9f819bc7206c8', 'admin', '::1', 'Depok', '2020-12-03 22:29:01'),
(99, 1, '3342d92db8583dc9fd1d6e5433345078', 'admin', '::1', 'Depok', '2020-12-04 09:25:36'),
(100, 1, '68b7e3bfdbd587df6adf02972e5aaadb', 'admin', '::1', 'Depok', '2020-12-04 09:25:38'),
(101, 1, '53e20e68dda4fcf4d0260e88a819f9eb', 'admin', '::1', 'Depok', '2020-12-05 08:06:33'),
(102, 1, '338323be71c3e44238744e6974be38bf', 'admin', '::1', 'Depok', '2020-12-05 08:06:36'),
(103, 1, 'da1c4e0621257d0ae0e93cfa8d7d526a', 'admin', '::1', 'Depok', '2020-12-05 13:26:47'),
(104, 1, 'efa154ae79d3a73732cf940e0c0bcd1a', 'admin', '::1', 'Depok', '2020-12-06 05:17:35'),
(105, 1, 'a091452a6c1ac0b1da490b12dc6b2180', 'admin', '::1', 'Depok', '2020-12-06 05:17:38'),
(106, 1, 'a3d40afa77a6d160358671c3f3ceaa98', 'admin', '::1', 'Depok', '2020-12-06 18:38:03'),
(107, 1, 'f17741466e9c8cdd2a84cad48f4f1181', 'admin', '::1', 'Depok', '2020-12-06 18:38:07'),
(108, 1, '164ba63c5eb0d2c8c5ce86d06f95d87a', 'admin', '::1', 'Depok', '2020-12-07 08:37:17'),
(109, 1, 'c249a800a4cae79d11426158dee67753', 'admin', '::1', 'Depok', '2020-12-07 08:37:20'),
(110, 1, '8fdd5ec2d3533ec32aa4167eb0ad596d', 'admin', '::1', 'Depok', '2020-12-07 13:15:10'),
(111, 1, '4e284adfcffd2e74d798af00670226a2', 'admin', '::1', 'Depok', '2020-12-08 15:47:22'),
(112, 1, '8602c47f4a8ecc7f307d336ea1aae798', 'admin', '::1', 'Depok', '2020-12-08 15:47:39'),
(113, 1, '3bc8d9c3953d1047a2147c17f8578b7c', 'admin', '::1', 'Depok', '2020-12-09 07:48:20'),
(114, 1, '464aca1290432453d06cd77941f36e35', 'admin', '::1', 'Depok', '2020-12-09 07:48:37'),
(115, 1, '1be5c501cd4b03a76423bf91fdf87394', 'admin', '::1', 'Depok', '2020-12-10 09:27:35'),
(116, 1, '51de909f289c4a61b630d3907f0bd23c', 'admin', '::1', 'Depok', '2020-12-12 17:14:12'),
(117, 1, '9a7896614d5a37d21414ec65b07646de', 'admin', '::1', 'Depok', '2020-12-13 18:53:41'),
(118, 1, '8b01dc32ff250c24d3e5a30bd3b265e3', 'admin', '::1', 'Depok', '2020-12-13 18:53:41'),
(119, 1, 'ecbf3afe724e96bb8e8bd8b837ba80d1', 'admin', '::1', 'Depok', '2020-12-13 19:10:51'),
(120, 1, '576de74d24c5c4c460c5a5e6c6e29ba4', 'admin', '::1', 'Depok', '2020-12-14 08:07:03'),
(121, 1, 'df2306715826e9e58f0fc5e48918dfcd', 'admin', '::1', 'Depok', '2020-12-14 08:07:18'),
(122, 1, '62b828f00bd84ee0f1ff7cd3504d8080', 'admin', '::1', 'Depok', '2020-12-15 20:03:59'),
(123, 1, '1da8e37960873fd15f523d4eed6bcbef', 'admin', '::1', 'Depok', '2020-12-15 21:05:35'),
(124, 1, 'cff526dfd49436a50e11400f8722ce0a', 'admin', '::1', 'Depok', '2020-12-16 08:01:53'),
(125, 1, '7a3d608a689a7f131e4c4b4d8b5065dd', 'admin', '::1', 'Depok', '2020-12-16 08:01:53'),
(126, 1, 'b8d7888cc5b24ca3b748ab5b5ec5488b', 'admin', '::1', 'Depok', '2020-12-18 06:55:02'),
(127, 1, '095ee18fd810aca545addb44673b9ddc', 'admin', '::1', 'Depok', '2020-12-18 06:55:16'),
(128, 1, '2b842fc8564143dd2b7bf49df293d27a', 'admin', '::1', 'Bogor', '2020-12-22 15:38:27'),
(129, 1, 'aaede761b2deddd2c9b8f49fa7341a59', 'admin', '::1', 'Bogor', '2020-12-22 15:38:28'),
(130, 1, 'd58a67237680c97d56bb14ca5259797c', 'admin', '::1', 'Bogor', '2020-12-23 05:12:20'),
(131, 1, '474fc141d375c63a23d429660f805d06', 'admin', '::1', 'Bogor', '2020-12-23 05:12:21'),
(132, 1, '4a39fcb9713d3a6d63485966302bd451', 'admin', '::1', 'Bogor', '2020-12-23 17:41:53'),
(133, 1, '390e700d02b3cf145f579a1d9367ac76', 'admin', '::1', 'Bogor', '2020-12-23 17:41:53'),
(134, 6, '8ebcda71fad956298cd02cd97b7e37ff', '916031', '::1', 'Depok', '2021-02-18 09:46:12'),
(135, 6, '7fb4ace494e837a2ab95b393f065d137', '916031', '::1', 'Depok', '2021-02-18 14:25:48'),
(136, 1, 'aa8e52b603220266d55984bfeea6a8ac', 'admin', '::1', 'Depok', '2021-02-18 14:27:06'),
(137, 1, '3bd33c81482b4a5c2cc23c0e30abff61', 'admin', '127.0.0.1', 'Bogor', '2021-02-19 07:11:19'),
(138, 1, '17ca2d3f292e9b58e464c7eb7b5995fa', 'admin', '::1', 'Bogor', '2021-02-21 20:55:39'),
(139, 1, '73eb63e871480d4f667e1cf9f35f3edb', 'admin', '::1', 'Bogor', '2021-02-24 16:46:27'),
(140, 1, 'e5bc0983cbf5e6617add234a1dcaee71', 'admin', '::1', 'Bogor', '2021-02-25 09:33:41'),
(141, 1, '6789786e590f7b20e78a9f426c48652f', 'admin', '::1', 'Bogor', '2021-02-25 13:41:03'),
(142, 1, '4d50f5ad7368c65d8f7f4c8a35d9358e', 'admin', '::1', 'Bogor', '2021-02-25 13:41:03'),
(143, 1, 'a82bda6e6bb85a85935100c3559a5b50', 'admin', '::1', 'Singapore', '2021-03-12 16:33:49'),
(144, 1, '944eb53c22b2a6a2eb9caf811cb852f3', 'admin', '::1', 'Singapore', '2021-03-12 16:33:51'),
(145, 1, '26f3237d1cc014091643380b63806de0', 'admin', '::1', 'Singapore', '2021-03-14 20:17:05'),
(146, 1, '8d16f5d176ed41e78f5dd358dd10181a', 'admin', '::1', 'Singapore', '2021-03-14 20:17:06'),
(147, 1, 'c680b42be11cb9366def3e17ebf75ecf', 'admin', '::1', 'Jakarta', '2021-03-15 14:29:19'),
(148, 1, 'd55e00582f99359c5217a23e23a4d17b', 'admin', '::1', 'Jakarta', '2021-03-15 14:29:19'),
(149, 1, '88511c496fbc5a48e5c13b1e82aab464', 'admin', '::1', 'Singapore', '2021-03-16 11:57:05'),
(150, 1, '53c3a7445cf07ed66e55cc3bc82efee7', 'admin', '::1', 'Singapore', '2021-03-16 11:57:06'),
(151, 1, '51264838f59d660759991791dccda971', 'admin', '::1', 'Singapore', '2021-03-16 15:54:45'),
(152, 1, '55516d693f17ea022af9245f05cad0ad', 'admin', '::1', 'Singapore', '2021-03-16 15:54:49'),
(153, 1, 'b0c396f50be441ed400dff4db6d12f85', 'admin', '::1', 'Singapore', '2021-03-17 10:00:53'),
(154, 1, 'b8e6530fa15e75fbe6426b1ce44c1059', 'admin', '::1', 'Singapore', '2021-03-17 14:51:05'),
(155, 1, 'd55699938f04425bb9ba9d1eadcdf508', 'admin', '::1', 'Singapore', '2021-03-17 14:51:06'),
(156, 1, 'cb42b1d6b031477174a10a8db3804088', 'admin', '::1', 'Singapore', '2021-03-19 10:09:42'),
(157, 6, 'f48ffc29c2b00dcd69e300465803fbb7', '916031', '::1', 'Depok', '2021-04-06 13:17:04'),
(158, 1, '1feaeb62d1638445a4001938b2cee8fc', 'admin', '127.0.0.1', 'Depok', '2021-04-06 13:50:59'),
(159, 1, '37ece1595e36eb298b888126ac051a6e', 'admin', '::1', 'Depok', '2021-04-06 15:44:44'),
(160, 6, '6ea489945230091f079fc0dff9cacbf4', '916031', '::1', 'Depok', '2021-04-06 15:54:22'),
(161, 1, '6882d2d1845f8ca85fc6aeb824a8460f', 'admin', '::1', 'Depok', '2021-04-06 18:59:45'),
(162, 1, 'b2d42b36cc98a487a3486af8e828e841', 'admin', '::1', 'Depok', '2021-04-07 09:09:31'),
(163, 1, '7d36752f94dcbc457cf34200732feccb', 'admin', '127.0.0.1', 'Depok', '2021-04-07 09:34:46'),
(164, 1, '448a4257ccedfd1754a03042c5522165', 'admin', '::1', 'Depok', '2021-04-08 09:35:54'),
(165, 1, '472a834c4fd00aead2c205d45cce56b0', 'admin', '::1', 'Depok', '2021-04-08 09:35:54'),
(166, 1, '8727eb5425ffeb7be9bb9da7d59d7c19', 'admin', '::1', 'Depok', '2021-04-08 12:49:57'),
(167, 1, '9732e28d3f5c223025897b7a214e21d9', 'admin', '::1', 'Depok', '2021-04-08 12:49:58'),
(168, 1, 'bdaabc6269da5ed330d64613341afb53', 'admin', '::1', 'Depok', '2021-04-09 08:40:33'),
(169, 1, '80420fdf2e2bb0c3c74b8186d78d1dde', 'admin', '::1', 'Depok', '2021-04-09 08:40:34'),
(170, 1, '642820ac4db1d7a1198603d8950d6629', 'admin', '::1', 'Depok', '2021-04-09 15:04:07'),
(171, 1, 'edba0ecc242b5e63712b55542a7e56d9', 'admin', '::1', 'Depok', '2021-04-09 15:04:08'),
(172, 1, '128dd77cca16450f86843fc7850e545d', 'admin', '::1', 'Depok', '2021-04-10 18:48:40'),
(173, 1, '36c412465168473f4161aa568d6dd44b', 'admin', '::1', 'Singapore', '2021-04-11 09:22:46'),
(174, 1, '7165ab19d4ed24a8c36fb97862d72485', 'admin', '::1', 'Singapore', '2021-04-11 09:22:47'),
(175, 1, '98e3a804ea1322b1703f29065f85d5f4', 'admin', '::1', 'Singapore', '2021-04-11 13:48:05'),
(176, 1, '298f864b3fec275853bb8c12ce818fc7', 'admin', '::1', 'Singapore', '2021-04-11 13:48:06'),
(177, 2, 'b39e0794aad4a14aece291960c720fa8', 'admin', '::1', 'Singapore', '2021-04-11 20:16:52'),
(178, 6, '1f77ca3688d65531351bf49f70db39b9', '916031', '::1', 'Singapore', '2021-04-12 10:23:17'),
(179, 1, '58e4061e729cb873808a417792b88aa3', 'admin', '::1', 'Singapore', '2021-04-12 10:25:08'),
(180, 1, '3430ce39f9946b26137b4459660b14e4', 'admin', '::1', 'Singapore', '2021-04-12 10:25:09'),
(181, 1, '38378d96e764d61f496e0939ffb5f63e', 'admin', '::1', 'Singapore', '2021-04-12 13:37:56'),
(182, 1, 'c29f225acbb652b9b5587881a2bbfc21', 'admin', '::1', 'Singapore', '2021-04-12 13:37:57'),
(183, 1, 'ad3ea91cef9ab7932967ef43447f52a5', 'admin', '::1', 'Singapore', '2021-04-12 18:56:08'),
(184, 1, '1c76337a6a072725fff70d5e387e72e8', 'admin', '::1', 'Singapore', '2021-04-12 18:56:12'),
(185, 1, '643ec7511fcb60f2d23a3bd4659fef47', 'admin', '::1', 'Singapore', '2021-04-13 09:24:01'),
(186, 1, '9ca59e074a7a500edeb0d809bc655e11', 'admin', '::1', 'Singapore', '2021-04-13 09:24:02'),
(187, 1, '2e1749656be62683bcc3bb6f3c7f95e9', 'admin', '::1', 'Jakarta', '2021-04-13 13:33:36'),
(188, 1, '116687824f2cde7cb705fb3ea0d052f7', 'admin', '::1', 'Jakarta', '2021-04-13 13:33:36'),
(189, 1, 'a67eed463b0118c90e2584b190c3a1ba', 'admin', '::1', 'Singapore', '2021-04-13 17:49:26'),
(190, 1, '5b50f83de1789f82d96f9cca17d4e9dd', 'admin', '::1', 'Singapore', '2021-04-13 17:49:27'),
(191, 1, 'e4d27a9ace8f98244e41bc3bff641ac0', 'admin', '::1', 'Singapore', '2021-04-13 20:27:34'),
(192, 1, 'ca36e0e452880097fce2b21de7b90cf5', 'admin', '::1', 'Singapore', '2021-04-13 20:27:34'),
(193, 2, '4d4aeb2185a5ae13934ba308028d0c55', 'admin', '::1', 'Bekasi', '2021-04-15 10:28:38'),
(194, 2, '6d3c637d6c71263653c093416aafe8f0', 'admin', '::1', 'Bekasi', '2021-04-15 10:33:34'),
(195, 1, '85beda2aeb73137f3db7b636376098d2', 'admin', '::1', 'Singapore', '2021-04-16 09:39:11'),
(196, 1, '0eeeaf0f42f703d4647e54e018343cfb', 'admin', '::1', 'Singapore', '2021-04-16 09:39:12'),
(197, 1, 'f63d5b9f1f4a1776afefee8843267200', 'admin', '::1', 'Singapore', '2021-04-16 13:19:32'),
(198, 1, '3dfde5282a128b6a4fddc9e31e282015', 'admin', '::1', 'Singapore', '2021-04-16 13:19:33'),
(199, 1, '692a20061f1cbf16af9cbd9adcfe1444', 'admin', '::1', 'Singapore', '2021-04-16 15:56:00'),
(200, 1, 'c32251faf27b54608a4d9f87aebeddbe', 'admin', '::1', 'Singapore', '2021-04-16 15:56:01'),
(201, 1, '334a88a1b027a707ff5cc7cd7047e683', 'admin', '127.0.0.1', 'Singapore', '2021-04-16 16:00:18'),
(202, 1, '0a79168c9ecec67b4292b7e5073996a5', 'admin', '::1', 'Singapore', '2021-04-18 05:06:41'),
(203, 1, 'd6ac08a4f7cca907db5c62cec7990b9b', 'admin', '::1', 'Singapore', '2021-04-18 05:06:42'),
(204, 1, '12f1c6e40225005da6430a6e0cde02b4', 'admin', '::1', 'Singapore', '2021-04-18 14:07:49'),
(205, 1, '166e2fcf11d7d92073d4ae63e0e9d58a', 'admin', '::1', 'Singapore', '2021-04-18 14:07:49'),
(206, 1, '77047152992ec0c1db356897494d807e', 'admin', '::1', 'Singapore', '2021-04-18 19:57:56'),
(207, 1, '563f010dd181435a4371cd095e786624', 'admin', '::1', 'Singapore', '2021-04-18 19:57:57'),
(208, 1, '8bbac919187260c753712511019f543a', 'admin', '::1', 'Singapore', '2021-04-19 11:36:49'),
(209, 1, '160858a932e8732a412c26fbe43af827', 'admin', '::1', 'Singapore', '2021-04-19 11:36:50'),
(210, 9, '8c1eef6a9771ed67a0385606b73973f8', 'denny', '::1', 'Singapore', '2021-04-19 11:42:50'),
(211, 1, '18cbef904c55347cd9dec7c0b1bea3d2', 'admin', '::1', 'Singapore', '2021-04-19 11:43:12'),
(212, 10, '8f1472c15782fe667b6d86ec78b22d88', 'ppk', '::1', 'Singapore', '2021-04-19 11:51:46'),
(213, 1, '7952637acd05830e66c1c82eed2b5636', 'admin', '::1', 'Singapore', '2021-04-19 12:49:35'),
(214, 1, '59f3d22c99f8a46bd847580fbdb38b58', 'admin', '::1', 'Singapore', '2021-04-19 12:49:35'),
(215, 1, '99f6a30a9a7641361cf7af1850dfa56d', 'admin', '::1', 'Singapore', '2021-04-20 06:57:59'),
(216, 1, '49c4cde9d15b6d4e86857805bc58c084', 'admin', '::1', 'Singapore', '2021-04-20 06:58:00'),
(217, 1, 'f4e5a26eddce69766149f23a5dcef58d', 'admin', '::1', 'Singapore', '2021-04-20 14:33:24'),
(218, 1, 'd0db0296c5525ea0ced96ffa174e5245', 'admin', '::1', 'Singapore', '2021-04-20 14:33:24'),
(219, 1, '587fecddcc4833b7fca19a95a07a1ab0', 'admin', '::1', 'Depok', '2021-04-21 06:57:15'),
(220, 1, '54b8bed4f98ebac3fd22cca39bc34ef9', 'admin', '::1', 'Depok', '2021-04-21 06:57:15'),
(221, 1, '8f064873e0410f7844f7940fda7beb14', 'admin', '::1', 'Depok', '2021-04-21 12:37:41'),
(222, 1, '96807eaa712ec7b96cf8150d692f30cc', 'admin', '::1', 'Singapore', '2021-04-22 11:34:39'),
(223, 1, '2502f705ad365726b776a7aeb2b00187', 'admin', '::1', 'Singapore', '2021-04-22 11:34:41'),
(224, 1, '13083542d7b32d4a320ff5206dcdaa83', 'admin', '::1', 'Singapore', '2021-04-22 15:41:37'),
(225, 1, 'b4e3fba008e97544a65ec4aaa69d9e3c', 'admin', '::1', 'Singapore', '2021-04-22 15:41:38'),
(226, 1, '6ed55d19c9586d569df2f57335755ab4', 'admin', '::1', 'Singapore', '2021-04-22 18:42:32'),
(227, 1, 'e009cb7d409114b753e6dfa96734e33c', 'admin', '::1', 'Singapore', '2021-04-22 18:42:33'),
(228, 2, '8497b36c454d85a78f475b766cf46230', 'admin', '::1', 'Jakarta', '2021-04-23 21:45:39'),
(229, 2, '764b5edba72665a74f49fd7f2e56d311', 'admin', '::1', 'Jakarta', '2021-04-24 13:14:45'),
(230, 2, '05921565b4a6afceb5374aaad41216e6', 'admin', '::1', 'Jakarta', '2021-04-25 22:01:45'),
(231, 2, '3f7013884096407e4275c7a852d38165', 'admin', '::1', 'Bang Pakong', '2021-04-26 11:26:07'),
(232, 2, '5946b8bd698c72d2f2c64d905790548d', 'admin', '::1', 'Bang Pakong', '2021-04-26 20:15:51'),
(233, 2, '6099f6699b0c59d914f4695f7270ec66', 'admin', '::1', 'Bang Pakong', '2021-04-27 12:35:37'),
(234, 2, '022d8e70ffe2463d684a71058e770471', 'admin', '::1', 'Bang Pakong', '2021-04-27 20:24:33'),
(235, 2, '8801d81ce7acec9cdb45688b7bb92496', 'admin', '::1', 'Jakarta', '2021-04-28 17:08:37'),
(236, 2, '14f30a07807aad0bd427161eab574370', 'admin', '::1', 'Jakarta', '2021-04-29 05:27:24'),
(237, 2, 'bc84711154bbdb99f31b1d1e5ae94665', 'admin', '::1', 'Jakarta', '2021-04-29 14:40:39'),
(238, 2, '56802ef398cd334aaa9f888ca9457018', 'admin', '::1', 'Bang Pakong', '2021-04-30 07:30:11'),
(239, 2, '2adf485ed23d750e2cec33dfdb41aad8', 'admin', '::1', 'Bang Pakong', '2021-04-30 20:35:16'),
(240, 2, 'b4f7774d9c036824049973e81bd636fd', 'admin', '::1', 'Jakarta', '2021-05-01 04:37:45'),
(241, 2, '114c0a3b6edd298bc8889b54bec252f5', 'admin', '::1', 'Jakarta', '2021-05-01 07:27:07'),
(242, 2, '739b33968b7a53171d42a0e6a43876a5', 'admin', '::1', 'Jakarta', '2021-05-01 11:25:44'),
(243, 2, '7c91d254b1e4c7135764e99f183aaa3c', 'admin', '::1', 'Jakarta', '2021-05-01 14:14:29'),
(244, 2, '2a655c91d162e071efd5a199b33b32c8', 'admin', '::1', 'Jakarta', '2021-05-01 23:08:34'),
(245, 2, '14091e03347ae23119e640a2ccb1efb1', 'admin', '::1', 'Jakarta', '2021-05-02 04:38:05'),
(246, 2, '8a0760f38cf9ac7cc0ec7fda38ef3eb3', 'admin', '::1', 'Jakarta', '2021-05-02 12:01:40'),
(247, 2, '3c76868bb59c49ed05f1255dd722a3c8', 'admin', '::1', 'Jakarta', '2021-05-02 19:01:58'),
(248, 2, '04585ff20b539b2bb570ab44528ea913', 'admin', '::1', 'Jakarta', '2021-05-04 19:59:17'),
(249, 2, '8762487b9a6c7e14373b697fada63764', 'admin', '::1', 'Jakarta', '2021-05-05 21:22:28'),
(250, 2, '2a6fd2efa9db573ea9af774500b27f9e', 'admin', '::1', 'Jakarta', '2021-05-06 05:29:35'),
(251, 2, '1430f4f4cd27eae9d310b2ba674e8b3e', 'admin', '::1', 'Jakarta', '2021-05-06 11:33:44'),
(252, 2, 'db3c8df989ed13409d4e166f838a8a54', 'admin', '::1', 'Jakarta', '2021-05-07 14:02:58'),
(253, 2, '6b1653c0e013bb6117ecde3b6cf57224', 'admin', '::1', 'Jakarta', '2021-05-08 15:40:59'),
(254, 2, '866486f56ac8e442c711eb48ffaf02d5', 'admin', '::1', 'Jakarta', '2021-05-10 05:05:58'),
(255, 2, '4aca28c51b569e43ebade549971de0f5', 'admin', '::1', 'Jakarta', '2021-05-10 15:01:05'),
(256, 2, 'c2c3fbb371b64761872d7c46696c5c91', 'admin', '::1', 'Jakarta', '2021-05-10 18:45:51'),
(257, 2, '50155cbcffa5333c2970b36bcfd0e714', 'admin', '::1', 'Jakarta', '2021-05-11 12:22:16'),
(258, 2, 'e52fb83e77f300de0543e08166d90db3', 'admin', '::1', 'Jakarta', '2021-05-11 21:57:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_anggaran`
--
ALTER TABLE `m_anggaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_groups`
--
ALTER TABLE `m_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `division` (`division_id`);

--
-- Indexes for table `m_menu`
--
ALTER TABLE `m_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_users`
--
ALTER TABLE `m_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group fk` (`group_id`),
  ADD KEY `division fk` (`division_id`);

--
-- Indexes for table `tr_detail_kegiatan`
--
ALTER TABLE `tr_detail_kegiatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_detail_lampiran`
--
ALTER TABLE `tr_detail_lampiran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_group_menu`
--
ALTER TABLE `tr_group_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`) USING BTREE,
  ADD KEY `menu_id` (`menu_id`);

--
-- Indexes for table `tr_kegiatan`
--
ALTER TABLE `tr_kegiatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_kontrak`
--
ALTER TABLE `tr_kontrak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_kontrak_termin`
--
ALTER TABLE `tr_kontrak_termin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_tagihan`
--
ALTER TABLE `tr_tagihan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_user_login_records`
--
ALTER TABLE `tr_user_login_records`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_anggaran`
--
ALTER TABLE `m_anggaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `m_groups`
--
ALTER TABLE `m_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `m_menu`
--
ALTER TABLE `m_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `m_users`
--
ALTER TABLE `m_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tr_detail_kegiatan`
--
ALTER TABLE `tr_detail_kegiatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tr_detail_lampiran`
--
ALTER TABLE `tr_detail_lampiran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tr_group_menu`
--
ALTER TABLE `tr_group_menu`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `tr_kegiatan`
--
ALTER TABLE `tr_kegiatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tr_kontrak`
--
ALTER TABLE `tr_kontrak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `tr_kontrak_termin`
--
ALTER TABLE `tr_kontrak_termin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tr_tagihan`
--
ALTER TABLE `tr_tagihan`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `tr_user_login_records`
--
ALTER TABLE `tr_user_login_records`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=259;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tr_user_login_records`
--
ALTER TABLE `tr_user_login_records`
  ADD CONSTRAINT `tr_user_login_records_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `m_users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
