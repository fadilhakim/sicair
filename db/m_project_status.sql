-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Sep 18, 2020 at 09:30 AM
-- Server version: 5.7.25
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `telkomsat_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_project_status`
--

CREATE TABLE `m_project_status` (
  `id` int(11) NOT NULL,
  `status_name` varchar(255) NOT NULL,
  `status_color` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `m_project_status`
--

INSERT INTO `m_project_status` (`id`, `status_name`, `status_color`) VALUES
(1, 'Pengadaan', '#38b6ff'),
(2, 'MOD', '#5271ff'),
(3, 'MOS', '#004aad'),
(4, 'Intalasi', '#5e17eb'),
(5, 'Online', '#03989e'),
(6, 'Pending', '#ff5757'),
(7, 'Cancel', '#ff1616'),
(8, 'BA Diterima', '#00a33d');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_project_status`
--
ALTER TABLE `m_project_status`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_project_status`
--
ALTER TABLE `m_project_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
