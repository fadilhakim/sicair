-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2020 at 02:41 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `telkomsat_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tr_project_update`
--

CREATE TABLE `tr_project_update` (
  `id` int(11) NOT NULL,
  `pelanggan` int(10) NOT NULL,
  `h_workorder_id` int(10) NOT NULL,
  `no_wo` varchar(100) NOT NULL,
  `project_code` varchar(255) NOT NULL,
  `tgl_terima_order` date NOT NULL,
  `RFS` date NOT NULL,
  `jenis_layanan` int(10) NOT NULL,
  `teknologi` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tr_project_update`
--

INSERT INTO `tr_project_update` (`id`, `pelanggan`, `h_workorder_id`, `no_wo`, `project_code`, `tgl_terima_order`, `RFS`, `jenis_layanan`, `teknologi`, `created_at`, `updated_at`) VALUES
(1, 311, 1, 'MI.0001/D1.200/MS.00/TSAT/04.2020', '3020001-Penyediaan Layanan Sewa dan Managed Service Radio IP di Lokasi Cargill Ketapang untuk PT Reach Network Service Indonesia', '2020-04-28', '2020-05-05', 1, '', '0000-00-00 00:00:00', '2020-09-24 08:10:50'),
(2, 563, 2, 'MI.0002/D1.200/MS.00/TSAT/04.2020', '5360001-Pengadaan Layanan Metro E P2P Manage Service Radio IP dan Mikrotik CCR pada PT. Bank IBK Indonesia lokasi Cikarang Bekasi', '2020-04-28', '2020-05-26', 1, '', '0000-00-00 00:00:00', '2020-09-24 08:11:37'),
(3, 570, 7, 'MI.0007/D1.200/MS.00/TSAT/05.2020', '5430001-Instalasi Layanan VSAT IP untuk Kejagung', '2020-05-12', '2020-05-15', 1, '', '0000-00-00 00:00:00', '2020-09-24 08:11:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tr_project_update`
--
ALTER TABLE `tr_project_update`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tr_project_update`
--
ALTER TABLE `tr_project_update`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
