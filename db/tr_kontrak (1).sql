-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 02, 2021 at 08:49 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apa_ditlala`
--

-- --------------------------------------------------------

--
-- Table structure for table `tr_kontrak`
--

CREATE TABLE `tr_kontrak` (
  `id` int(11) NOT NULL,
  `id_detail_kegiatan` int(11) NOT NULL,
  `no_kontrak` varchar(255) DEFAULT NULL,
  `tanggal_kontrak` date DEFAULT NULL,
  `uraian` text DEFAULT NULL,
  `status` enum('Verfikasi Bendahara','Terdaftar','Rejected','') DEFAULT NULL,
  `jenis_kontrak` int(11) DEFAULT NULL,
  `nomor_addendum` varchar(255) DEFAULT NULL,
  `tanggal_addendum` date DEFAULT NULL,
  `nama_kontraktor` varchar(255) DEFAULT NULL,
  `alamat_kontraktor` text DEFAULT NULL,
  `npwp_kontraktor` varchar(255) DEFAULT NULL,
  `rekening_kontraktor` varchar(20) DEFAULT NULL,
  `nama_rekening` varchar(255) DEFAULT NULL,
  `nilai_kontrak` bigint(20) DEFAULT NULL,
  `cara_pembayaran` enum('sekaligus','termin') DEFAULT NULL,
  `waktu_penyelesaian` int(11) DEFAULT NULL,
  `waktu_pemeliharaan` int(11) DEFAULT NULL,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `file_kontrak` varchar(255) DEFAULT NULL,
  `file_addendum` varchar(255) DEFAULT NULL,
  `file_myc` varchar(255) DEFAULT NULL,
  `jumlah_termin` int(11) DEFAULT NULL,
  `uang_muka` int(100) DEFAULT NULL,
  `subdit` enum('Subdit 1','Subdit 2','Subdit 3','Subdit 4','Subdit 5','Subbag TU') DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tr_kontrak`
--

INSERT INTO `tr_kontrak` (`id`, `id_detail_kegiatan`, `no_kontrak`, `tanggal_kontrak`, `uraian`, `status`, `jenis_kontrak`, `nomor_addendum`, `tanggal_addendum`, `nama_kontraktor`, `alamat_kontraktor`, `npwp_kontraktor`, `rekening_kontraktor`, `nama_rekening`, `nilai_kontrak`, `cara_pembayaran`, `waktu_penyelesaian`, `waktu_pemeliharaan`, `tanggal_mulai`, `tanggal_selesai`, `file_kontrak`, `file_addendum`, `file_myc`, `jumlah_termin`, `uang_muka`, `subdit`, `created_at`, `deleted_at`) VALUES
(1, 1, 'CON/123/111', '2021-04-25', 'jadi begini isi kontraknya ', NULL, 1, NULL, NULL, 'Aries Dimas Yudhistira', 'jl. warakas 4', '8560.0256.0.55485', '345243534', 'Aries Dimas', 80250000, '', 1, 1, '2021-04-22', '2021-04-30', 'kontrak-con/123/111-20210502124738.', 'sdafsdf', 'sdfsdf', NULL, 70000000, 'Subdit 4', '2021-04-25 15:08:56', '2021-04-25 15:08:56'),
(2, 2, 'CTR/11/11', '2021-04-21', 'fderyayfdhggfgh', NULL, 0, NULL, NULL, 'dimas', 'warakas', '123/123/123', '2147483647', 'dimas', 78979800, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8000000, NULL, '2021-05-01 00:45:48', '2021-05-01 00:45:48'),
(4, 7, 'CTR/11/11', '2021-05-19', '67hgjdgfhsfghfsgh', NULL, 0, NULL, NULL, 'dimas', 'warakas', '123/123/123', '45645634', 'dimas', 78979800, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '2021-05-01 01:42:18', '2021-05-01 01:42:18'),
(28, 1, 'CTR/67/67', '2021-05-02', 'kontrak ini adalah pengadaan 10 PC ', NULL, 1, NULL, NULL, 'Aries Dimas Yudhistira', 'jl. warakas 4', '8560.0256.0.55485', '5670123478', 'Aries Dimas', 14000000000, 'sekaligus', 1, 1, '2021-05-10', '2021-05-31', 'kontrak-ctr/67/67-20210502133848.', NULL, NULL, NULL, 2000000000, 'Subdit 5', '2021-05-02 05:42:48', '2021-05-02 05:42:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tr_kontrak`
--
ALTER TABLE `tr_kontrak`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tr_kontrak`
--
ALTER TABLE `tr_kontrak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
