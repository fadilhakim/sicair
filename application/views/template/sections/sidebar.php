<nav class="sidebar">
  <div style="background-color:#241373; border-right:0px;" class="sidebar-header">
    <a style="font-size:20px; color: #FFC414" href="#" class="sidebar-brand">
      SiCair -<span style="color:#fff;"> Ditlala</span>
    </a>
    <div class="sidebar-toggler not-active">
      <span style="background:#fff"></span>
      <span style="background:#fff"></span>
      <span style="background:#fff"></span>
    </div>
  </div>
  <div class="sidebar-body" style="border-right:0px;">
    <ul class="nav">
      <li style="color:#FFC414;" class="nav-item nav-category">Menu Utama</li>
      <?php if ($this->authorization->display_menu("dashboard")) { ?>

        </li>
        <li class="nav-item">
          <a href="<?= base_url("dashboard") ?>" class="nav-link">
            <i class="link-icon" data-feather="monitor"></i>
            <span class="link-title">Dashboard</span>
          </a>

        </li>
      <?php } ?>
      <?php if ($this->authorization->display_menu("daftar_anggaran")) { ?>

        <!-- <li class="nav-item">
          <a class="nav-link" data-toggle="collapse" href="#anggaran" role="button" aria-expanded="false" aria-controls="history">
            <i class="link-icon" data-feather="shopping-cart"></i>
            <span class="link-title">Kl.Anggaran</span>
            <i class="link-arrow" data-feather="chevron-down"></i>
          </a>
          <div class="collapse" id="anggaran">
            <ul class="nav sub-menu">
              <li class="nav-item">
                <a href="<?= base_url("anggaran") ?>" class="nav-link">Daftar Kegiatan</a>
              </li>

              <li class="nav-item">
                <a href="<?= base_url("kegiatan") ?>" class="nav-link">Daftar Kegiatan</a>
              </li>

              <li class="nav-item">
                <a href="<?= base_url("anggaran/pencairan_list") ?>" class="nav-link">Pencairan Anggaran</a>
              </li>
            </ul>
          </div>
        </li> -->

        <li class="nav-item">
          <a href="<?= base_url("anggaran_list") ?>" class="nav-link">
            <i class="link-icon" data-feather="file-text"></i>
            <span class="link-title">Daftar Anggaran</span>
          </a>

        </li>

      <?php } ?>

      <?php if ($this->authorization->display_menu("daftar_kegiatan")) { ?>

        <li class="nav-item">
          <a href="<?= base_url("kegiatan") ?>" class="nav-link">
            <i class="link-icon" data-feather="file-text"></i>
            <span class="link-title">Daftar Kegiatan</span>
          </a>

        </li>

      <?php } ?>


      <?php if ($this->authorization->display_menu("daftar_kontrak")) { ?>

        <li class="nav-item">
          <a href="<?= base_url("kontrak") ?>" class="nav-link">
            <i class="link-icon" data-feather="file-text"></i>
            <span class="link-title">Daftar Kontrak</span>
          </a>

        </li>

      <?php } ?>

      <?php if ($this->authorization->display_menu("pencairan_anggaran")) { ?>

        <li class="nav-item">
          <a href="<?= base_url("tagihan/pencairan_anggaran") ?>" class="nav-link">
            <i class="link-icon" data-feather="file-text"></i>
            <span class="link-title">Daftar Tagihan</span>
          </a>

        </li>

      <?php } ?>

      <li class="nav-item">
        <a href="<?= base_url("laporan") ?>" class="nav-link">
          <i class="link-icon" data-feather="trello"></i>
          <span class="link-title">Laporan</span>
        </a>

      </li>

      <!-- <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#laporan" role="button" aria-expanded="false" aria-controls="history">
          <i class="link-icon" data-feather="clipboard"></i>
          <span class="link-title">Laporan</span>
          <i class="link-arrow" data-feather="chevron-down"></i>
        </a>
        <div class="collapse" id="laporan">
          <ul class="nav sub-menu">
            <li class="nav-item">
              <a href="<?= base_url("laporan/kegiatan") ?>" class="nav-link">Lap. Kontrak</a>
            </li>

            <li class="nav-item">
              <a href="<?= base_url("laporan/tagihan") ?>" class="nav-link">Lap. Tagihan</a>
            </li>
          </ul>
        </div>
      </li> -->

      <!-- <li class="nav-item">
          <a href="<?= base_url("tagihan") ?>" class="nav-link">
            <i class="link-icon" data-feather="file-text"></i>
            <span class="link-title">Kl.Tagihan</span>
          </a>

        </li> -->

      <?php if ($this->authorization->display_menu("project_history")) { ?>
        <!-- <li class="nav-item">
          <a class="nav-link" data-toggle="collapse" href="#history" role="button" aria-expanded="false" aria-controls="history">
            <i class="link-icon" data-feather="save"></i>
            <span class="link-title">History</span>
            <i class="link-arrow" data-feather="chevron-down"></i>
          </a>
          <div class="collapse" id="history">
            <ul class="nav sub-menu">
              <li class="nav-item">
                <a href="<?= base_url("history/project-update") ?>" class="nav-link">Kegiatan Logs</a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url("history/users") ?>" class="nav-link">Users Logs</a>
              </li>
            </ul>
          </div>
        </li> -->
      <?php } ?>
      <?php if ($this->authorization->display_menu("user_setting") || $this->authorization->display_menu("group_setting")) { ?>
        <li style="color:#FFC414;" class="nav-item nav-category">Menu Admin</li>
      <?php } ?>
      <?php if ($this->authorization->display_menu("user_setting") || $this->authorization->display_menu("group_setting")) { ?>
        <li class="nav-item">
          <a href="<?= base_url("user") ?>" class="nav-link">
            <i class="link-icon" data-feather="user"></i>
            <span class="link-title">User List</span>
          </a>

        </li>
      <?php } ?>
      <?php if ($this->authorization->display_menu("group_setting")) { ?>
        <li class="nav-item">
          <a href="<?= base_url("group") ?>" class="nav-link">
            <i class="link-icon" data-feather="layers"></i>
            <span class="link-title">Group Authorization</span>
          </a>
        </li>
      <?php } ?>

    </ul>
  </div>
</nav>