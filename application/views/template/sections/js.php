<!-- core:js -->
<script src="<?= base_url("assets/vendors/core/core.js") ?>"></script>

<!-- endinject -->

<!-- plugin js for this page -->

<!-- end plugin js for this page -->

<!-- inject:js -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?= base_url("assets/vendors/feather-icons/feather.min.js") ?>"></script>
<script src="<?= base_url("assets/js/template.js") ?>"></script>
<script src="<?= base_url("assets/vendors/select2/select2.min.js") ?>"></script>
<script src="<?= base_url("assets/vendors/typeahead.js/typeahead.bundle.min.js") ?>"></script>
<script src="<?= base_url("assets/js/autoNumeric.js") ?>"></script>
<!-- endinject  -->

<!-- custom js for this page -->
<?php !isset($js) || empty($js) ? "" : $this->load->view($js); ?>
<script>
    $("#kegiatan-search").autocomplete({
        source: function(request, response) {
            $.ajax({
                // url:"https://swapi.dev/api/planets/",
                url: "<?= base_url("kegiatan/kegiatan_search") ?>",
                dataType: "json",
                data: {
                    search: request.term
                },
                success: function(data) {
                    response($.map(data.results, function(item) {
                        return {
                            label: `[${item.kode_aktivitas}] ${item.nama_kegiatan}`,
                            value: item.id,
                            detail: item
                        }
                    }));
                }
            })
        },
        minLength: 2,

        select: function(event, ui) {
            //   console.log(" ui.item => ",ui.item.detail)
            const id_kegiatan = ui.item.detail.id
            const url_str = "<?= base_url() ?>" + "kegiatan/detail_kegiatan/" + id_kegiatan
            window.location.href = url_str
            //log( "Selected: " + ui.item.name + " aka " + ui.item.id );
        }
    });
</script>
<!-- end custom js for this page -->