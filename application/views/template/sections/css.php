<link rel="stylesheet" href="<?= base_url("assets/vendors/core/core.css") ?>">
<!-- endinject -->

<!-- plugin css for this page -->
<?php !isset($css) || empty($css) ? "" : $this->load->view($css) ?>
<!-- end plugin css for this page -->

<!-- inject:css -->
<link rel="stylesheet" href="<?= base_url("assets/fonts/feather-font/css/iconfont.css") ?>">
<link rel="stylesheet" href="<?= base_url("assets/vendors/flag-icon-css/css/flag-icon.min.css") ?>">
<link rel="stylesheet" href="<?= base_url("assets/vendors/select2/select2.min.css") ?>">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- endinject -->

<!-- Layout styles -->
<link rel="stylesheet" href="<?= base_url("assets/css/demo_1/style.css") ?>">
<!-- End layout styles -->

<link rel="shortcut icon" href="<?= base_url("assets/images/kemenhublogo.png") ?>" />

<style>
    .card-header-title {
        width: 80%;
        float: left;
    }

    .card-tools {
        display: inline;
        width: 20%;
        float: "right"
    }

    .name {
        text-align: right;
        min-width: 150px;
    }

    .navbar {
        background: #241373 !important;
        color: #fff !important;
    }

    .btn-primary {
        background-color: #241373;
        border-color: #241373;
    }

    .card-title {
        font-size: 18px !important;
        font-weight: bold !important;

    }

    .table thead th {
        font-size: 14px !important;
        color: #727cf5;
    }

    .dt-buttons {
        margin-bottom: 30px !important;
    }
</style>