<style>
    .ui-menu {
        list-style: none;
        padding: 0;
        margin: 0 0 0 100px;
        display: block;
        outline: none;
        z-index: 999;
    }
</style>

<nav class="navbar">
    <a href="#" class="sidebar-toggler">
        <i data-feather="menu"></i>
    </a>

    <div class="navbar-content">

        <form class="form-group" style="margin-top:15px; width:50%">
            <div class="row">
                <div class="col-lg-8">

                    <input style="z-index:999" type="text" name="project-update" id="kegiatan-search" placeholder="Cari Kegiatan / Kode Aktivitas" class="form-control">

                </div>
                <div class="col-lg-4">
                    <!-- <input style="display:inline-block;" class="btn btn-primary" type="submit" value="Cari"> -->
                </div>
            </div>

        </form>
        <?php
        $roles = '';
        if ($this->session->userdata("group_id") == '1') {
            $roles = 'Admin';
        } else if ($this->session->userdata("group_id") == '2') {
            $roles = 'PPK';
        } else if ($this->session->userdata("group_id") == '3') {
            $roles = 'Bend.Pembantu';
        } else if ($this->session->userdata("group_id") == '4') {
            $roles = 'Bend.Pengeluaran';
        } else if ($this->session->userdata("group_id") == '5') {
            $roles = 'PPSPM';
        } else if ($this->session->userdata("group_id") == '6') {
            $roles = 'Direktur';
        }
        ?>
        <ul class="navbar-nav">
            <li class="nav-item">
                <p class="name mb-0">Selamat Datang
                    <span class="font-weight-bold"><?= $this->session->userdata("fullname") ?> / <?= $roles ?></span>
                    <!-- <span class="font-weight-bold">Fahmi</span>     -->
                </p>
            </li>
            <li class="nav-item dropdown nav-profile">
                <a class="nav-link dropdown-toggle" href="#" id="profileDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="<?= base_url() ?>/assets/images/man.png" alt="userr">
                </a>
                <div class="dropdown-menu" aria-labelledby="profileDropdown">
                    <div class="dropdown-header d-flex flex-column align-items-center">
                        <div class="figure mb-3">
                            <img src="<?= base_url() ?>/assets/images/kemenhublogo.png" alt="">
                        </div>
                        <div class="info text-center">
                            <p class="name font-weight-bold mb-0"><?= $this->session->userdata("fullname") ?></p>
                            <p class="email text-muted mb-3"><?= $this->session->userdata("email") ?></p>
                        </div>
                    </div>
                    <div class="dropdown-body">
                        <ul class="profile-nav p-0 pt-3">
                            <!-- <li class="nav-item">
                              <a href="pages/general/profile.html" class="nav-link">
                                  <i data-feather="user"></i>
                                  <span>Profile</span>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="javascript:;" class="nav-link">
                                  <i data-feather="edit"></i>
                                  <span>Edit Profile</span>
                              </a>
                          </li> -->

                            <li class="nav-item">
                                <a href="<?= base_url("logout") ?>" class="nav-link">
                                    <i data-feather="log-out"></i>
                                    <span>Log Out</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</nav>