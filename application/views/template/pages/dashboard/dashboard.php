<div class="page-content">

  <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
    <div>
      <h4 class="mb-3 mb-md-0">Dashboard <strong> SiCair</strong></h4>
    </div>
    <div class="d-flex align-items-center flex-wrap text-nowrap">

      <button type="button" data-toggle="modal" data-target="#pilihTahun" class="btn btn-outline-primary btn-icon-text mb-2 mb-md-0">
        <i class="link-icon" style="width:15px; margin-top:-4px" data-feather="calendar"></i>
        Tahun Anggaran : <strong class="year-display"></strong>
      </button>
    </div>
  </div>

  <div class="row flex-grow">

    <a href="<?= base_url("kegiatan") ?>" class="col-md-3 grid-margin stretch-card">

      <div class="card" style="background-color:#727cf5;">
        <div class="card-body">
          <div class="d-flex justify-content-between align-items-baseline">
            <h6 style="color:#fff;" class="card-title mb-2">Total Kegiatan :</h6>

          </div>
          <div class="row">
            <div class="col-12 col-md-12 col-xl-12">
              <h3 class="mb-2">
                <span class=" badge badge-primary" style="font-size:40px !important;" id="count-detail-kegiatan"></span>
              </h3>
              <p style="color:#fff">dari <span id="count-kegiatan"></span> Aktivitas </p>
              <i style="color:#fff;" class="incon i-1" class="link-icon" data-feather="file-text"></i>
            </div>

          </div>
        </div>
      </div>

    </a>

    <a href="<?= base_url("anggaran_list") ?>" class="col-md-3 grid-margin stretch-card">
      <div class="card" style="background-color:#66d1d1;">
        <div class="card-body">
          <div class="d-flex justify-content-between align-items-baseline">

            <h6 class="card-title mb-2" style="color:#fff;">Total Anggaran <span class="year-display"></span> :</h6>


          </div>
          <div class="row">
            <div class="col-12 col-md-12 col-xl-12">
              <h3 class="mb-2">

                <span style="font-size:20px !important; color:#fff;">Rp. <span id="countAnggaran"></span></span>
              </h3>
              <i style="color:#fff;" class="incon i-2" class="link-icon" data-feather="layers"></i>
            </div>

          </div>
        </div>
      </div>
    </a>

    <a href="<?= base_url("") ?>" class="col-md-3 grid-margin stretch-card">
      <div class="card" style="background-color:#ff3366;">
        <div class="card-body">
          <div class="d-flex justify-content-between align-items-baseline">
            <h6 class="card-title mb-2" style="color:#fff;">Total Tagihan:</h6>

          </div>
          <div class="row">
            <div class="col-12 col-md-12 col-xl-12">
              <h3 class="mb-2">
                <span style="font-size:20px !important; color:#fff;" class=" badge badge-danger">Rp. <span id="count-tagihan"></span></span>
              </h3>
              <i style="color:#fff;" class="incon i-2" class="link-icon" data-feather="inbox"></i>
            </div>

          </div>
        </div>
      </div>
    </a>

    <a href="<?= base_url("") ?>" class="col-md-3 grid-margin stretch-card">
      <div class="card" style="background-color:#10b759;">
        <div class="card-body">
          <div class="d-flex justify-content-between align-items-baseline">
            <h6 class="card-title mb-2" style="color:#fff;">Total Tagihan Verified:</h6>

          </div>
          <div class="row">
            <div class="col-12 col-md-12 col-xl-12">
              <h3 class="mb-2">

                <span style="font-size:20px !important; color:#fff;" class=" badge">Rp. <span id="count-tagihan-verified"></span></span>
              </h3>
              <i style="color:#fff;" class="incon i-2" class="link-icon" data-feather="inbox"></i>
            </div>

          </div>
        </div>
      </div>
    </a>

  </div>

  <div class="row">

    <div class="col-xl-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h6 class="card-title">Sisa Anggaran Daya Serap</h6>
          <div id="apexPie"></div>
        </div>
      </div>
    </div>

    <div class="col-xl-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h6 class="card-title">Daya Serap PPK</h6>
          <div id="apexBar"></div>
        </div>
      </div>
    </div>

    <div class="col-xl-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h6 class="card-title">Kegiatan Prioritas</h6>
          <div id="apexBar4"></div>
        </div>
      </div>
    </div>

  </div>



  <div class="row">

    <div class="col-lg-12 col-xl-12 stretch-card">
      <div class="card">
        <div class="card-body">
          <div class="d-flex justify-content-between align-items-baseline mb-2">
            <h6 class="card-title mb-0">Lastest Updates Kontrak</h6>
          </div>
          <br>
          <div class="table-responsive">
            <table ui-jp="dataTable" id="updated-kontrak" class="table table-hover mb-0">
              <thead>
                <tr>
                  <th class="pt-0">No Kontrak</th>
                  <th class="pt-0">Tanggal Kontrak</th>
                  <th class="pt-0">Nilai Kontrak</th>
                  <th class="pt-0">Status</th>
                  <th class="pt-0">Nama Kontraktor</th>
                  <th class="pt-0">Aksi</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>


</div>

<!-- modal pilih tahun -->

<div class="modal fade" id="pilihTahun" tabindex="-1" role="dialog" aria-labelledby="pilihTahun" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pilih Tahun</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="form-change-year">
        <div class="modal-body">

          <div class="row">
            <div class="col-lg-12">
              <div class="form-group tahun_anggaran">
                <label style="display: block;" for>Pilih Tahun Anggaran :</label>
                <select required style="display: block;" class="js-example-basic-single w-100 form-control" id="year" name="year">
                  <option value="2021" selected>2021</option>
                  <option value="2022">2022</option>
                  <option value="2023">2023</option>
                  <option value="2024">2024</option>
                  <option value="2025">2025</option>
                  <option value="2026">2026</option>

                </select>
              </div>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button id="change-year-btn" class="float-right btn btn-primary success" type="button">Pilih</button>
          </div>
      </form>
    </div>
  </div>
</div>


<style>
  #updated-kontrak_info {
    display: none;
  }

  #updated-kontrak_paginate {
    display: none;
  }
</style>