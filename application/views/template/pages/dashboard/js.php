<!-- plugin js for this page -->
<script src="<?= base_url("assets/vendors/datatables.net/jquery.dataTables.js") ?>"></script>
<script src="<?= base_url("assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js") ?>"></script>
<script src="<?= base_url("assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js") ?>"></script>
<script src="<?= base_url("assets/vendors/chartjs/Chart.min.js") ?>"></script>
<script src="<?= base_url("assets/vendors/apexcharts/apexcharts.min.js") ?>"></script>
<!-- end plugin js for this page -->
<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
<!-- custom js for this page -->

<script src="<?= base_url("assets/js/counter.js") ?>"></script>

<script>
  function count_anggaran() {
    $.ajax({
      "url": "<?= base_url("dashboard/count_anggaran") ?>",

      type: "post",
      dataType: "json",
      data: {
        year: $("form#form-change-year select#year").val()
      },
      success: function(res) {
        $(".year-display").html(res.data.tahun_anggaran)

        if (res.data.totalPagu) {
          $("#countAnggaran").html(String(res.data.totalPagu).replace(/(.)(?=(\d{3})+$)/g, '$1,'))
        } else {
          $("#countAnggaran").html(0)
        }

      }
    })
  }

  function count_kegiatan() {
    $.ajax({
      "url": "<?= base_url("dashboard/count_kegiatan") ?>",

      type: "post",
      dataType: "json",
      success: function(res) {

        $("#count-kegiatan").html(res.data)
      }
    })
  }

  function count_detail_kegiatan() {
    $.ajax({
      "url": "<?= base_url("dashboard/count_detail_kegiatan") ?>",

      type: "post",
      dataType: "json",
      success: function(res) {

        $("#count-detail-kegiatan").html(res.data)
      }
    })
  }

  function count_tagihan() {
    $.ajax({
      "url": "<?= base_url("dashboard/count_tagihan") ?>",

      type: "post",
      dataType: "json",
      success: function(res) {
        $("#count-tagihan").html(String(res.data.TotalTagihan).replace(/(.)(?=(\d{3})+$)/g, '$1,'))
      }
    })
  }

  function count_tagihan_verified() {
    $.ajax({
      "url": "<?= base_url("dashboard/count_tagihan_verified") ?>",

      type: "post",
      dataType: "json",
      success: function(res) {
        $("#count-tagihan-verified").html(String(res.data.TotalTagihan).replace(/(.)(?=(\d{3})+$)/g, '$1,'))
      }
    })
  }

  function getUpdatedKontrak() {

    $('#updated-kontrak').DataTable({
      dom: 'Bfrtip',
      paging: 'false',
      order: [
        [1, 'desc']
      ],
      buttons: [{
        text: 'Export to Excel',
        extend: 'excelHtml5',
        title: 'Daftar Kontrak Ter-updates',
        exportOptions: {
          columns: [0, 1, 2, 3, 4, 5]
        }
      }],
      serverSide: true,
      ajax: {
        // data: {
        //     tahun_anggaran: $(form + "#tahun_anggaran").val()
        // },
        url: "<?= base_url("dashboard/updatedKontrak") ?>",
        type: 'POST'
      },
      columns: [{
          "data": "no_kontrak"
        },
        {
          "data": "tanggal_kontrak"
        },
        {
          "data": "nilai_kontrak",
        },
        {
          "data": "status"
        },
        {
          "data": "nama_kontraktor"
        },
        {
          data: "Aksi"
        }
      ],
      columnDefs: [{
          "sorting": true,
          "orderable": true,
          "type": "html",
          "targets": 4,
          // "data": id,
          "render": function(data, type, row) {
            //console.log("data => ", data, type, row)

            const newRow = row

            return `<span style="display:block; text-align:center">${row.nama_kontraktor}</span>`
          }
        }, {
          "sorting": true,
          "orderable": true,
          "type": "html",
          "targets": 2,
          // "data": id,
          "render": function(data, type, row) {
            //console.log("data => ", data, type, row)

            const newRow = row

            return `<span style="font-weight:bold;">Rp. ${String(row.nilai_kontrak).replace(/(.)(?=(\d{3})+$)/g,'$1,')}</span>`
          }
        },
        {
          "sorting": true,
          "orderable": true,
          "type": "html",
          "targets": 0,
          // "data": id,
          "render": function(data, type, row) {
            //console.log("data => ", data, type, row)

            const newRow = row

            return `<a href="detail_kontrak/${row.id}">${row.no_kontrak}</a>`
          }
        },
        {
          "sorting": true,
          "orderable": true,
          "type": "html",
          "targets": 3,
          // "data": id,
          "render": function(data, type, row) {
            //console.log("data => ", data, type, row)

            const newRow = row
            var status = ''
            if (row.status == 'Terdaftar Belum di Verifikasi') {
              status = "badge-primary"
            } else if (row.status == 'Verifikasi Bendahara') {
              status = "badge-success"
            } else {
              status = 'badge-danger'
            }

            return `<span class="badge ${status}">${row.status}</span>`
          }
        },
        {
          "sorting": true,
          "orderable": true,
          "type": "html",
          "targets": 5,
          // "data": id,
          "render": function(data, type, row) {
            //console.log("data => ", data, type, row)

            const newRow = row

            return `<a href="detail_kontrak/${row.id}" class="btn btn-primary">Lihat Detail</a>`
          }
        }
      ]
    })
  }

  function pie_chart_daya_serap_all() {

    $("#apexPie").html("")

    $.ajax({
      type: "POST",
      url: "<?= base_url("dashboard/pie_chart_daya_serap_all") ?>",
      data: {
        year: $("form#form-change-year select#year").val() // sementara
      },
      dataType: "json",
      success: function(res) {

        var daya_serap = res.data.daya_serap ? res.data.daya_serap : 0
        var total_anggaran = res.data.total_anggaran ? res.data.total_anggaran : 0

        // Apex Pie chart start

        var options2 = {
          title: {
            display: true,
            text: "Tahun Anggaran " + $("form#form-change-year select#year").val()
          },
          chart: {
            height: 350,
            type: "pie"
          },
          labels: ['Daya Serap', 'Sisa Anggaran'],

          legend: {
            position: 'right'
          },
          colors: ["#f77eb9", "#7ee5e5"],
          breakpoint: 480,
          legend: {
            position: 'top',
            horizontalAlign: 'center'
          },
          stroke: {
            colors: ['rgba(0,0,0,0)']
          },
          dataLabels: {
            enabled: true,
            formatter: function(val) {
              return val.toFixed(2) + " %"
            }
          },
          series: [parseInt(daya_serap), parseInt(total_anggaran) - parseInt(daya_serap)],
          tooltip: {
            enabled: true,
            style: {
              fontSize: "14px",
            },
            y: {
              formatter: function(val) {
                return "Rp. " + new Intl.NumberFormat().format(val)
              }
            }


          }
        };

        var chart = new ApexCharts(document.querySelector("#apexPie"), options2)


        chart.render()
      }
    })
  }

  function bar_chart_daya_serap_user() {

    $("#apexBar").html("")
    var year = $("form#form-change-year select#year").val()

    $.ajax({
      type: "POST",
      dataType: "json",
      url: "<?= base_url("dashboard/bar_chart_daya_serap_pkk") ?>",
      data: {
        year: year
      }, // change to dynamic
      success: function(res) {
        // Apex Bar chart start

        options = {
          title: {
            display: true,
            text: "YEAR " + year
          },
          series: [{
            name: 'Anggaran',
            data: res.data.map(item => parseInt(item.total_anggaran))
          }, {
            name: 'Daya Serap',
            data: res.data.map(item => parseInt(item.daya_serap))
          }],
          chart: {
            type: 'bar',
            height: 350
          },
          plotOptions: {
            bar: {
              horizontal: false,
              columnWidth: '55%',
              endingShape: 'rounded'
            },
          },
          dataLabels: {
            enabled: false
          },
          stroke: {
            show: true,
            width: 2,
            colors: ['transparent']
          },
          xaxis: {
            categories: res.data.map(item => item.fullname),
          },
          yaxis: {
            title: {
              text: ""
            },
            labels: {
              formatter: function(value) {
                return "Rp. " + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
              }
            },
          },
          fill: {
            opacity: 1
          },
          tooltip: {
            y: {
              formatter: function(val) {
                return "Rp. " + new Intl.NumberFormat().format(val) + ""
              }
            }
          }
        };

        var chart = new ApexCharts(document.querySelector("#apexBar"), options)
        chart.render()
        // Apex Bar chart end
      }
    })
  }

  function bar_chart_daya_serap_kegiatan() {

    $("#apexBar4").html("")

    $.ajax({
      type: "POST",
      url: "<?= base_url("dashboard/bar_chart_daya_serap_kegiatan") ?>",
      dataType: "json",
      data: {
        year: $("form#form-change-year select#year").val()
      },
      success: function(res) {
        // Apex Bar chart start
        var options4 = {
          title: {
            display: true,
            text: "Tahun Anggaran " + $("form#form-change-year select#year").val()
          },
          series: [{
            name: 'Daya Serap',
            colors: '#f77eb9',
            data: res.data.map(item => item.daya_serap)
          }, {
            name: 'Anggaran',
            colors: '#7ee5e5',
            data: res.data.map(item => item.total_anggaran)
          }],
          chart: {
            type: 'bar',
            height: 430
          },

          plotOptions: {
            bar: {
              horizontal: true,
              dataLabels: {
                position: 'top',
              },
            }
          },
          dataLabels: {
            enabled: false,
            offsetX: -6,
            style: {
              fontSize: '15px',
              colors: ['#fff']
            }
          },
          stroke: {
            show: true,
            width: 1,
            colors: ['#fff']
          },
          tooltip: {
            shared: true,
            intersect: false,
            y: {
              formatter: function(val) {
                return "Rp. " + new Intl.NumberFormat().format(val) + ""
                //return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
              }
            }
          },

          xaxis: {
            categories: res.data.map(item => item.nama_kegiatan),
            labels: {
              formatter: function(val) {
                return "Rp. " + new Intl.NumberFormat().format(val) + ""
                //return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
              }
            }

          },
        };

        var apexBarChart4 = new ApexCharts(document.querySelector("#apexBar4"), options4);

        apexBarChart4.render();
      }
    })
  }

  function change_year(year) {
    $(".year-display").html(year)

    count_kegiatan()
    count_detail_kegiatan()

    count_anggaran()
    count_tagihan()
    count_tagihan_verified()

    pie_chart_daya_serap_all()
    bar_chart_daya_serap_user()
    bar_chart_daya_serap_kegiatan()
  }

  $(document).ready(function() {
    change_year($("form#form-change-year select#year").val())
  })

  $(function() {
    'use strict';
    getUpdatedKontrak()
  });

  $("#pilihTahun #change-year-btn").click(function(e) {
    change_year($("form#form-change-year select#year").val())

    $("#pilihTahun").modal("hide")
  })
</script>