
<script src="<?=base_url("assets/vendors/chartjs/Chart.min.js")?>"></script>

<script>
    if($('#chartjsLine').length) {

        $.ajax({
            type:"post",
            url:"<?=base_url("dashboard/tren_order_process")?>",
            dataType:"json",
            success:function(res) {

                new Chart($('#chartjsLine'), {
                    type: 'line',
                    data: {
                    labels: ["Jan","Feb","Mar","Apr","Mei","June","Jul","Aug","Sep","Oct","Nov","Dec"],
                    datasets: [{
                        data: res["<?=date("Y")-1?>"],
                        label: <?=date("Y")-1?>,
                        borderColor: "#7ee5e5",
                        backgroundColor: "rgba(0,0,0,0)",
                        fill: false
                        }, {
                        data:res["<?=date("Y")?>"],
                        label: <?=date("Y")?>,
                        borderColor: "#f77eb9",
                        backgroundColor: "rgba(0,0,0,0)",
                        fill: false
                        }
                    ]
                    }
                });

            }
        })

        
    }

    function rekapOrderMonthChart() {
        $.ajax({
            type:"post",
            url:"<?=base_url("dashboard/rekap_order_bulanan")?>",
            dataType:"json",
            success:function(data) {
                new Chart($('#chartjsGroupedBar'), {
                type: 'bar',
                data: {
                    labels: ["Jan", "Feb", "Mar", "April" , "May" , "Juni" , "Juli" , "Agustus" , "September" , "Oktobe" , "November" , "Desember"],
                    datasets: data
                }
                });

                $.each(data,function(index, val){
                $("#rekap-order-table").append(
                    $(`<tr>
                        <td>${val["label"]}</td>
                        <td>${val["data"][0]}</td>
                        <td>${val["data"][1]}</td>
                        <td>${val["data"][2]}</td>
                        <td>${val["data"][3]}</td>
                        <td>${val["data"][4]}</td>
                        <td>${val["data"][5]}</td>
                        <td>${val["data"][6]}</td>
                        <td>${val["data"][7]}</td>
                        <td>${val["data"][8]}</td>
                        <td>${val["data"][9]}</td>
                        <td>${val["data"][10]}</td>
                        <td>${val["data"][11]}</td>
                    </tr>`)
                )
                })

            }
        })
    }

    function rekapDismantleChart() {
        $.ajax({
            type:"post",
            url:"<?=base_url("dashboard/rekap_order_dismantle_bulanan")?>",
            dataType:"json",
            success:function(data) {
                new Chart($('#rekap-order-dismantle-chart'), {
                type: 'bar',
                data: {
                    labels: ["Jan", "Feb", "Mar", "April" , "May" , "Juni" , "Juli" , "Agustus" , "September" , "Oktobe" , "November" , "Desember"],
                    datasets: data
                }
                });

                $.each(data,function(index, val){
                $("#rekap-order-dismantle-table").append(
                    $(`<tr>
                        <td>${val["label"]}</td>
                        <td>${val["data"][0]}</td>
                        <td>${val["data"][1]}</td>
                        <td>${val["data"][2]}</td>
                        <td>${val["data"][3]}</td>
                        <td>${val["data"][4]}</td>
                        <td>${val["data"][5]}</td>
                        <td>${val["data"][6]}</td>
                        <td>${val["data"][7]}</td>
                        <td>${val["data"][8]}</td>
                        <td>${val["data"][9]}</td>
                        <td>${val["data"][10]}</td>
                        <td>${val["data"][11]}</td>
                    </tr>`)
                )
                })

            }
        })
    }

    $(function() {
        'use strict';
        if($('#chartjsGroupedBar').length) {

            rekapOrderMonthChart()
        }

        if($('#rekap-order-dismantle-chart').length) {

            rekapDismantleChart()
        }
    })


</script>
