<div class="modal fade" id="uploadSpm" tabindex="-1" role="dialog" aria-labelledby="uploadSpm" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Upload File SPM</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-spm">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-lg-12">

                            <div class="form-group">
                                <input style="display: none;" type="text" value='<?= $tagihan_detail["id"] ?>' id="id_tagihan" name="id_tagihan" class="form-control">
                            </div>

                            <div class="form-group">
                                <input style="display: none;" type="text" value='<?= $tagihan_detail["nomor_tagihan"] ?>' id="nomor_tagihan" name="nomor_tagihan" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputUsername1">Upload File :</label>
                                <input type="file" id="file_spm" name="file_spm" class="form-control">
                            </div>

                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button id="btn-upload-file-spm" class="float-right btn btn-primary success">Submit File</button>
                </div>
            </form>
        </div>
    </div>
</div>