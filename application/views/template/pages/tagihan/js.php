<!-- plugin js for this page -->
<script src="<?= base_url("assets/vendors/datatables.net/jquery.dataTables.js") ?>"></script>
<script src="<?= base_url("assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js") ?>"></script>
<script src="<?= base_url("assets/vendors/sweetalert2/sweetalert2.min.js") ?>"></script>
<script src="<?= base_url("assets/vendors/select2/select2.min.js") ?>"></script>
<script src="<?= base_url("assets/js/select2.js") ?>"></script>
<!-- end plugin js for this page -->



<!-- <script src=" https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script> -->

<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>


<script>
    function tagihanTable() {
        $("#table-tagihan").DataTable({
            dom: 'Bfrtip',
            // order: [
            //     [1, 'desc']
            // ],
            buttons: [{
                text: 'Export to Excel',
                extend: 'excelHtml5',
                title: 'Daftar Tagihan',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]
                }
            }],
            serverSide: true,
            ajax: {
                data: {
                    filter_status_tagihan: $("#filter_status_tagihan").val(),
                    filter_jenis_tagihan: $("#filter_jenis_tagihan").val(),
                    filter_subdit: $("#filter_subdit").val(),
                    filter_tanggal_tagihan: $("#filter_tanggal_tagihan").val(),
                    filter_nama_ppk: $("#filter_nama_ppk").val()

                },
                url: "<?= base_url("tagihan/get_all_tagihan") ?>",
                type: 'POST'
            },
            columns: [{
                    "data": "no"
                },
                {
                    "data": "status_tagihan"
                },
                {
                    "data": "nomor_tagihan" // no sptb
                },
                {
                    "data": "tanggal_tagihan"
                },
                {
                    "data": "uraian_tagihan"
                },
                {
                    "data": "nilai_tagihan"
                },
                {
                    "data": "jenis_tup"
                },
                {
                    "data": "potongan_pajak_ppn"
                },
                {
                    "data": "jenis_pajak_pph"
                },
                {
                    "data": "potongan_pajak_pph"
                },
                {
                    "data": "id_user"
                },
                {
                    "data": "subdit"
                },
                {
                    "data": "penyedia"
                },
                {
                    "data": "nama_kegiatan"
                },
                {
                    "data": "detail_kegiatan"
                },
                {
                    "data": "kode_mak"
                },
                {
                    "data": "uraian_mak"
                },
                {
                    "data": "action"
                },
            ],
            columnDefs: [{
                    "sorting": false,
                    "orderable": false,
                    "type": "html",
                    "targets": 2,
                    "render": function(data, type, row) {
                        var newRow = row

                        return `<a href="<?= base_url('tagihan/detail_tagihan/') ?>${row.id}" style="font-weight:bold;">${row.nomor_tagihan}</a>`

                    }
                },
                {
                    "sorting": false,
                    "orderable": false,
                    "type": "html",
                    "targets": 5,
                    "render": function(data, type, row) {
                        var newRow = row
                        return `<span style="font-weight:bold;">Rp. ${String(row.nilai_tagihan).replace(/(.)(?=(\d{3})+$)/g,'$1')}</span>`

                    }
                },
                {
                    "sorting": false,
                    "orderable": false,
                    "type": "html",
                    "targets": 6,
                    "render": function(data, type, row) {
                        var newRow = row

                        var jst = ''
                        if (row.jenis_tagihan == 'non_kontrak') {
                            jst = "Non Kontraktual"
                        } else {
                            jst = "Kontraktual"
                        }
                        return `<span style="font-weight:bold;">${jst}</span>`

                    }
                },
                {
                    "sorting": false,
                    "orderable": false,
                    "type": "html",
                    "targets": 7,
                    "render": function(data, type, row) {
                        var newRow = row
                        var tup = ''
                        if (row.jenis_tup) {
                            tup = row.jenis_tup
                        }
                        return `<span style="font-size:15px;" class="badge badge-light"> ${tup} </span>`

                    }
                },
                {
                    "sorting": false,
                    "orderable": false,
                    "type": "html",
                    "targets": 8,
                    "render": function(data, type, row) {
                        return `<span style="font-size:15px;font-weight:bold;" class="">Rp. ${row.potongan_pajak_ppn} </span>`

                    }
                },
                {
                    "sorting": false,
                    "orderable": false,
                    "type": "html",
                    "targets": 10,
                    "render": function(data, type, row) {

                        return `<span style="font-size:15px;font-weight:bold;" class="">Rp. ${row.potongan_pajak_pph} </span>`

                    }
                },
                {
                    "sorting": false,
                    "orderable": false,
                    "type": "html",
                    "targets": 9,
                    "render": function(data, type, row) {
                        return `<span style="font-size:15px;font-weight:bold;" class="badge badge-info">${row.jenis_pajak_pph} </span>`

                    }
                },
                {
                    "sorting": true,
                    "orderable": true,
                    "type": "html",
                    "targets": 1,
                    "render": function(data, type, row) {

                        const newRow = row
                        var status = ''
                        if (row.status_tagihan == 'ppspm') {
                            status = "badge-primary"
                        } else if (row.status_tagihan == 'verifikasi bendahara') {
                            status = "badge-success"
                        } else if (row.status_tagihan == 'upload') {
                            status = "badge-light"
                        } else if (row.status_tagihan == 'terbit spm') {
                            status = "badge-info"
                        } else {
                            status = 'badge-danger'

                        }

                        return `<span style="font-size:13.5px;" class="badge ${status}">${row.status_tagihan}</span>`
                    }
                },
                {
                    "sorting": false,
                    "orderable": false,
                    "type": "html",
                    "targets": 11,
                    "render": function(data, type, row) {
                        return `<span style="font-size:15px;font-weight:bold;">${row.id_user} </span>`

                    }
                },
                {
                    "sorting": true,
                    "orderable": true,
                    "type": "html",
                    "targets": 12,
                    "render": function(data, type, row) {


                        var subdit = ''
                        if (row.subdit == '') {
                            subdit = row.subditKontraktor
                        } else {
                            subdit = row.subdit

                        }

                        return `<span style="font-size:13.5px;">${subdit}</span>`
                    }
                },
                {
                    "sorting": true,
                    "orderable": true,
                    "type": "html",
                    "targets": 13,
                    "render": function(data, type, row) {


                        var penyedia = ''
                        if (row.nama_penerima == '') {
                            penyedia = row.nama_kontraktor
                        } else {
                            penyedia = row.nama_penerima

                        }

                        return `<span style="font-size:13.5px;">${penyedia}</span>`
                    }
                },
                {
                    "sorting": true,
                    "orderable": true,
                    "type": "html",
                    "targets": 14,
                    "render": function(data, type, row) {


                        return `<span style="font-size:13.5px;">${row.nama_kegiatan}</span>`
                    }
                },
                {
                    "sorting": true,
                    "orderable": true,
                    "type": "html",
                    "targets": 15,
                    "render": function(data, type, row) {


                        return `<span style="font-size:13.5px;">${row.detail_kegiatan}</span>`
                    }
                },
                {
                    "sorting": true,
                    "orderable": true,
                    "type": "html",
                    "targets": 16,
                    "render": function(data, type, row) {


                        return `<span style="font-size:13.5px;">${row.kode_mak}</span>`
                    }
                },
                {
                    "sorting": true,
                    "orderable": true,
                    "type": "html",
                    "targets": 17,
                    "render": function(data, type, row) {


                        return `<span style="font-size:13.5px;">${row.uraian_mak}</span>`
                    }
                },
                {
                    "sorting": false,
                    "orderable": false,
                    "type": "html",
                    "targets": 18,
                    "render": function(data, type, row) {
                        var newRow = row
                        var buttonPdf = '';
                        if (row.jenis_tagihan == 'kontrak') {
                            buttonPdf = `<li style="display:inline-block;"><a href="<?= base_url('printpdf/cetakSPTBKontrak/') ?>${row.id}" class="btn btn-dark">Cetak SPTB</a></li>`
                        } else {
                            buttonPdf = `<li style="display:inline-block;"><a href="<?= base_url('printpdf/cetakSPTBNonKontrak/') ?>${row.id}" class="btn btn-dark">Cetak SPTB</a></li>`
                        }

                        return `<div class="btn-group dropdown">
                            <button type="button" class="btn btn-primary" data-toggle="dropdown" aria-expanded="false">
                                Aksi <span class="caret"><i class="fa fa-caret-down"></i></span>
                            </button>
                            <ul class="dropdown-menu animated fadeIn">                          
                                <li style="display:none;"><a href="<?= base_url('tagihan/pengajuan_tagihan/') ?>${row.id}" class="btn btn-warning">Edit</a></li>
                                <li style="display:inline-block;"><a href="<?= base_url('tagihan/detail_tagihan/') ?>${row.id}" class="btn btn-info">Detail</a></li>
                                ${buttonPdf}
                                <li style="display:inline-block;"><a href="#" onClick="delete_tagihan(${row.id}, '${row.nomor_tagihan}')" class="btn btn-danger">Hapus</a></li>
                            </ul>
                        </div>`
                    }
                }
            ]
        })
    }

    function filterTagihan() {
        var filter_status_tagihan = $("#filter_status_tagihan").val()
        var filter_jenis_tagihan = $("#filter_jenis_tagihan").val()
        var filter_subdit = $("#filter_subdit").val()
        var filter_tanggal_tagihan = $("#filter_tanggal_tagihan").val()
        var filter_nama_ppk = $("#filter_nama_ppk").val()

        var data = {}

        if (filter_status_tagihan) {
            data.filter_status_tagihan = filter_status_tagihan
        }

        if (filter_jenis_tagihan) {
            data.filter_jenis_tagihan = filter_jenis_tagihan
        }

        if (filter_tanggal_tagihan) {
            data.filter_tanggal_tagihan = filter_tanggal_tagihan
        }
        if (filter_nama_ppk) {
            data.filter_nama_ppk = filter_nama_ppk
        }
        if (filter_subdit) {
            data.filter_subdit = filter_subdit
        }
        var param = $.param(data);
        param = decodeURIComponent(param)
        window.location = '<?= base_url("tagihan/pencairan_anggaran") ?>?' + param;
    }

    function delete_tagihan(id_tagihan, no_tagihan) {

        Swal.fire({
            title: "Hapus Tagihan ",
            html: `<p>Apakah anda yakin ingin menghapus tagihan ini ?</p> <li><b> ${no_tagihan} </b></li> `,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'mr-2',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true,
            closeOnConfirm: true
        }).then((result) => {

            if (result.value) {
                $.ajax({
                    url: "<?= base_url("tagihan/delete_tagihan") ?>",
                    data: {
                        id_tagihan: id_tagihan
                    },
                    dataType: "json",
                    method: "post",
                    success: function(res) {
                        if (res.success === true) {

                            $('#table-tagihan').DataTable().ajax.reload();

                            Swal.fire(
                                'Success!',
                                res.message,
                                'success'
                            )
                        } else {
                            Swal.fire({
                                title: '<strong> Error !</strong>',
                                icon: 'error',
                                html: res.message
                            })
                        }
                    }
                })
            }

        })


    }

    function clearTagihan() {
        window.location = '<?= base_url("tagihan/pencairan_anggaran") ?>'
    }

    $(function() {
        tagihanTable()

        $(".money").autoNumeric("init", {
            aForm: true,
            vMax: "999999999999999"
        });
    })

    $("#filter-tagihan-btn").click(function(e) {
        //alert("filter...")
        filterTagihan()
    })

    $("#clear-tagihan-btn").click(function(e) {
        //alert("filter...")
        clearTagihan()
    })
</script>