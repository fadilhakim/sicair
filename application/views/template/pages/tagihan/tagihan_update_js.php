<script src="<?= base_url("assets/vendors/sweetalert2/sweetalert2.min.js") ?>"></script>
<script src="<?= base_url("assets/vendors/select2/select2.min.js") ?>"></script>
<script src="<?= base_url("assets/js/select2.js") ?>"></script>
<script src="<?= base_url("assets/vendors/datatables.net/jquery.dataTables.js") ?>"></script>
<script src="<?= base_url("assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js") ?>"></script>

<!-- custom js for this page -->
<script src="<?= base_url("assets/js/data-table.js") ?>"></script>

<script>
    function get_kegiatan_detail_list(id_kegiatan) {
        $.ajax({
            "type": "POST",
            "url": "<?= base_url("kegiatan/get_kegiatan_detail_list") ?>",
            "dataType": 'json',
            data: {
                id_kegiatan: id_kegiatan
            },
            success: function(res) {

                $("select#id_detail_kegiatan").html("")

                $("select#id_detail_kegiatan").append(`<option value="">-- Pilih Detail Kegiatan --</option>`);
                var detail_kegiatan_list = $.each(res.data, function(index, item) {
                    $("select#id_detail_kegiatan").append(`<option value=${item.id}>${item.kode_mak} - ${item.detail_kegiatan}</option>`);
                })


            }

        })
    }

    function get_detail_kegiatan(id_detail_kegiatan) {

        $.ajax({
            "type": "POST",
            "url": "<?= base_url("kegiatan/get_kegiatan_detail") ?>",
            "dataType": 'json',
            data: {
                id: id_detail_kegiatan
            },
            success: function(res) {

                var xk = `Rp. ` + parseInt(res.data.pagu_kegiatan).toLocaleString()


                $("#real_id_kontrak").val("")
                $("#real_id_detail_kegiatan").val(res.data.id)

                $("#id_user").val(res.data.id_user)
                $("select#id_kegiatan").val(res.data.id_kegiatan)
                // $("#uraian_mak").val(res.data.uraian_mak)
                $("#pagu_kegiatan").val(xk)
                $("#detail_kegiatan").val(res.data.detail_kegiatan)
                $("#uraian_mak").val(res.data.uraian_mak)
                $("#id_kontrak").val(res.data.id_kontrak)
            }

        })
    }

    // function get_detail_tagihan(id_tagihan) {
    //     $.ajax({
    //         "type": "POST",
    //         "url": "<?= base_url("tagihan/get_detail_tagihan") ?>",
    //         "dataType": 'json',
    //         data: {
    //             id_tagihan: id_tagihan
    //         },
    //         success: function(res) {
    //             $("#tanggal_tagihan").val(res.data.tanggal_tagihan)
    //             $("#alamat_tagihantor").val(res.data.alamat_tagihantor)
    //             $("#nama_tagihantor").val(res.data.nama_tagihantor)
    //             $("#npwp_tagihantor").val(res.data.npwp_tagihantor)
    //             $("#rekening_tagihantor").val(res.data.rekening_tagihantor)
    //             $("#nama_rekening").val(res.data.nama_rekening)
    //         }

    //     })
    // }

    function get_detail_kontrak(id_kontrak) {
        $.ajax({
            "type": "POST",
            "url": "<?= base_url("kontrak/get_detail_kontrak") ?>",
            "dataType": 'json',
            data: {
                id_kontrak: id_kontrak
            },
            success: function(res) {
                $("#real_id_kontrak").val(res.data.id)
                $("#real_id_detail_kegiatan").val(res.data.id_detail_kegiatan)

                $("#id_user").val(res.data.id_user)

                $("#tanggal_kontrak").val(res.data.tanggal_kontrak)
                $("#alamat_kontraktor").val(res.data.alamat_kontraktor)
                $("#nama_kontraktor").val(res.data.nama_kontraktor)
                $("#npwp_kontraktor").val(res.data.npwp_kontraktor)
                $("#rekening_kontraktor").val(res.data.rekening_kontraktor)
                $("#nama_rekening").val(res.data.nama_rekening)

            }

        })
    }

    function tagihan_update() {

        var form_tambah_tagihan = $("form#form-tagihan")[0];
        const data = new FormData(form_tambah_tagihan)

        $.ajax({
            type: "post",
            url: "<?= base_url("tagihan/update_tagihan") ?>",
            enctype: "multipart/form-data",
            type: "post",
            data: data,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function(res) {
                if (res.success === true) {

                    Swal.fire(
                        'Success!',
                        res.message,
                        'success'
                    )

                    setTimeout(() => {
                        //location.href = "<?= base_url("tagihan/pencairan_anggaran") ?>"
                        location.reload();

                    }, 3000);

                } else {
                    Swal.fire({
                        title: '<strong> Error !</strong>',
                        icon: 'error',
                        html: res.message
                    })
                }
            }
        })
    }

    // lampiran

    function showDetailLampiran(id_lampiran) {
        return new Promise((resolve, reject) => {
            $.ajax({
                method: "post",
                url: "<?= base_url("tagihan/detail_lampiran") ?>",
                dataType: "json",
                data: {
                    lampiran_id: id_lampiran
                },
                success: function(res) {
                    resolve(res)
                },
                error: function(error) {
                    reject(error)
                },
            })
        })
    }

    function clearModalLampiran() {
        $("#lampiranModal #form-lampiran #id_lampiran").val("")
        $("#lampiranModal #form-lampiran #lampiran_id_tagihan").val($("#id_tagihan").val())
        $("#lampiranModal #form-lampiran #no_tagihan").val($("#nomor_tagihan").val())
        $("#lampiranModal #form-lampiran #file_name").val("")
        $("#lampiranModal #form-lampiran #file").val("")

        $("#lampiranModal").modal("show")
    }

    function showModalLampiran(id_lampiran) {

        showDetailLampiran(id_lampiran).then(res => {

                $("#lampiranModal #form-lampiran #id_lampiran").val(res.data.id)
                $("#lampiranModal #form-lampiran #lampiran_id_tagihan").val($("#id_tagihan").val())
                $("#lampiranModal #form-lampiran #no_tagihan").val($("#nomor_tagihan").val())
                $("#lampiranModal #form-lampiran #file_name").val(res.data.file_name)
                $("#lampiranModal #form-lampiran #file").val("")

                $("#lampiranModal").modal("show")
            })
            .catch(err => {
                console.log(err)
            })
    }

    function submitLampiran() {
        var id = $("form#form-lampiran #id_lampiran").val()

        if (id) {
            update_lampiran()
        } else {
            add_lampiran()
        }
    }

    function add_lampiran() {

        var form_tambah_lampiran = $("form#form-lampiran")[0];
        const data = new FormData(form_tambah_lampiran)

        $.ajax({
            url: "<?= base_url("tagihan/add_lampiran") ?>",
            enctype: "multipart/form-data",
            type: "post",
            data: data,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function(res) {
                if (res.success) {

                    $('#lampiran-table').DataTable().ajax.reload();
                    $("#lampiranModal").modal("hide")

                    Swal.fire(
                        'Success!',
                        res.message,
                        'success'
                    )
                } else {
                    Swal.fire({
                        title: '<strong> Error !</strong>',
                        icon: 'error',
                        html: res.message
                    })

                }
            }
        })
    }

    function update_lampiran() {
        var form_tambah_lampiran = $("form#form-lampiran")[0];
        const data = new FormData(form_tambah_lampiran)

        $.ajax({
            url: "<?= base_url("tagihan/update_lampiran") ?>",
            enctype: "multipart/form-data",
            type: "post",
            data: data,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function(res) {
                if (res.success) {

                    $('#lampiran-table').DataTable().ajax.reload();
                    $("#lampiranModal").modal("hide")


                    Swal.fire(
                        'Success!',
                        res.message,
                        'success'
                    )
                } else {
                    Swal.fire({
                        title: '<strong> Error !</strong>',
                        icon: 'error',
                        html: res.message
                    })

                }
            }
        })
    }

    function upload_file_spm() {
        var form_spm = $("form#form-spm")[0];
        const data = new FormData(form_spm)

        $.ajax({
            url: "<?= base_url("tagihan/update_file_spm") ?>",
            enctype: "multipart/form-data",
            type: "post",
            data: data,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function(res) {
                if (res.success) {
                    //alert('berhasil')
                    $("#uploadSpm").modal("hide")

                    Swal.fire(
                        'Success!',
                        res.message,
                        'success'
                    )
                } else {
                    Swal.fire({
                        title: '<strong> Error !</strong>',
                        icon: 'error',
                        html: res.message
                    })

                }
            }
        })
    }

    function delete_lampiran(id, file_name) {

        Swal.fire({
            title: "Hapus Lampiran ",
            html: `<p>Apakah anda yakin ingin menghapus lampiran ini ?</p> <li><b>  ${file_name} </b></li> `,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'mr-2',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true,
            closeOnConfirm: true
        }).then((result) => {

            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "<?= base_url("tagihan/delete_lampiran") ?>",
                    data: {
                        id: id
                    },
                    dataType: "json",
                    success: function(res) {

                        $('#lampiran-table').DataTable().ajax.reload();

                        if (res.success) {
                            Swal.fire(
                                'Success!',
                                res.message,
                                'success'
                            )
                        } else {
                            Swal.fire(
                                'Error!',
                                res.message,
                                'error'
                            )
                        }

                    }
                })
            } else {
                Swal.fire({
                    title: '<strong> Error !</strong>',
                    icon: 'error',
                    html: res.message
                })
            }


        })
    }

    function get_all_lampiran(id_tagihan) {

        $("#lampiran-table").DataTable({
            order: [
                [1, 'desc']
            ],
            serverSide: true,
            ajax: {
                data: {
                    id_tagihan: id_tagihan
                },
                url: "<?= base_url("tagihan/get_all_lampiran") ?>",
                type: 'POST',
                dataType: "json"
            },
            columns: [{
                    data: "no"
                },
                {
                    data: "file_name",
                },
                {
                    data: "file"
                }
            ],
            columnDefs: [{
                    "sorting": false,
                    "orderable": false,
                    "type": "html",
                    "targets": 2,
                    "render": function(data, type, row) {
                        return `<a class="btn btn-success" href="<?= base_url("tagihan/download_lampiran/") ?>${row.id}"> Download </a>`
                    }
                },
                {
                    "sorting": false,
                    "orderable": false,
                    "type": "html",
                    "targets": 3,
                    "render": function(data, type, row) {
                        return `<button type="button" onclick="showModalLampiran(${row.id})" class="btn btn-warning">Edit</button>
                      <button type="button" onclick="delete_lampiran(${row.id}, '${row.file_name}')" class="btn btn-danger">Delete</button>`
                    }
                },

            ]
        })
    }

    function showKontrak() {

        var colKontrak = document.getElementById("colKontrak");
        var colNonKontrak = document.getElementById("colNonKontrak");

        if (colKontrak.style.display === "none") {
            colKontrak.style.display = "block";
            colNonKontrak.style.display = "none";

        }
    }

    function showNonKontrak() {

        var colKontrak = document.getElementById("colKontrak");
        var colNonKontrak = document.getElementById("colNonKontrak");

        if (colNonKontrak.style.display === "none") {
            colNonKontrak.style.display = "block";
            colKontrak.style.display = "none";
        }
    }

    function verifikasi_tagihan(id) {

        Swal.fire({
            title: "Verifikasi Tagihan",
            text: "Apakah anda yakin verifikasi Tagihan ini?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'mr-2',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true,
            closeOnConfirm: true
        }).then((result) => {

            if (result.value) {

                $.ajax({
                    url: "<?= base_url("tagihan/verifikasi_tagihan") ?>",
                    type: "POST",
                    data: {
                        id: id
                    },
                    dataType: "json",
                    success: function(res) {
                        if (res.success === true) {

                            // $('#table-kontrak').DataTable().ajax.reload();

                            Swal.fire(
                                'Success!',
                                res.message,
                                'success'
                            )

                            setTimeout(() => {
                                window.location = '<?= base_url("tagihan/pencairan_anggaran") ?>';
                            }, 2000);

                        } else {
                            Swal.fire({
                                title: '<strong> Error !</strong>',
                                icon: 'error',
                                html: res.message
                            })
                        }
                    }

                })
            }
        })
    }

    function reject_tagihan(id) {
        Swal.fire({
            title: "Reject Tagihan ",
            text: "Apakah anda yakin Reject Tagihan ini?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'mr-2',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true,
            closeOnConfirm: true
        }).then((result) => {

            if (result.value) {

                $.ajax({
                    url: "<?= base_url("tagihan/reject_tagihan") ?>",
                    type: "POST",
                    data: {
                        id: id,
                        reject_note: $("#reject_note").val()
                    },
                    dataType: "json",
                    success: function(res) {
                        if (res.success === true) {

                            // $('#table-kontrak').DataTable().ajax.reload();

                            Swal.fire(
                                'Success!',
                                res.message,
                                'success'
                            )

                            setTimeout(() => {
                                window.location = '<?= base_url("tagihan/pencairan_anggaran") ?>';
                            }, 2000);

                        } else {
                            Swal.fire({
                                title: '<strong> Error !</strong>',
                                icon: 'error',
                                html: res.message
                            })
                        }
                    }

                })
            }
        })
    }

    $(document).ready(function() {

        var colKontrak = document.getElementById("colKontrak");
        var colNonKontrak = document.getElementById("colNonKontrak");
        colKontrak.style.display = 'none';
        colNonKontrak.style.display = 'none';

        if ($("input#non-kontrak").is(':checked')) {

            showNonKontrak()
        } else {

            showKontrak()
        }

        get_detail_kegiatan($("#id_detail_kegiatan").val())
        // get_kegiatan_detail_list($("#id_kegiatan").val())
        // get_detail_tagihan($("#id_tagihan").val())
        get_all_lampiran($("#id_tagihan").val())
        get_detail_kontrak($("#id_kontrak").val())
    })

    $(function() {

        $(".money").autoNumeric("init", {
            aForm: true,
            vMax: "999999999999999"
        });

        $("form#form-tagihan").submit(function(e) {
            tagihan_update()
            e.preventDefault()
        })

        $("#btn-tambah-lampiran").click(function() {
            clearModalLampiran()
        })

        $("#lampiranModal form#form-lampiran").submit(function(e) {

            submitLampiran()
            e.preventDefault()
        })

        $("select#id_detail_kegiatan").change(function(e) {
            get_detail_kegiatan($("#id_detail_kegiatan").val())
        })

        $("select#id_kegiatan").change(function(e) {
            get_kegiatan_detail_list($("#id_kegiatan").val())
        })

        //  $("select#id_tagihan").change(function(e){
        //     get_detail_tagihan($("#id_tagihan").val())
        // })

        $("select#id_kontrak").change(function(e) {
            get_detail_kontrak($("#id_kontrak").val())
        })


    })



    // $(document).on('click', '#non-tagihan', function(e) {
    //     get_detail_kegiatan($("#id_detail_kegiatan").val())
    //     showNonKontrak()

    // })

    // $(document).on('click', '#tagihan', function(e) {
    //     get_detail_tagihan($("#id_tagihan").val())
    //     showKontrak()

    // })

    $(document).on('click', '#non-kontrak', function(e) {
        //get_detail_kegiatan($("#id_detail_kegiatan").val())
        showNonKontrak()

    })

    $(document).on('click', '#kontrak', function(e) {
        //get_detail_kontrak($("#id_kontrak").val())
        showKontrak()

    })

    $(document).on('click', '#btn-upload-file-spm', function(e) {
        //get_detail_kontrak($("#id_kontrak").val())
        upload_file_spm()

    })
</script>