<?php
$group_id = $this->session->userdata("group_id");
$disable = '';
$disable_select = '';
if ($tagihan_detail["status_tagihan"] == 'verifikasi bendahara' || $tagihan_detail["status_tagihan"] == 'terbit spm') {
    $disable = 'readonly';
    $disable_select = 'disabled';
}


?>

<style>
    .badge-info {
        display: inline-block;
        margin: auto;
        font-size: 15px;
        text-align: center;
    }
</style>
<div class="page-content">
    <nav class="page-breadcrumb" style="position:relative">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url('tagihan/pencairan_anggaran') ?>">Daftar Tagihan</a></li>
            <li class="breadcrumb-item active" aria-current="page">Tambah Tagihan</li>
        </ol>
        <?php if (!empty($tagihan_detail["status_tagihan"])) { ?>
            <?php
            $badge = '';
            $infoText = '';
            if ($tagihan_detail["status_tagihan"] == 'terbit spm') {
                $badge = 'badge-info';
                $infoText = 'TERBIT SPM';
            } else if ($tagihan_detail["status_tagihan"] == 'verifikasi bendahara') {
                $badge = "badge-success";
                $infoText = 'VERIFIKASI BENDAHARA';
            } else if ($tagihan_detail["status_tagihan"] == 'upload') {
                $badge = "badge-light";
                $infoText = 'UPLOAD';
            } else {
                $badge = 'badge-danger';
                $infoText = 'REJECTED';
            }
            ?>
            <span style="display: inline-block; position: absolute; top: -3px; left: 41%; font-size: 14px;" class="badge <?= $badge ?>">STATUS : <?= $infoText ?></span>
        <?php } ?>
        <?php if ($this->uri->segment(3)) { ?>
            <p style="display: inline-block; float: right; font-weight:bold; margin-top:-20px;">Nomor SPTB : <?= $tagihan_detail["nomor_tagihan"] ?> </p>
        <?php } ?>
    </nav>
    <?php if ($tagihan_detail["status_tagihan"] == 'rejected') { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <textarea style="border: 1px solid #ff3366;" readonly disabled class="form-control" id="">Alasan di Reject : <?= $tagihan_detail["reject_note"] ?></textarea>
                </div>
            </div>
        </div>
    <?php } ?>
    <div style="clear: both;"></div>
    <form id="form-tagihan">
        <input type="hidden" value="<?= $tagihan_detail["id"] ?>" id="id_tagihan" name="id_tagihan">
        <input type="hidden" value="" id="id_user" name="id_user">
        <div style="display: none;" class="form-group">
            <label for="">status tagihan :</label>
            <input <?= $disable ?> type="text" placeholder="Nama Bank Penerima" value="<?= $tagihan_detail["status_tagihan"] ?>" id="status_tagihan" name="status_tagihan" class="form-control"></input>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xl-12 stretch-card">
                <div id="" class="card">
                    <div class="card-body">
                        <div class="row">


                            <div style="display:none" class="form-group">
                                <label> Nomor SPBT </label>
                                <input readonly type="text" value="<?= $tagihan_detail["nomor_tagihan"] ?>" name="nomor_tagihan" id="nomor_tagihan" class="form-control">
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group" style="margin-bottom: 2px;">
                                    <label style="font-weight: bold;" style="display: block;" for="">Pilih Jenis Tagihan :</label>
                                    <br>
                                    <div style="display: inline-block; margin-right:15px;" class="form-check">

                                        <label class="form-check-label">
                                            <input <?= $disable ?> id="non-kontrak" type="radio" class="form-check-input" name="jenis_tagihan" value="non_kontrak" <?= $tagihan_detail["jenis_tagihan"] == "non_kontrak" ? "checked" : "" ?>>
                                            Non Kontraktual
                                            <i class="input-frame"></i></label>

                                    </div>

                                    <div style="display: inline-block;" class="form-check">
                                        <label class="form-check-label">
                                            <input <?= $disable ?> id="kontrak" type="radio" class="form-check-input" name="jenis_tagihan" value="kontrak" <?= $tagihan_detail["jenis_tagihan"] == "kontrak" ? "checked" : "" ?>>
                                            Kontraktual
                                            <i class="input-frame"></i></label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group" style="margin-bottom: 2px;">
                                    <label style="font-weight: bold;" style="display: block;" for="">Jenis Pembayaran :</label>
                                    <br>
                                    <div style="display: inline-block; margin-right:15px;" class="form-check">

                                        <label class="form-check-label">
                                            <input <?= $disable ?> id="tup" type="radio" class="form-check-input" name="jenis_tup" value="tup" <?= $tagihan_detail["jenis_tup"] == "tup" ? "checked" : "" ?>>
                                            TUP
                                            <i class="input-frame"></i></label>

                                    </div>

                                    <div style="display: inline-block;" class="form-check">
                                        <label class="form-check-label">
                                            <input <?= $disable ?> id="lp" type="radio" class="form-check-input" name="jenis_tup" value="ls" <?= $tagihan_detail["jenis_tup"] == "ls" ? "checked" : "" ?>>
                                            LS
                                            <i class="input-frame"></i></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-lg-12 col-xl-12 stretch-card">
                <div id="colKontrak" class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline mb-2">
                            <h6 class="card-title mb-0">Data Kontrak</h6>
                        </div>
                        <br>

                        <div class="row">

                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <input type="hidden" value="" id="real_id_kontrak" name="real_id_kontrak">
                                        <div class="form-group">
                                            <label for="">Nomor Kontrak / Kuitansi :</label>
                                            <select <?= $disable ?> class="js-example-basic-single w-100 form-control mb-3" name="id_kontrak" id="id_kontrak" class="">
                                                <option selected="" value="">-- Please Select Kontrak --</option>
                                                <?php foreach ($kontrak_list as $row) {
                                                    $selected = $row["id"] == $tagihan_detail["id_kontrak"] ? "selected" : "";
                                                ?>
                                                    <option value="<?= $row["id"] ?>" <?= $selected ?>><?= $row["no_kontrak"] ?></option>
                                                <?php } ?>


                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="">Tanggal Kontrak :</label>
                                            <input <?= $disable ?> id="tanggal_kontrak" name="tanggal_kontrak" type="text" value="" readonly disabled class="form-control">
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="">Alamat Kontraktor:</label>
                                    <input <?= $disable ?> type="text" id="alamat_kontraktor" name="alamat_kontraktor" disabled readonly value="" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="">Nama Penerima :</label>
                                    <input <?= $disable ?> type="text" id="nama_kontraktor" name="nama_kontraktor" disabled readonly value="" class="form-control">
                                </div>

                            </div>
                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label for="">No. NPWP :</label>
                                    <input <?= $disable ?> type="text" id="npwp_kontraktor" name="npwp_kontraktor" disabled readonly value="" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="">No. Rekening :</label>
                                    <input <?= $disable ?> type="text" id="rekening_kontraktor" name="rekening_kontraktor" disabled readonly value="" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="">Nama Rekening :</label>
                                    <input <?= $disable ?> type="text" id="nama_rekening" name="nama_rekening" disabled readonly value="" class="form-control">
                                </div>



                            </div>

                        </div>


                    </div>
                </div>

                <div class="card" id="colNonKontrak">

                    <div id="colNonKontrak" class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline mb-2">
                            <h6 class="card-title mb-0">Data Non Kontrak</h6>
                        </div>
                        <br>
                        <div class="row">
                            <input type="hidden" value="" id="real_id_detail_kegiatan" name="real_id_detail_kegiatan">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="">Pilih Kegiatan :</label>

                                    <select style="width:100%;" <?= $disable ?> class="js-example-basic-single w-100 form-control mb-3 " name="id_kegiatan" id="id_kegiatan">
                                        <option value="">-- Pilih Kegiatan --</option>

                                        <?php
                                        foreach ($kegiatan_list as $row3) {
                                            $selected1 = $row3["id"] == $kegiatan_detail_dt["id_kegiatan"] ? "selected" : "";
                                        ?>
                                            <option value="<?= $row3["id"] ?>" <?= $selected1 ?>><?= $row3["nama_kegiatan"] ?></option>
                                        <?php } ?>


                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label for="">Klasifikasi Anggaran / Kode MAK :</label>

                                    <select style="width:100%;" <?= $disable ?> class="js-example-basic-single w-100 form-control mb-3 " name="id_detail_kegiatan" id="id_detail_kegiatan">
                                        <option value="">-- Pilih Detail Kegiatan -- </option>
                                        <?php foreach ($kegiatan_detail_list as $row2) {

                                            $selected2 = $row2["id"] == $tagihan_detail["id_detail_kegiatan"] ? "selected" : "";

                                        ?>
                                            <option value="<?= $row2["id"] ?>" <?= $selected2 ?>><?= $row2["kode_mak"] . " / " . $row2["detail_kegiatan"] ?></option>
                                        <?php } ?>


                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Subdit :</label>
                                    <select style="width:100%;" <?= $disable ?> class="js-example-basic-single w-100 form-control mb-3 " name="subdit" id="subdit">
                                        <option value="">-- Pilih Subdit -- </option>
                                        <option value="Subdit 1" <?= $tagihan_detail["subdit"] == "Subdit 1" ? "selected" : "" ?>>Subdit 1</option>
                                        <option value="Subdit 2" <?= $tagihan_detail["subdit"] == "Subdit 2" ? "selected" : "" ?>>Subdit 2</option>
                                        <option value="Subdit 3" <?= $tagihan_detail["subdit"] == "Subdit 3" ? "selected" : "" ?>>Subdit 3</option>
                                        <option value="Subdit 4" <?= $tagihan_detail["subdit"] == "Subdit 4" ? "selected" : "" ?>>Subdit 4</option>
                                        <option value="Subdit 5" <?= $tagihan_detail["subdit"] == "Subdit 5" ? "selected" : "" ?>>Subdit 5</option>
                                        <option value="Subbag TU" <?= $tagihan_detail["subdit"] == "Subbag TU" ? "selected" : "" ?>>Subbag TU</option>
                                    </select>
                                </div>


                                <div class="form-group">
                                    <label for="">Bank Penerima :</label>
                                    <input <?= $disable ?> type="text" placeholder="Nama Bank Penerima" value="<?= $tagihan_detail["nama_bank_penerima"] ?>" id="nama_bank_penerima" name="nama_bank_penerima" class="form-control"></input>
                                </div>

                                <div class="form-group">
                                    <label for="">No. Rekening :</label>
                                    <input <?= $disable ?> type="text" placeholder="Nomor Rekening Penerima" value="<?= $tagihan_detail["nomor_rekening_penerima"] ?>" id="nomor_rekening_penerima" name="nomor_rekening_penerima" class="form-control"></input>
                                </div>
                            </div>
                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label for="">Nama Penerima :</label>
                                    <input <?= $disable ?> type="text" placeholder="Masukan Nama Penerima" value="<?= $tagihan_detail["nama_penerima"] ?>" id="nama_penerima" name="nama_penerima" class="form-control"></input>
                                </div>
                                <div class="form-group">
                                    <label for="">Alamat Penerima :</label>
                                    <input <?= $disable ?> type="text" placeholder="Alamat Penerima" value="<?= $tagihan_detail["alamat_penerima"] ?>" id="alamat_penerima" name="alamat_penerima" class="form-control"></input>
                                </div>
                                <div class="form-group">
                                    <label for="">No. Kuitansi :</label>
                                    <input <?= $disable ?> type="text" placeholder="Nomor Kuitansi" value="<?= $tagihan_detail["nomor_kuitansi"] ?>" id="nomor_kuitansi" name="nomor_kuitansi" class="form-control"></input>
                                </div>

                                <div class="form-group">
                                    <label for="">Pagu Kegiatan :</label>
                                    <input id="pagu_kegiatan" name="pagu_kegiatan" type="text" disabled readonly value="" class="form-control money">
                                </div>


                            </div>

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="">Detail Kegiatan :</label>
                                    <textarea id="detail_kegiatan" row="15" cols="20" name="detail_kegiatan" disabled readonly class="form-control"></textarea>
                                </div>
                            </div>

                            <div class="col-lg-12">

                                <div class="form-group">
                                    <label for="">Uraian MAK :</label>
                                    <textarea id="uraian_mak" name="uraian_mak" row="15" cols="20" disabled readonly class="form-control"></textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <!-- Data Tagihan -->
        <div class="row">
            <div class="col-lg-12 col-xl-12 stretch-card">
                <div class="card">

                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline mb-2">
                            <h6 class="card-title mb-0">Data Tagihan</h6>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="">Nilai Tagihan :</label>
                                    <input <?= $disable ?> type="text" data-a-sign="Rp. " data-a-dec="," data-a-sep="." id="nilai_tagihan" name="nilai_tagihan" value="<?= $tagihan_detail["nilai_tagihan"] ?>" class="form-control money">
                                </div>

                                <div class="form-group">
                                    <label for="">Potongan Pajak PPN :</label>
                                    <input <?= $disable ?> type="text" data-a-sign="Rp. " data-a-dec="," data-a-sep="." id="potongan_pajak_ppn" name="potongan_pajak_ppn" value="<?= $tagihan_detail["potongan_pajak_ppn"] ?>" class="form-control money">
                                </div>

                            </div>
                            <div class="col-lg-6">
                                <?php if ($disable_select == 'disabled') { ?>
                                    <div class="form-group">
                                        <label for="">Kategori Pajak PPH :</label>
                                        <input <?= $disable ?> name="jenis_pajak_pph" id="jenis_pajak_pph" class="form-control" value="<?= $tagihan_detail["jenis_pajak_pph"] ?>">
                                    </div>
                                <?php } else { ?>
                                    <div class="form-group">
                                        <label for="">Kategori Pajak PPH :</label>
                                        <select <?= $disable_select ?> class="form-control js-example-basic-single" name="jenis_pajak_pph" id="jenis_pajak_pph">
                                            <option value="pph 15" <?= $tagihan_detail["jenis_pajak_pph"] == "pph 15" ? "selected" : "" ?>>PPH 15</option>
                                            <option value="pph 21" <?= $tagihan_detail["jenis_pajak_pph"] == "pph 21" ? "selected" : "" ?>>PPH 21</option>
                                            <option value="pph 22" <?= $tagihan_detail["jenis_pajak_pph"] == "pph 22" ? "selected" : "" ?>>PPH 22</option>
                                            <option value="pph 23" <?= $tagihan_detail["jenis_pajak_pph"] == "pph 23" ? "selected" : "" ?>>PPH 23</option>
                                        </select>
                                    </div>
                                <?php }   ?>

                                <div class="form-group">
                                    <label for="">Potongan PPH :</label>
                                    <input <?= $disable ?> type="text" data-a-sign="Rp. " data-a-dec="," data-a-sep="." id="potongan_pajak_pph" name="potongan_pajak_pph" class="form-control money" value="<?= $tagihan_detail["potongan_pajak_pph"] ?>">
                                </div>

                            </div>

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="">Uraian :</label>
                                    <textarea <?= $disable ?> name="uraian_tagihan" id="uraian_tagihan" class="form-control" cols="30" rows="10"><?= $tagihan_detail["uraian_tagihan"] ?></textarea>
                                </div>
                            </div>
                            <?php if ($tagihan_detail["file_spm"]) { ?>
                                <div class="col-lg-6">

                                </div>
                                <div class="col-lg-6">
                                    <?php $newNomor =  str_replace('/', '', $tagihan_detail["nomor_tagihan"]) ?>
                                    <a href="<?= base_url() ?>/files/tagihan/<?= $newNomor ?>/<?= $tagihan_detail["file_spm"] ?>" download style="float: right;" class="btn btn-success"> <i style="font-size:12px;" class="link-icon" data-feather="download"></i>&nbsp; Download file spm </a>
                                </div>
                            <?php } ?>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>

        <!-- Ini Cara Lampiran Yang Lama BACKUPAN -->
        <?php if ($this->uri->segment(2) == "detail_tagihan" && !empty($this->uri->segment(3))) { ?>
            <div class="row">
                <div class="col-lg-12 col-xl-12 stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex justify-content-between align-items-baseline mb-2">
                                <h6 class="card-title mb-0">Lampiran Tagihan (Umum)</h6>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="">Surat Permohonan :</label>
                                                <div>
                                                    <input type="file" id="file_surat_permohonan" name="file_surat_permohonan" value="" class="form-control col-md-9 float-left">
                                                    <?php if ($tagihan_detail["file_surat_permohonan"]) {
                                                        echo "<a class='btn btn-success col-md-3 float-right' href=" . base_url("tagihan/download/$tagihan_detail[id]/file_surat_permohonan") . "> <i class='fa fa-download'></i> Download </a>";
                                                    } ?>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="">Invoice :</label>
                                                <div>
                                                    <input type="file" id="file_invoice" name="file_invoice" value="" class="form-control col-md-9 float-left">
                                                    <?php if ($tagihan_detail["file_invoice"]) {
                                                        echo "<a class='btn btn-success col-md-3 float-right' href=" . base_url("tagihan/download/$tagihan_detail[id]/file_invoice") . "> <i class='fa fa-download'></i> Download </a>";
                                                    } ?>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="">Kuitansi :</label>
                                                <div>
                                                    <input type="file" name="file_kwitansi" id="file_kwitansi" value="" class="form-control col-md-9 float-left">
                                                    <?php if ($tagihan_detail["file_kwitansi"]) {
                                                        echo "<a class='btn btn-success col-md-3 float-right' href=" . base_url("tagihan/download/$tagihan_detail[id]/file_kwitansi") . "> <i class='fa fa-download'></i> Download </a>";
                                                    } ?>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="">File SPP PPN :</label>
                                                <div>
                                                    <input type="file" id="file_spp_ppn" name="file_spp_ppn" value="" class="form-control col-md-9 float-left">
                                                    <?php if ($tagihan_detail["file_spp_ppn"]) {
                                                        echo "<a class='btn btn-success col-md-3 float-right' href=" . base_url("tagihan/download/$tagihan_detail[id]/file_spp_ppn") . "> <i class='fa fa-download'></i> Download </a>";
                                                    } ?>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="">File SPP PPH :</label>
                                                <div>
                                                    <input type="file" id="file_spp_pph" name="file_spp_pph" value="" class="form-control col-md-9 float-left">
                                                    <?php if ($tagihan_detail["file_spp_pph"]) {
                                                        echo "<a class='btn btn-success col-md-3 float-right' href=" . base_url("tagihan/download/$tagihan_detail[id]/file_spp_pph") . "> <i class='fa fa-download'></i> Download </a>";
                                                    } ?>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="">Faktur Pajak :</label>
                                                <div>
                                                    <input type="file" id="file_faktur_pajak" name="file_faktur_pajak" class="form-control col-md-9 float-left">
                                                    <?php if ($tagihan_detail["file_faktur_pajak"]) {
                                                        echo "<a class='btn btn-success col-md-3 float-right' href=" . base_url("tagihan/download/$tagihan_detail[id]/file_faktur_pajak") . "> <i class='fa fa-download'></i> Download </a>";
                                                    } ?>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="">BAP :</label>
                                                <div>
                                                    <input type="file" id="file_bap" name="file_bap" value="" class="form-control col-md-9 float-left">
                                                    <?php if ($tagihan_detail["file_bap"]) {
                                                        echo "<a class='btn btn-success col-md-3 float-right' href=" . base_url("tagihan/download/$tagihan_detail[id]/file_bap") . "> <i class='fa fa-download'></i> Download </a>";
                                                    } ?>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="">BAPP :</label>
                                                <div>
                                                    <input type="file" id="file_bapp" name="file_bapp" value="" class="form-control col-md-9 float-left">
                                                    <?php if ($tagihan_detail["file_bapp"]) {
                                                        echo "<a class='btn btn-success col-md-3 float-right' href=" . base_url("tagihan/download/$tagihan_detail[id]/file_bapp") . "> <i class='fa fa-download'></i> Download </a>";
                                                    } ?>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="">BAST :</label>
                                                <div>
                                                    <input type="file" id="file_bast" name="file_bast" value="" class="form-control col-md-9 float-left">
                                                    <?php if ($tagihan_detail["file_bast"]) {
                                                        echo "<a class='btn btn-success col-md-3 float-right' href=" . base_url("tagihan/download/$tagihan_detail[id]/file_bast") . "> <i class='fa fa-download'></i> Download </a>";
                                                    } ?>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="">Laporan :</label>
                                                <div>
                                                    <input type="file" name="file_laporan" id="file_laporan" value="" class="form-control col-md-9 float-left">
                                                    <?php if ($tagihan_detail["file_laporan"]) {
                                                        echo "<a class='btn btn-success col-md-3 float-right' href=" . base_url("tagihan/download/$tagihan_detail[id]/file_laporan") . "> <i class='fa fa-download'></i> Download </a>";
                                                    } ?>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="">Foto Kegiatan :</label>
                                                <div>
                                                    <input type="file" id="file_foto_kegiatan" name="file_foto_kegiatan" value="" class="form-control col-md-9 float-left">
                                                    <?php if ($tagihan_detail["file_foto_kegiatan"]) {
                                                        echo "<a class='btn btn-success col-md-3 float-right' href=" . base_url("tagihan/download/$tagihan_detail[id]/file_foto_kegiatan") . "> <i class='fa fa-download'></i> Download </a>";
                                                    } ?>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if ($group_id == 5 || $group_id == 1) { ?>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="">File SPM :</label>
                                                    <div>
                                                        <input type="file" id="file_spm" name="file_spm" value="" class="form-control col-md-6 float-left">
                                                        <?php if ($tagihan_detail["file_spm"]) {
                                                            echo "<a class='btn btn-success col-md-4 float-right' href=" . base_url("tagihan/download/$tagihan_detail[id]/file_spm") . "> <i class='fa fa-download'></i> Download File SPM</a>";
                                                        } ?>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                            <button class="btn btn-light">Batal</button>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <br>
        <br>
        <?php if ($this->uri->segment(2) == "detail_tagihan" && !empty($this->uri->segment(3))) { ?>
            <div class="row">
                <div class="col-lg-12 col-xl-12 stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex justify-content-between align-items-baseline mb-2">
                                <h6 class="card-title mb-0">Lampiran Lainya</h6>
                            </div>
                            <br>

                            <a style="color:#fff; margin-bottom:30px; float:right;" id="btn-tambah-lampiran" class="btn btn-primary">
                                <i class="link-icon" data-feather="plus"></i>&nbsp; Tambah Lampiran</a>

                            <div class="table-responsive">
                                <table id="lampiran-table" class="table table-hover mb-0">
                                    <thead>
                                        <tr>
                                            <th class="pt-0">No</th>
                                            <th class="pt-0">Nama Lampiran</th>
                                            <th class="pt-0">File Lampiran</th>
                                            <th class="pt-0"></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                            <!-- if belum ada lampiran -->
                            <!-- <div style="border:1px solid #ccc; border-radius:6px; margin-top:10px;">
                            <h2 style="color:#ccc; font-size:19px; text-align:center; padding:20px;">Belum Ada Lampiran</h2>
                        </div> -->
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <br>
        <br>

        <?php if ($group_id == 5) { ?>
            <!-- <a style="float: right; width:20%; color:#fff;" data-toggle="modal" data-target="#uploadSpm" class="btn btn-success mr-2"> <i style="font-size:12px;" class="link-icon" data-feather="upload"></i>&nbsp; Upload SPM</a> -->
            <a style="float: right; width:20%; color:#fff;" data-toggle="modal" data-target="#modalRejectTagihan" class="btn btn-danger mr-2"> <i style="font-size:12px;" class="link-icon" data-feather="x"></i>&nbsp; Reject</a>

        <?php } else if ($group_id == 3 || $group_id == 4) { ?>
            <a style="float: right; width:20%; color:#fff;" onclick="verifikasi_tagihan(<?= $tagihan_detail['id'] ?>)" class="btn btn-success mr-2"> <i style="font-size:12px;" class="link-icon" data-feather="check"></i>&nbsp; Verifikasi Bendahara</a>
            <a style="float: right; width:20%; color:#fff;" data-toggle="modal" data-target="#modalRejectTagihan" class="btn btn-danger mr-2"> <i style="font-size:12px;" class="link-icon" data-feather="x"></i>&nbsp; Reject</a>


        <?php } else if ($group_id == 1) { ?>
            <!-- <a style="float: right; width:20%; color:#fff;" data-toggle="modal" data-target="#uploadSpm" class="btn btn-success mr-2"> <i style="font-size:12px;" class="link-icon" data-feather="upload"></i>&nbsp; Upload SPM</a> -->
            <a style="float: right; width:20%; color:#fff;" onclick="verifikasi_tagihan(<?= $tagihan_detail['id'] ?>)" class="btn btn-success mr-2"> <i style="font-size:12px;" class="link-icon" data-feather="check"></i>&nbsp; Verifikasi Bendahara</a>
            <a style="float: right; width:20%; color:#fff;" data-toggle="modal" data-target="#modalRejectTagihan" class="btn btn-danger mr-2"> <i style="font-size:12px;" class="link-icon" data-feather="x"></i>&nbsp; Reject</a>
            <button style="float: right; width:20%" type="submit" class="btn btn-primary mr-2"> <i style="font-size:12px;" class="link-icon" data-feather="save"></i>&nbsp; Simpan Tagihan</button>
        <?php } else { ?>
            <button style="float: right; width:20%" type="submit" class="btn btn-primary mr-2"> <i style="font-size:12px;" class="link-icon" data-feather="save"></i>&nbsp; Simpan Tagihan</button>
        <?php }  ?>


    </form>


</div>

<!--modal form tambah lampiran -->
<?php $this->load->view("template/pages/tagihan/modal_lampiran") ?>
<!--modal form upload SP2D -->
<?php $this->load->view("template/pages/tagihan/modal_spm") ?>
<!--modal reject tagihan -->
<?php $this->load->view("template/pages/tagihan/modal_reject_tagihan") ?>