<div class="modal fade" id="lampiranModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Lampiran</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-lampiran">
                <div class="modal-body">
                   
                        <div class="row">
                            <div class="col-lg-12">
                                <input type="hidden" id="lampiran_id_tagihan" name="id_tagihan" value="" >
                                <input type="hidden" id="id_lampiran" name="id_lampiran" value="">
                                <input type="hidden" id="no_tagihan" name="no_tagihan" value="">
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Nama Lampiran :</label>
                                    <input type="text" id="file_name" name="file_name" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputUsername1">File Lampiran :</label>
                                    <input type="file" id="file" name="file" class="form-control">
                                </div>

                            </div>

                        </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button id="lampiran-btn" class="float-right btn btn-primary success" type="submit">Submit Lampiran</button>
                </div>
            </form>
        </div>
    </div>
</div>