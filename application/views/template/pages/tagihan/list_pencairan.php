<div class="page-content">
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Kelola Anggaran</li>
            <li class="breadcrumb-item active" aria-current="page">Daftar Pencairan</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <form method="get" action="">
                <div class="card">
                    <div class="card-body">
                        <h6 style="font-size:15px !important; color: #241373 !important;">Filter Tagihan By</h6>
                        <br>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="">Status Tagihan :</label>
                                    <select class="js-example-basic-single w-100" name="status" id="filter_status_tagihan" class="form-control mb-3">
                                        <option selected="" value="">Semua Status</option>
                                        <option value="verifikasi bendahara" <?= $_GET["filter_status_tagihan"] == "verifikasi bendahara" ? "selected" : "" ?>>Verifikasi Bendahara</option>
                                        <option value="terbit spm" <?= $_GET["filter_status_tagihan"] == "terbit spm" ? "selected" : "" ?>>Terbit Spm</option>
                                        <option value="upload" <?= $_GET["filter_status_tagihan"] == "upload" ? "selected" : "" ?>>Upload</option>
                                        <option value="rejected" <?= $_GET["filter_status_tagihan"] == "rejected" ? "selected" : "" ?>>Rejected</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group">
                                    <label for="">Jenis Kontrak :</label>
                                    <select class="js-example-basic-single w-100" name="jenis_tagihan" class="form-control" id="filter_jenis_tagihan">
                                        <option value="">Semua Jenis</option>
                                        <option value="kontrak" <?= $_GET["filter_jenis_tagihan"] == "kontrak" ? "selected" : "" ?>>
                                            Kontraktual
                                        </option>
                                        <option value="non_kontrak" <?= $_GET["filter_jenis_tagihan"] == "non_kontrak" ? "selected" : "" ?>>
                                            Non Kontraktrual
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group">
                                    <label for="">Subdit :</label>
                                    <select class="js-example-basic-single w-100" name="subdit" class="form-control" id="filter_subdit">
                                        <option value="">Semua Subdit</option>
                                        <option value="Subdit 1" <?= $_GET["filter_subdit"] == "Subdit 1" ? "selected" : "" ?>> Subdit 1</option>
                                        <option value="Subdit 2" <?= $_GET["filter_subdit"] == "Subdit 2" ? "selected" : "" ?>> Subdit 2</option>
                                        <option value="Subdit 3" <?= $_GET["filter_subdit"] == "Subdit 3" ? "selected" : "" ?>> Subdit 3</option>
                                        <option value="Subdit 4" <?= $_GET["filter_subdit"] == "Subdit 4" ? "selected" : "" ?>> Subdit 4</option>
                                        <option value="Subdit 5" <?= $_GET["filter_subdit"] == "Subdit 5" ? "selected" : "" ?>> Subdit 5</option>
                                        <option value="Subbag TU" <?= $_GET["filter_subdit"] == "Subdit TU" ? "selected" : "" ?>> Subbag TU</option>
                                    </select>
                                </div>
                            </div>

                            <?php if ($this->session->userdata("group_id") != 2) { ?>

                                <div class="col">
                                    <div class="form-group">
                                        <label for="">Nama PPK :</label>
                                        <select class="js-example-basic-single w-100" data-width="100%" id="filter_nama_ppk" name="nama_ppk">
                                            <option value="">Semua PPK</option>
                                            <?php foreach ($all_user as $rowuser => $value) {

                                                $selected = "";
                                                if ($value["id"] == $_GET["filter_nama_ppk"]) {
                                                    $selected = "selected";
                                                }

                                            ?>
                                                <option value="<?= $value['id'] ?>" <?= $selected ?>><?= $value['fullname'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            <?php } ?>

                            <div class="col">
                                <div class="form-group">
                                    <label for="">Tanggal Tagihan :</label>
                                    <input type="date" name="tanggal_tagihan" id="filter_tanggal_tagihan" class="form-control" value="<?= $_GET["filter_tanggal_tagihan"] ?>">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group float-right">
                                    <button type="button" id="filter-tagihan-btn" style="display: block !important; width:100%; height:35px; background-color:#8a3cc1; border-color:#8a3cc1;" class="btn btn-primary"> <i class="link-icon" data-feather="filter"></i>&nbsp; Filter</button>
                                </div>

                                <div class="form-group float-right mr-2">
                                    <button type="button" id="clear-tagihan-btn" style="display: block !important; width:100%; height:35px" class="btn btn-secondary">Clear</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
        </div>

    </div>
    <br>

    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline mb-2">
                        <h6 class="card-title mb-0">Daftar Pencairan</h6>
                        <a style="color:#fff;" href="<?= base_url('tagihan/pengajuan_tagihan') ?>" class="btn btn-primary"> <i class="link-icon" data-feather="plus"></i>&nbsp; Pengajuan Tagihan</a>
                    </div>
                    <br>
                    <br>
                    <div class="table-responsive">
                        <table id="table-tagihan" class="table table-hover mb-0">
                            <thead>
                                <tr>
                                    <th class="pt-0">#</th>
                                    <th class="pt-0">Status</th>
                                    <th class="pt-0">Nomor SPTB</th>
                                    <th class="pt-0">Tanggal Tagihan</th>
                                    <th class="pt-0">Uraian</th>
                                    <th class="pt-0">Nilai Tagihan</th>
                                    <th class="pt-0">Jenis Kontrak</th>
                                    <th class="pt-0">Jenis Pembayaran</th>
                                    <th class="pt-0">PPN</th>
                                    <th class="pt-0">PPH</th>
                                    <th class="pt-0">Potongan PPH</th>

                                    <th class="pt-0">PPK</th>
                                    <th class="pt-0">Subdit</th>
                                    <th class="pt-0">Penyedia</th>
                                    <th class="pt-0">Kegiatan Utama</th>
                                    <th class="pt-0">Detail Kegiatan</th>
                                    <th class="pt-0">Kode MAK</th>
                                    <th class="pt-0">Uraian MAK</th>
                                    <th class="pt-0">Aksi</th>

                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>