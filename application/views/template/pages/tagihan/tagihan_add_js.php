<script src="<?= base_url("assets/vendors/sweetalert2/sweetalert2.min.js") ?>"></script>
<script src="<?= base_url("assets/vendors/select2/select2.min.js") ?>"></script>
<script src="<?= base_url("assets/js/select2.js") ?>"></script>

<script>
    function get_kegiatan_detail_list(id_kegiatan) {
        $.ajax({
            "type": "POST",
            "url": "<?= base_url("kegiatan/get_kegiatan_detail_list") ?>",
            "dataType": 'json',
            data: {
                id_kegiatan: id_kegiatan
            },
            success: function(res) {

                $("select#id_detail_kegiatan").html("")

                $("select#id_detail_kegiatan").append(`<option value="">-- Pilih Detail Kegiatan --</option>`);
                var detail_kegiatan_list = $.each(res.data, function(index, item) {
                    $("select#id_detail_kegiatan").append(`<option value=${item.id}>${item.kode_mak} - ${item.detail_kegiatan}</option>`);
                })


            }

        })
    }

    function get_detail_kegiatan(id_detail_kegiatan) {

        $.ajax({
            "type": "POST",
            "url": "<?= base_url("kegiatan/get_kegiatan_detail") ?>",
            "dataType": 'json',
            data: {
                id: id_detail_kegiatan
            },
            success: function(res) {

                var xk = `Rp. ` + parseInt(res.data.pagu_kegiatan).toLocaleString()

                $("#real_id_kontrak").val("")
                $("#real_id_detail_kegiatan").val(res.data.id)

                $("#id_user").val(res.data.id_user)
                $("select#id_kegiatan").val(res.data.id_kegiatan)
                // $("#uraian_mak").val(res.data.uraian_mak)
                $("#pagu_kegiatan").val(xk)
                $("#detail_kegiatan").val(res.data.detail_kegiatan)
                $("#uraian_mak").val(res.data.uraian_mak)
                $("#id_kontrak").val(res.data.id_kontrak)
            }

        })
    }

    function get_detail_kontrak(id_kontrak) {
        $.ajax({
            "type": "POST",
            "url": "<?= base_url("kontrak/get_detail_kontrak") ?>",
            "dataType": 'json',
            data: {
                id_kontrak: id_kontrak
            },
            success: function(res) {
                $("#real_id_kontrak").val(res.data.id)
                $("#real_id_detail_kegiatan").val(res.data.id_detail_kegiatan)

                $("#id_user").val(res.data.id_user)

                $("#tanggal_kontrak").val(res.data.tanggal_kontrak)
                $("#alamat_kontraktor").val(res.data.alamat_kontraktor)
                $("#nama_kontraktor").val(res.data.nama_kontraktor)
                $("#npwp_kontraktor").val(res.data.npwp_kontraktor)
                $("#rekening_kontraktor").val(res.data.rekening_kontraktor)
                $("#nama_rekening").val(res.data.nama_rekening)
            }

        })
    }

    function tagihan_add() {

        var form_tambah_tagihan = $("form#form-tagihan")[0];
        const data = new FormData(form_tambah_tagihan)

        $.ajax({
            url: "<?= base_url("tagihan/add_tagihan") ?>",
            enctype: "multipart/form-data",
            type: "post",
            data: data,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function(res) {
                if (res.success === true) {

                    Swal.fire(
                        'Success!',
                        res.message,
                        'success'
                    )

                    setTimeout(() => {
                        location.href = "<?= base_url("tagihan/detail_tagihan/") ?>" + res.last_id
                    }, 3000);

                } else {
                    Swal.fire({
                        title: '<strong> Error !</strong>',
                        icon: 'error',
                        html: res.message
                    })
                }
            }
        })
    }

    function showKontrak() {

        var colKontrak = document.getElementById("colKontrak");
        var colNonKontrak = document.getElementById("colNonKontrak");

        if (colKontrak.style.display === "none") {
            colKontrak.style.display = "block";
            colNonKontrak.style.display = "none";
        }
    }

    function showNonKontrak() {

        var colKontrak = document.getElementById("colKontrak");
        var colNonKontrak = document.getElementById("colNonKontrak");

        if (colNonKontrak.style.display === "none") {
            colNonKontrak.style.display = "block";
            colKontrak.style.display = "none";
        }
    }

    $(document).ready(function(e) {
        var colKontrak = document.getElementById("colKontrak");
        var colNonKontrak = document.getElementById("colNonKontrak");
        colKontrak.style.display = 'none';
        colNonKontrak.style.display = 'none';

        // if ($(".select2").length) {
        //     $(".select2").select2();
        // }  
    })

    $(function() {

        $("form#form-tagihan").submit(function(e) {
            tagihan_add()
            e.preventDefault()
        })

        $(".money").autoNumeric("init", {
            aForm: true,
            vMax: "999999999999999"
        });

    })

    $("select#id_kegiatan").change(function(e) {
        get_kegiatan_detail_list($("#id_kegiatan").val())
    })

    $("select#id_detail_kegiatan").change(function(e) {
        get_detail_kegiatan($("#id_detail_kegiatan").val())
    })

    $("select#id_kontrak").change(function(e) {
        get_detail_kontrak($("#id_kontrak").val())
    })

    $(document).on('click', '#non-kontrak', function(e) {
        get_detail_kegiatan($("#id_detail_kegiatan").val())
        showNonKontrak()

    })

    $(document).on('click', '#kontrak', function(e) {
        get_detail_kontrak($("#id_kontrak").val())
        showKontrak()

    })
</script>