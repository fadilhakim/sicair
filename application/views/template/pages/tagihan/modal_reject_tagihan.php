<div class="modal fade" id="modalRejectTagihan" tabindex="-1" role="dialog" aria-labelledby="modalRejectTagihan" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Reject Tagihan?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-reject-tagihan">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-lg-12">

                            <div class="form-group">
                                <input style="display: none;" type="text" value='<?= $tagihan_detail["id"] ?>' id="id_tagihan" name="id_tagihan" class="form-control">
                            </div>


                            <div class="form-group">
                                <label for="exampleInputUsername1">Alasan Reject:</label>
                                <textarea class="form-control" name="reject_note" id="reject_note" cols="30" rows="10"></textarea>
                            </div>

                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <a style="color:#fff;" onclick="reject_tagihan(<?= $tagihan_detail['id'] ?>)" class="float-right btn btn-danger">Reject</a>
                </div>
            </form>
        </div>
    </div>
</div>