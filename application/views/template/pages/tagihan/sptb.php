<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document RESUME</title>
</head><body>
  <?php
  $month = date('m');
  $romanM = '';
  if($month == 01){
    $romanM = 'I';
  }
  else if($month == 02){
    $romanM = 'II';
  }
  else if($month == 03){
    $romanM = 'III';
  }
  else if($month == 04){
    $romanM = 'IV';
  }
  else if($month == 05){
    $romanM = 'V';
  }
  else if($month == 06){
    $romanM = 'VI';
  }
  else if($month == 07){
    $romanM = 'VII';
  }
  else if($month == 8){
    $romanM = 'VIII';
  }
  else if($month == 9){
    $romanM = 'IX';
  }else if($month == 10){
    $romanM = 'X';
  }else if($month == 11){
    $romanM = 'XI';
  }else{
    $romanM = 'XII';
  }

  ?>
    <table style="font-size:14px; font-family: Arial, Helvetica, sans-serif; width:100%;">
        <tr>
          <td style="text-align:center; font-weight:bold;">
            SURAT PERNYATAAN TANGGUNG JAWAB BELANJA
          </td>
        </tr>
        <tr>
          <td style="text-align:center; line-height: 30px; font-weight:bold; border-bottom:1px solid #000;">
            Nomor : <?= $nomor ?>/PPK/SPTB/<?= $romanM ?>/<?= date('Y') ?>
            <!-- {ID}/PPK/SPTB/bulanRomawi/2021 -->
          </td>
        </tr>
    </table>
    <br><br><br>
    <table style="font-size:14px; font-family: Arial, Helvetica, sans-serif;">
      <tr>
        <td style="font-weight:bold;">Kode Satuan Kerja</td>
        <td>:</td>
        <td><?= $sptb['kode_satker'] ?></td>
      </tr>

      <tr>
        <td style="font-weight:bold;">Nama Satuan Kerja</td>
        <td>:</td>
        <td><?= $sptb['nama_satker'] ?></td>
      </tr>

      <tr>
        <td style="font-weight:bold;">No. DIPA / Tanggal</td>
        <td>:</td>
        <td><?=$sptb["nomor_dipa"]?> / <?=$sptb["tanggal_dipa"]?></td>
      </tr>

      <tr>
        <td style="font-weight:bold;">Klasifikasi Anggaran</td>
        <td>:</td>
        <td><?=$sptb["kode_aktivitas"]?>/<?=$sptb["kro"]?>/<?=$sptb["ro"]?>/<?=$sptb["komponen"]?>/<?=$sptb["subkomponen"]?>- <?=$sptb["kode_mak"]?>  <?=$sptb["nama_kegiatan"]?></td>
      </tr>
    </table>
    <br><br>
    <table style="font-family: Arial, Helvetica, sans-serif; text-align:justify; font-size:14px;">
      <tr>
        <td style="line-height:23px;">Yang bertandatangan dibawah ini atas nama Kuasa Pengguna Anggaran Satuan Kerja Peningkatan Keselamatan Lalu Lintas Angkutan Laut Pusat, menyatakan bahwa saya bertanggung jawab secara formal dan material dan kebenaran perhitungan pemungutan pajak atas segala pembayaran tagihan yang kami perintahkan dalam SPM ini dengan perincian sebagai berikut :</td>
      </tr>
    </table>
    <br><br>
    <table style="font-size:14px; font-family: Arial, Helvetica, sans-serif; text-align:justify; width:100%; border:1px solid #000">
      <tr style="font-weight:bold; border-right:1px solid #000;">
        <td style="border-bottom:1px solid #000; border-right:1px solid #000;" rowspan="2">No</td>
        <td style="border-bottom:1px solid #000; border-right:1px solid #000;" rowspan="2">MAK</td>
        <td style="border-bottom:1px solid #000; border-right:1px solid #000;" rowspan="2">Penerima</td>
        <td style="border-bottom:1px solid #000; border-right:1px solid #000;" rowspan="2">Uraian</td>
        <td style="border-bottom:1px solid #000; border-right:1px solid #000;" rowspan="2">Jumlah (Rp)</td>
        <td  style="border-bottom:1px solid #000;border-left:1px solid #000; text-align:center;"colspan="2">Pajak Yang Dipungut</td>
      </tr>
      <tr>
        <td style="border-bottom:1px solid #000; border-left:1px solid #000; text-align:center;">PPN</td>
        <td style="border-bottom:1px solid #000; border-left:1px solid #000; text-align:center;">PPH 23</td>
      </tr>
      <tr>
        <td style="line-height:30px; text-align:center; padding-top:20px; border-right:1px solid #000;">1</td>
        <td style="border-right:1px solid #000;"><?=$sptb["kode_mak"]?></td>
        <?php if(!empty($sptb["nama_kontraktor"])) {?>
        <td style="border-right:1px solid #000;"><br> <?=$sptb["nama_kontraktor"]?> <br><br> <?=$sptb["alamat_kontraktor"]?> </td>
        <?php }else {?>
        <td style="border-right:1px solid #000;"><br> <?=$sptb["nama_penerima"]?></td>
        <?php } ?>
        <td style="border-right:1px solid #000;"><?= $sptb["uraian_tagihan"] ?></td>
        <td style="border-right:1px solid #000;">Rp. <?= number_format($sptb["nilai_tagihan"],2,",","."); ?></td>
        <td style="border-right:1px solid #000;">Rp. <?= number_format($sptb["potongan_pajak_ppn"],2,",",".");?></td>
        <td>Rp. <?= number_format($sptb["potongan_pajak_pph"],2,",",".");?></td>
      </tr>
      <tr>
        <td style="border-top:2px solid #000; font-weight:bold; text-align:center;" colspan="4">JUMLAH TOTAL</td>
        <td style="border-top:2px solid #000; font-weight:bold">Rp. <?= number_format($sptb["nilai_tagihan"],2,",",".");?></td>
        <td style="border-top:2px solid #000; font-weight:bold; text-align:center;" colspan="2">
          <?php $sumPajak = $sptb["potongan_pajak_ppn"] + $sptb["potongan_pajak_pph"] ;  ?>
          Rp. <?= number_format($sumPajak,2,",",".") ?>
        </td>

      </tr>
    </table>
    <br>
    <br>
    <table style="font-family: Arial, Helvetica, sans-serif; text-align:justify; font-size:14px;">
      <tr>
        <td style="line-height:23px;">
          Bukti-bukti pengeluran anggaran dan asli setoran pajak (SSP/BPN) tersebut diatas disimpan oleh Pengguna Anggaran/Kuasa Pengguna Anggaran untuk Kelengkapan administrasi dan pemeriksaan aparat pengawasan fungsional.
        </td>
      </tr>
    </table>
    <br>
    <table style="font-family: Arial, Helvetica, sans-serif; text-align:justify; font-size:14px;">
      <tr>
        <td>Demikian Surat Pernyataan ini dibuat dengan sebenarnya.</td>
      </tr>
    </table>

    <br>
    <br>
    <br><br><br><br><br><br>
    <table style="width:100%; font-family: Arial, Helvetica, sans-serif;">
      <tr>
        <td style="width:30%">&nbsp;</td>
        <td style="width:30%">&nbsp;</td>
        <td style="text-align:center; width:40%">
          <span style="font-size:13.5px">Jakarta, <? $d = new DateTime($sptb['created_at']); echo $d->format('d-m-Y'); ?><br></span>
          <strong style="font-size:14px">A.N. KUASA PENGGUNA ANGGARAN</strong><br>
          <strong style="font-size:14px">PEJABAT PEMBUAT KOMITMEN</strong>

          <br><br><br><br>
            <strong style="font-size:14px; border-bottom:0.5px solid #000; padding-bottom:5px"><?= strtoupper($sptb['fullname'])  ?></strong> <br>
            <strong style="font-size:14px">NIP :<?= $sptb['nip'] ?></strong>
        </td>
      </tr>
    </table>

</body></html>
