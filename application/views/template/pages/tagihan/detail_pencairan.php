<div class="page-content">
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Kelola Anggaran</li>
            <li class="breadcrumb-item active" aria-current="page">Pencairan Anggaran</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">

                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline mb-2">
                        <h6 class="card-title mb-0">Kelola Anggaran</h6>
                    </div>
                    <br>

                    <form class="forms-sample">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="">Tahun Anggaran</label>
                                    <input readonly disabled type="text" value="2021" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputUsername1">Tanggal</label>
                                    <input type="date" readonly disabled value="21/02/2021" class="form-control">
                                </div>

                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Nomor DIPA</label>
                                    <input type="text" value="DPA/02/09/2021" readonly disabled class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputUsername1">Pagu Anggaran</label>
                                    <input type="text" value="20.000.000" readonly disabled class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Total Anggaran</label>
                                    <input type="text" readonly disabled value="400.000.000" class="form-control">
                                </div>

                            </div>

                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline mb-2">
                        <h6 class="card-title mb-0">Daftar Tagihan</h6>
                        <a style="color:#fff;" href="<?= base_url('anggaran/pengajuan_tagihan') ?>" class="btn btn-primary"> <i class="link-icon" data-feather="plus"></i>&nbsp; Pengajuan Tagihan</a>
                    </div>
                    <br>
                    <br>
                    <div class="table-responsive">
                        <table class="table table-hover table-striped mb-0">
                            <thead>
                                <tr>
                                    <th class="pt-0">#</th>
                                    <th class="pt-0">Nomor SPTB</th>
                                    <th class="pt-0">Tanggal Tagihan</th>
                                    <th class="pt-0">Uraian</th>
                                    <th class="pt-0">Nilai Tagihan</th>
                                    <th class="pt-0">Status</th>
                                    <th class="pt-0">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td><strong> <a href=""> 4659/BAH/003</a></strong></td>
                                    <td style="text-align:center;">20/02/2021</td>
                                    <td>Pencaiaran dana bantuan angkutan udara ke timur indonesia</td>
                                    <td>200.000.000</td>
                                    <td>
                                        <span class="badge badge-success">
                                            Verifikasi Bendahara
                                        </span>
                                    </td>
                                    <td>
                                        <div class="btn-group dropdown">
                                            <button type="button" class="btn btn-primary" data-toggle="dropdown" aria-expanded="false">
                                                Aksi <span class="caret"><i class="fa fa-caret-down"></i></span>
                                            </button>
                                            <ul class="dropdown-menu animated fadeIn">
                                                <li style="display:inline-block;"><a data-toggle="modal" data-target="#exampleModal2" style="color:#fff;" class="btn btn-success">Cetak SPTB</a></li>
                                                <li style="display:inline-block;"><a href="<?= base_url('detail_anggaran/1') ?>" class="btn btn-warning">Edit</a></li>
                                                <li style="display:inline-block;"><a href="<?= base_url('hapus_anggaran/1') ?>" class="btn btn-danger">Hapus</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td><strong> <a href=""> 4658/BAH/004</a></strong></td>
                                    <td style="text-align:center;">20/02/2021</td>
                                    <td>Pencaiaran dana bantuan angkutan udara ke timur indonesia</td>
                                    <td>200.000.000</td>
                                    <td>
                                        <span class="badge badge-warning">
                                            TerbiT SPM
                                        </span>
                                    </td>
                                    <td>
                                        <div class="btn-group dropdown">
                                            <button type="button" class="btn btn-primary" data-toggle="dropdown" aria-expanded="false">
                                                Aksi <span class="caret"><i class="fa fa-caret-down"></i></span>
                                            </button>
                                            <ul class="dropdown-menu animated fadeIn">
                                                <li style="display:inline-block;"><a data-toggle="modal" data-target="#exampleModal2" style="color:#fff;" class="btn btn-success">Cetak SPTB</a></li>
                                                <li style="display:inline-block;"><a href="<?= base_url('detail_anggaran/1') ?>" class="btn btn-warning">Edit</a></li>
                                                <li style="display:inline-block;"><a href="<?= base_url('hapus_anggaran/1') ?>" class="btn btn-danger">Hapus</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--modal form tambah kegiatan -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Kegiatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="exampleInputUsername1">Kode Aktivitas :</label>
                            <input type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputUsername1">Kode Kro :</label>
                            <input type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputUsername1">Kode RO :</label>
                            <input type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputUsername1">Kode Komponen :</label>
                            <input type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputUsername1">Kode Sub Komponen :</label>
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="exampleInputUsername1">Pagu :</label>
                            <input type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputUsername1">Nama PPK :</label>
                            <div class="form-group">
                                <select class="js-example-basic-single w-100" data-width="100%">
                                    <option value="TX">Texas</option>
                                    <option value="NY">New York</option>
                                    <option value="FL">Florida</option>
                                    <option value="KN">Kansas</option>
                                    <option value="HW">Hawaii</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputUsername1">Nama Kegiatan : </label>
                            <textarea id="maxlength-textarea" class="form-control" maxlength="200" rows="8" placeholder="This textarea has a limit of 200 chars."></textarea>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>


<!-- modal form tambah detail -->

<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Detail </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="exampleInputUsername1">Kode MAK :</label>
                            <input type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputUsername1">Nama Detail Kegiatan :</label>
                            <input type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputUsername1">PAGU :</label>
                            <input type="text" class="form-control">
                        </div>

                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>