<div class="page-content">
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"> Group </li>
            <li class="breadcrumb-item active"> Group list </li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">

                    <h4 class="card-header-title">Group List</h4>
                    <div class="card-tools">
                        <a href="<?= base_url("group/add") ?>" class="btn btn-success float-right text-white">
                            <i data-feather="plus">&nbsp;</i>
                            Add Group
                        </a>

                    </div>
                </div>
                <div class="card-body">
                    <table id="group-table" class="table ">
                        <thead>
                            <th> ID</th>
                            <th> Nama Jabatan </th>
                            <th> Akses Menu </th>
                            <th> Action </th>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>