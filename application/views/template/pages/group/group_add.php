<div class="page-content">
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"> group </li>
            <li class="breadcrumb-item active"> group Add </li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-md-6">
            <form id="group-add-form">
                <div class="card">
                
                    <div class="card-header">
                    
                        <h4 class="card-header-title">Group Add</h4>
                        <div class="card-tools">
                        
                        
                        </div>
                    </div>
                    <div class="card-body">
                        
                        <div class="form-group">
                            <label for="groupname"> Group Name </label>
                            <input type="text" id="group_name" name="group_name" class="form-control">
                        </div>
					</div>
					<div class="card-footer">
                        <button type="submit" class="btn btn-success float-right">
                            <i data-feather="plus">&nbsp;</i>
                            Add Group
                        </button>    
                    </div>
                    
                </div>
            </form>
        </div>
    </div>
</div>
