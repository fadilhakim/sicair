<link rel="stylesheet" href="<?= base_url("assets/vendors/sweetalert2/sweetalert2.min.css") ?>">

<style>
    .auth-page .auth-left-wrapper {
        background-color: #241373;
        border-top-left-radius: 5px;
        border-bottom-left-radius: 5px;
        background-image: url('./assets/images/kemenhublogo.png');
        background-size: 65% !important;
        background-position: center center;
        background-repeat: no-repeat;
    }

    .auth-page .auth-left-wrapper h1 {
        text-align: center;
        font-size: 18px;
        position: absolute;
        bottom: 15px;
        display: block;
        width: 100%;
        color: #fff;
    }
</style>