<div class="page-content d-flex align-items-center justify-content-center">
    <div class="row w-100 mx-0 auth-page">
        <div class="col-md-9 col-xl-8 mx-auto">
            <div class="card">
                <div class="row">
                    <div class="col-md-4 pr-md-0">
                        <div class="auth-left-wrapper">
                            <h1>Kementerian Perhubungan <br> Republik Indonesia</h1>
                        </div>
                    </div>
                    <div class="col-md-8 pl-md-0">
                        <div class="auth-form-wrapper px-4 py-5">
                            <a href="#" style="color:#241373;" class="noble-ui-logo d-block mb-2">Aplikasi Pencairan Anggaran - <span style="color:#FFC414">SiCair</span></a>
                            <h5 class="text-muted font-weight-normal mb-4">Welcome back! Log in to your account.</h5>
                            <form class="forms-sample" id="login-form">
                                <div class="form-group">
                                    <label for="form-email-login">Email</label>
                                    <input type="email" name="email" class="form-control" id="form-email-login" placeholder="emaill">
                                </div>
                                <div class="form-group">
                                    <label for="form-password-login">Password</label>
                                    <input type="password" name="password" class="form-control" id="form-password-login" placeholder="password">
                                </div>
                                <div class="form-check form-check-flat form-check-primary">

                                </div>
                                <div class="mt-3">
                                    <button id="login-btn" type="submit" class="btn btn-primary mr-2 mb-2 mb-md-0 text-white">Login</button>

                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>