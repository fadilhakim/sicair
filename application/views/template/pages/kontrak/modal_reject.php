<!-- modal update status -->

<div class="modal fade" id="updateStatusReject" tabindex="-1" role="dialog" aria-labelledby="updateStatus" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Reject Kontrak : <input type="text" style="border: none; font-weight: bold; color: chocolate;" id="nomor_kontrak_span" readonly></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-update-status-reject" action="">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">

                            <div style="display: none;" class="form-group">
                                <label for="">Id Kontrak:</label>
                                <input id="kontrak_id_modal" type="text" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="">Alasan Reject :</label>
                                <textarea id="catatan_reject" value="" name="catatan_reject" id="" cols="30" rows="10" class="form-control"></textarea>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <a onclick="reject_kontrak()" type="submit" class="btn btn-danger">Submit Reject</a>
                </div>
            </form>
        </div>
    </div>
</div>