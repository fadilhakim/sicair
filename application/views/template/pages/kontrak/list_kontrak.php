<?php
$group_id = $this->session->userdata("group_id");
?>
<style>
  .badge-info {
    display: inline-block;
    margin: auto;
    font-size: 15px;
    text-align: center;
  }
</style>
<div class="page-content">
  <nav class="page-breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item">Kontrak</li>
      <li class="breadcrumb-item active" aria-current="page">Daftar Kontrak</li>
    </ol>
  </nav>

  <div class="row">
    <div class="col-lg-12 col-xl-12 stretch-card">
      <form method="get" action="">
        <div class="card">
          <div class="card-body">
            <h6 style="font-size:15px !important; color: #241373 !important;">Filter Kontrak By</h6>
            <br>
            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label for="">Status Kontrak :</label>
                  <select class="js-example-basic-single w-100" name="status" id="filter_status_kontrak" class="form-control mb-3">
                    <option selected="" value="">Semua Status</option>
                    <option value="Verifikasi Bendahara" <?= $_GET["filter_status_kontrak"] == "Verifikasi Bendahara" ? "selected" : "" ?>>Verifikasi Bendahara</option>
                    <option value="Terdaftar Belum di Verifikasi" <?= $_GET["filter_status_kontrak"] == "Terdaftar Belum di Verifikasi" ? "selected" : "" ?>>Terdaftar Belum di Verifikasi</option>
                    <option value="Rejected" <?= $_GET["filter_status_kontrak"] == "Rejected" ? "selected" : "" ?>>Rejected</option>
                  </select>
                </div>
              </div>

              <div class="col">
                <div class="form-group">
                  <label for="">Jenis Kontrak :</label>
                  <select class="js-example-basic-single w-100" name="jenis_kontrak" class="form-control" id="filter_jenis_kontrak">
                    <option value="">Semua Jenis</option>
                    <option value="1" <?= $_GET["filter_jenis_kontrak"] == "1" ? "selected" : "" ?>>
                      Single Year
                    </option>
                    <option value="2" <?= $_GET["filter_jenis_kontrak"] == "2" ? "selected" : "" ?>>
                      Multi Year
                    </option>
                  </select>
                </div>
              </div>

              <div class="col">
                <div class="form-group">
                  <label for="">Subdit :</label>
                  <select class="js-example-basic-single w-100" name="subdit" class="form-control" id="filter_subdit">
                    <option value="">Semua Subdit</option>
                    <option value="Subdit 1" <?= $_GET["filter_subdit"] == "Subdit 1" ? "selected" : "" ?>> Subdit 1</option>
                    <option value="Subdit 2" <?= $_GET["filter_subdit"] == "Subdit 2" ? "selected" : "" ?>> Subdit 2</option>
                    <option value="Subdit 3" <?= $_GET["filter_subdit"] == "Subdit 3" ? "selected" : "" ?>> Subdit 3</option>
                    <option value="Subdit 4" <?= $_GET["filter_subdit"] == "Subdit 4" ? "selected" : "" ?>> Subdit 4</option>
                    <option value="Subdit 5" <?= $_GET["filter_subdit"] == "Subdit 5" ? "selected" : "" ?>> Subdit 5</option>
                    <option value="Subbag TU" <?= $_GET["filter_subdit"] == "Subdit TU" ? "selected" : "" ?>> Subbag TU</option>
                  </select>
                </div>
              </div>

              <!-- <div class="col">
                <div class="form-group">
                  <label for="">Tanggal Mulai :</label>
                  <input type="date" name="tanggal_mulai" id="filter_tanggal_mulai" class="form-control" value="<?= $_GET["filter_tanggal_mulai"] ?>">
                </div>
              </div> -->
              <?php if ($this->session->userdata("group_id") != 2) { ?>

                <div class="col">
                  <div class="form-group">

                    <label for="">Nama PPK :</label>
                    <select class="js-example-basic-single w-100" data-width="100%" id="filter_nama_ppk" name="nama_ppk">
                      <option value="">Semua PPK</option>
                      <?php foreach ($all_user as $rowuser => $value) {

                        $selected = "";
                        if ($value["id"] == $_GET["filter_nama_ppk"]) {
                          $selected = "selected";
                        }

                      ?>
                        <option value="<?= $value['id'] ?>" <?= $selected ?>><?= $value['fullname'] ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

              <?php } ?>


              <!-- <div class="col">
                <div class="form-group">
                  <label for="">Tanggal Selesai :</label>
                  <input type="date" name="tanggal_selesai" id="filter_tanggal_selesai" class="form-control" value="<?= $_GET["filter_tanggal_selesai"] ?>">
                </div>
              </div> -->
              <div class="col">
                <div class="form-group">
                  <label for="">Tanggal Kontrak :</label>
                  <input type="date" name="tanggal_kontrak" id="filter_tanggal_kontrak" class="form-control" value="<?= $_GET["filter_tanggal_kontrak"] ?>">
                </div>
              </div>

            </div>
            <div class="row">
              <div class="col-lg-12">
                <div class="form-group float-right">
                  <button type="button" id="filter-kontrak-btn" style="display: block !important; width:100%; height:35px; background-color:#8a3cc1; border-color:#8a3cc1;" class="btn btn-primary"> <i class="link-icon" data-feather="filter"></i>&nbsp; Filter</button>
                </div>

                <div class="form-group float-right mr-2">
                  <button type="button" id="clear-kontrak-btn" style="display: block !important; width:100%; height:35px" class="btn btn-secondary">Clear</button>
                </div>
              </div>
            </div>

          </div>
        </div>
      </form>
    </div>

  </div>
  <br>

  <div class="row">
    <div class="col-lg-12 col-xl-12 stretch-card">
      <div class="card">

        <div class="card-body">
          <div class="d-flex justify-content-between align-items-baseline mb-2">
            <h6 class="card-title mb-0">Daftar Kontrak</h6>
            <?php if ($group_id != 3 && $group_id != 4 && $group_id != 5) { ?>
              <a style="color:#fff;" href="<?= base_url('tambah_kontrak') ?>" class="btn btn-primary">
                <i class="link-icon" data-feather="plus"></i>&nbsp; Tambah Kontrak</a>
            <?php } ?>
          </div>
          <br>
          <div class="table-responsive">
            <table ui-jp="dataTable" id="table-kontrak" class="table table-hover table-striped mb-0">
              <thead>
                <tr>
                  <th class="pt-0">#</th>
                  <th class="pt-0">Status</th>
                  <th class="pt-0">Nomor Kontrak</th>
                  <th class="pt-0">Tanggal Kontrak</th>
                  <th class="pt-0">Tanggal Addendum</th>
                  <th class="pt-0">Nomor Addendum</th>
                  <th class="pt-0">Uraian Kontrak</th>
                  <th class="pt-0">Masa Kontrak</th>
                  <th class="pt-0">Nilai Kontrak</th>
                  <th class="pt-0">Jenis Kontrak</th>
                  <th class="pt-0">Penyedia</th>
                  <th class="pt-0">Subdit</th>
                  <th class="pt-0">Nama PPK</th>
                  <th class="pt-0">Kode Mak</th>
                  <th class="pt-0">Uraian Mak</th>
                  <th class="pt-0">Kegiatan Utama</th>
                  <th class="pt-0">Detail Kegiatan</th>

                  <th class="pt-0">Aksi</th>
                </tr>
              </thead>
              <tbody>
                <!-- <tr>
                  <td>1</td>
                  <td>
                    <div class="badge badge-info">2021</div>
                  </td>
                  <td>
                    <a href="<?= base_url('detail_kontrak/1') ?>">
                      <strong>
                        019/920/1001
                      </strong>
                    </a>
                  </td>
                  <td>01/02/2021</td>
                  <td>20.000.000</td>
                  <td>
                    <a class="btn btn-primary" href="<?= base_url('detail_kontrak/1') ?>">Detail</a>
                  </td>
                </tr> -->
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view("template/pages/kontrak/modal_addendum") ?>
<?php $this->load->view("template/pages/kontrak/modal_status") ?>