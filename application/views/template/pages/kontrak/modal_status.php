<!-- modal update status -->

<div class="modal fade" id="updateStatus" tabindex="-1" role="dialog" aria-labelledby="updateStatus" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Verifikasi Kontrak : <span id="nomor_kontrak_span">{nomor_kontrak}</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-update-status" action="">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="form-group" style="display:none;">
                                <label for="">Id Kontrak:</label>
                                <input id="id_kontrak" name="id_kontrak" type="text" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="">Nomor Kontrak:</label>
                                <input readonly id="no_kontrak" value="" name="no_kontrak" type="text" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="">Verifikasi:</label>
                                <select class="form-control" name="status_kontrak" id="status_kontrak">
                                    <option value="">-- Pilih Status Kontrak--</option>
                                    <option value="Verifikasi Bendahara">Verifikasi Bendahara</option>
                                    <option value="Terdaftar Belum di Verifikasi">Terdaftar Belum di Verifikasi</option>
                                    <option value="Rejected">Rejected</option>
                                </select>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button id="verifikasi-btn" type="submit" class="btn btn-success">Verifikasi</button>
                </div>
            </form>
        </div>
    </div>
</div>