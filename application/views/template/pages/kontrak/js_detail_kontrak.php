<script src="<?= base_url("assets/vendors/sweetalert2/sweetalert2.min.js") ?>"></script>
<script src="<?= base_url("assets/vendors/datatables.net/jquery.dataTables.js") ?>"></script>
<script src="<?= base_url("assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js") ?>"></script>

<script>
    function showDetailKontrak(id_kontrak) {

        return new Promise((resolve, reject) => {
            $.ajax({
                method: "post",
                url: "<?= base_url("kontrak/get_detail_kontrak") ?>",
                dataType: "json",
                data: {
                    id_kontrak: id_kontrak
                },
                success: function(res) {
                    resolve(res)
                },
                error: function(error) {
                    reject(error)
                },
            })
        })
    }

    function update_kontrak() {

        var form_tambah_kontrak = $("form#form-kontrak")[0];
        const data = new FormData(form_tambah_kontrak)

        $.ajax({
            url: "<?= base_url("kontrak/update_kontrak") ?>",
            enctype: "multipart/form-data",
            type: "post",
            data: data,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function(res) {
                if (res.success === true) {

                    //$('#kontrak-table').DataTable().ajax.reload();
                    // $("#kontrak-modal").modal('hide');

                    Swal.fire(
                        'Success!',
                        res.message,
                        'success'
                    )

                    setTimeout(() => {
                        window.location.reload()
                    }, 2000);
                } else {
                    Swal.fire({
                        title: '<strong> Error !</strong>',
                        icon: 'error',
                        html: res.message
                    })
                }
            }
        })
    }

    function tambah_addendum() {

        var form_tambah_addendum = $("form#form-addendum")[0];
        const data = new FormData(form_tambah_addendum)

        $.ajax({
            url: "<?= base_url("kontrak/update_addendum") ?>",
            enctype: "multipart/form-data",
            type: "post",
            data: data,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function(res) {
                if (res.success === true) {

                    //$('#kontrak-table').DataTable().ajax.reload();
                    // $("#kontrak-modal").modal('hide');

                    Swal.fire(
                        'Success!',
                        res.message,
                        'success'
                    )

                    setTimeout(() => {
                        location.reload()
                    }, 2000);

                } else {
                    Swal.fire({
                        title: '<strong> Error !</strong>',
                        icon: 'error',
                        html: res.message
                    })
                }
            },
            error: function(error) {
                console.log(error)
            },
        })
    }

    function showTermin() {
        document.getElementById("tambah-termin").style.display = "block";
    }

    function unshowTermin() {
        document.getElementById("tambah-termin").style.display = "none";
        document.getElementById("input-termin").innerHTML = ""
    }

    function get_detail_kegiatan(id_detail_kegiatan) {

        $.ajax({
            type: "post",
            url: "<?= base_url("kegiatan/get_kegiatan_detail") ?>",
            dataType: "json",
            data: {
                id: id_detail_kegiatan
            },
            success: function(res) {
                $("form#form-kontrak #id_user").val(res.data.id_user)
            }
        })

    }

    function get_kegiatan_detail_list(id_kegiatan) {
        $.ajax({
            "type": "POST",
            "url": "<?= base_url("kegiatan/get_kegiatan_detail_list") ?>",
            "dataType": 'json',
            data: {
                id_kegiatan: id_kegiatan
            },
            success: function(res) {

                $("select#id_detail_kegiatan").html("")

                $("select#id_detail_kegiatan").append(`<option value="">-- Pilih Detail Kegiatan --</option>`);
                var detail_kegiatan_list = $.each(res.data, function(index, item) {
                    $("select#id_detail_kegiatan").append(`<option value=${item.id}>${item.kode_mak} - ${item.detail_kegiatan}</option>`);
                })


            }

        })
    }

    function verifikasi_kontrak() {

        Swal.fire({
            title: "Update Status Kontrak ",
            text: "Apakah anda yakin verifikasi Kontrak ini?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'mr-2',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true,
            closeOnConfirm: true
        }).then((result) => {

            if (result.value) {

                $.ajax({
                    url: "<?= base_url("kontrak/update_status") ?>",
                    type: "POST",
                    data: {
                        id: $("#kontrak_id").val(),
                        // status: $("#updateStatus #status_kontrak").val()
                    },
                    dataType: "json",
                    success: function(res) {
                        if (res.success === true) {

                            // $('#table-kontrak').DataTable().ajax.reload();

                            Swal.fire(
                                'Success!',
                                res.message,
                                'success'
                            )

                            setTimeout(() => {
                                window.location.reload()
                            }, 2000);

                        } else {
                            Swal.fire({
                                title: '<strong> Error !</strong>',
                                icon: 'error',
                                html: res.message
                            })
                        }
                    }

                })
            }
        })
    }

    function showModalReject(id_kontrak) {
        showDetailKontrak(id_kontrak).then(res => {
                console.log('ini res id', res.data.id)
                $("#kontrak_id_modal").val(res.data.id)
                $("#nomor_kontrak_span").val(res.data.no_kontrak)
                $('#updateStatusReject').modal('show')
            })
            .catch(err => {
                console.log("ERR => ", err)
            })
    }

    function reject_kontrak() {

        Swal.fire({
            title: "Update Status Kontrak ",
            text: "Apakah anda yakin Reject Kontrak ini?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'mr-2',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true,
            closeOnConfirm: true
        }).then((result) => {

            if (result.value) {

                $.ajax({
                    url: "<?= base_url("kontrak/update_status_reject") ?>",
                    type: "POST",
                    data: {
                        id: $("#kontrak_id_modal").val(),
                        catatan_reject: $("#catatan_reject").val()
                    },
                    dataType: "json",
                    success: function(res) {
                        if (res.success === true) {

                            // $('#table-kontrak').DataTable().ajax.reload();

                            Swal.fire(
                                'Success!',
                                res.message,
                                'success'
                            )

                            setTimeout(() => {
                                window.location.reload();
                            }, 2000);

                        } else {
                            Swal.fire({
                                title: '<strong> Error !</strong>',
                                icon: 'error',
                                html: res.message
                            })
                        }
                    }

                })
            }
        })
    }

    function showDetailTermin(id_termin) {
        return new Promise((resolve, reject) => {
            $.ajax({
                method: "post",
                url: "<?= base_url("kontrak/detail_termin") ?>",
                dataType: "json",
                data: {
                    termin_id: id_termin
                },
                success: function(res) {
                    resolve(res)
                },
                error: function(error) {
                    reject(error)
                },
            })
        })
    }

    function clearModalTermin() {
        $("#terminModal #form-termin #id_termin").val("")
        $("#terminModal #form-termin #termin_phase").val("")
        $("#terminModal #form-termin #harga_termin").val("")

        $("#terminModal").modal("show")
    }

    function showModalTermin(id_termin) {

        showDetailTermin(id_termin).then(res => {

                console.log("ngeRES => ", res)

                $("#terminModal #form-termin #id_termin").val(res.data.id)
                $("#terminModal #form-termin #termin_phase").val(res.data.termin_phase)
                $("#terminModal #form-termin #harga_termin").val(res.data.harga_termin)

                $("#terminModal").modal("show")
            })
            .catch(err => {
                console.log(err)
            })
    }

    function submitTermin() {
        var id = $("form#form-termin #id_termin").val()

        if (id) {
            update_termin()
        } else {
            add_termin()
        }
    }

    function add_termin() {
        $.ajax({
            type: "POST",
            data: {
                id_kontrak: $("form#form-kontrak #kontrak_id").val(),
                termin_phase: $("form#form-termin #termin_phase").val(),
                harga_termin: $("form#form-termin #harga_termin").val()
            },
            url: "<?= base_url("kontrak/add_termin") ?>",
            dataType: "json",
            success: function(res) {
                if (res.success) {

                    $('#termin-table').DataTable().ajax.reload();
                    $("#terminModal").modal("hide")

                    Swal.fire(
                        'Success!',
                        res.message,
                        'success'
                    )
                } else {
                    Swal.fire({
                        title: '<strong> Error !</strong>',
                        icon: 'error',
                        html: res.message
                    })

                }
            }
        })
    }

    function update_termin() {
        //
        $.ajax({
            type: "POST",
            data: {
                id: $("form#form-termin #id_termin").val(),
                termin_phase: $("form#form-termin #termin_phase").val(),
                harga_termin: $("form#form-termin #harga_termin").val()
            },
            url: "<?= base_url("kontrak/update_termin") ?>",
            dataType: "json",
            success: function(res) {
                if (res.success) {

                    $('#termin-table').DataTable().ajax.reload();
                    $("#terminModal").modal("hide")


                    Swal.fire(
                        'Success!',
                        res.message,
                        'success'
                    )
                } else {
                    Swal.fire({
                        title: '<strong> Error !</strong>',
                        icon: 'error',
                        html: res.message
                    })

                }
            }
        })
    }

    function delete_termin(id, harga_termin) {

        Swal.fire({
            title: "Hapus Termin ",
            html: `<p>Apakah anda yakin ingin menghapus termin ini ?</p> <li><b>  ${harga_termin} </b></li> `,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'mr-2',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true,
            closeOnConfirm: true
        }).then((result) => {

            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "<?= base_url("kontrak/delete_termin") ?>",
                    data: {
                        id: id
                    },
                    dataType: "json",
                    success: function(res) {

                        $('#termin-table').DataTable().ajax.reload();

                        if (res.success) {
                            Swal.fire(
                                'Success!',
                                res.message,
                                'success'
                            )
                        } else {
                            Swal.fire(
                                'Error!',
                                res.message,
                                'error'
                            )
                        }

                    }
                })
            } else {
                Swal.fire({
                    title: '<strong> Error !</strong>',
                    icon: 'error',
                    html: res.message
                })
            }


        })
    }

    function get_all_termin(id_kontrak) {

        $("#termin-table").DataTable({
            order: [
                [0, 'asc']
            ],
            serverSide: true,
            searching: false,
            paging: false,
            info: false,
            ajax: {
                data: {
                    id_kontrak: id_kontrak
                },
                url: "<?= base_url("kontrak/get_all_termin") ?>",
                type: 'POST',
                dataType: "json"
            },
            columns: [

                {
                    data: "termin_phase",
                }, {
                    data: "harga_termin"
                }
            ],
            columnDefs: [{
                    "sorting": false,
                    "orderable": false,
                    "type": "html",
                    "targets": 1,
                    "render": function(data, type, row) {
                        return `Rp. ${row.harga_termin}`
                    }
                }, {
                    "sorting": false,
                    "orderable": false,
                    "type": "html",
                    "targets": 2,
                    "render": function(data, type, row) {
                        return `<button type="button" onclick="showModalTermin(${row.id})" class="btn btn-warning">Edit</button>
                      <button type="button" onclick="delete_termin(${row.id}, '${row.harga_termin}')" class="btn btn-danger">Delete</button>`
                    }
                },

            ]
        })
    }

    function get_all_termin2(id_kontrak) {

        $("#termin-table2").DataTable({
            order: [
                [0, 'asc']
            ],
            searching: false,
            paging: false,
            info: false,
            serverSide: true,
            ajax: {
                data: {
                    id_kontrak: id_kontrak
                },
                url: "<?= base_url("kontrak/get_all_termin") ?>",
                type: 'POST',
                dataType: "json"
            },
            columns: [

                {
                    data: "termin_phase",
                }, {
                    data: "harga_termin"
                }
            ],
            columnDefs: [{
                    "sorting": false,
                    "orderable": false,
                    "type": "html",
                    "targets": 1,
                    "render": function(data, type, row) {
                        return `Rp. ${row.harga_termin}`
                    }
                }

            ]
        })
    }

    $(document).ready(function() {
        get_detail_kegiatan($("select#id_detail_kegiatan").val())
        get_detail_kontrak(<?= $this->uri->segment(2) ?>)
    })

    $(function() {

        $(".money").autoNumeric("init", {
            aForm: true,
            vMax: "999999999999999"
        });

        $("form#form-kontrak").submit(function(e) {

            update_kontrak()

            e.preventDefault()
        })

        $("#btn-tambah-termin").click(function() {
            clearModalTermin()
        })

        $("#terminModal form#form-termin").submit(function(e) {

            submitTermin()
            e.preventDefault()
        })

        // get_detail_kontrak(<?= $this->uri->segment(2) ?>)
        get_all_termin(<?= $this->uri->segment(2) ?>)
        get_all_termin2(<?= $this->uri->segment(2) ?>)


        if ($(".select2").length) {
            $(".select2").select2();
        }

        if ($("input[name='cara_pembayaran']:checked").val() == "termin") {
            showTermin()
        } else {
            // alert(JSON.stringify($("input[name='cara_pembayaran']").attr("checked",true)))
            unshowTermin()
        }

        $('#addTmn').on('click', function() {

            var jumlah_termin = $("form #jumlah_termin").val()
            jumlah_termin = parseInt(jumlah_termin) + 1
            $("form #jumlah_termin").val(jumlah_termin)

            $('#input-termin').append(`<div class="form-group col-md-4">
                <label for="">Termin <span id="terminNumber"> ${jumlah_termin} </span> :</label>
                <input type="number" name="termin[]" value="0" class="form-control">
            </div>`)

        });

        $("#reject-btn").click(function(e) {
            console.log('masuk keisini')
            reject_kontrak()
            e.preventDefault()
        })

        $("form button#tambahAddendum").click(function(e) {
            tambah_addendum()
            e.preventDefault()
        })


    })

    $("select#id_detail_kegiatan").change(function(e) {

        get_detail_kegiatan($("select#id_detail_kegiatan").val())
    })

    $("select#id_kegiatan").change(function(e) {
        get_kegiatan_detail_list($("#id_kegiatan").val())
    })
</script>