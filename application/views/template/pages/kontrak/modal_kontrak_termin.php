<!-- modal update status -->

<div class="modal fade" id="terminModal" tabindex="-1" role="dialog" aria-labelledby="terminModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"> Termin Pembayaran</span></h5>

            </div>
            <form id="form-termin" action="">
                <input type="hidden" value="" id="id_termin" name="id_termin">

                <div class="modal-body">
                    <div class="form-group">
                        <label> Termin Fase : </label>
                        <input type="number" value="" id="termin_phase" name="termin_phase" class="form-control col-md-2">
                    </div>
                    <div class="form-group">
                        <label> Nilai / Jumlah Termin </label>
                        <input data-a-sign="Rp. " data-a-dec="," data-a-sep="." type="text" value="" id="harga_termin" name="harga_termin" class="form-control money">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button id="termin-btn" type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>