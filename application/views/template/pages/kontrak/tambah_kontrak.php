<?php
$group_id = $this->session->userdata("group_id");
$blocking = 'display:none;';
if ($dt_kontrak["status"] == 'Verifikasi Bendahara') {
  $disable = 'readonly';
  $blocking = 'display:block;';
}

?>
<style>
  .badge-info {
    display: inline-block;
    margin: auto;
    font-size: 15px;
    text-align: center;
  }

  .statusInfo {
    float: right;
    margin-right: 15px;
    margin-top: -23px;
  }

  .statusInfo p {
    font-weight: bold;
    font-size: 16px;
  }

  .statusInfo span {
    font-size: 16px;
  }
</style>
<div class="page-content">
  <nav class="page-breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?= base_url('kontrak') ?>">Kontrak</a></li>
      <li class="breadcrumb-item active" aria-current="page"><a href="<?= base_url('detail_kontrak/1') ?>">Detail Kontrak</a></li>
      <li class="breadcrumb-item active" aria-current="page"><?= $title ?></li>
    </ol>
    <div class="statusInfo">
      <?php
      $badge = '';
      if (!empty($dt_kontrak["status"])) {
        if ($dt_kontrak["status"] == 'Terdaftar Belum di Verifikasi') { ?>
          <p>Status Kontrak : <span class="badge badge-primary">Terdaftar Belum di Verifikasi</span></p>
        <?php } else if ($dt_kontrak["status"] == 'Verifikasi Bendahara') { ?>
          <p>Status Kontrak : <span class="badge badge-success">Verifikasi Bendahara</span></p>
        <?php } else {  ?>
          <p>Status Kontrak : <span class="badge badge-danger">Rejected</span></p>
      <?php }
      } ?>
    </div>
  </nav>
  <br>

  <?php if ($dt_kontrak["status"] == 'Rejected') { ?>
    <div class="row">
      <div class="col-lg-12">
        <div class="form-group">
          <textarea style="border: 1px solid #ff3366;" readonly disabled class="form-control" id="">Alasan di Reject : <?= $dt_kontrak["catatan_reject"] ?></textarea>
        </div>
      </div>
    </div>
  <?php } ?>

  <div style="clear: both;"></div>

  <form id="form-kontrak" method="post">
    <input type="hidden" style="color:black" id="kontrak_id" name="kontrak_id" value="<?= $dt_kontrak["id"] ?>">
    <div class="row">
      <div class="col-lg-12 col-xl-12 stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-baseline mb-2">
              <h6 class="card-title mb-0">Data Kegiatan</h6>
            </div>
            <br>
            <div class="row">
              <div class="col-lg-12">
                <div <?= $blocking ?> position: absolute; height: 148%; width: 98%; background-color: rgba(0,0,0,0.1); z-index: 1;"></div>
                <div class="form-group">
                  <label for="">Kegiatan Utama :</label>

                  <select <?= $disable ?> style="width:100% !important;" class="select2 form-control" name="id_kegiatan" id="id_kegiatan">
                    <option value="0"> -- Pilih Kegiatan -- </option>
                    <?php foreach ($list_kegiatan as $rowKegiatan) {

                      $selected1 = $rowKegiatan["id"] == $kegiatan_detail_dt["id_kegiatan"] ? "selected" : "";

                    ?>
                      <option <?= $selected1 ?> value="<?= $rowKegiatan["id"] ?>"><?= $rowKegiatan["nama_kegiatan"] ?> </option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <input id="id_user" name="id_user" type="hidden" value="">
                  <label for="">Kode Kegiatan / MAK / Akun :</label>
                  <select <?= $disable ?> style="width:100% !important;" class="select2 form-control" name="id_detail_kegiatan" id="id_detail_kegiatan">
                    <option value="0"> -- Pilih MAK -- </option>
                    <?php foreach ($list_detail_kegiatan as $row) {

                      $selected2 = "";
                      if ($row["id"] == $dt_kontrak["id_detail_kegiatan"]) {
                        $selected2 = "selected";
                      }

                    ?>
                      <option value="<?= $row["id"] ?>" <?= $selected2 ?>> <?= $row["kode_mak"] ?> - <?= $row["detail_kegiatan"] ?> </option>
                    <?php } ?>

                  </select>
                </div>

              </div>


              <div class="col-lg-6">
                <div class="form-group">
                  <label for="" style="display: block;"> Subdit :</label>
                  <select <?= $disable ?> style="width:100% !important;" class="select2 form-control" name="subdit" id="subdit">
                    <option value=""> -- Pilih Subdit -- </option>
                    <option value="Subdit 1" <?= $dt_kontrak["subdit"] == "Subdit 1" ? "selected" : "" ?>> Subdit 1</option>
                    <option value="Subdit 2" <?= $dt_kontrak["subdit"] == "Subdit 2" ? "selected" : "" ?>> Subdit 2</option>
                    <option value="Subdit 3" <?= $dt_kontrak["subdit"] == "Subdit 3" ? "selected" : "" ?>> Subdit 3</option>
                    <option value="Subdit 4" <?= $dt_kontrak["subdit"] == "Subdit 4" ? "selected" : "" ?>> Subdit 4</option>
                    <option value="Subdit 5" <?= $dt_kontrak["subdit"] == "Subdit 5" ? "selected" : "" ?>> Subdit 5</option>
                    <option value="Subbag TU" <?= $dt_kontrak["subdit"] == "Subbag TU" ? "selected" : "" ?>> Subbag TU</option>
                  </select>
                </div>
              </div>
              <div class="col-lg-6">


                <div class="form-group">
                  <label for="">Jenis Kontrak :</label>
                  <select <?= $disable ?> style="width:100% !important;" class="select2 form-control" name="jenis_kontrak" id="jenis_kontrak">
                    <option value=""> -- Pilih jenis Kontrak --</option>
                    <option value="1" <?= $dt_kontrak["jenis_kontrak"] == 1 ? "selected" : "" ?>>
                      Single Year
                    </option>
                    <option value="2" <?= $dt_kontrak["jenis_kontrak"] == 2 ? "selected" : "" ?>>
                      Multi Year
                    </option>
                  </select>
                </div>

              </div>

            </div>


          </div>
        </div>
      </div>
    </div>

    <br>
    <br>
    <!-- Data Kontraktor -->
    <div class="row">
      <div class="col-lg-12 col-xl-12 stretch-card">
        <div class="card">

          <div class="card-body">
            <div class="d-flex justify-content-between align-items-baseline mb-2">
              <h6 class="card-title mb-0">Data Kontrak</h6>
            </div>
            <br>


            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label for="">Nomor Kontrak :</label>
                  <input <?= $disable ?> type="text" name="no_kontrak" value="<?= $dt_kontrak["no_kontrak"] ?>" class="form-control">
                </div>

                <div class="form-group">
                  <label for="">Tanggal Kontrak :</label>
                  <input <?= $disable ?> type="date" name="tanggal_kontrak" value="<?= $dt_kontrak["tanggal_kontrak"] ?>" class="form-control">
                </div>

                <div class="form-group">
                  <label for="">No. NPWP Penyedia :</label>
                  <input <?= $disable ?> type="text" name="npwp_kontraktor" value="<?= $dt_kontrak["npwp_kontraktor"] ?>" class="form-control">
                </div>

                <div class="form-group">
                  <label for="">No. Rekening :</label>
                  <input <?= $disable ?> type="text" name="rekening_kontraktor" value="<?= $dt_kontrak["rekening_kontraktor"] ?>" class="form-control">
                </div>

              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <label for="">Nama Penyedia :</label>
                  <input <?= $disable ?> type="text" name="nama_kontraktor" value="<?= $dt_kontrak["nama_kontraktor"] ?>" class="form-control">
                </div>

                <div class="form-group">
                  <label for="">Alamat Penyedia :</label>
                  <input <?= $disable ?> type="text" name="alamat_kontraktor" value="<?= $dt_kontrak["alamat_kontraktor"] ?>" class="form-control">
                </div>

                <div class="form-group">
                  <label for="">Nama Rekening & Bank :</label>
                  <input <?= $disable ?> type="text" name="nama_rekening" value="<?= $dt_kontrak["nama_rekening"] ?>" placeholder="contoh : BANK BNI an PT Galangan Raya" class="form-control">
                </div>

                <div class="form-group">
                  <label for="">Nilai Kontrak :</label>
                  <input data-a-sign="Rp. " data-a-dec="," data-a-sep="." type="text" name="nilai_kontrak" value="<?= $dt_kontrak["nilai_kontrak"] ?>" class="form-control money">
                </div>

              </div>

              <div class="col-lg-12">
                <div class="form-group">
                  <label for="">Uraian :</label>
                  <textarea <?= $disable ?> name="uraian" class="form-control" id="" cols="30" rows="10"><?= $dt_kontrak["uraian"] ?></textarea>
                </div>
              </div>

              <div class="col-lg-12">
                <div class="row">

                  <div class="col-lg-4">
                    <div class="form-group">
                      <label for="">Jangka Waktu Pemeliharaan (Hari) :</label>
                      <input type="number" name="waktu_pemeliharaan" value="<?= $dt_kontrak["waktu_pemeliharaan"] ?>" class="form-control">
                    </div>
                  </div>

                  <div class="col-lg-4">
                    <div class="form-group">
                      <label for="">File Kontrak :</label>
                      <div>
                        <input <?= $disable ?> type="file" name="file_kontrak" value="" class="form-control">
                        <?php if ($dt_kontrak["file_kontrak"]) {
                          echo "<a class='btn btn-success mt-1' href=" . base_url("kontrak/download/$dt_kontrak[id]/kontrak") . "> <i class='fa fa-download'></i> $dt_kontrak[file_kontrak] </a>";
                        } ?>
                        <!-- <div class="clearfix"></div> -->
                      </div>

                    </div>
                  </div>

                  <div class="col-lg-4">
                    <div class="form-group">
                      <label for="">Persetujuan MYC :</label>
                      <div>
                        <input <?= $disable ?> type="file" name="file_myx" value="" class="form-control">
                        <?php if ($dt_kontrak["file_myx"]) {
                          echo "<a class='btn btn-success mt-1' href=" . base_url("kontrak/download/$dt_kontrak[id]/myx") . "> <i class='fa fa-download'></i> $dt_kontrak[file_myx] </a>";
                        } ?>
                        <div class="clearfix"></div>
                      </div>

                    </div>
                  </div>

                </div>
              </div>

              <div class="col-lg-12">
                <div class="row">

                  <div class="col-lg-4">
                    <div class="form-group">
                      <label for="">Jangka Waktu Penyelesaian (Hari) :</label>
                      <input type="number" name="waktu_penyelesaian" value="<?= $dt_kontrak["waktu_penyelesaian"] ?>" class="form-control">
                    </div>
                  </div>

                  <div class="col-lg-4">

                    <div class="form-group">
                      <label for="">Tanggal Mulai :</label>
                      <input type="date" name="tanggal_mulai" value="<?= $dt_kontrak["tanggal_mulai"] ?>" class="form-control">
                    </div>

                  </div>

                  <div class="col-lg-4">

                    <div class="form-group">
                      <label for="">Tanggal Selesai :</label>
                      <input type="date" name="tanggal_selesai" value="<?= $dt_kontrak["tanggal_selesai"] ?>" class="form-control">
                    </div>

                  </div>
                </div>

              </div>

              <div class="col-lg-12">
                <div class="form-group">
                  <label for="">Ketentuan Sanksi :</label>
                  <textarea <?= $disable ?> name="ketentuan_saksi" class="form-control" id="" cols="30" rows="10"> Apabila terjadi keterlambatan penyelesaian pekerjaan akibat kelalaian Penyedia barang/jasa dalam jangka waktu tersebut diatas, maka Penyedia barang/jasa yang bersangkutan akan dikenakan denda keterlambatan sekurang-kurangnya 1/1000 (satu per seribu) per hari dari harga Kontrak dan disetor ke kas negara.</textarea>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>

    <br>
    <br>

    <div class="row">
      <div class="col-lg-12 col-xl-12 stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-baseline mb-2">
              <h6 class="card-title mb-0">Term Of Payment</h6>
            </div>

            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label for="">Uang Muka :</label>
                  <input data-a-sign="Rp. " data-a-dec="," data-a-sep="." type="text" name="uang_muka" value="<?= $dt_kontrak["uang_muka"] ?>" class="form-control money">
                </div>

              </div>

              <div class="col-lg-6">
                <div class="form-group">
                  <label for="">Cara Pembayaran :</label>
                  <div class="form-check">

                    <label class="form-check-label">
                      <input onclick="unshowTermin()" type="radio" class="form-check-input" name="cara_pembayaran" id="optionsRadios" value="sekaligus" <?= $dt_kontrak["cara_pembayaran"] == "sekaligus" ? "checked" : "" ?>>
                      Sekaligus
                      <i class="input-frame"></i></label>

                  </div>

                  <div class="form-check">
                    <label class="form-check-label">
                      <input onclick="showTermin()" type="radio" class="form-check-input" name="cara_pembayaran" id="optionsRadios1" value="termin" <?= $dt_kontrak["cara_pembayaran"] == "termin" ? "checked" : "" ?>>
                      Termin
                      <i class="input-frame"></i></label>
                  </div>

                </div>
              </div>
            </div>
            <style>
              #termin-table_filter {
                display: none;
              }

              #termin-table_length {
                display: none;
              }

              #termin-table_info {
                display: none;
              }

              #termin-table_paginate {
                display: none;
              }
            </style>
            <!--  nah table ini tampil kalau pilihan termin -->
            <?php if ($this->uri->segment(1) == "detail_kontrak" && !empty($this->uri->segment(2))) { ?>
              <div id="tambah-termin">

                <a style="color:#fff; margin-bottom:30px;" id="btn-tambah-termin" class="btn btn-primary"> <i class="link-icon" data-feather="plus"></i>&nbsp; Tambah Termin</a>


                <div class="table-responsive">

                  <table id="termin-table" class="table table-hover mb-0" style="width:100%">
                    <thead>
                      <tr>
                        <th class="pt-0">Termin </th>
                        <th class="pt-0">Nilai Termin</th>
                        <th class="pt-0">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>

                </div>
              </div>
            <?php } ?>

          </div>
        </div>
      </div>
    </div>

    <br>
    <br>

    <!-- TOP -->
    <div class="row">
      <div class="col-lg-12 col-xl-12 stretch-card" style="display: block;">

        <?php if ($group_id == 3 || $group_id == 4) { ?>
          <button style="float: right;" type="button" onclick="verifikasi_kontrak()" class="btn btn-success mr-2">Verifikasi Kontrak</button>
          <button style="float: right;" type="button" onclick="showModalReject(<?= $dt_kontrak['id'] ?>)" class="btn btn-danger mr-2">Reject Kontrak</button>
          <a style="float: right;" href="<?= base_url('kontrak') ?>" class=" btn btn-light mr-2">Batal</a>
        <?php } else if ($group_id == 1) { ?>
          <button style="float: right;" type="button" onclick="verifikasi_kontrak()" class="btn btn-success mr-2">Verifikasi Kontrak</button>
          <button style="float: right;" type="button" onclick="showModalReject(<?= $dt_kontrak['id'] ?>)" class="btn btn-danger mr-2">Reject Kontrak</button>
          <button style="float: right;" type="submit" class="btn btn-primary mr-2">Submit Kontrak</button>
          <a style="float: right;" href="<?= base_url('kontrak') ?>" class=" btn btn-light mr-2">Batal</a>
        <?php } else { ?>
          <button style=" float: right;" type="submit" class="btn btn-primary mr-2">Submit Kontrak</button>
          <a style="float: right;" href="<?= base_url('kontrak') ?>" class=" btn btn-light mr-2">Batal</a>
        <?php } ?>

      </div>
    </div>

  </form>

  <br><br>

  <?php if (!empty($dt_kontrak["status"])) { ?>
    <form id="form-addendum" action="">
      <div class="row">

        <div class="col-lg-12 col-xl-12 stretch-card">
          <div class="card" style="border: 1px solid aquamarine;">

            <div class="card-body">
              <div class="d-flex justify-content-between align-items-baseline mb-2">
                <h6 class="card-title mb-0">Addendum Kontrak</h6>
              </div>
              <br>
              <div class="row">

                <div class="col-lg-6">

                  <div class="form-group" style="display:none;">
                    <label for="">Id Kontrak:</label>
                    <input id="id_kontrak" name="id_kontrak" value="<?= $dt_kontrak["id"] ?>" type="text" class="form-control">
                  </div>

                  <div class="form-group">
                    <label for="">Nomor Addendum:</label>
                    <input id="nomor_addendum" name="nomor_addendum" value="<?= $dt_kontrak["nomor_addendum"] ?>" type="text" class="form-control">
                  </div>
                </div>

                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="">File Kontrak Addendum:</label>
                    <input id="file_addendum" name="file_addendum" type="file" class="form-control">
                    <br>

                    <?php if ($dt_kontrak["file_addendum"]) {
                      echo "<a style='max-width:280px !important;' class='btn btn-success col-md-3 float-right' href=" . base_url("kontrak/download_file/" . $dt_kontrak[id] . "/addendum/") . "> <i class='link-icon' data-feather='download'></i> Download File Addendum </a>";
                    } ?>

                  </div>
                </div>

                <div class=" col-lg-6">
                  <div class="form-group">
                    <label for="">Tanggal Addendum:</label>
                    <input id="tanggal_addendum" name="tanggal_addendum" type="date" value="<?= $dt_kontrak["tanggal_addendum"] ?>" class="form-control">
                  </div>
                </div>

                <div class="col-lg-6">
                  <button id="tambahAddendum" type="submit" style="float:right; margin-right:0px !important; margin-top:28px;" class="btn btn-primary mr-2">
                    <i class="link-icon" data-feather="edit-3"></i> Submit Addendum
                  </button>
                </div>

              </div>
            </div>

          </div>
        </div>

      </div>
    </form>
  <?php } ?>
  <br>
  <br>

</div>

<?php $this->load->view("template/pages/kontrak/modal_reject") ?>
<?php $this->load->view("template/pages/kontrak/modal_kontrak_termin") ?>