
<!-- modal addendum -->

<div class="modal fade" id="AddAddendum" tabindex="-1" role="dialog" aria-labelledby="AddAddendum" aria-hidden="true">
  <div class="modal-dialog modal-lg	" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Addendum : <span id="no_kontrak"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="form-addendum" action="">
        <div class="modal-body">
          <div class="row">
            <div class="col-lg-6">

              <div class="form-group" style="display:none;">
                <label for="">Id Kontrak:</label>
                <input id="id_kontrak" name="id_kontrak" type="text" class="form-control">
              </div>

              <div class="form-group">
                <label for="">Nama Kontraktor:</label>
                <input readonly id="nama_kontraktor" value="" name="nama_kontraktor" type="text" class="form-control">
              </div>

              <div class="form-group">
                <label for="">Nomor Addendum:</label>
                <input id="nomor_addendum" name="nomor_addendum" type="text" class="form-control">
              </div>

              <div class="form-group">
                <label for="">File Kontrak Addendum:</label>
                <input id="file_addendum" name="file_addendum" type="file" class="form-control">
                <div id="download-addendum"></div>
                
              </div>

            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="">Tanggal Kontrak:</label>
                <input readonly id="tanggal_kontrak" name="tanggal_kontrak" type="text" class="form-control">
              </div>

              <div class="form-group">
                <label for="">Tanggal Addendum:</label>
                <input id="tanggal_addendum" name="tanggal_addendum" type="date" class="form-control">
              </div>
            </div>

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
          <button id="tambahDetailKegiatan" type="submit" class="btn btn-primary">Simpan Addendum</button>
        </div>
      </form>
    </div>
  </div>
</div>