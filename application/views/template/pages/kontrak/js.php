<!-- plugin js for this page -->
<script src="<?= base_url("assets/vendors/sweetalert2/sweetalert2.min.js") ?>"></script>
<script src="<?= base_url("assets/vendors/datatables.net/jquery.dataTables.js") ?>"></script>
<script src="<?= base_url("assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js") ?>"></script>

<script src="<?= base_url("assets/vendors/select2/select2.min.js") ?>"></script>
<script src="<?= base_url("assets/js/select2.js") ?>"></script>
<!-- end plugin js for this page -->



<script src=" https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<!-- custom js for this page -->
<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>

<!-- end custom js for this page -->


<script>
    function showDetailKontrak(id_kontrak) {

        return new Promise((resolve, reject) => {
            $.ajax({
                method: "post",
                url: "<?= base_url("kontrak/get_detail_kontrak") ?>",
                dataType: "json",
                data: {
                    id_kontrak: id_kontrak
                },
                success: function(res) {
                    resolve(res)
                },
                error: function(error) {
                    reject(error)
                },
            })
        })
    }

    function clearKontrak() {
        window.location = '<?= base_url("kontrak/") ?>'
    }

    function filterKontrak() {

        var filter_status_kontrak = $("#filter_status_kontrak").val()
        var filter_jenis_kontrak = $("#filter_jenis_kontrak").val()
        var filter_subdit = $("#filter_subdit").val()
        var filter_tanggal_kontrak = $("#filter_tanggal_kontrak").val()
        var filter_nama_ppk = $("#filter_nama_ppk").val()
        // var filter_tanggal_mulai = $("#filter_tanggal_mulai").val()
        // var filter_tanggal_selesai = $("#filter_tanggal_selesai").val()


        // if (filter_tanggal_mulai && !filter_tanggal_selesai) {
        //     Swal.fire({
        //         title: '<strong> Error !</strong>',
        //         icon: 'error',
        //         html: "<b>Tanggal Selesai</b> harus diisi"
        //     })
        //     return false
        // }

        // if (!filter_tanggal_mulai && filter_tanggal_selesai) {
        //     Swal.fire({
        //         title: '<strong> Error !</strong>',
        //         icon: 'error',
        //         html: "<b>Tanggal Mulai</b> harus diisi"
        //     })
        //     return false
        // }

        // if (filter_tanggal_mulai > filter_tanggal_selesai) {
        //     Swal.fire({
        //         title: '<strong> Error !</strong>',
        //         icon: 'error',
        //         html: "<b>Tanggal Mulai</b> harus sebelum <b>Tanggal selesai</b>"
        //     })
        //     return false
        // }

        var data = {}

        if (filter_status_kontrak) {
            data.filter_status_kontrak = filter_status_kontrak
        }

        if (filter_jenis_kontrak) {
            data.filter_jenis_kontrak = filter_jenis_kontrak
        }

        if (filter_tanggal_kontrak) {
            data.filter_tanggal_kontrak = filter_tanggal_kontrak
        }
        if (filter_nama_ppk) {
            data.filter_nama_ppk = filter_nama_ppk
        }
        if (filter_subdit) {
            data.filter_subdit = filter_subdit
        }

        // if (filter_tanggal_mulai) {
        //     data.filter_tanggal_mulai = filter_tanggal_mulai
        // }

        // if (filter_tanggal_selesai) {
        //     data.filter_tanggal_selesai = filter_tanggal_selesai
        // }

        var param = $.param(data);
        param = decodeURIComponent(param)
        window.location = '<?= base_url("kontrak/") ?>?' + param;
    }

    function kontrakTable() {

        $("#table-kontrak").DataTable({
            dom: 'Bfrtip',
            order: [
                [1, 'desc']
            ],
            buttons: [{
                text: 'Export to Excel',
                extend: 'excelHtml5',
                title: 'Daftar Kontrak',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
                }
            }],
            serverSide: true,
            ajax: {
                data: {
                    filter_status_kontrak: $("#filter_status_kontrak").val(),
                    filter_jenis_kontrak: $("#filter_jenis_kontrak").val(),
                    filter_subdit: $("#filter_subdit").val(),
                    filter_tanggal_kontrak: $("#filter_tanggal_kontrak").val(),
                    filter_nama_ppk: $("#filter_nama_ppk").val(),
                    // filter_tanggal_mulai: $("#filter_tanggal_mulai").val(),
                    // filter_tanggal_selesai: $("#filter_tanggal_selesai").val()

                },
                url: "<?= base_url("kontrak/get_kontrak") ?>",
                type: 'POST'
            },
            columns: [{
                    "data": "no"
                },
                {
                    "data": "status"
                },
                {
                    "data": "no_kontrak"
                },
                {
                    "data": "tanggal_kontrak"

                },
                {
                    "data": "tanggal_addendum"
                },
                {
                    "data": "nomor_addendum"
                },
                {
                    "data": "uraian_kontrak"
                },
                {
                    "data": "masa_kontrak"
                },
                {
                    "data": "nilai_kontrak"
                },
                {
                    "data": "jenis_kontrak"
                },
                {
                    "data": "nama_kontraktor"
                },
                {
                    "data": "subdit"
                },
                {
                    "data": "id_user"
                },
                {
                    "data": "kode_mak"
                },
                {
                    "data": "uraian_mak"
                },
                {
                    "data": "nama_kegiatan"
                },
                {
                    "data": "detail_kegiatan"
                },

                {
                    "data": "action"
                },
            ],
            columnDefs: [{
                    "sorting": false,
                    "orderable": false,
                    "type": "html",
                    "targets": 2,
                    "render": function(data, type, row) {
                        return `<a style="font-weight:bold;" href="<?= base_url() ?>detail_kontrak/${row.id}">${row.no_kontrak}</a>`
                    }
                },
                {
                    "sorting": false,
                    "orderable": false,
                    "type": "html",
                    "targets": 8,
                    "render": function(data, type, row) {
                        return `<span style="font-weight:bold;">Rp. ${row.nilai_kontrak}</span>`
                    }
                },
                {
                    "sorting": false,
                    "orderable": false,
                    "type": "html",
                    "targets": 9,
                    "render": function(data, type, row) {
                        var kontrak = ''
                        if (row.jenis_kontrak == 1) {
                            kontrak = 'Single Year'
                        } else if (row.jenis_kontrak == 2) {
                            kontrak = 'Multi Year'
                        }
                        return `<span style="font-weight:bold;">${kontrak}</span>`
                    }
                },
                {
                    "sorting": false,
                    "orderable": false,
                    "type": "html",
                    "targets": 1,
                    "render": function(data, type, row) {
                        const newRow = row
                        var status = ''
                        if (row.status == 'Terdaftar Belum di Verifikasi') {
                            status = "badge-primary"
                        } else if (row.status == 'Verifikasi Bendahara') {
                            status = "badge-success"
                        } else {
                            status = 'badge-danger'
                        }

                        return `<span style="font-size:13.5px" class="badge ${status}">${row.status}</span>`
                    }
                },
                {
                    "sorting": false,
                    "orderable": false,
                    "type": "html",
                    "targets": -1,
                    "render": function(data, type, row) {
                        const newRow = row
                        let menuVerifikasi = ''
                        let showBtn = 'inline-block'
                        if (<?= $this->session->userdata("group_id") ?> === 3 || <?= $this->session->userdata("group_id") ?> === 1) {
                            menuVerifikasi = `<li style="display:inline-block;"><a onclick="showModalVerifikasi('${row.id}')" class="btn btn-success">Verifikasi</a></li>`
                        }
                        if (<?= $this->session->userdata("group_id") ?> === 3 || <?= $this->session->userdata("group_id") ?> === 4) {
                            return `<a href="<?= base_url('detail_kontrak/') ?>${row.id}" class="btn btn-primary">Lihat Detail</a>`
                        } else {
                            return `<div class="btn-group dropdown">
                                <button type="button" class="btn btn-primary" data-toggle="dropdown" aria-expanded="false">
                                    Aksi <span class="caret"><i class="fa fa-caret-down"></i></span>
                                </button>
                                <ul class="dropdown-menu animated fadeIn">
                                    <li style="display:none;"><a href="#" onClick="showModalAddendum(${row.id})" class="btn btn-info">Addendum</a></li> 
                                    <li style="display:inline-block;"><a href="<?= base_url('printpdf/cetakKontrak/') ?>${row.id}"  class="btn btn-dark">Cetak Resume</a></li>
                                    <li style="display:inline-block;"><a href="<?= base_url('detail_kontrak/') ?>${row.id}" class="btn btn-warning">Edit/Addendum</a></li>
                                    <li style="display:inline-block;"><a href="#" onClick="delete_kontrak(${row.id}, '${row.no_kontrak}')" class="btn btn-danger">Hapus</a></li>
                                </ul>
                            </div>`
                        }

                    }
                }
            ]
        })
    }

    function delete_kontrak(id_kontrak, no_kontrak) {

        Swal.fire({
            title: "Hapus Kontrak ",
            html: `<p>Apakah anda yakin ingin menghapus kontrak ini ?</p> <li><b> ${no_kontrak} </b></li> `,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'mr-2',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true,
            closeOnConfirm: true
        }).then((result) => {

            if (result.value) {
                $.ajax({
                    url: "<?= base_url("kontrak/delete_kontrak") ?>",
                    data: {
                        id_kontrak: id_kontrak
                    },
                    dataType: "json",
                    method: "post",
                    success: function(res) {
                        if (res.success === true) {
                            $('#table-kontrak').DataTable().ajax.reload();
                            Swal.fire(
                                'Success!',
                                res.message,
                                'success'
                            )
                        } else {
                            Swal.fire({
                                title: '<strong> Error !</strong>',
                                icon: 'error',
                                html: res.message
                            })
                        }
                    }
                })
            }

        })


    }

    function showModalVerifikasi(id_kontrak) {
        showDetailKontrak(id_kontrak).then(res => {

            $("#updateStatus #nomor_kontrak_span").html(res.data.no_kontrak)
            $("#updateStatus #id_kontrak").val(res.data.id)
            $("#updateStatus #no_kontrak").val(res.data.no_kontrak)
            //$(`#updateStatus #status_kontrak option[value='${res.data.status_tagihan}]'`).attr('selected','selected');
            $("#updateStatus #status_kontrak").val(res.data.status).change()

            $('#updateStatus').modal('show')
        })
    }

    function showModalAddendum(id_kontrak) {
        showDetailKontrak(id_kontrak).then(res => {

                $("#form-addendum #id_kontrak").val(id_kontrak)
                $("#form-addendum #nama_kontraktor").val(res.data.nama_kontraktor)
                $("#form-addendum #nomor_addendum").val(res.data.nomor_addendum)
                $("#form-addendum #tanggal_addendum").val(res.data.tanggal_addendum)
                $("#form-addendum #tanggal_kontrak").val(res.data.tanggal_kontrak)

                if (!res.data.file_addendum) {

                    $("#download-addendum").html(``)
                } else {
                    $("#download-addendum").html(`<a href='./kontrak/download_file/${id_kontrak}/addendum'> Download Addendum </a>`)
                }

                $("#no_kontrak").val(res.data.no_kontrak)

                $('#AddAddendum').modal('show')
                console.log(" RES ==> ", res)
            })
            .catch(err => {
                console.log("ERR => ", err)
            })
    }

    $(function() {

        kontrakTable()

        $(".money").autoNumeric("init", {
            aForm: true,
            vMax: "999999999999999"
        });

    })

    $("#updateStatus form#form-update-status").submit(function(e) {
        verifikasi_kontrak()
        e.preventDefault()
    })

    $("#filter-kontrak-btn").click(function(e) {
        //alert("filter...")
        filterKontrak()
    })

    $("#clear-kontrak-btn").click(function(e) {
        //alert("filter...")
        clearKontrak()
    })
</script>