<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RESUME KONTRAK</title>
</head><body>
    <table style="font-size:14px; font-family: Arial, Helvetica, sans-serif; width:100%;">
        <tr>
          <td style="text-align:left; font-weight:bold; font-size:15px;">
            RESUME KONTRAK
          </td>
          <td style="text-align:right;">  Untuk kegiatan yang dananya berasal dari Rupiah Murni</td>
        </tr>
    </table>
    <hr>
    <br>
    <table style="font-size:14px; font-family: Arial, Helvetica, sans-serif; width:100%;">
        <tr>
          <td style="font-weight:bold">
          Nomor dan tanggal DIPA
          </td>
          <td>:</td>
          <td><?=$kontrak["nomor_dipa"]?> / <?=$kontrak["tanggal_dipa"]?></td>
        </tr>
        <tr>
          <td style="font-weight:bold">
          Kegiatan/Output/Akun
          </td>
          <td>:</td>
          <td><?=$kontrak["kode_aktivitas"]?>/<?=$kontrak["kro"]?>/<?=$kontrak["ro"]?>- <?=$kontrak["kode_mak"]?>  <?=$kontrak["nama_kegiatan"]?></td>
        </tr>
        <tr>
          <td style="font-weight:bold">
          Nomor dan tanggal SPK/Kontrak
          </td>
          <td>:</td>
          <td><?= $kontrak['no_kontrak'] ?> / <? $d2 = new DateTime($kontrak['tanggal_kontrak']); echo $d2->format('d-m-Y'); ?></td>
        </tr>
        <tr>
          <td style="font-weight:bold">
          Nama Kontraktor/Perusahaan
          </td>
          <td>:</td>
          <td><?= $kontrak['nama_kontraktor'] ?></td>
        </tr>
        <tr>
          <td style="font-weight:bold">
          Alamat Kontraktor
          </td>
          <td>:</td>
          <td><?= $kontrak['alamat_kontraktor'] ?></td>
        </tr>
        <tr>
          <td style="font-weight:bold">
          NPWP
          </td>
          <td>:</td>
          <td><?= $kontrak['npwp_kontraktor'] ?></td>
        </tr>

        <tr>
          <td style="font-weight:bold">
          BANK
          </td>
          <td>:</td>
          <td><?= $kontrak['nama_rekening'] ?> / <?= $kontrak['rekening_kontraktor'] ?></td>
        </tr>

        <tr>
          <td style="font-weight:bold">
          Nilai SPK / Kontrak
          </td>
          <td>:</td>
          <td>Rp. <?= number_format($kontrak['nilai_kontrak'],2,",",".") ?></td>
        </tr>

        <tr>
          <td style="font-weight:bold">
          Uraian
          </td>
          <td>:</td>
          <td><?= $kontrak['uraian'] ?></td>
        </tr>

        <tr>
          <td style="font-weight:bold">
          Cara pembayaran
          </td>
          <td>:</td>
          <td> <?php if($kontrak['cara_pembayaran'] == 'termin' ) { ?>
                  <?php echo 'Termin' ?><br>
                  <?php foreach($kontrakList as $rowKontrak) {?>
                      <?= $rowKontrak['termin_phase'] ?>. Rp. <?= number_format($rowKontrak['harga_termin']) ?> <br>
                  <?php } ?>
          <?php } else { echo "Sekaligus";} ?>
          </td>
        </tr>

        <tr>
          <td style="font-weight:bold">
          Jangka Waktu Penyelesaian
          </td>
          <td>:</td>
          <td><?= $kontrak['waktu_penyelesaian'] ?> hari kalendar sejak diterbitkan Surat Perintah Mulai Kerja (SPMK), <br> <?= $kontrak['tanggal_mulai'] ?> sd <?= $kontrak['tanggal_selesai'] ?> </td>
        </tr>

        <tr>
          <td style="font-weight:bold">
          Jangka Waktu Pemeliharaan
          </td>
          <td>:</td>
          <td><?= $kontrak['waktu_pemeliharaan'] ?> hari </td>
        </tr>

        <tr>
          <td style="font-weight:bold">
          Ketentuan Saksi
          </td>
          <td>:</td>
          <td style="text-align:justify; line-height:23px;"> <br>Apabila terjadi keterlambatan penyelesaian pekerjaan akibat kelalaian Penyedia barang/jasa dalam jangka waktu tersebut diatas, maka Penyedia barang/jasa yang bersangkutan akan dikenakan denda keterlambatan sekurang-kurangnya 1/1000 (satu per seribu) per hari dari harga Kontrak dan disetor ke kas negara.  </td>
        </tr>
    </table>
    <br><br><br><br><br>

    <table style="width:100%; font-family: Arial, Helvetica, sans-serif;">
      <tr>
        <td style="width:30%">&nbsp;</td>
        <td style="width:30%">&nbsp;</td>
        <td style="text-align:center; width:40%">
          <span style="font-size:13.5px">Jakarta, <? $d = new DateTime($kontrak['created_at']); echo $d->format('d-m-Y'); ?><br></span>
          <strong style="font-size:14px">A.N. KUASA PENGGUNA ANGGARAN</strong><br>
          <strong style="font-size:14px">PEJABAT PEMBUAT KOMITMEN</strong>

          <br><br><br><br>
            <strong style="font-size:14px; border-bottom:0.5px solid #000; padding-bottom:5px"><?= strtoupper($kontrak['fullname'])  ?></strong> <br>
            <strong style="font-size:14px">NIP :<?= $kontrak['nip'] ?></strong>
        </td>
      </tr>
    </table>


</body></html>
© 2021 GitHub, Inc.
Terms
Privacy
Security
Status
Docs
Contact GitHub
Pricing
API
Training
Blog
About
Loading complete
