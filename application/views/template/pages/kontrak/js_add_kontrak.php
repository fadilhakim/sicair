<script src="<?= base_url("assets/vendors/sweetalert2/sweetalert2.min.js") ?>"></script>

<script>
    function add_kontrak() {

        var form_tambah_kontrak = $("form#form-kontrak")[0];
        const data = new FormData(form_tambah_kontrak)

        $.ajax({
            url: "<?= base_url("kontrak/add_kontrak") ?>",
            enctype: "multipart/form-data",
            type: "post",
            data: data,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function(res) {
                if (res.success === true) {

                    Swal.fire(
                        'Success!',
                        res.message,
                        'success'
                    )

                    setTimeout(() => {
                        location.href = "<?= base_url('kontrak') ?>"
                    }, 3000);

                } else {
                    Swal.fire({
                        title: '<strong> Error !</strong>',
                        icon: 'error',
                        html: res.message
                    })
                }
            }
        })

    }

    function get_detail_kegiatan(id_detail_kegiatan) {

        $.ajax({
            type: "post",
            url: "<?= base_url("kegiatan/get_kegiatan_detail") ?>",
            dataType: "json",
            data: {
                id: id_detail_kegiatan
            },
            success: function(res) {
                $("form#form-kontrak #id_user").val(res.data.id_user)

            }
        })

    }

    $("select#id_detail_kegiatan").change(function(e) {

        get_detail_kegiatan($("select#id_detail_kegiatan").val())
    })

    function get_kegiatan_detail_list(id_kegiatan) {
        $.ajax({
            "type": "POST",
            "url": "<?= base_url("kegiatan/get_kegiatan_detail_list") ?>",
            "dataType": 'json',
            data: {
                id_kegiatan: id_kegiatan
            },
            success: function(res) {

                $("select#id_detail_kegiatan").html("")

                $("select#id_detail_kegiatan").append(`<option value="">-- Pilih Detail Kegiatan --</option>`);
                var detail_kegiatan_list = $.each(res.data, function(index, item) {
                    $("select#id_detail_kegiatan").append(`<option value=${item.id}>${item.kode_mak} - ${item.detail_kegiatan}</option>`);
                })


            }

        })
    }

    $("select#id_kegiatan").change(function(e) {
        get_kegiatan_detail_list($("#id_kegiatan").val())
    })



    // $("#bp_detail").change(function(e) {
    //     alert($("#bp_detail").val())
    //     //postIdKegiatan($("#bp_detail").val())
    // })

    $(function() {
        $("form#form-kontrak").submit(function(e) {

            add_kontrak()

            e.preventDefault()
        })

        if ($(".select2").length) {
            $(".select2").select2();
        }


        $('#addTmn').on('click', function() {

            var jumlah_termin = $("form #jumlah_termin").val()
            jumlah_termin = parseInt(jumlah_termin) + 1
            $("form #jumlah_termin").val(jumlah_termin)

            $('#input-termin').append(`
                <label for="">Termin ${jumlah_termin} :</label>
                <div class="input-group mb-3 col-md-4">
                    <input type="number" name="termin[]" value="0" class="form-control">
                    <a href="#" id="termin-${jumlah_termin}" data-id="${jumlah_termin}" class=" input-group-append">
                        <span class="input-group-text bg-danger"> x </span>
                    </a>
                </div>
            `)

        });

        $(".money").autoNumeric("init", {
            aForm: true,
            vMax: "999999999999999"
        });
    })
</script>