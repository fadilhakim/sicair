<div class="page-content">
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"> User </li>
            <li class="breadcrumb-item active"> User Add </li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-md-6">
            <form id="user-add-form">
                <div class="card">

                    <div class="card-header">

                        <h4 class="card-header-title">Tambah User</h4>
                        <div class="card-tools">


                        </div>
                    </div>
                    <div class="card-body">

                        <div class="form-group">
                            <label for="username"> Username </label>
                            <input type="text" id="username" name="username" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="fullname"> Fullname </label>
                            <input type="text" id="fullname" name="fullname" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="username">New Password </label>
                            <input type="password" id="password" name="password" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="username">NIP</label>
                            <input type="text" id="nip" name="nip" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="Status"> Status </label><br>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="status form-check-input" value="1" name="status">
                                    Active
                                </label>
                            </div>

                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="status form-check-input" value="0" name="status">
                                    Inactive
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="group"> Jabatan </label>
                            <select class="form-control" id="group" name="group">
                                <option value=""> -- Pilih Jabatan -- </option>
                                <?php foreach ($groups as $row) { ?>
                                    <option value="<?= $row["id"] ?>"><?= $row["group_name"] ?></option>
                                <?php } ?>
                            </select>
                        </div>

                    </div>
                    <div class="card-footer">
                        <div class="form-group">
                            <label for="email"> Email </label>
                            <input type="email" class="form-control" name="email" id="email">
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-success float-right">
                            <i data-feather="plus">&nbsp;</i>
                            Add User
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>