<div class="page-content">
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"> User </li>
            <li class="breadcrumb-item active"> User Detail </li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-md-6">
            <form id="user-update-form">
                <div class="card">

                    <div class="card-header">

                        <h4 class="card-header-title">User Detail</h4>
                        <div class="card-tools">


                        </div>
                    </div>
                    <div class="card-body">
                        <input type="hidden" name="user_id" id="user_id" value="<?= $user["id"] ?>">

                        <div class="form-group">
                            <label for="username"> Username :</label>
                            <input type="text" id="username" name="username" value="<?= $user["username"] ?>" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="fullname"> Fullname :</label>
                            <input type="text" id="fullname" name="fullname" value="<?= $user["fullname"] ?>" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="fullname"> NIP :</label>
                            <input type="text" id="nip" name="nip" value="<?= $user["nip"] ?>" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="Status"> Status :</label><br>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="status form-check-input" value="1" name="status" <?= $user["status_id"] == 1 ? "checked" : "" ?>>
                                    Active
                                </label>
                            </div>

                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="status form-check-input" value="0" name="status" <?= $user["status_id"] == 0 ? "checked" : "" ?>>
                                    Inactive
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="group"> Group :</label>
                            <select class="form-control" id="group" name="group">
                                <option value=""> -- please select group -- </option>
                                <?php

                                foreach ($groups as $row) {
                                    $selected = "";
                                    if ($row["id"] == $user["group_id"]) {
                                        $selected = "selected='selected'";
                                    }
                                ?>
                                    <option value="<?= $row["id"] ?>" <?= $selected ?>><?= $row["group_name"] ?></option>
                                <?php } ?>
                            </select>
                        </div>

                    </div>
                    <div class="card-footer">
                        <div class="form-group">
                            <label for="email"> Email :</label>
                            <input type="email" class="form-control" name="email" id="email" value="<?= $user["email"] ?>">
                        </div>
                        <div class="form-group">
                            <label for="password"> Set New Password :</label>
                            <input class="form-control" name="new_password" id="new_password">
                        </div>

                        <div class="form-group" style="display: none;">
                            <label for="password"> Old Password :</label>
                            <input class="form-control" name="old_password" value="<?= $user["password"] ?>" id="old_password">
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-success float-right">
                            <i data-feather="edit">&nbsp;</i>
                            Update User
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>