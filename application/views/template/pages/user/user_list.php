<div class="page-content">
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"> User </li>
            <li class="breadcrumb-item active"> User list </li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">

                    <h4 class="card-header-title">User List</h4>
                    <div class="card-tools">
                        <a href="<?= base_url("user/add") ?>" class="btn btn-success float-right text-white">
                            <i data-feather="plus">&nbsp;</i>
                            Add User
                        </a>

                    </div>
                </div>
                <div class="card-body">
                    <table id="user-table" class="table ">
                        <thead>
                            <th> ID</th>
                            <th> Username </th>
                            <th> Fullname </th>
                            <th> Email </th>
                            <th> Jabatan </th>
                            <th> Action </th>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>