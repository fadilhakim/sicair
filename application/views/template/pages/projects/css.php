<link rel="stylesheet" href="<?=base_url("assets/vendors/datatables.net-bs4/dataTables.bootstrap4.css")?>">
<link rel="stylesheet" href="<?=base_url("assets/vendors/sweetalert2/sweetalert2.min.css")?>">

<style>
    .floating-btn {
        float: right;
        display : flex;
        position : fixed;
        right : 20px;
        bottom : 60px;
        z-index : 999;
    }
    .form-check .form-check-label input[type="checkbox"] + .input-frame:before, .form-check .form-check-label input[type="checkbox"] + .input-frame:after {
      left: 20px !important;
    }
</style>
