<script src="<?=base_url("assets/vendors/sweetalert2/sweetalert2.min.js")?>"></script>
<script src="<?=base_url("assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js")?>"></script>
<script src="<?=base_url("assets/vendors/moment/moment.min.js")?>"></script>
<script src="<?=base_url("assets/vendors/tempusdominus-bootstrap-4/tempusdominus-bootstrap-4.js")?>"></script>
<script>

	function uploadSpreadsheet(_this) {
		$.ajax({
			url:"<?=base_url("project/upload_spreadsheet")?>",
			method:"POST",
			data:new FormData(_this),
			dataType:"json",
			contentType:false,
			cache:false,
			processData:false,
			beforeSend:function(){
				$('#upload-csv').attr('disabled', 'disabled');
				$('#upload-csv').html('Importing...');
			},
			success:function(data)
			{
				console.log("res => ", data)
				//$('#message').html(data);
				
				$('#import_excel_form')[0].reset();
				$('#upload-csv').attr('disabled', false);
				$('#upload-csv').html('Import');

				Swal.fire({
					title: '<strong> '+data.status+' !</strong>',
					icon: data.status,
					html:data.message
				})

				// setTimeout(() => {
				// 	location.reload()
				// }, 3000);
				
			},
			error:function(err) {

				$('#upload-csv').attr('disabled', 'disabled');
				$('#upload-csv').html('Importing...');

				Swal.fire({
					title: '<strong> Error !</strong>',
					icon: "error",
					html: JSON.stringify(err)
				})
			}
		})
	}

	function updateProjectNode() {

		const card = "#project-update-location-card"

		Swal.fire({
			title: "Update Project ",
			text: "Are you sure you want to update this project !",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonClass: 'mr-2',
			confirmButtonText: 'Yes, Update it!',
			cancelButtonText: 'No, cancel!',
			reverseButtons: true
		}).then((result) => {
			if (result.value) {
				
				$.ajax({
					type:"POST",
					url:"<?=base_url("project/project_location_update")?>",
					data:{
						workorder_id:$(`#workorder_id`).val(),
						project_update_location_id:$(`${card} #project_update_location_id`).val(),
						jenis_teknisi:$(`${card} #jenis_teknisi`).val(),
						nama_teknisi:$(`${card} #nama_teknisi`).val(),
						hp_teknisi:$(`${card} #hp_teknisi`).val(), 
						tgl_terima_proses:$(`${card} #tgl_terima_progress`).val(),
						tol:$(`${card} #tol`).val(),
						project_status_id:$(`${card} #project_status_id`).val(),
						project_status_date:$(`${card} #project_status_date`).val(),
						project_status_time:$(`${card} #project_status_time_input`).val(),
						keterangan:$(`${card} #keterangan`).val(),
					},
					dataType:"JSON",
					success:function(data) {

						if(data.success) {
							Swal.fire({
								title: '<strong> Success !</strong>',
								icon: 'success',
								html:data.messgae
							})

							setTimeout(() => {
								location.reload()
							}, 2000);
						} else {
							Swal.fire({
								title: '<strong> Error !</strong>',
								icon: 'error',
								html:data.message
							})
						}
							
					}
				})

			} else if (
			// Read more about handling dismissals
			result.dismiss === Swal.DismissReason.cancel
			) {
			
			}
		})
	}

	function handleProvinsiChange(provinsi_id) {
		
		get_cities_byprov(provinsi_id)
	}

	function get_cities_byprov(provinsi_id) { 
		
		$.ajax({
			type:"post" , 
			url:"<?=base_url("project/get_cities_byprov")?>",
			data:{
				provinsi_id:provinsi_id
			},
			success:function(data) {
				var parse =  $.parseJSON(data)
				var opts = parse.data

				console.log(" ===> ",opts)
				// Use jQuery's each to iterate over the opts value
				$.each(opts, function(i, d) {

					var selected = ""
					//console.log("d",d)
					// You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
					$('#city_list_id').append('<option value="' + d.id + '">' + d.nama + '</option>');
				});
			}
		})
	}

	function showProjectUpdateLocationDetail(project_update_location_id) {
		$.ajax({
			type:"post",
			url:"<?=base_url("project/get_project_update_location_detail")?>",
			data: {
				project_update_location_id:project_update_location_id
			},
			success:function(data) {

				var parse =  $.parseJSON(data)
				// console.log("parse ==> ",parse)
				
				var res = parse.data

				if(res.teknisi_type === "freelance") {
					
					get_freelance(res.freelance_id)
				} else {
					get_teknisi(res.teknisi_id)
				}

				var status_datetime = res.status_date ? res.status_date.split(" ") : null
				var status_date = status_datetime ? status_datetime[0] : null
				var status_time = status_datetime ? status_datetime[1] : null

				//project_update_location_title
				//alert("change")
				$("#exampleModal #workorder_id").val(res.h_workorder_id)
				$("#exampleModal #project_update_location_title").html(res.project_update.project_code)
				$("#exampleModal #project_update_location_id").val(res.id)
				$("#exampleModal #no_provisioning").val(res.no_provisioning)
				$("#exampleModal #sid").val(res.sid) 
				$("#exampleModal #bw").val(res.bw) 
				$("#exampleModal #lokasi_instalasi").val(res.lokasi_instalasi) 
				$("#exampleModal #project_status_id").val(res.status) 
				$("#exampleModal #provinsi_list_id").val(res.provinsi)
				$("#exampleModal #city_list_id").val(res.kab)  
				$("#exampleModal #alamat").val(res.alamat)
				$("#exampleModal #hp_pic").val(res.pic_phone_number)    
				$("#exampleModal #nama_pic").val(res.pic)
				$(`#exampleModal #jenis_teknisi option[value="${res.teknisi_type}"]`).attr("selected",true);    
				
				$("#exampleModal #nama_teknisi").val(res.teknisi)    
				$("#exampleModal #hp_teknisi").val(res.teknisi_phone_number)  
				$("#exampleModal #tgl_terima_progress").val(res.tgl_mulai_progress) 
				$("#exampleModal #project_status_date").val(status_date) 
				// $("#exampleModal #project_status_time_input").val(status_time) 
				$("#exampleModal #ao").val(res.ao) 
				$("#exampleModal #tol").val(res.TOL) 
				$("#exampleModal #keterangan").val(res.keterangan) 


				// populate one by one 
			}

		})
		
	}

	function callbackProjectUpdateLocationDetail(project_update_location_id) {
		
	}

	function get_teknisi(teknisi_id) {
		$.ajax({
			type: "post",
			url:"<?=base_url("Project/get_karyawan")?>",
			dataType:"json",
			success:function(data){
				var opts = data.result

				//console.log("opts teknisi", data)
				// Use jQuery's each to iterate over the opts value
				$.each(opts, function(i, d) {
					// console.log(" ===> ",d.id, d.nama)
					// You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
					$('#nama_teknisi').append('<option value="' + d.id + '">' + d.nama + '</option>');
				});

				$(`#exampleModal #nama_teknisi option[value="${teknisi_id}"]`).attr("selected",true)

				
				
			},
			completed:function() {
				
				
				
			},
			error:function(err) {
				console.log(" ==<> ", err )
			}
		})
	}

	function get_freelance(freelance_id) {
		$.ajax({
			type: "post",
			url:"<?=base_url("Project/get_freelance")?>",
			// dataType:"json",
			dataType:"json",
			success:function(data){
				var opts = data.result

				//console.log("opts freelance", data)

				// Use jQuery's each to iterate over the opts value
				$.each(opts, function(i, d) {
					// console.log(" ===> ",d.id, d.nama)
					// You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
					$('#nama_teknisi').append('<option value="' + d.id + '">' + d.nama + '</option>');
				});

				$(`#exampleModal #nama_teknisi option[value="${freelance_id}"]`).attr("selected",true);

			}
		})
	}

	function showTimeline(id) {

		$('#exampleModalTimeline ul.timeline').empty()

		$.ajax({
			type:"POST",
			url:"<?=base_url("Project/get_project_timeline")?>",
			data:{
				project_update_location_id:id
			},
			dataType:"json",
			success:function(data) {

				var last  = data.data.last_timeline

				$('#exampleModalTimeline ul.timeline').empty()
				$.each(
					data.data.timeline,
					function(intIndex,objValue){

						var datetime = objValue.status_date.split(" ")
						var current = ""

						if(objValue.id === last.id) {
							current = "current"
						}

						$('#exampleModalTimeline ul.timeline').append(
							$(`<li class="event active ${current}" data-date="${datetime[0]}">
								<h3>${objValue.status_name}</h3>
								<p>${objValue.keterangan}</p>
							</li>`)
						);
					}
				);

				//console.log(" timeline ==> ",data.data)
			}
		})
	}

	function showHighlighted(project_update_id) {
		$.ajax({
			type:"post",
			url:"<?=base_url("project/get_project_update_detail")?>",
			data: {
				project_update_id:project_update_id
			},
			success:function(data) {

				var parse =  $.parseJSON(data)
				// console.log("parse ==> ",parse)
				
				var res = parse.data

				$("#show-highlighted").click()
				$("#modalHighlighted #highlighted-desc").html(res.highlighted_desc)
			}

		})
	}

	function highlightedProcess(project_update_id) {
		$.ajax({
			url:"<?=base_url("project/project_update_highlighted")?>",
			method:"POST",
			data:{
				project_update_id:project_update_id,
				"highlighted":1,
				"highlighted_desc":$("#modalHighlighted #highlighted-desc").val()
			},
			success:function(data) {
				//$('#form-check-input-highlighted').prop('checked', true); // Unchecks it
				Swal.fire({
					title: '<strong> Success !</strong>',
					icon: 'success',
					html:"You Successfully Highlight this project"
				})

				setTimeout(() => {
					location.reload()
				}, 3000);
			}
		})
	}

	$(function(){

		$("#tol").val("")
		$("#tgl_terima_progress").val("")
		$("#tol-field").hide()
		$("#ttp-field").hide()

		if( $(this).val() == 5 || $(this).val() == 8) {
			$("#tol-field").show()
		}

		if( $(this).val() >= 4 ) {
			$("#ttp-field").show()
		}

		$("#project_status_id").change(function(){
			$("#tol").val("")
			$("#tgl_terima_progress").val("")
			$("#tol-field").hide()
			$("#ttp-field").hide()

			if( $(this).val() == 5 || $(this).val() == 8) {
				$("#tol-field").show()
			}

			if( $(this).val() >= 4 ) {
				$("#ttp-field").show()
			}
		})

		$("#update-project-node-btn").click(function(e){
			// alert("test")
			updateProjectNode()
		})

		

		$('#project_status_date').datepicker({
			format: 'yyyy-mm-dd',
			
		});

		$('#tgl_terima_progress').datepicker({
			format: 'yyyy-mm-dd',
			
		});

		$('#tol').datepicker({
			format: 'yyyy-mm-dd',
			
		});

		$('#project_status_time').datetimepicker({
            format: 'LT'
        });

		$('#import_excel_form').on('submit', function(event){

			const _this = this

			uploadSpreadsheet(_this)
			event.preventDefault();

		})

		$("#jenis_teknisi").change(function(e){
			
			$('#nama_teknisi').empty()
			const jenis_teknisi = e.target.value

			//console.log(" jenis_teknisi ==> ",jenis_teknisi)

			if(jenis_teknisi === "teknisi") {

				
				get_teknisi()

			} else if (jenis_teknisi === "freelance") {

				get_freelance()
			}
		})

		$("#modalHighlighted").on("hide.bs.modal",function(){
			$('#form-check-input-highlighted').prop('checked', false);
		})

		$("#form-highlighted-submit").click(function(){
			var project_update_id = $("#project_update_id").val()
			highlightedProcess(project_update_id)
		})

		$("#form-check-input-highlighted").change(function(e){

			var project_update_id = $("#project_update_id").val()
			if(this.checked) {
				showHighlighted(project_update_id)
			} else {

				Swal.fire({
					title: "Project Highlighted ",
					text: "Are you sure you want to remove the highlighted Project !",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonClass: 'mr-2',
					confirmButtonText: 'Yes, Remove it!',
					cancelButtonText: 'No, cancel!',
					reverseButtons: true
				}).then((result) => {

					if (result.value) {
						$.ajax({
							url:"<?=base_url("project/project_update_highlighted")?>",
							method:"POST",
							data:{
								project_update_id:project_update_id,
								"highlighted":0,
								"highlighted_desc":""
							},
							success:function(data) {
								$('#form-check-input-highlighted').prop('checked', false); // Unchecks it
								Swal.fire({
									title: '<strong> Success !</strong>',
									icon: 'success',
									html:"You Successfully remove this project from Highlight"
								})
							}
						})
					} else {
						$('#form-check-input-highlighted').prop('checked', true); 
					}
				})

				
			}
			
		})

	})

</script>
