<!-- plugin js for this page -->
<script src="<?=base_url("assets/vendors/sweetalert2/sweetalert2.min.js")?>"></script>
<script src="<?=base_url("assets/vendors/datatables.net/jquery.dataTables.js")?>"></script>
<script src="<?=base_url("assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js")?>"></script>
<!-- end plugin js for this page -->

<!-- custom js for this page -->
<script src="<?=base_url("assets/js/data-table.js")?>"></script>
<!-- end custom js for this page -->


<script>



function projectTable() {

	

    $("#project-table").DataTable({
        serverSide : true,
		
        ajax : {
            data : {
                foo : "bar"
            },
            // url : "<?= base_url("all-projects") ?>",
            type : 'POST'
        },
        columns : [
           {"data" : "nama", width:100},
           {"data" : "nomor", width:100},
           {"data" : "RFS", width:100},
           {"data" : "h_kontrak_nodelink.nama", width:100},
           {data : "action", width:200},
        ],
        columnDefs : [
            {
                "sorting" : false,
                "orderable" : false,
                "type" : "html",
                "targets" : -1
            }
        ]
    })
}

function search() {

    // console.log('run search')

	var layanan = $("#jenis_layanan").val()
	var pelanggan = $("#pelanggan").val()
	var jumlah_node = $("#jumlah_node").val()

    var data = {}

    if(layanan) {
        data.jenis_layanan = layanan
    }

    if(pelanggan) {
        data.pelanggan = pelanggan
    }

    if(jumlah_node) {
        data.jumlah_node = jumlah_node
    }

	var param = $.param(data);
    param = decodeURIComponent( param )
    window.location = '<?=base_url("all-projects/")?>?' + param;
}

    function showHighlighted(project_update_id) {
		$.ajax({
			type:"post",
			url:"<?=base_url("project/get_project_update_detail")?>",
			data: {
				project_update_id:project_update_id
			},
			success:function(data) {

				var parse =  $.parseJSON(data)
				// console.log("parse ==> ",parse)
				
				var res = parse.data

				$("#show-highlighted").click()
                $("#modalHighlighted #project-update-id").val(res.id)
				$("#modalHighlighted #highlighted-desc").html(res.highlighted_desc)
			}

		})
	}

	function highlightedProcess(project_update_id) {
		$.ajax({
			url:"<?=base_url("project/project_update_highlighted")?>",
			method:"POST",
			data:{
				"project_update_id":project_update_id,
				"highlighted":1,
				"highlighted_desc":$("#modalHighlighted #highlighted-desc").val()
			},
			success:function(data) {
				//$('#form-check-input-highlighted').prop('checked', true); // Unchecks it
				Swal.fire({
					title: '<strong> Success !</strong>',
					icon: 'success',
					html:"You Successfully Highlight this project"
				})

				setTimeout(() => {
					location.reload()
				}, 3000);
			}
		})
	}



$(function(){

	$('#project-update-list').DataTable({
		"order": [[ 0, "desc" ]]
	});
	
	$('#project-update-list').each(function() {
      var datatable = $(this);
      // SEARCH - Add the placeholder for Search and Turn this into in-line form control
      var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
      search_input.attr('placeholder', 'Search');
      search_input.removeClass('form-control-sm');
      // LENGTH - Inline-Form control
      var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
      length_sel.removeClass('form-control-sm');
	});

    $("#filter-search").click(function(e){

        search()

    })

    $("#modalHighlighted").on("hide.bs.modal",function(){
       	setTimeout(() => {
            location.reload()
        }, 1000);
		
    })

    $("#form-highlighted-submit").click(function(){
       var project_update_id =   $("#modalHighlighted #project-update-id").val()
        highlightedProcess(project_update_id)
    })

    $(".highlighted-check").change(function(e){

        var project_update_id = $(this).val()

        if(this.checked) {
          
            showHighlighted(project_update_id)
        } else {

           
            Swal.fire({
                title: "Project Highlighted ",
                text: "Are you sure you want to remove the highlighted Project !",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'mr-2',
                confirmButtonText: 'Yes, Remove it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {

                if (result.value) {
                    $.ajax({
                        url:"<?=base_url("project/project_update_highlighted")?>",
                        method:"POST",
                        data:{
                            project_update_id:project_update_id,
                            "highlighted":0,
                            "highlighted_desc":""
                        },
                        success:function(data) {
                            $(this).prop('checked', false); // Unchecks it
                            Swal.fire({
                                title: '<strong> Success !</strong>',
                                icon: 'success',
                                html:"You Successfully remove this project from Highlight"
                            })
                        }
                    })
                } else {
                    $(this).prop('checked', true); 
                }
            })

            
        }
        
    })


})


</script>
