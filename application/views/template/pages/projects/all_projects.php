<div class="page-content">
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"> Main </li>
            <li class="breadcrumb-item"> Projects </li>
            <li class="breadcrumb-item active"> All Projects </li>
        </ol>
    </nav>

    <br/>
    <div class="row">
      <div class="col-lg-12 col-xl-12 stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-baseline mb-2">
              <h6 class="card-title mb-0">Project Filter</h6>

            </div>

            <form class="row">
              <div class="col-lg-3">

                <select style="display:inline-block;" class="js-example-basic-single" data-placeholder="Layanan :" id="jenis_layanan" name="jenis_layanan">
                  <option value="">Jenis Layanan :</option>
                  <?php

                      foreach($layanan_list as $row) {
												$selected = "";
												if($row["id"] == $this->input->get("jenis_layanan")) {
													$selected = "selected='selected'";
												}
                        echo "<option $selected value='$row[id]'>$row[nama] </option>";
                      }
                  ?>

                </select>
              </div>

              <div class="col-lg-3">
                <select style="display:inline-block;" class="js-example-basic-single" data-placeholder="Pelanggan :" id="pelanggan" name="pelanggan">
                  <option value="">Nama Pelanggan :</option>
                  <?php
                      foreach($pelanggan_list as $row2) {
												$selected = "";
												if($row2["id"] == $this->input->get("pelanggan")) {
													$selected = "selected='selected'";
												}
                        echo "<option $selected value='$row2[id]'>$row2[nama] </option>";
                      }
                  ?>

                </select>
              </div>

              <style>
                  select {
                    color: #444 !important;
                    }
                  option:not(:first-of-type) {
                    color: #444 !important;
                  }
              </style>

              <div class="col-lg-2">
                <select class="form-control" name="jumlah_node" id="jumlah_node">
                  <option value="">Jumlah Node :</option>
                  <option value="A" <?=$this->input->get("jumlah_node") == "A" ? "selected" : "" ?> > 1 - 10</option>
                  <option value="B" <?=$this->input->get("jumlah_node") == "B" ? "selected" : "" ?>> 11 - 50</option>
                  <option value="C" <?=$this->input->get("jumlah_node") == "C" ? "selected" : "" ?>> 51 - 100</option>
                  <option value="D" <?=$this->input->get("jumlah_node") == "D" ? "selected" : "" ?> > 100  </option>
                </select>
              </div>

              <div class="col-lg-2">
                <button style="height:35px;" type="button" id="filter-search" class="btn btn-primary btn-block">Filter</button>
              </div>
              <div class="col-lg-2">
                  <a style="height:35px;" href="<?= base_url("all-projects") ?>" class="btn btn-secondary btn-block">Clear Filter</a>
                 <!-- <button style="height:35px;" type="button" onclick="reset()" class="btn btn-secondary btn-block">Clear</button> -->
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                     <h4 class="card-header-title">Project List</h4>
                </div>
                <div class="card-body">
                    <button id="show-highlighted" style="display:none" data-toggle="modal" data-target="#modalHighlighted"></button>
                    <div class="row">

                    </div>
                    <div class="table-responsive">
                        <table id="project-update-list" class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <?php if($this->session->userdata("group_id") == 1){ ?>
                                      <th>HL</th>
                                    <?php } ?>
                                    <th>Nomor WO</th>
                                    <th>Layanan</th>
                                    <th>Pelanggan</th>
                                    <th>Project Code</th>
                                    <th>Tanggal Terima Order</th>
                                    <th>RFS</th>
                                    <th>Jumlah Node</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $x = 1; ?>
                                <?php foreach($data as $row) :

                                    $params["workorder_id"] = $row["h_workorder_id"];
                                    $project_update_detail = $this->projects_model->get_detail_data_project_update($params);

                                  ?>
                                    <tr>
                                        <td><?=$row["h_workorder_id"] ?></td>

                                        <?php if($this->session->userdata("group_id") == 1){ ?>

                                          <td>
                                            <?php if(isset($project_update_detail["highlighted"])){ ?>
                                            <div style="margin:0px;" class="form-check form-check-flat form-check-primary">
                                              <label class="form-check-label">

                                                <input type="checkbox" value="<?=$project_update_detail["project_update_id"]?>" class="form-check-input highlighted-check" <?=$project_update_detail["highlighted"] == 1 ? "checked" : ""?>>
                                                <i class="input-frame"></i>
                                              </label>

                                            </div>
                                            <?php } ?>
                                          </td>
                                        <?php } ?>
                                        <td> <a href="<?= base_url("all-projects/update/$row[h_workorder_id]") ?>" style="color:#727cf5; font-weight:bold;"> <?= $row["no_wo"] ?> </a> </td>
                                        <td>
                                            <?php
                                            foreach ($row["list_layanan"] as $row2) {
                                                echo "<span class='badge badge-warning'>$row2</span> &nbsp;";
                                            }
                                            ?>


                                        </td>
                                        <td><?= strlen($row["nama_pelanggan"]) > 15 ? substr($row["nama_pelanggan"],0,15)."..." : $row['nama_pelanggan'] ?></td>
                                        <td>
                                          <span style="font-weight:bold;">[<?= $row["project_id"]?>]</span>  / <?= strlen($row["project_nama"]) > 50 ? substr($row["project_nama"],0,50)."..." : $row['project_nama'] ?>
                                        </td>
                                        <td><?= $row["tanggal_terima_order"] ?></td>
                                        <td><?= $row["rfs"] ?></td>
                                        <td style="text-align:center;"><?= $row["jumlah_node"] ?></td>
                                        <td>
                                            <a  class="btn btn-danger" href="<?= base_url("all-projects/update/$row[h_workorder_id]") ?>">Detail</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalHighlighted">
  Launch demo modal
</button> -->

<!-- Modal Highligted -->
<?php $this->load->view("template/pages/projects/modals/highlighted-modal") ?>
