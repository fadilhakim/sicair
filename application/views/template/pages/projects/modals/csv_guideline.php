<div class="modal fade" id="guideLineModal" tabindex="-1" role="dialog" aria-labelledby="guideLineModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Contoh Format File CSV yang dapat diupload</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-12">
            <img style="width:100%" src="<?= base_url() ?>/assets/images/sample_img_csv_2.png" alt="">
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6">
            <br>
            <p style="color:#fa5b33; font-weight:bold;">Berikut urutan Kolom data dari yang harus di Isi:</p>
          </div>
          <div class="col-lg-6">

          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-lg-6">
            <ul class="list-group">
              <li class="list-group-item"><b>1. SID</b></li>
              <li class="list-group-item"><b>2. Status</b>
                <p>Pilihan Status dari sistem</p>
                <ul style="color:#fa5b33;">
                  <li>
                    Pengadaan
                  </li>
                  <li>
                    MOD (Material On Delivery)
                  </li>
                  <li>
                    MOS (Material On Site)
                  </li>
                  <li>
                    Intalasi
                  </li>
                  <li>
                    Online
                  </li>
                  <li>
                    Pending
                  </li>
                  <li>
                    Cancel
                  </li>
                  <li>
                    BAA Diterima
                  </li>
                </ul>
                <br>
                <p><span style="color:red;"> * </span>Pastikan status yang anda isi pada node sesuai dengan pilihan status dari sistem ini</p>
              </li>

              <li class="list-group-item">
                <b>3. Update Status Date</b>
                <p><span style="color:red;"> * </span> Adalah tanggal kapan status dari node ini berubah</p>
              </li>
            </ul>
          </div>
          <div class="col-lg-6">
            <ul class="list-group" style="margin-bottom:15px;">
              <li class="list-group-item"><b>4. Start Progress</b></li>
              <li class="list-group-item"><b>5. TOL</b></li>
              <li class="list-group-item"><b>6. Keterangan</b></li>
            </ul>
            <a download href="<?= base_url() ?>/assets/images/csv file sample for upload node -sd tracking.csv" class="btn btn-primary"> <i class="link-icon" data-feather="download"></i> Download Sample File</a>
          </div>




        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

      </div>
    </div>
  </div>
</div>
