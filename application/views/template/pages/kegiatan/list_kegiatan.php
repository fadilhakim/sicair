<div class="page-content">

  <div class="row">
    <div class="col-lg-12 col-xl-12 stretch-card">

      <div class="card">
        <div class="card-body">
          <h6 style="font-size:15px !important; color: #241373 !important;">Filter Kegiatan</h6>
          <br>
          <form method="get" action="">
            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label for="">Tahun Anggaran :</label>
                  <select class="js-example-basic-single w-100" name="filter_anggaran_id" class="form-control" id="filter_anggaran_id">
                    <option value="">Semua Tahun</option>
                    <option value="1" <?= $_GET["filter_anggaran_id"] == "1" ? "selected" : "" ?>>2021</option>
                    <option value="2" <?= $_GET["filter_anggaran_id"] == "2" ? "selected" : "" ?>>2022</option>
                    <option value="3" <?= $_GET["filter_anggaran_id"] == "3" ? "selected" : "" ?>>2023</option>
                    <option value="4" <?= $_GET["filter_anggaran_id"] == "4" ? "selected" : "" ?>>2024</option>
                    <option value="5" <?= $_GET["filter_anggaran_id"] == "5" ? "selected" : "" ?>>2025</option>
                  </select>
                </div>
              </div>
              <?php if ($this->session->userdata("group_id") != 2) { ?>
                <div class="col">
                  <div class="form-group">
                    <label for="">Nama PPK :</label>
                    <select class="js-example-basic-single w-100" data-width="100%" id="filter_nama_ppk" name="filter_nama_ppk">
                      <option value="">Semua PPK</option>
                      <?php foreach ($all_user as $rowuser => $value) {

                        $selected = "";
                        if ($value["id"] == $_GET["filter_nama_ppk"]) {
                          $selected = "selected";
                        }

                      ?>
                        <option value="<?= $value['id'] ?>" <?= $selected ?>><?= $value['fullname'] ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              <?php } ?>

              <div class="col">
                <div class="form-group">
                  <label for="">Prioritas :</label>
                  <select class="js-example-basic-single w-100" name="filter_prioritas" class="form-control" id="filter_prioritas">
                    <option value="">Semua Prioritas</option>
                    <option value="1" <?= $_GET["filter_prioritas"] == 1 ? "selected" : "" ?>>Ya</option>
                    <option value="2" <?= $_GET["filter_prioritas"] == 2 ? "selected" : "" ?>>Tidak</option>
                  </select>
                </div>
              </div>

              <div class="col">
                <div class="form-group">
                  <label for="">&nbsp;<br></label>
                  <br>
                  <button id="btn-filter-kegiatan" type="button" style="display: inlibe-block !important; width:70%; height:35px; background-color:#8a3cc1; border-color:#8a3cc1;" class="btn btn-primary"> <i class="link-icon" data-feather="filter"></i>&nbsp; Filter</button>
                  <!-- <a href="<?= base_url('kegiatan') ?>" type="button" style="" class="btn btn-secondary"> Clear</a> -->

                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div>

  <br>
  <style>
    #all-kegiatan-table_filter {
      display: inline-block;
      float: right;
      margin-bottom: 20px;
    }

    .dt-buttons {
      display: inline-block;
    }

    .tahun_anggaran .select2-container--default .select2-selection--single {
      width: 200px !important;
    }
  </style>
  <div class="row">
    <div class="col-lg-12 col-xl-12 stretch-card">
      <div class="card">
        <div class="card-body">
          <div class="d-flex justify-content-between align-items-baseline mb-2">
            <h6 class="card-title mb-0">Daftar Kegiatan</h6>
            <a style="color:#fff; margin-bottom:30px;" data-toggle="modal" data-target="#exampleModal" class="btn btn-primary"> <i class="link-icon" data-feather="plus"></i>&nbsp; Tambah Kegiatan</a>
          </div>
          <div class="table-responsive">
            <table ui-jp="dataTable" id="all-kegiatan-table" class="table table-hover mb-0">
              <thead>
                <tr>
                  <th class="pt-0">No</th>
                  <th class="pt-0">Nama Kegiatan</th>
                  <th class="pt-0">Pagu Kegiatan</th>
                  <th class="pt-0" style="text-align: center;">Kode Aktivitas/KRO/RO</th>
                  <th class="pt-0" style="text-align: center;">Komponen/Sub</th>
                  <th class="pt-0">Nama PPK</th>
                  <th class="pt-0" style="text-align: center;">Prioritas</th>
                  <!-- <th class="pt-0" style="text-align: center;">Created At</th> -->
                  <th class="pt-0">Aksi</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!--modal form tambah kegiatan -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Kegiatan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="form-tambah-kegiatan">
        <div class="modal-body">
          <form id="form-tambah-kegiatan">
            <div class="row">
              <div class="col-lg-6">
                <div class="form-group tahun_anggaran">
                  <label style="display: block;" for=>Pilih Tahun Anggaran :</label>
                  <select required style="display: block;" class="js-example-basic-single w-100 form-control" id="anggaran_id" name="anggaran_id">
                    <option value="1">2021</option>
                    <option value="2">2022</option>
                    <option value="3">2023</option>
                    <option value="4">2024</option>
                  </select>
                </div>

                <div class="form-group">
                  <label for="exampleInputUsername1">Kode Aktivitas :</label>
                  <input type="text" style="text-transform:uppercase" id="kode_aktivitas" name="kode_aktivitas" class="form-control">
                </div>
                <div class="form-group">
                  <label for="exampleInputUsername1">Kode Kro :</label>
                  <input type="text" style="text-transform:uppercase" id="kro" name="kro" class="form-control">
                </div>
                <div class="form-group">
                  <label for="exampleInputUsername1">Kode RO :</label>
                  <input id="ro" style="text-transform:uppercase" type="text" name="ro" class="form-control">
                </div>
                <div class="form-group">
                  <label for="exampleInputUsername1">Kode Komponen :</label>
                  <input id="komponen" style="text-transform:uppercase" name="komponen" type="text" class="form-control">
                </div>
                <div class="form-group">
                  <label for="exampleInputUsername1">Kode Sub Komponen :</label>
                  <input id="subkomponen" style="text-transform:uppercase" name="subkomponen" type="text" class="form-control">
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <label for="exampleInputUsername1">Pagu Kegiatan :</label>
                  <input id="pagu_kegiatan" name="pagu_kegiatan" data-a-sign="Rp. " data-a-dec="," data-a-sep="." type="text" class="form-control money">
                </div>
                <div class="form-group">
                  <label for="exampleInputUsername1">Nama PPK :</label>
                  <div class="form-group">
                    <select class="js-example-basic-single w-100" data-width="100%" id="id_user" name="id_user">
                      <?php foreach ($all_user as $rowuser => $value) { ?>
                        <option value="<?= $value['id'] ?>"><?= $value['fullname'] ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputUsername1">Nama Kegiatan : </label>
                  <textarea id="nama_kegiatan" style="text-transform:uppercase" name="nama_kegiatan" id="maxlength-textarea" class="form-control" maxlength="300" rows="8"></textarea>
                </div>

                <div class="form-group">
                  <div class="form-check form-check-flat form-check-primary">
                    <label class="form-check-label">
                      <input id="prioritas" name="prioritas" type="checkbox" class="form-check-input">
                      <i class="input-frame"></i>Check untuk kegiatan prioritas</label>
                  </div>
                </div>


              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
          <button id="tambah-kegiatan-btn" class="float-right btn btn-primary success" type="button">Submit Kegiatan</button>
        </div>
      </form>
    </div>
  </div>
</div>