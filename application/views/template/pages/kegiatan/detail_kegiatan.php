<div class="page-content">
    <nav class="page-breadcrumb">
        <ol style="display: inline-block; width:80%" class="breadcrumb">
            <li style="display: inline-block;" class="breadcrumb-item">Detail Kegiatan</li>
            <li style="display: inline-block;" class="breadcrumb-item active" aria-current="page"><?= $data_kegiatan["nama_kegiatan"] ?></li>
        </ol>
        <!-- <button style="float:right;" onclick="confirmationDeleteKegiatan(<?= $data_kegiatan['id'] ?>)" class="btn btn-danger">Delete Kegiatan</button> -->
    </nav>
    <br>
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">

                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline mb-2">
                        <h6 class="card-title mb-0">Kegiatan Utama</h6>
                    </div>
                    <br>

                    <form id="form-edit-kegiatan">
                        <div class="row">
                            <div class="col">

                                <div class="form-group">
                                    <label for="">Kode Aktivitas :</label>
                                    <input disabled readonly id="kode_aktivitas" name="kode_aktivitas" value="<?= $data_kegiatan["kode_aktivitas"] ?>" type="text" class="form-control">
                                </div>

                            </div>

                            <div class="col">

                                <div class="form-group">
                                    <label for="">KRO :</label>
                                    <input disabled readonly id="kro" name="kro" value="<?= $data_kegiatan["kro"] ?>" type="text" class="form-control">
                                </div>

                            </div>

                            <div class="col">

                                <div class="form-group">
                                    <label for="">RO :</label>
                                    <input disabled readonly id="ro" name="ro" value="<?= $data_kegiatan["ro"] ?>" type="text" class="form-control">
                                </div>

                            </div>

                            <div class="col">

                                <div class="form-group">
                                    <label for="">Komponen :</label>
                                    <input disabled readonly id="komponen" name="komponen" value="<?= $data_kegiatan["komponen"] ?>" type="text" class="form-control">
                                </div>

                            </div>

                            <div class="col">

                                <div class="form-group">
                                    <label for="">Subkomponen :</label>
                                    <input disabled readonly id="subkomponen" name="subkomponen" value="<?= $data_kegiatan["subkomponen"] ?>" type="text" class="form-control">
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col">

                                <div style="display: none;" class="form-group">
                                    <label for="">id Kegiatan</label>
                                    <input readonly id="id_kegiatan" name="id_kegiatan" value="<?= $data_kegiatan["id"] ?>" type="text" class="form-control">
                                </div>

                                <div style="display: none;" class="form-group">
                                    <label for="">id Kegiatan</label>
                                    <input readonly id="id_anggaran" name="id_anggaran" value="<?= $data_kegiatan["id_anggaran"] ?>" type="text" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="">Nama Kegiatan :</label>
                                    <input disabled readonly id="nama_kegiatan" name="nama_kegiatan" value="<?= $data_kegiatan["nama_kegiatan"] ?>" type="text" class="form-control">
                                </div>


                            </div>

                            <div class="col">
                                <div class="form-group">
                                    <label for="">Pagu Kegiatan :</label>
                                    <input disabled readonly id="pagu_kegiatan" name="pagu_kegiatan" data-a-sign="Rp. " data-a-dec="," data-a-sep="." value="<?= $data_kegiatan["pagu_kegiatan"] ?>" type="text" class="form-control money">
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group">
                                    <label for="">Nama PPK :</label>
                                    <select disabled readonly class="js-example-basic-single w-100" data-width="100%" name="id_user" id="id_user">
                                        <?php

                                        $ppk = $this->user_model->get_all_user_ppk();

                                        foreach ($ppk as $rowuser) {
                                            $selected = '';
                                            if ($rowuser['id'] == $data_kegiatan['id_user']) {
                                                $selected = 'selected';
                                            }
                                        ?>
                                            <option <?= $selected ?> value="<?= $rowuser['id'] ?>"><?= $rowuser['fullname'] ?></option>
                                        <?php

                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                        </div>


                        <!-- <button id="edit_kegiatan" type="button" class="btn btn-warning mr-2">Edit</button> -->
                        <!-- <button class="btn btn-light">Batal</button> -->
                    </form>

                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline mb-2">
                        <h6 class="card-title mb-0">Daftar Detail Kegiatan</h6>
                        <a style="color:#fff;" data-toggle="modal" data-target="#AddDetailKegiatan" class="btn btn-primary"> <i class="link-icon" data-feather="plus"></i>&nbsp; Tambah Detail Kegiatan</a>
                    </div>
                    <br>
                    <br>
                    <div class="table-responsive">
                        <table id="list-detail-kegiatan" class="table table-hover table-striped mb-0">
                            <thead>
                                <tr>
                                    <th class="pt-0">Detail Kegiatan</th>
                                    <th class="pt-0">Volume</th>
                                    <th class="pt-0">Pagu Kegiatan</th>
                                    <th class="pt-0">Kode MAK</th>
                                    <th class="pt-0">Uraian MAK</th>
                                    <th class="pt-0">Aksi</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- modal form tambah detail kegiatan -->

<div class="modal fade" id="AddDetailKegiatan" tabindex="-1" role="dialog" aria-labelledby="AddDetailKegiatan" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Detail Kegiatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-tambah-detail-kegiatan" action="">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="form-group" style="display:none;">
                                <label for="">Id Kegiatan:</label>
                                <input id="id_kegiatan" name="id_kegiatan" value="<?= $data_kegiatan['id'] ?>" type="text" class="form-control">
                            </div>

                            <div class="form-group" style="display: none;">
                                <label for="">Id user:</label>
                                <input id="id_user" name="id_user" value="<?= $data_kegiatan['id_user'] ?>" type="text" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="">Kode MAK :</label>
                                <select class="js-example-basic-single w-100 getMakDetailAdd" data-width="100%" name="kode_mak_add" id="kode_mak_add">
                                    <?php
                                    $mak = $this->kegiatan_model->list_mak();

                                    foreach ($mak as $rowTambahMak) { ?>
                                        <option value="<?= $rowTambahMak['kode_mak'] ?>"><?= $rowTambahMak['kode_mak'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">Uraian MAK :</label>
                                <textarea id="uraian_mak" name="uraian_mak" rows="5" class="form-control"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="">Volume :</label>
                                <input type="text" class="form-control" id="volume" name="volume">
                            </div>

                            <div class="form-group">
                                <label for="">Detail Nama Kegiatan :</label>
                                <textarea id="detail_kegiatan" name="detail_kegiatan" rows="5" class="form-control"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="">PAGU Anggaran :</label>
                                <input id="pagu_detail_kegiatan" data-a-sign="Rp. " data-a-dec="," data-a-sep="." name="pagu_detail_kegiatan" type="text" class="form-control money">
                            </div>


                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button id="tambahDetailKegiatan" type="button" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- modal form edit detail kegiatan -->

<div class="modal fade" id="EditDetailKegiatan" tabindex="-1" role="dialog" aria-labelledby="EditDetailKegiatan" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Detail Kegiatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-edit-detail-kegiatan" action="">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="form-group" style="display:none;">
                                <label for="">Id Kegiatan:</label>
                                <input id="id_kegiatan_detail" name="id_kegiatan_detail" value="" type="text" class="form-control">
                            </div>

                            <div class="form-group" style="display: none;">
                                <label for="">Id user:</label>
                                <input id="id_user" name="id_user" value="<?= $data_kegiatan['id_user'] ?>" type="text" class="form-control">
                            </div>

                            <div class="form-group" style="display:none;">
                                <label for="">Id Kegiatan:</label>
                                <input id="id_kegiatan" name="id_kegiatan" value="" type="text" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="">Kode MAK :</label>
                                <select class="js-example-basic-single w-100 getMakDetail" data-width="100%" name="kode_mak" id="kode_mak">
                                    <?php

                                    $mak = $this->kegiatan_model->list_mak();
                                    foreach ($mak as $rowmak) {
                                        $detail_kegiatan = $this->kegiatan_model->detail_detail_kegiatan_by_mak($rowmak['kode_mak']);
                                    ?>

                                        <option value="<?= $rowmak['kode_mak'] ?>"><?= $rowmak['kode_mak'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">Uraian MAK :</label>
                                <textarea style="text-transform:uppercase" id="uraian_mak" name="uraian_mak" value="" rows="5" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Volume :</label>
                                <input type="text" class="form-control" id="volume" name="volume">
                            </div>

                            <div class="form-group">
                                <label for="">Detail Nama Kegiatan :</label>
                                <textarea style="text-transform:uppercase" id="detail_kegiatan" value="" name="detail_kegiatan" rows="5" class="form-control"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="">PAGU Anggaran :</label>
                                <input id="pagu_detail_kegiatan" data-a-sign="Rp. " data-a-dec="," data-a-sep="." value="" name="pagu_detail_kegiatan" type="text" class="form-control money">
                            </div>


                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button id="edit-detail-kegiatan-btn" type="button" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>