<!-- plugin js for this page -->
<script src="<?= base_url("assets/vendors/datatables.net/jquery.dataTables.js") ?>"></script>
<script src="<?= base_url("assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js") ?>"></script>
<script src="<?= base_url("assets/vendors/sweetalert2/sweetalert2.min.js") ?>"></script>
<script src="<?= base_url("assets/vendors/select2/select2.min.js") ?>"></script>
<script src="<?= base_url("assets/js/select2.js") ?>"></script>
<!-- end plugin js for this page -->



<script src=" https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>

<script>
    function getAllKegiatan() {

        $('#all-kegiatan-table').DataTable({
            dom: 'Bfrtip',
            // order: [
            //     [6, 'DESC']
            // ],
            buttons: [{
                text: 'Export to Excel',
                extend: 'excelHtml5',
                title: 'Daftar Kegiatan',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, ]
                }
            }],
            serverSide: true,
            ajax: {
                data: {
                    filter_nama_ppk: $("#filter_nama_ppk").val(),
                    filter_anggaran_id: $("#filter_anggaran_id").val(),
                    filter_prioritas: $("#filter_prioritas").val()
                },
                url: "<?= base_url("kegiatan/get_all_kegiatan") ?>",
                type: 'POST'
            },
            columns: [{
                    "data": "no"

                },
                {
                    "data": "nama_kegiatan"

                },
                {
                    "data": "pagu_kegiatan"

                },
                {
                    "data": "kode_aktivitas"

                },
                {
                    "data": "komponen"

                },
                {
                    "data": "id_user"

                },
                {
                    "data": "prioritas"

                },
                // {
                //     "data": "created_at"

                // },
                {
                    data: "Aksi"

                }
            ],
            columnDefs: [{
                    "sorting": true,
                    "orderable": true,
                    "type": "html",
                    "targets": 1,
                    "render": function(data, type, row) {

                        const newRow = row
                        return `<a href="<?= base_url() ?>kegiatan/detail_kegiatan/${row.id}">${row.nama_kegiatan}</a>`
                    }
                },
                {
                    "sorting": true,
                    "orderable": true,
                    "type": "html",
                    "targets": 2,
                    "render": function(data, type, row) {

                        const newRow = row
                        return `<span style="display:block; text-align:center; font-weight:bold;">Rp. ${row.pagu_kegiatan}</span>`
                    }
                },
                {
                    "sorting": true,
                    "orderable": true,
                    "type": "html",
                    "targets": 3,
                    "render": function(data, type, row) {

                        const newRow = row
                        return `<span style="display:block; text-align:center; font-weight:bold;">${row.kode_aktivitas}/${row.kro}/${row.ro}</span>`
                    }
                },
                {
                    "sorting": true,
                    "orderable": true,
                    "type": "html",
                    "targets": 4,
                    "render": function(data, type, row) {

                        const newRow = row
                        return `<span style="display:block; text-align:center;">${row.komponen}/${row.subkomponen}</span>`
                    }
                },
                {
                    "sorting": true,
                    "orderable": true,
                    "type": "html",
                    "targets": 5,
                    "render": function(data, type, row) {
                        //get_user_in_table(row.id_user)
                        return `${row.id_user}`
                    }
                },
                {
                    "sorting": true,
                    "orderable": true,
                    "type": "html",
                    "targets": 6,
                    "render": function(data, type, row) {

                        const newRow = row
                        var check = 'display:none';
                        if (row.prioritas == 1) {
                            check = 'display:block'
                        }
                        return `<badge style="${check}; text-align:center; width:60px; margin:auto;" class="badge badge-info"> Priority</badge>`
                    }
                },
                {
                    "sorting": true,
                    "orderable": true,
                    "type": "html",
                    "targets": 7,
                    // "data": id,
                    "render": function(data, type, row) {
                        //console.log("data => ", data, type, row)

                        const newRow = row

                        return `<a href="kegiatan/detail_kegiatan/${row.id}" class="btn btn-primary">Lihat Detail</a>`
                    }
                }
            ]
        })
    }

    function edit_kegiatan() {

        var form_edit_anggaran = $("#form-edit-kegiatan")[0]

        Swal.fire({
            title: "Edit Kegiatan ",
            text: "Apakah anda yakin ingin meng-edit Kegiatan ini?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'mr-2',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true,
            closeOnConfirm: true
        }).then((result) => {

            if (result.value) {

                var kode_aktivitas = $('#kode_aktivitas').val();
                var kro = $('#kro').val();
                var ro = $('#ro').val();
                var komponen = $('#komponen').val();
                var subkomponen = $('#subkomponen').val();
                var nama_kegiatan = $('#nama_kegiatan').val();
                var pagu_kegiatan = $('#pagu_kegiatan').val();
                var id_user = $('#id_user').val();
                var id_kegiatan = $('#id_kegiatan').val();
                var id_anggaran = $('#id_anggaran').val();
                // var data = new FormData(form_edit_anggaran);
                // console.log(data)
                console.log(id_user);
                $.ajax({
                    url: "<?= base_url("kegiatan/edit_kegiatan") ?>",
                    type: "post",
                    data: {
                        kode_aktivitas: kode_aktivitas,
                        id_kegiatan: id_kegiatan,
                        komponen: komponen,
                        subkomponen: subkomponen,
                        nama_kegiatan: nama_kegiatan,
                        pagu_kegiatan: pagu_kegiatan,
                        kro: kro,
                        ro: ro,
                        id_user: id_user,
                        id_anggaran: id_anggaran
                    },
                    dataType: "json",
                    success: function(data) {

                        if (data.success) {
                            Swal.fire({
                                title: '<strong> Success !</strong>',
                                icon: 'success',
                                html: data.message
                            })

                            setTimeout(() => {
                                window.location.reload()
                            }, 2000);
                        } else {
                            Swal.fire({
                                title: '<strong> Error !</strong>',
                                icon: 'error',
                                html: data.message

                            })
                        }



                    }

                })
            }
        })
    }

    function getAllListDetailKegiatan() {

        var form = "#form-filter-detail-kegiatan"
        var form2 = "#form-main-kegiatan"

        $('#list-detail-kegiatan').DataTable({
            dom: 'Bfrtip',
            order: [
                [1, 'desc']
            ],
            buttons: [{
                text: 'Export to Excel',
                extend: 'excelHtml5',
                title: 'Daftar Detail Kegiatan',
                exportOptions: {
                    columns: [0, 1, 2, 3]
                }
            }],
            serverSide: true,
            ajax: {
                data: {
                    id_kegiatan: $("#id_kegiatan").val()
                },
                url: "<?= base_url("kegiatan/get_detail_list_kegiatan") ?>",
                type: 'POST'
            },
            columns: [{
                    "data": "detail_kegiatan"

                },
                {
                    "data": "volume"

                },
                {
                    "data": "pagu_kegiatan"
                },
                {
                    "data": "kode_mak"

                },
                {
                    "data": "uraian_mak"

                },
                {
                    data: "Aksi"

                }
            ],
            columnDefs: [{
                    "sorting": true,
                    "orderable": true,
                    "type": "html",
                    "targets": 2,

                    "render": function(data, type, row) {
                        const newRow = row
                        return `<span style="font-size:15px;font-weight:bold;" class="">Rp. ${row.pagu_kegiatan} </span>`
                    }
                },
                {

                    "sorting": true,
                    "orderable": true,
                    "type": "html",
                    "targets": 5,
                    "render": function(data, type, row) {
                        const newRow = row
                        return `<a data-toggle="modal" data-target="#EditDetailKegiatan" onClick="showModalDetailKegiatan(${row.id})" class="btn btn-info">Detail</a> <a onClick="confirmationDeleteDetailKegiatan(${row.id})" class="btn btn-danger">Delete</a>`
                    }
                }
            ]
        })
    }

    function tambah_kegiatan(data) {
        var form_tambah_kegiatan = $("#form-tambah-kegiatan")[0];

        Swal.fire({
            title: "Tambah Kegiatan ",
            text: "Apakah anda yakin ingin menambahkan Kegiatan ini",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'mr-2',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true,
            closeOnConfirm: true
        }).then((result) => {

            if (result.value) {

                var kode_aktivitas = $('#kode_aktivitas').val();
                var kro = $('#kro').val();
                var ro = $('#ro').val();
                var komponen = $('#komponen').val();
                var subkomponen = $('#subkomponen').val();
                var pagu_kegiatan = $('#pagu_kegiatan').val();
                var id_user = $('#id_user').val();
                var nama_kegiatan = $('#nama_kegiatan').val();
                var anggaran_id = $('#anggaran_id').val();
                var prioritas = 0;
                if ($('#prioritas').is(':checked')) {
                    prioritas += 1;
                } else {
                    prioritas = 2;
                }

                $.ajax({
                    url: "<?= base_url("anggaran/tambah_kegiatan_process") ?>",
                    type: "post",
                    data: {
                        kode_aktivitas: kode_aktivitas,
                        kro: kro,
                        ro: ro,
                        komponen: komponen,
                        subkomponen: subkomponen,
                        pagu_kegiatan: pagu_kegiatan,
                        id_user: id_user,
                        nama_kegiatan: nama_kegiatan,
                        prioritas: prioritas,
                        anggaran_id: anggaran_id
                    },
                    dataType: "json",
                    success: function(data) {

                        if (data.success) {
                            Swal.fire({
                                title: '<strong> Success !</strong>',
                                icon: 'success',
                                html: data.message
                            })

                            setTimeout(() => {
                                window.location.reload()
                            }, 2000);
                        } else {
                            Swal.fire({
                                title: '<strong> Error !</strong>',
                                icon: 'error',
                                html: data.message

                            })
                        }



                    }

                })
            }
        })
    }

    function confirmationDeleteKegiatan(kegiatan_id) {
        Swal.fire({
            title: 'Delete Kegiatan',
            text: "Apakah anda yakin ingin menghapus kegiatan ini !?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'mr-2',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {

            if (result.value) {
                delete_kegiatan(kegiatan_id)

                console.log('kesini')
            } else if (
                // Read more about handling dismissals
                result.dismiss === Swal.DismissReason.cancel
            ) {

            }
        })
    }

    function delete_kegiatan(kegiatan_id) {
        $.ajax({
            url: "<?= base_url("kegiatan/delete_kegiatan") ?>",
            type: "post",
            data: {
                kegiatan_id: kegiatan_id
            },

            dataType: "json",
            success: function(res) {

                if (res.success === true) {

                    Swal.fire(
                        'Success!',
                        res.message,
                        'success'
                    )
                    setTimeout(() => {
                        window.location = '<?= base_url("kegiatan") ?>';
                    }, 2000);

                } else {
                    Swal.fire({
                        title: '<strong> Error !</strong>',
                        icon: 'error',
                        html: res.message
                    })
                }

            },
            error: function(jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }

                console.log(" err msg ==> ", msg)
            }
        })
    }

    function tambah_detail_kegiatan() {
        var form_detail_tambah_kegiatan = $("#form-tambah-detail-kegiatan")[0];

        Swal.fire({
            title: "Tambah Detail Kegiatan ",
            text: "Apakah anda yakin ingin menambahkan Detail Kegiatan ini",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'mr-2',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true,
            closeOnConfirm: true
        }).then((result) => {

            if (result.value) {

                var kode_mak = $('#kode_mak_add').val();
                var uraian_mak = $('#uraian_mak').val();
                var detail_kegiatan = $('#detail_kegiatan').val();
                var pagu_detail_kegiatan = $('#pagu_detail_kegiatan').val();
                var id_kegiatan = $('#id_kegiatan').val();
                var volume = $('#volume').val();
                var id_user = $('#id_user').val();
                $.ajax({
                    url: "<?= base_url("kegiatan/tambah_detail_kegiatan_process") ?>",
                    type: "post",
                    data: {
                        kode_mak: kode_mak,
                        uraian_mak: uraian_mak,
                        detail_kegiatan: detail_kegiatan,
                        pagu_detail_kegiatan: pagu_detail_kegiatan,
                        id_kegiatan: id_kegiatan,
                        volume: volume,
                        id_user: id_user
                    },
                    dataType: "json",
                    success: function(data) {

                        if (data.success) {
                            Swal.fire({
                                title: '<strong> Success !</strong>',
                                icon: 'success',
                                html: data.message
                            })

                            setTimeout(() => {
                                window.location.reload()
                            }, 2000);
                        } else {
                            Swal.fire({
                                title: '<strong> Error !</strong>',
                                icon: 'error',
                                html: data.message

                            })
                        }



                    }

                })
            }
        })
    }

    function showModalDetailKegiatan(id) {
        $("button#edit-detail-kegiatan-btn").attr("onclick", "edit_kegiatan_detail_submit()")
        console.log(id)
        $.ajax({
            url: "<?= base_url("kegiatan/get_kegiatan_detail") ?>",
            data: {
                id: id
            },
            type: "post",
            dataType: "json",
            success: function(res) {
                console.log(res)
                var form = "form#form-edit-detail-kegiatan"
                $(form + " #id_kegiatan_detail").val(res.data.id)
                $(form + " #volume").val(res.data.volume)
                $(form + " #kode_mak").val(res.data.kode_mak).trigger('change')
                ///$(form + ' #kode_mak').select2().select2('val', res.data.kode_mak);
                $(form + " #uraian_mak").val(res.data.uraian_mak)
                $(form + " #detail_kegiatan").val(res.data.detail_kegiatan)
                $(form + " #pagu_detail_kegiatan").val(res.data.pagu_kegiatan)
                $(form + " #id_kegiatan").val(res.data.id_kegiatan)
            }
        })
    }

    function edit_kegiatan_detail_submit() {
        Swal.fire({
                title: "Update Kegiatan ",
                text: "Apakah anda yakin ingin men-update detail kegiatan ini ?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'mr-2',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {

                if (result.value) {

                    const form = "#form-edit-detail-kegiatan"

                    const data = {
                        id_kegiatan_detail: $(form + " #id_kegiatan_detail").val(),
                        kode_mak: $(form + " #kode_mak").val(),
                        uraian_mak: $(form + " #uraian_mak").val(),
                        volume: $(form + " #volume").val(),
                        detail_kegiatan: $(form + " #detail_kegiatan").val(),
                        pagu_detail_kegiatan: $(form + " #pagu_detail_kegiatan").val(),
                        id_kegiatan: $(form + " #id_kegiatan").val(),
                    }
                    console.log(data)

                    $.ajax({
                        url: "<?= base_url("kegiatan/update_process_detail_kegiatan") ?>",
                        type: 'post',
                        data: data,
                        dataType: 'json',
                        success: function(res) {
                            if (res.success === true) {
                                $('#list-detail-kegiatan').DataTable().ajax.reload();
                                $("#EditDetailKegiatan").modal('hide');

                                Swal.fire(
                                    'Success!',
                                    res.message,
                                    'success'
                                )
                            } else {
                                Swal.fire({
                                    title: '<strong> Error !</strong>',
                                    icon: 'error',
                                    html: res.message
                                })
                            }
                        }
                    })
                }


            })
            .catch(err => {
                console.log('tidak terupdate')
            })
    }

    function confirmationDeleteDetailKegiatan(detail_kegiatan_id) {
        Swal.fire({
            title: 'Delete Detail Kegiatan',
            text: "Apakah anda yakin ingin menghapus detail kegiatan ini !?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'mr-2',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {

            if (result.value) {
                delete_detail_kegiatan(detail_kegiatan_id)

                console.log('kesini')
            } else if (
                // Read more about handling dismissals
                result.dismiss === Swal.DismissReason.cancel
            ) {

            }
        })
    }

    function delete_detail_kegiatan(detail_kegiatan_id) {
        $.ajax({
            url: "<?= base_url("kegiatan/delete_detail_kegiatan") ?>",
            type: "post",
            data: {
                detail_kegiatan_id: detail_kegiatan_id
            },

            dataType: "json",
            success: function(res) {

                if (res.success === true) {

                    Swal.fire(
                        'Success!',
                        res.message,
                        'success'
                    )
                    setTimeout(() => {
                        $('#list-detail-kegiatan').DataTable().ajax.reload();
                    }, 2000);

                } else {
                    Swal.fire({
                        title: '<strong> Error !</strong>',
                        icon: 'error',
                        html: res.message
                    })
                }

            },
            error: function(jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }

                console.log(" err msg ==> ", msg)
            }
        })
    }

    function getUraianMak() {
        $.ajax({
            url: "<?= base_url("kegiatan/getUraianMak") ?>",
            data: {
                kode_mak: $('#EditDetailKegiatan #kode_mak').val()
            },
            type: "post",
            dataType: "json",
            success: function(res) {
                $('#form-edit-detail-kegiatan #uraian_mak').val(res.results.uraian_mak)
            }
        })
    }

    function getUraianMakAdd() {
        $.ajax({
            url: "<?= base_url("kegiatan/getUraianMak") ?>",
            data: {
                kode_mak: $('#AddDetailKegiatan #kode_mak_add').val()
            },
            type: "post",
            dataType: "json",
            success: function(res) {
                $('#form-tambah-detail-kegiatan #uraian_mak').val(res.results.uraian_mak)
            }
        })
    }

    function filterKegiatan() {

        var filter_nama_ppk = $("#filter_nama_ppk").val()
        var filter_anggaran_id = $("#filter_anggaran_id").val()
        var filter_prioritas = $("#filter_prioritas").val()
        // console.log(filter_nama_ppk)
        let data = {}

        if (filter_nama_ppk) {
            data.filter_nama_ppk = filter_nama_ppk
        }

        if (filter_anggaran_id) {
            data.filter_anggaran_id = filter_anggaran_id
        }

        if (filter_prioritas) {
            data.filter_prioritas = filter_prioritas
        }

        var param = $.param(data);
        param = decodeURIComponent(param)
        console.log(data)
        window.location = '<?= base_url("kegiatan/") ?>?' + param;

    }

    $(".getMakDetail").change(function(e) {
        //alert('ge uraian mak')
        getUraianMak()
    })

    $(".getMakDetailAdd").change(function(e) {
        //alert('ge uraian mak')
        getUraianMakAdd()
    })

    $(document).on('click', '#edit_kegiatan', function(e) {
        edit_kegiatan()
    })
    $(document).on('click', '#tambah-kegiatan-btn', function(e) {
        tambah_kegiatan()
    })

    $(document).on('click', '#delete-kegiatan-btn', function(e) {
        delete_kegiatan()
    })

    $(document).on('click', '#tambahDetailKegiatan', function(e) {
        tambah_detail_kegiatan()
    })

    $(document).on('click', '#btn-delete-detail-kegiatan', function(e) {
        confirmationDeleteDetailKegiatan()
    })

    // $(document).on('click', '#btn-filter-kegiatan', function(e) {
    //     filterKegiatan()
    // })

    $("#btn-filter-kegiatan").click(function(e) {
        //alert("filter...")
        filterKegiatan()
    })

    $(function() {

        getAllKegiatan()
        getAllListDetailKegiatan()

        $(".money").autoNumeric("init", {
            aForm: true,
            vMax: "999999999999999"
        });

    })
</script>