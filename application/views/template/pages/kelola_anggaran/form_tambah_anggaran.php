<div class="page-content">
  <nav class="page-breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item">Kelola Anggaran</li>
      <li class="breadcrumb-item active" aria-current="page">Tambah Anggaran</li>
    </ol>
  </nav>

  <div class="row">
    <div class="col-lg-12 col-xl-12 stretch-card">
      <div class="card">

        <div class="card-body">
          <div class="d-flex justify-content-between align-items-baseline mb-2">
            <h6 class="card-title mb-0">Form Tambah Anggaran</h6>
          </div>
          <br>

          <form id="form-tambah-anggaran" class="forms-sample">
            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label for="">Tahun Anggaran</label>
                  <input required id="tahun_anggaran" name="tahun_anggaran" type="number" class="form-control">
                </div>

                <div class="form-group">
                  <label for="exampleInputUsername1">Kode SATKER</label>
                  <input required id="kode_satker" name="kode_satker" type="text" class="form-control">
                </div>

                <div class="form-group">
                  <label for="exampleInputUsername1">Nama SATKER</label>
                  <input required id="nama_satker" style="text-transform:uppercase" name="nama_satker" type="text" class="form-control">
                </div>

              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <label for="exampleInputUsername1">Nomor DIPA</label>
                  <input required id="nomor_dipa" name="nomor_dipa" type="text" class="form-control">
                </div>

                <div class="form-group">
                  <label for="exampleInputUsername1">Tanggal</label>
                  <input required id="tanggal_dipa" name="tanggal_dipa" type="date" class="form-control">
                </div>

                <div class="form-group">
                  <label for="exampleInputUsername1">Pagu Anggaran</label>
                  <input required id="pagu_anggaran" name="pagu_anggaran" data-a-sign="Rp. " data-a-dec="," data-a-sep="." type="text" class="form-control money">
                </div>

              </div>

            </div>

            <button id="tambah-anggaran-btn" class="float-right btn btn-primary success" type="button">Submit Anggaran</button>
            <button type="reset" style="margin-right:10px;" class=" float-right btn btn-light">Batal</button>
          </form>

        </div>
      </div>
    </div>
  </div>
  <!-- <br>
  <div class="row">
    <div class="col-lg-12 col-xl-12 stretch-card">
      <div class="card">
        <div class="card-body">
          <div class="d-flex justify-content-between align-items-baseline mb-2">
            <h6 class="card-title mb-0">Daftar Kegiatan</h6>
            <a style="color:#fff;" data-toggle="modal" data-target="#exampleModal" class="btn btn-primary"> <i class="link-icon" data-feather="plus"></i>&nbsp; Tambah Kegiatan</a>
          </div>
          <br>
          <br>
          <div class="table-responsive">
            <table class="table table-hover table-striped mb-0">
              <thead>
                <tr>
                  <th class="pt-0">#</th>
                  <th class="pt-0">Kode Aktivitas/KRO/RO</th>
                  <th class="pt-0">Komponen/Sub</th>
                  <th class="pt-0">Nama Kegiatan</th>
                  <th class="pt-0">Pagu</th>
                  <th class="pt-0">Nama PPK</th>
                  <th class="pt-0">Prioritas</th>
                  <th class="pt-0">Aksi</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div> -->
</div>


<!--modal form tambah kegiatan -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Kegiatan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-6">
            <div class="form-group">
              <label for="exampleInputUsername1">Kode Aktivitas :</label>
              <input type="text" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputUsername1">Kode Kro :</label>
              <input type="text" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputUsername1">Kode RO :</label>
              <input type="text" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputUsername1">Kode Komponen :</label>
              <input type="text" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputUsername1">Kode Sub Komponen :</label>
              <input type="text" class="form-control">
            </div>
          </div>
          <div class="col-lg-6">
            <div class="form-group">
              <label for="exampleInputUsername1">Pagu :</label>
              <input type="text" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputUsername1">Nama PPK :</label>
              <div class="form-group">
                <select class="js-example-basic-single w-100" data-width="100%">
                  <option value="TX">Texas</option>
                  <option value="NY">New York</option>
                  <option value="FL">Florida</option>
                  <option value="KN">Kansas</option>
                  <option value="HW">Hawaii</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="exampleInputUsername1">Nama Kegiatan : </label>
              <textarea id="maxlength-textarea" class="form-control" maxlength="200" rows="8" placeholder="This textarea has a limit of 200 chars."></textarea>
            </div>
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary">Simpan</button>
      </div>
    </div>
  </div>
</div>


<!-- modal form tambah detail -->

<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Detail </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-12">
            <div class="form-group">
              <label for="exampleInputUsername1">Kode MAK :</label>
              <input type="text" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputUsername1">Nama Detail Kegiatan :</label>
              <input type="text" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputUsername1">PAGU :</label>
              <input type="text" class="form-control">
            </div>

          </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary">Simpan</button>
      </div>
    </div>
  </div>
</div>