<div class="page-content">
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Data Anggaran</li>
            <li class="breadcrumb-item active" aria-current="page"><?= $anggaran_detail['nama_satker'] ?> - <?= $anggaran_detail['nomor_dipa'] ?>/<?= $anggaran_detail['tanggal_dipa'] ?></li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">

                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline mb-2">
                        <h6 class="card-title mb-0">Data Anggaran</h6>
                    </div>
                    <br>

                    <form id="form-edit-anggaran">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="">Tahun Anggaran</label>
                                    <input id="tahun_anggaran" name="tahun_anggaran" value="<?= $anggaran_detail['tahun_anggaran'] ?>" type="text" class="form-control">
                                </div>

                                <div style="display: none;" class="form-group">
                                    <label for="">id anggaran</label>
                                    <input id="id_anggaran" name="id_anggaran" name="id_anggaran" value="<?= $anggaran_detail['id'] ?>" type="text" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputUsername1">Kode SATKER</label>
                                    <input id="kode_satker" name="kode_satker" value="<?= $anggaran_detail['kode_satker'] ?>" type="text" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputUsername1">Nama SATKER</label>
                                    <input id="nama_satker" name="nama_satker" value="<?= $anggaran_detail['nama_satker'] ?>" type="text" class="form-control">
                                </div>

                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Nomor DIPA</label>
                                    <input id="nomor_dipa" name="nomor_dipa" value="<?= $anggaran_detail['nomor_dipa'] ?>" type="text" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputUsername1">Tanggal</label>
                                    <input id="tanggal_dipa" name="tanggal_dipa" value="<?= $anggaran_detail['tanggal_dipa'] ?>" type="date" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputUsername1">Pagu Anggaran</label>
                                    <input id="pagu_anggaran" data-a-sign="Rp. " data-a-dec="," data-a-sep="." name="pagu_anggaran" value="<?= $anggaran_detail['pagu_anggaran'] ?>" type="text" class="form-control money">
                                </div>

                            </div>

                        </div>

                        <button id="edit_anggaran" type="button" class="btn btn-warning mr-2">Edit</button>
                        <!-- <button class="btn btn-light">Batal</button> -->
                    </form>

                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">

            <div class="card">
                <div class="card-body">
                    <h6 style="font-size:15px !important; color: #241373 !important;">Filter Kegiatan</h6>
                    <br>
                    <form method="get" action="">
                        <div class="row">
                            <!-- <div class="col">
                                <div class="form-group">
                                    <label for="">Tahun Anggaran :</label>
                                    <select class="js-example-basic-single w-100" name="filter_anggaran_id" class="form-control" id="filter_anggaran_id">
                                        <option value="">Semua Tahun</option>
                                        <option value="1" <?= $_GET["filter_anggaran_id"] == "1" ? "selected" : "" ?>>2021</option>
                                        <option value="2" <?= $_GET["filter_anggaran_id"] == "2" ? "selected" : "" ?>>2022</option>
                                        <option value="3" <?= $_GET["filter_anggaran_id"] == "3" ? "selected" : "" ?>>2023</option>
                                        <option value="4" <?= $_GET["filter_anggaran_id"] == "4" ? "selected" : "" ?>>2024</option>
                                        <option value="5" <?= $_GET["filter_anggaran_id"] == "5" ? "selected" : "" ?>>2025</option>
                                    </select>
                                </div>
                            </div> -->
                            <?php if ($this->session->userdata("group_id") != 2) { ?>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="">Nama PPK :</label>
                                        <select class="js-example-basic-single w-100" data-width="100%" id="filter_nama_ppk" name="filter_nama_ppk">
                                            <option value="">Semua PPK</option>
                                            <?php foreach ($all_user as $rowuser => $value) {

                                                $selected = "";
                                                if ($value["id"] == $_GET["filter_nama_ppk"]) {
                                                    $selected = "selected";
                                                }

                                            ?>
                                                <option value="<?= $value['id'] ?>" <?= $selected ?>><?= $value['fullname'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                            <?php } ?>

                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="">Prioritas :</label>
                                    <select class="js-example-basic-single w-100" name="filter_prioritas" class="form-control" id="filter_prioritas">
                                        <option value="">Semua Prioritas</option>
                                        <option value="1" <?= $_GET["filter_prioritas"] == 1 ? "selected" : "" ?>>Ya</option>
                                        <option value="2" <?= $_GET["filter_prioritas"] == 2 ? "selected" : "" ?>>Tidak</option>
                                    </select>
                                </div>
                                <!-- <input type="number" id="id_anggaran" value="<?= $this->uri->segment(2) ?>"> -->
                            </div>

                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="">&nbsp;<br></label>
                                    <br>
                                    <button id="btn-filter-kegiatan" type="button" style="display: block !important; width:70%; height:35px; background-color:#8a3cc1; border-color:#8a3cc1;" class="btn btn-primary"> Filter</button>
                                </div>
                            </div>

                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="">&nbsp;<br></label>
                                    <br>
                                    <button type="button" id="clear-kontrak-btn" style="display: block !important; width:100%; height:35px" class="btn btn-secondary">Clear</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
    <br>
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline mb-2">
                        <h6 class="card-title mb-0">Daftar Kegiatan</h6>
                        <a style="color:#fff;" data-toggle="modal" data-target="#exampleModal" class="btn btn-primary"> <i class="link-icon" data-feather="plus"></i>&nbsp; Tambah Kegiatan</a>
                    </div>
                    <br>
                    <br>
                    <div class="table-responsive">
                        <table id="list-kegiatan" class="table table-hover table-striped mb-0">
                            <thead>
                                <tr>
                                    <th class="pt-0">Kode Aktivitas/KRO/RO</th>
                                    <th class="pt-0">Komponen/Sub</th>
                                    <th class="pt-0">Nama Kegiatan</th>
                                    <th class="pt-0">Pagu</th>
                                    <th class="pt-0">Nama PPK</th>
                                    <th class="pt-0">Prioritas</th>
                                    <th class="pt-0">Aksi</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--modal form tambah kegiatan -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Kegiatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-tambah-kegiatan">
                <div class="modal-body">
                    <form id="form-tambah-kegiatan">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Kode Aktivitas :</label>
                                    <input type="text" id="kode_aktivitas" name="kode_aktivitas" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Kode Kro :</label>
                                    <input type="text" id="kro" name="kro" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Kode RO :</label>
                                    <input id="ro" type="text" name="ro" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Kode Komponen :</label>
                                    <input id="komponen" name="komponen" type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Kode Sub Komponen :</label>
                                    <input id="subkomponen" name="subkomponen" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-6">

                                <div style="display: none;" class="form-group">
                                    <label for="exampleInputUsername1">anggaran id :</label>
                                    <input id="anggaran_id" name="anggaran_id" value="<?= $anggaran_detail['id'] ?>" type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Pagu Kegiatan :</label>
                                    <input id="pagu_kegiatan" data-a-sign="Rp. " data-a-dec="," data-a-sep="." name="pagu_kegiatan" type="text" class="form-control money">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Nama PPK :</label>
                                    <div class="form-group">
                                        <select class="js-example-basic-single w-100" data-width="100%" id="id_user" name="id_user">
                                            <?php foreach ($all_user as $rowuser => $value) { ?>
                                                <option value="<?= $value['id'] ?>"><?= $value['fullname'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputUsername1">Nama Kegiatan : </label>
                                    <textarea id="nama_kegiatan" name="nama_kegiatan" id="maxlength-textarea" class="form-control" maxlength="200" rows="8" placeholder="This textarea has a limit of 200 chars."></textarea>
                                </div>

                                <!-- <div class="form-group">
                                    <div class="form-check form-check-flat form-check-primary">
                                        <label class="form-check-label">
                                            <input id="prioritas" name="prioritas" type="checkbox" class="form-check-input">
                                            <i class="input-frame"></i>Check untuk kegiatan prioritas</label>
                                    </div>
                                </div> -->

                                <div class="form-group">
                                    <label for="">Kegiatan Prioritas:</label>
                                    <select class="form-control" name="prioritas" id="prioritas">
                                        <option value="">pilih</option>
                                        <option value="1">Ya</option>
                                        <option value="2">Tidak</option>
                                    </select>
                                </div>


                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button id="tambah-kegiatan-btn" class="float-right btn btn-primary success" type="button">Submit Kegiatan</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- modal form tambah detail -->

<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Detail Kegiatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-tambah-detail-kegiatan" action="">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="form-group" style="display:none;">
                                <label for="">Id Kegiatan:</label>
                                <input id="id_kegiatan" name="id_kegiatan" value="" type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Nama Kegiatan:</label>
                                <input readonly id="nama_kegiatan" name="nama_kegiatan" type="text" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="">PAGU Kegiatan :</label>
                                <input readonly id="pagu_kegiatan" name="pagu_kegiatan" type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Kode MAK :</label>
                                <input id="kode_mak" name="nama_mak" type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Detail Kegiatan :</label>
                                <textarea id="detail_kegiatan" name="detail_kegiatan" rows="5" class="form-control"></textarea>
                            </div>


                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- modal form Edit -->

<div class="modal fade" id="editKegiatanModal" tabindex="-1" role="dialog" aria-labelledby="editKegiatanModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Kegiatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-edit-kegiatan" action="">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6">

                            <div class="form-group" style="display: none;">
                                <label for="">Id Kegiatan:</label>
                                <input id="id_kegiatan" name="id_kegiatan" value="" type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Kode Aktifitas:</label>
                                <input id="kode_aktivitas" name="kode_aktivitas" type="text" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="">KRO :</label>
                                <input id="kro" name="kro" type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">RO :</label>
                                <input id="ro" name="ro" type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Nama Kegiatan :</label>
                                <textarea id="nama_kegiatan" name="nama_kegiatan" rows="2" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Detail Kegiatan :</label>
                                <textarea id="detail_kegiatan" name="detail_kegiatan" rows="5" class="form-control"></textarea>
                            </div>


                        </div>

                        <div class="col-lg-6">

                            <div class="form-group">
                                <label for="">Nama PPK:</label>
                                <select class="js-example-basic-single w-100" data-width="100%" name="id_user" id="user-list"></select>
                            </div>

                            <div class="form-group">
                                <label for="">Pagu Kegiatan:</label>
                                <input id="pagu_kegiatan" data-a-sign="Rp. " data-a-dec="," data-a-sep="." name="pagu_kegiatan" value="" type="text" class="form-control money">
                            </div>

                            <div class="form-group">
                                <label for="">Komponen:</label>
                                <input id="komponen" name="komponen" value="" type="text" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="">Subkomponen:</label>
                                <input id="subkomponen" name="subkomponen" value="" type="text" class="form-control">
                            </div>

                            <!-- <div class="form-group">
                                <label for="">Kode MAK:</label>
                                <input id="kode_mak" name="kode_mak" value="" type="text" class="form-control">
                            </div> -->

                            <!-- <div class="form-group">
                                <div class="form-check form-check-flat form-check-primary">
                                    <label class="form-check-label">
                                        <input id="prioritas" checked name="prioritas" type="checkbox" class="form-check-input">
                                        <i class="input-frame"></i>Check untuk kegiatan prioritas</label>
                                </div>
                            </div> -->

                            <div class="form-group">
                                <label for="">Kegiatan Prioritas:</label>
                                <select class="form-control" name="prioritas" id="prioritas">
                                    <option value="">pilih</option>
                                    <option value="1">Ya</option>
                                    <option value="2">Tidak</option>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="button" id="edit-kegiatan-btn" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>