<div class="page-content">
  <nav class="page-breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item">Kelola Anggaran</li>
      <li class="breadcrumb-item active" aria-current="page">Daftar Anggaran</li>
    </ol>
  </nav>

  <div class="row">
    <div class="col-lg-12 col-xl-12 stretch-card">
      <div class="card">
        <div class="card-body">
          <div class="d-flex justify-content-between align-items-baseline mb-2">
            <h6 style="color: #241373 !important;" class="card-title mb-0">Daftar Anggaran</h6>
            <a style="color:#fff;" href="<?= base_url('tambah_anggaran') ?>" class="btn btn-primary"> 
              <i class="link-icon" data-feather="plus"></i>&nbsp; Tambah Anggaran</a>
          </div>
          <br>
          <br>

          <div class="table-responsive">
            <table ui-jp="dataTable" id="list-anggaran" class="table table-hover table-striped mb-0">
              <thead>
                <tr>
                  <!-- <th class="pt-0">#</th> -->
                  <th class="pt-0">No DIPA</th>
                  <th class="pt-0">Tanggal DIPA</th>
                  <th class="pt-0">Pagu Anggaran</th>
                  <!-- <th class="pt-0">Total Pagu Kegiatan</th> -->
                  <th class="pt-0">Tahun</th>
                  <th class="pt-0">Kode Satker</th>
                  <th class="pt-0">Nama Saktker</th>
                  <th class="pt-0">Aksi</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>