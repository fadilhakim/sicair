<!-- plugin js for this page -->
<script src="<?= base_url("assets/vendors/datatables.net/jquery.dataTables.js") ?>"></script>
<script src="<?= base_url("assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js") ?>"></script>
<script src="<?= base_url("assets/vendors/sweetalert2/sweetalert2.min.js") ?>"></script>
<script src="<?= base_url("assets/vendors/select2/select2.min.js") ?>"></script>
<script src="<?= base_url("assets/js/select2.js") ?>"></script>
<!-- end plugin js for this page -->



<!-- <script src=" https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script> -->

<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>


<script>
    function tambah_anggaran() {

        var form_tambah_anggaran = $("#form-tambah-anggaran")[0];

        Swal.fire({
            title: "Tambah Anggaran ",
            text: "Apakah anda yakin ingin menambahkan anggaran ini",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'mr-2',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true,
            closeOnConfirm: true
        }).then((result) => {
            // dijalankan kalau si fire selesai menjalankan task / process
            if (result.value) {

                var nomor_dipa = $('#nomor_dipa').val();
                var tahun_anggaran = $('#tahun_anggaran').val();
                var kode_satker = $('#kode_satker').val();
                var nama_satker = $('#nama_satker').val();
                var tanggal_dipa = $('#tanggal_dipa').val();
                var pagu_anggaran = $('#pagu_anggaran').val();

                $.ajax({
                    url: "<?= base_url("anggaran/tambah_anggaran_proses") ?>",
                    type: "post",
                    data: {
                        nomor_dipa: nomor_dipa,
                        tahun_anggaran: tahun_anggaran,
                        kode_satker: kode_satker,
                        nama_satker: nama_satker,
                        tanggal_dipa: tanggal_dipa,
                        pagu_anggaran: pagu_anggaran
                    },
                    dataType: "json",
                    success: function(data) {

                        if (data.success) {
                            Swal.fire({
                                title: '<strong> Success !</strong>',
                                icon: 'success',
                                html: data.message
                            })

                            setTimeout(() => {
                                window.location.href = "<?= base_url("anggaran") ?>";
                            }, 2000);
                            console.log('berhail')
                        } else {
                            Swal.fire({
                                title: '<strong> Error !</strong>',
                                icon: 'error',
                                html: data.message

                            })
                        }



                    }

                })
            }
        })
    }

    function edit_anggaran() {

        var form_edit_anggaran = $("#form-edit-anggaran")[0]

        Swal.fire({
            title: "Edit Anggaran ",
            text: "Apakah anda yakin ingin meng-edit Anggaran ini?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'mr-2',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true,
            closeOnConfirm: true
        }).then((result) => {

            if (result.value) {

                var tahun_anggaran = $('#tahun_anggaran').val();
                var id_anggaran = $('#id_anggaran').val();
                var kode_satker = $('#kode_satker').val();
                var nama_satker = $('#nama_satker').val();
                var nomor_dipa = $('#nomor_dipa').val();
                var tanggal_dipa = $('#tanggal_dipa').val();
                var pagu_anggaran = $('#pagu_anggaran').val();
                // var data = new FormData(form_edit_anggaran);
                // console.log(data)
                $.ajax({
                    url: "<?= base_url("anggaran/edit_anggaran") ?>",
                    type: "post",
                    data: {
                        tahun_anggaran: tahun_anggaran,
                        id_anggaran: id_anggaran,
                        kode_satker: kode_satker,
                        nomor_dipa: nomor_dipa,
                        tanggal_dipa: tanggal_dipa,
                        pagu_anggaran: pagu_anggaran,
                        nama_satker: nama_satker
                    },
                    dataType: "json",
                    success: function(data) {

                        if (data.success) {
                            Swal.fire({
                                title: '<strong> Success !</strong>',
                                icon: 'success',
                                html: data.message
                            })

                            setTimeout(() => {
                                window.location.reload()
                            }, 2000);
                        } else {
                            Swal.fire({
                                title: '<strong> Error !</strong>',
                                icon: 'error',
                                html: data.message

                            })
                        }



                    }

                })
            }
        })
    }

    function getTableAnggaran() {

        var form = "#form-filter-anggaran"

        $('#list-anggaran').DataTable({
            dom: 'Bfrtip',
            order: [
                [1, 'desc']
            ],
            buttons: [{
                text: 'Export to Excel',
                extend: 'excelHtml5',
                title: 'Daftar Anggaran',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5]
                }
            }],
            serverSide: true,
            ajax: {
                // data: {
                //     tahun_anggaran: $(form + "#tahun_anggaran").val()
                // },
                url: "<?= base_url("anggaran/get_anggaran") ?>",
                type: 'POST'
            },
            columns: [{
                    "data": "nomor_dipa"

                },
                {
                    "data": "tanggal_dipa"

                },
                {
                    "data": "pagu_anggaran"

                },
                {
                    "data": "tahun_anggaran"

                },
                {
                    "data": "kode_satker"

                },
                {
                    "data": "nama_satker"

                },
                {
                    "data": "Aksi"

                }
            ],
            columnDefs: [{
                "sorting": true,
                "orderable": true,
                "type": "html",
                "targets": 6,
                // "data": id,
                "render": function(data, type, row) {
                    //console.log("data => ", data, type, row)

                    const newRow = row

                    return `<a class="btn btn-info"
                            href="<?= base_url("detail_anggaran") ?>/${row.id}">Lihat Detail</a> 
                            <a class="btn btn-danger" id=""hapusAnggaran
                            onclick="confirmationDeleteAnggaran(${row.id})">Hapus</a>`
                }
            }, {
                "sorting": true,
                "orderable": true,
                "type": "html",
                "targets": 0,
                // "data": id,
                "render": function(data, type, row) {
                    //console.log("data => ", data, type, row)

                    const newRow = row

                    return `<a href="<?= base_url("detail_anggaran") ?>/${row.id}">${row.nomor_dipa}</a>`
                }
            }, {
                "sorting": true,
                "orderable": true,
                "type": "html",
                "targets": 2,
                // "data": id,
                "render": function(data, type, row) {
                    //console.log("data => ", data, type, row)

                    const newRow = row
                    row.pagu_anggaran = row.pagu_anggaran.replace(',00', '')
                    return `<span style="font-weight:bold;">Rp. ${row.pagu_anggaran}</a>`
                }
            }]
        })
    }

    function tambah_kegiatan() {
        var form_tambah_kegiatan = $("#form-tambah-kegiatan")[0];

        Swal.fire({
            title: "Tambah Kegiatan ",
            text: "Apakah anda yakin ingin menambahkan Kegiatan ini",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'mr-2',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true,
            closeOnConfirm: true
        }).then((result) => {

            if (result.value) {

                var kode_aktivitas = $('#kode_aktivitas').val();
                var kro = $('#kro').val();
                var ro = $('#ro').val();
                var komponen = $('#komponen').val();
                var subkomponen = $('#subkomponen').val();
                var pagu_kegiatan = $('#pagu_kegiatan').val();
                var id_user = $('#id_user').val();
                var nama_kegiatan = $('#nama_kegiatan').val();
                var anggaran_id = $('#anggaran_id').val();
                var prioritas = $('#anggaran_id').val();
                // if ($('#prioritas').is(':checked')) {
                //     prioritas += 1;
                // } else {
                //     prioritas = 0;
                // }

                $.ajax({
                    url: "<?= base_url("anggaran/tambah_kegiatan_process") ?>",
                    type: "post",
                    data: {
                        kode_aktivitas: kode_aktivitas,
                        kro: kro,
                        ro: ro,
                        komponen: komponen,
                        subkomponen: subkomponen,
                        pagu_kegiatan: pagu_kegiatan,
                        id_user: id_user,
                        nama_kegiatan: nama_kegiatan,
                        prioritas: prioritas,
                        anggaran_id: anggaran_id
                    },
                    dataType: "json",
                    success: function(data) {

                        if (data.success) {
                            Swal.fire({
                                title: '<strong> Success !</strong>',
                                icon: 'success',
                                html: data.message
                            })

                            setTimeout(() => {
                                window.location.reload()
                            }, 2000);
                        } else {
                            Swal.fire({
                                title: '<strong> Error !</strong>',
                                icon: 'error',
                                html: data.message

                            })
                        }



                    }

                })
            }
        })
    }

    function getTableKegiatan() {

        var form = "#form-filter-kegiatan"

        $('#list-kegiatan').DataTable({
            dom: 'Bfrtip',
            searching: false,
            order: [
                [1, 'desc']
            ],
            buttons: [{
                text: 'Export to Excel',
                extend: 'excelHtml5',
                title: 'Daftar Kegiatan',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6]
                }
            }],
            serverSide: true,
            ajax: {
                data: {
                    id_anggaran: $("#id_anggaran").val(),
                    filter_nama_ppk: $("#filter_nama_ppk").val(),
                    filter_anggaran_id: $("#filter_anggaran_id").val(),
                    filter_prioritas: $("#filter_prioritas").val()
                },
                url: "<?= base_url("anggaran/get_kegiatan") ?>",
                type: 'POST'
            },
            columns: [{
                    "data": "kode_aktivitas"

                },
                {
                    "data": "komponen"

                },
                {
                    "data": "nama_kegiatan"

                },
                {
                    "data": "pagu_kegiatan"

                },
                {
                    "data": "id_user"

                },
                {
                    "data": "prioritas"

                },
                {
                    data: "Aksi"

                }
            ],
            columnDefs: [{
                    "sorting": true,
                    "orderable": true,
                    "type": "html",
                    "targets": 0,
                    "render": function(data, type, row) {

                        const newRow = row
                        return `<span>${row.kode_aktivitas}/${row.kro}/${row.ro}</span>`
                    }
                },
                {
                    "sorting": true,
                    "orderable": true,
                    "type": "html",
                    "targets": 1,
                    "render": function(data, type, row) {

                        const newRow = row
                        return `<span>${row.komponen}/${row.subkomponen}</span>`
                    }
                },
                {
                    "sorting": false,
                    "orderable": false,
                    "type": "html",
                    "targets": 3,
                    "render": function(data, type, row) {
                        var newRow = row
                        return `<span style="font-weight:bold;">Rp. ${String(row.pagu_kegiatan).replace(/(.)(?=(\d{3})+$)/g,'$1')}</span>`

                    }
                },
                {
                    "sorting": true,
                    "orderable": true,
                    "type": "html",
                    "targets": 4,
                    "render": function(data, type, row) {
                        //get_user_in_table(row.id_user)
                        return `${row.id_user}`
                    }
                },
                {
                    "sorting": true,
                    "orderable": true,
                    "type": "html",
                    "targets": 5,
                    "render": function(data, type, row) {

                        const newRow = row
                        var check = 'display:none';
                        if (row.prioritas == 1) {
                            check = 'display:inline-block'
                        }
                        return `<badge style="${check}" class="badge badge-info"> Priority</badge>`
                    }
                },
                {
                    "sorting": true,
                    "orderable": true,
                    "type": "html",
                    "targets": 6,
                    // "data": id,
                    "render": function(data, type, row) {
                        //console.log("data => ", data, type, row)

                        const newRow = row

                        return `<div class="btn-group dropdown">
                                            <button type="button" class="btn btn-primary" data-toggle="dropdown" aria-expanded="false">
                                                Aksi <span class="caret"><i class="fa fa-caret-down"></i></span>
                                            </button>
                                            <ul class="dropdown-menu animated fadeIn">
                                                <li style="display:inline-block;"><a href="<?= base_url('kegiatan/detail_kegiatan') ?>/${row.id}" style="color:#fff;" class="btn btn-dark">Lihat Detail Kegiatan</a></li>
                                                <li style="display:inline-block;"><a onClick="show_form_edit_kegiatan(${row.id})" data-toggle="modal" data-target="#editKegiatanModal" class="btn btn-warning">Edit</a></li>
                                                <li style="display:inline-block;"><a onclick="confirmationDeleteKegiatan(${row.id})" class="btn btn-danger">Hapus</a></li>
                                            </ul>
                                        </div>`
                    }
                }
            ]

        })
    }


    function show_form_tambah_detail_kegiatan(id_kegiatan) {

        // $('form_tambah_kegiatan #mak').val("")
        // $('form_tambah_kegiatan #detail_kegiatan').val("")

        // $("button#btn-submit-action-plan").attr("onclick", "update_action_plan()")

        // $("button#btn-submit-action-plan").attr("disabled", true)

        // $("button#add-pic-btn").attr("onclick", "addPIC()")

        $.ajax({
            url: "<?= base_url("anggaran/get_tambah_kegiatan_detail") ?>",
            data: {
                id_kegiatan: id_kegiatan
            },
            type: "post",
            dataType: "json",
            success: function(res) {

                var form = "form#form-tambah-detail-kegiatan"
                $(form + " #id_kegiatan").val(res.data.id)
                $(form + " #nama_kegiatan").val(res.data.nama_kegiatan)
                $(form + " #pagu_kegiatan").val(res.data.pagu_kegiatan)

            }
        })
    }


    function get_user(id_user) {
        // alert(id_user);
        $.ajax({
            url: "<?= base_url("user/get_all_user_ppk") ?>",
            type: "post",
            dataType: "json",
            success: function(res) {
                $.each(res.data, function(i, item) {
                    var sel = ''
                    if (item.id == id_user) {
                        sel = 'selected'
                    }
                    $('#form-edit-kegiatan #user-list').append(`
                        <option ${sel} value="${item.id}">${item.fullname}</option>
                    `);
                });
            }
        })
    }

    function show_form_edit_kegiatan(id_kegiatan) {

        $("button#edit-kegiatan-btn").attr("onclick", "edit_kegiatan_submit()")

        $.ajax({
            url: "<?= base_url("anggaran/get_tambah_kegiatan_detail") ?>",
            data: {
                id_kegiatan: id_kegiatan
            },
            type: "post",
            dataType: "json",
            success: function(res) {

                var form = "form#form-edit-kegiatan"
                get_user(res.data.id_user);
                $(form + " #id_kegiatan").val(res.data.id)
                $(form + " #nama_kegiatan").val(res.data.nama_kegiatan)
                $(form + " #pagu_kegiatan").val(res.data.pagu_kegiatan)
                $(form + " #kro").val(res.data.kro)
                $(form + " #ro").val(res.data.ro)
                $(form + " #detail_kegiatan").val(res.data.detail_kegiatan)
                // $(form + " #id_user").val(res.data.id_user)
                $(form + " #prioritas").val(res.data.prioritas)
                $(form + " #komponen").val(res.data.komponen)
                $(form + " #subkomponen").val(res.data.subkomponen)
                // $(form + " #kode_mak").val(res.data.kode_mak)
                $(form + " #kode_aktivitas").val(res.data.kode_aktivitas)

            }
        })
    }

    function edit_kegiatan_submit() {

        Swal.fire({
                title: "Update Kegiatan ",
                text: "Apakah anda yakin ingin men-update kegiatan ini ?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'mr-2',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {

                if (result.value) {

                    const form = "#form-edit-kegiatan"

                    const data = {
                        id_kegiatan: $(form + " #id_kegiatan").val(),
                        kode_aktivitas: $(form + " #kode_aktivitas").val(),
                        kro: $(form + " #kro").val(),
                        ro: $(form + " #ro").val(),
                        nama_kegiatan: $(form + " #nama_kegiatan").val(),
                        detail_kegiatan: $(form + " #detail_kegiatan").val(),
                        nama_ppk: $("#user-list").val(),
                        pagu_kegiatan: $(form + " #pagu_kegiatan").val(),
                        komponen: $(form + " #komponen").val(),
                        subkomponen: $(form + " #subkomponen").val(),
                        // kode_mak: $(form + " #kode_mak").val(),
                        prioritas: $(form + " #prioritas").val()
                    }
                    console.log(data)

                    $.ajax({
                        url: "<?= base_url("anggaran/update_kegiatan") ?>",
                        type: 'post',
                        data: data,
                        dataType: 'json',
                        success: function(res) {
                            if (res.success === true) {
                                $('#list-kegiatan').DataTable().ajax.reload();
                                $("#editKegiatanModal").modal('hide');

                                Swal.fire(
                                    'Success!',
                                    res.message,
                                    'success'
                                )
                            } else {
                                Swal.fire({
                                    title: '<strong> Error !</strong>',
                                    icon: 'error',
                                    html: res.message
                                })
                            }
                        }
                    })
                }


            })
            .catch(err => {
                console.log('tidak terupdate')
            })
    }

    function delete_kegiatan(kegiatan_id) {
        $.ajax({
            url: "<?= base_url("kegiatan/delete_kegiatan") ?>",
            type: "post",
            data: {
                kegiatan_id: kegiatan_id
            },

            dataType: "json",
            success: function(res) {

                if (res.success === true) {

                    Swal.fire(
                        'Success!',
                        res.message,
                        'success'
                    )
                    setTimeout(() => {
                        window.location = '<?= base_url("kegiatan") ?>';
                    }, 2000);

                } else {
                    Swal.fire({
                        title: '<strong> Error !</strong>',
                        icon: 'error',
                        html: res.message
                    })
                }

            },
            error: function(jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }

                console.log(" err msg ==> ", msg)
            }
        })
    }

    function confirmationDeleteKegiatan(kegiatan_id) {
        Swal.fire({
            title: 'Delete Kegiatan',
            text: "Apakah anda yakin ingin menghapus kegiatan ini !?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'mr-2',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {

            if (result.value) {
                delete_kegiatan(kegiatan_id)

                console.log('kesini')
            } else if (
                // Read more about handling dismissals
                result.dismiss === Swal.DismissReason.cancel
            ) {

            }
        })
    }

    function delete_anggaran(anggaran_id) {
        $.ajax({
            url: "<?= base_url("anggaran/delete_anggaran") ?>",
            type: "post",
            data: {
                anggaran_id: anggaran_id
            },

            dataType: "json",
            success: function(res) {

                if (res.success === true) {

                    Swal.fire(
                        'Success!',
                        res.message,
                        'success'
                    )
                    setTimeout(() => {
                        window.location = '<?= base_url("anggaran_list") ?>';
                    }, 2000);

                } else {
                    Swal.fire({
                        title: '<strong> Error !</strong>',
                        icon: 'error',
                        html: res.message
                    })
                }

            },
            error: function(jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }

                console.log(" err msg ==> ", msg)
            }
        })
    }

    function confirmationDeleteAnggaran(anggaran_id) {
        Swal.fire({
            title: 'Delete Anggaran',
            text: "Apakah anda yakin ingin menghapus anggaran ini !?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'mr-2',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {

            if (result.value) {
                delete_anggaran(anggaran_id)
            } else if (
                // Read more about handling dismissals
                result.dismiss === Swal.DismissReason.cancel
            ) {

            }
        })
    }

    function filterKegiatan() {

        var filter_nama_ppk = $("#filter_nama_ppk").val()
        var filter_anggaran_id = $("#filter_anggaran_id").val()
        var filter_prioritas = $("#filter_prioritas").val()
        var id_anggaran = $("#id_anggaran").val()
        // console.log(filter_nama_ppk)
        let data = {}

        if (filter_nama_ppk) {
            data.filter_nama_ppk = filter_nama_ppk
        }

        if (filter_anggaran_id) {
            data.filter_anggaran_id = filter_anggaran_id
        }

        if (filter_prioritas) {
            data.filter_prioritas = filter_prioritas
        }

        var param = $.param(data);
        param = decodeURIComponent(param)
        console.log(data)
        window.location = '<?= base_url("detail_anggaran/") ?>' + id_anggaran + '?' + param;

    }

    function clearKontrak() {
        var id_anggaran = $("#id_anggaran").val()
        window.location = '<?= base_url("detail_anggaran/") ?>' + id_anggaran
    }

    $(document).on('click', '#tambah-anggaran-btn', function(e) {
        tambah_anggaran()
    })

    $(document).on('click', '#tambah-kegiatan-btn', function(e) {
        tambah_kegiatan()
    })

    $(document).on('click', '#delete-kegiatan-btn', function(e) {
        delete_kegiatan()
    })


    $(document).on('click', '#edit_anggaran', function(e) {
        edit_anggaran()
    })

    $(document).on('click', '#hapusAnggaran', function(e) {
        confirmationDeleteAnggaran()
    })



    $("#btn-filter-kegiatan").click(function(e) {
        //alert("filter...")
        filterKegiatan()
    })

    $("#clear-kontrak-btn").click(function(e) {
        //alert("filter...")
        clearKontrak()
    })

    $(function() {

        getTableAnggaran()
        getTableKegiatan()

        $(".money").autoNumeric("init", {
            aForm: true,
            vMax: "999999999999999"
        });
    })
</script>