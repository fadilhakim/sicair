<!-- plugin js for this page -->
<script src="<?=base_url("assets/vendors/datatables.net/jquery.dataTables.js")?>"></script>
<script src="<?=base_url("assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js")?>"></script>
<!-- end plugin js for this page -->

<!-- custom js for this page -->
<script src="<?=base_url("assets/js/data-table.js")?>"></script>
<!-- end custom js for this page -->

<script>

function projectUpdateHistoryTable() {
    // tambahan value
    $("#project-update-history-table").DataTable({
        serverSide: true,
        ajax: {
            data:{

                // tambahan value
            },
            url: "<?=base_url("history/project_update_location_record")?>",
            type: 'POST'
        },
        columns:[
            {"data":"id"},
            {"data":"nama_project",},
            {"data":"pelanggan"},
            {"data":"no_wo"},
            {"data":"created_at"},
            {"data":"pic"},
            {"data":"project_status"},
            {"data":"action"},

        ],
        columnDefs:[
            {
                "sorting": false,
                "orderable": false,
                "type":"html",
                "targets":-2,
                // "data": id,
                "render": function (data, type, row) {
                    //console.log("data => ", data, type, row)
                    //const newRow = row

                    return `<span class="badge" style="color:white;background-color:${row.status_color}">${row.project_status}</span>`

                }
            },
            {
                "sorting": false,
                "orderable": false,
                "type":"html",
                "targets":-1,
                // "data": id,
                "render": function (data, type, row) {
                    //console.log("data => ", data, type, row)
                    //const newRow = row

                    return `<a type="button" class="btn btn-primary" href="<?=base_url("all-projects/update/")?>${row.workorder_id}">Detail</a>`

                }

            }
        ]
    })
}

function userLoginTable() {

    $("#user-login-tbl").DataTable({
        serverSide: true,
        ajax: {
            data:{

                // tambahan value
            },
            url: "<?=base_url("history/login_record_list")?>",
            type: 'POST'
        },
        columns:[
            {"data": "id"},
            {"data": "username",},
            {"data": "created_at"},
            {"data":"last_update"},
            {"data":"action"}

        ],
        columnDefs:[
            {
                "sorting": false,
                "orderable": false,
                "type":"html",
                "targets":-1,
                // "data": id,
                "render": function (data, type, row) {
                    //console.log("data => ", data, type, row)

                    // const newRow = row

                    return `<a type="button" class="btn btn-primary" href="<?=base_url("history/show_history_users_detail/")?>${row.session_id}">Detail</a>`

                }

            }
        ]

    })
}

$(function() {
    "use strict"
    $(userLoginTable)
    $(projectUpdateHistoryTable)

});
</script>
