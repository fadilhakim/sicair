<div class="page-content">
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"> Main </li>
            <li class="breadcrumb-item"> History </li>
            <li class="breadcrumb-item active"> Users </li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-header-title">Users Logs</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <!-- <table id="user-login-tbl" class="table"> -->
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Username</th>
                                    <th>Last Login</th>
                                    <th>Last Update</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr>
                                    <td>5</td>
                                    <td>Fadil Hakim</td>
                                    <td>2020/09/13</td>
                                    <td>Pembuatan DSCPC / <strong>2020/08/25</strong></td>
                                    <td>
                                        <a type="button" class="btn btn-primary" href="#">Detail</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>