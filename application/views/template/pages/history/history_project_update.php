<div class="page-content">
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"> Main </li>
            <li class="breadcrumb-item"> History </li>
            <li class="breadcrumb-item active"> Kontrak Updates </li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-header-title">Kontrak Logs</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <!-- <table id="project-update-history-table" class="table"> -->
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Kontrak</th>
                                    <th>Kontraktor</th>
                                    <th>Nomor DIPA</th>
                                    <th>Last Update</th>
                                    <th>Updated By</th>
                                    <th>Status</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Pengembangan Applikasi Pencairan Anggaran</td>
                                    <td>Negeri Kolam Susu</td>
                                    <td>DPA/091/20/2021</td>
                                    <td>2020/04/25</td>
                                    <td>Fadil Hakim</td>
                                    <td><span class="badge badge-info">Pengadaan</span></td>
                                    <td>
                                        <a type="button" class="btn btn-primary" href="<?= base_url('history/project-update/detail') ?>">Detail</a>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>