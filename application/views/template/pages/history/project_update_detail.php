<div class="page-content">
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"> Main </li>
            <li class="breadcrumb-item"> History </li>
            <li class="breadcrumb-item"> Project Update </li> 
            <li class="breadcrumb-item active"> Project Update Detail </li> 
        </ol>
    </nav>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                     <h4 class="card-header-title">Project Update Detail</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="dataTableExample" class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal Dan Jam</th>
                                    <th>Status</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- <tr>
                                    <td>1</td>
                                    <td>2020/09/16 | 15.00</td>
                                    <td><span class="badge badge-info">Pengadaan</span></td>
                                    <td>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Enim eum nobis recusandae quod debitis delectus? Architecto hic debitis, odit repellendus qui nihil repudiandae quasi at dolorem molestiae ad. Corporis, deserunt?</td>
                                </tr> -->
                              
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>