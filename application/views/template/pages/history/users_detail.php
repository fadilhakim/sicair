<div class="page-content">
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"> Main </li>
            <li class="breadcrumb-item"> History </li>
            <li class="breadcrumb-item">  Users </li>
            <li class="breadcrumb-item active"> Users Detail </li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                     <h4 class="card-header-title">Users Nik : <?=$user_detail["username"]?> - <?=$user_detail["fullname"]?></h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="user-detail-logs-tbl" class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    
                                    <th>Updating Project</th>
      
                                </tr>
                            </thead>
                            <tbody>
                              <?php 
                                if(!empty($project_update_history) ) {
                                    foreach($project_update_history as $row){
                                ?>
                                    <tr>
                                        <td><?=$row["id"]?></td>
                                        
                                        <td><?=$row["nama_project"]?> / <strong><?=$row["created_at"]?></strong></td>
                                    </tr>
                                <?php 
                                    }
                                }
                              ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
