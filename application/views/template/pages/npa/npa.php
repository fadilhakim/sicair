<div class="page-content">

  <nav class="page-breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="#fulfillment">NPA</a></li>
			<li class="breadcrumb-item active" aria-current="page">Create New</li>
		</ol>
	</nav>
  <style media="screen">
    label {
      font-weight: bolder;
    }
    .form-control {
      width: 70%;
    }
    button {
      margin-right: 15px;
    }
    .tbl-asset td .form-control {
      width: 18%;
      margin: auto;
    }
    .table td {
      padding: 5px;
    }
  </style>
  <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
			<div class="card">
				<div class="card-body">
					<h6 class="card-title">Nota Pengecekan Asset </h6>
					<form>
            <div class="row">
              <div class="col-lg-6">

                <div class="form-group">
    							<label>Pilih Work Order Internal :</label>
                  <select class="form-control" name="">
                    <option value="">HUB 1</option>
                    <option value="">HUB 2</option>
                  </select>
    						</div>
              </div>

              <div class="col-lg-6"></div>

              <div class="col-lg-12">
                <label>Pilih Node / Link :</label>
                <table class="table tbl-asset table-bordered table-hover">
                  <tr>
                    <td style="text-align:center;"> <strong> Pilih</strong></td>
                    <td> <strong> Nama</strong></td>
                    <td> <strong>Alamat</strong></td>
                    <td> <strong> Layanan</strong></td>
                  </tr>
                  <tr>
                    <td>
                      <input type="checkbox" class="form-control" name="" value="">
                    </td>
                    <td>Kehutanan</td>
                    <td>Jayaputra</td>
                    <td>
                      VSAT IP C-Band
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <input type="checkbox" class="form-control" name="" value="">
                    </td>
                    <td>Kehutanan</td>
                    <td>Jayaputra</td>
                    <td>
                      VSAT IP C-Band
                    </td>
                  </tr>

                  <tr>
                    <td>
                      <input type="checkbox" class="form-control" name="" value="">
                    </td>
                    <td>Kehutanan</td>
                    <td>Jayaputra</td>
                    <td>
                      VSAT IP C-Band
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <input type="checkbox" class="form-control" name="" value="">
                    </td>
                    <td>Kehutanan</td>
                    <td>Jayaputra</td>
                    <td>
                      VSAT IP C-Band
                    </td>
                  </tr>
                </table>
              </div>


              <div class="col-lg-12">
                <br/>
                <br/>
                <label>Input Asset :</label>
                <table class="table table-bordered table-hover">
                  <tr>
                    <td>Pilih Barang</td>
                    <td>Jumlah</td>
                    <td>Satuan</td>
                    <td>Note</td>
                  </tr>
                  <tr>
                    <td>
                      <select class="form-control" name="">
                        <option value="">Pilih Asset</option>
                      </select>
                    </td>
                    <td>
                      <input type="text" class="form-control" name="" value="">
                    </td>
                    <td>
                      <input type="text" class="form-control" name="" value="">
                    </td>
                    <td>
                      <input type="text" class="form-control" name="" value="">
                    </td>
                  </tr>
                </table>
              </div>
              <div class="col-lg-12">
                <div class="form-group">
                  <br/>
                  <br/>
                  <label>Keterangan :</label>
                  <textarea name="" style="width:85%" class="form-control" rows="8" cols="80"></textarea>
                </div>

                <div class="form-group">

                  <button type="button" class="btn btn-success" name="button">Simpan & Usulkan</button>
                </div>


              </div>
            </div>



					</form>
				</div>
			</div>
		</div>
  </div>
</div>
