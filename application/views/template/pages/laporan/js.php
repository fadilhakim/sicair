<!-- plugin js for this page -->
<script src="<?= base_url("assets/vendors/datatables.net/jquery.dataTables.js") ?>"></script>
<script src="<?= base_url("assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js") ?>"></script>
<script src="<?= base_url("assets/vendors/sweetalert2/sweetalert2.min.js") ?>"></script>
<script src="<?= base_url("assets/vendors/select2/select2.min.js") ?>"></script>
<script src="<?= base_url("assets/js/select2.js") ?>"></script>
<!-- end plugin js for this page -->



<script src=" https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>

<script>
    var displayTable = document.getElementById("tableLaporanKegiatan");
    displayTable.style.display = 'none';
    // var table = getLaporanKegiatan()

    function showTable() {

        // table.ajax.reload()

        if (displayTable.style.display === "none") {
            displayTable.style.display = "block";
        }
    }

    function getLaporanKegiatan() {

        table = $("#table-laporan-kegiatan").DataTable({
            dom: 'Bfrtip',
            paging: true,
            order: [
                [1, 'desc']
            ],
            buttons: [{
                text: 'Export to Excel',
                extend: 'excelHtml5',
                title: 'Daftar Laporan Kegiatan',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6]
                }
            }],
            serverSide: true,
            ajax: {
                data: function(data) {
                    data.filter_tahun_anggaran = $("#filter_tahun_anggaran").val()
                    data.filter_nama_ppk = $("#filter_nama_ppk").val()
                    data.filter_prioritas = $("#filter_prioritas").val()
                },
                url: "<?= base_url("laporan/get_laporan_kegiatan") ?>",
                type: 'POST',
                dataType: "json",
                //success:function(res) { 
                //var total_pagu_kegiatan = `Rp. ${new Intl.NumberFormat().format(res.total_pagu_anggaran.pagu_kegiatan)}`
                //$("#total-pagu-kegiatan").html(total_pagu_kegiatan)
                //}
                // 

            },
            drawCallback: (res) => {

                var total_pagu_kegiatan = `Rp. ${new Intl.NumberFormat().format(res.json.total_pagu_anggaran.pagu_kegiatan)}`
                $("#tableLaporanKegiatan #total-pagu-kegiatan").html(total_pagu_kegiatan)

                var total_kontrak = `Rp. ${new Intl.NumberFormat().format(res.json.total_kontrak)}`
                $("#tableLaporanKegiatan #total-kontrak").html(total_kontrak)

                // var total_tagihan = `Rp. ${new Intl.NumberFormat().format(res.json.total_tagihan)}`
                // $("#total-tagihan").val(total_kontrak)
            },
            columns: [{
                    "data": "no"
                },
                {
                    "data": "kode_aktivitas"
                },
                {
                    "data": "nama_kegiatan"
                },
                {
                    "data": "pagu_kegiatan"
                },
                {
                    "data": "nilai_kontrak"
                },
                {
                    "data": "sisa_pagu"
                },
                {
                    "data": "nama_ppk"
                },
                {
                    "data": "Rincian"
                }

            ],
            columnDefs: [{
                    targets: 3,
                    render: function(data, type, row) {
                        return `Rp. ${new Intl.NumberFormat().format(row.pagu_kegiatan)}`
                    }
                },
                {
                    targets: 4,
                    render: function(data, type, row) {
                        return `Rp. ${new Intl.NumberFormat().format(row.nilai_kontrak)}`
                    }
                },
                {
                    targets: 5,
                    render: function(data, type, row) {
                        return `Rp. ${new Intl.NumberFormat().format(row.sisa_pagu)}`
                    }
                },

                {
                    "sorting": false,
                    "orderable": false,
                    "type": "html",
                    "targets": 7,
                    "render": function(data, type, row) {
                        //console.log("data => ", data, type, row)

                        const newRow = row

                        return `<a data-toggle="modal" data-target="#modalDetailLaporanKegiatan" 
                            onclick="getLaporanDetailKegiatan(${row.id})" 
                            class="btn btn-info showLaporanDetailKegiatan">Rincian Kegiatan</a>`

                    }
                }
            ]
        })

        $("#showTableKegiatan").click(function(e) {
            showTable()
            table.draw()
        })


    }

    function getLaporanDetailKegiatan(id) {

        $('#table-laporan-kegiatan-detail').DataTable().destroy()
        $('#table-laporan-kegiatan-detail').DataTable({

            dom: 'Bfrtip',
            order: [
                [0, 'desc']
            ],
            buttons: [{
                text: 'Export to Excel',
                extend: 'excelHtml5',
                title: 'Daftar Laporan Kontrak Kegiatan Detail',
                exportOptions: {
                    columns: [0, 1, 2, 3]
                }
            }],
            serverSide: true,
            ajax: {
                data: {
                    id: id
                },
                url: "<?= base_url("laporan/get_detail_laporan_kegiatan") ?>",
                type: 'POST',
                dataType: "json"
            },
            drawCallback: (res) => {

                $("#nama-kegiatan").html(res.json.kegiatan.nama_kegiatan)

                var total_pagu_kegiatan = `Rp. ${new Intl.NumberFormat().format(res.json.total_pagu)}`
                $("#modalDetailLaporanKegiatan #total_pagu").val(total_pagu_kegiatan)

                var total_kontrak = `Rp. ${new Intl.NumberFormat().format(res.json.total_nilai_kontrak)}`
                $("#modalDetailLaporanKegiatan #total_nilai_kontrak").val(total_kontrak)

                var total_tagihan = `Rp. ${new Intl.NumberFormat().format(res.json.total_nilai_tagihan)}`
                $("#modalDetailLaporanKegiatan #total_nilai_tagihan").val(total_tagihan)
            },
            columns: [{
                    "data": "no"

                }, {
                    "data": "detail_kegiatan"

                },
                {
                    "data": "pagu_kegiatan"

                },
                {
                    "data": "kode_mak"

                },
                {
                    data: "Aksi"

                }
            ],
            columnDefs: [{
                    targets: 2,
                    render: function(data, type, row) {
                        return `Rp. ${new Intl.NumberFormat().format(row.pagu_kegiatan)}`
                    }
                },
                {
                    "sorting": true,
                    "orderable": true,
                    "type": "html",
                    "targets": 4,
                    // "data": id,
                    "render": function(data, type, row) {
                        //console.log("data => ", data, type, row)

                        const newRow = row

                        return `<a class="btn btn-primary" style="color:#fff;" onclick="getLaporanKontrakKegiatan(${row.id})"> Lihat Kontrak</a> 
                        <a class="btn btn-danger" style="color:#fff;" onclick="getLaporanTagihanKegiatan(${row.id})"> Lihat Tagihan</a> `
                    }
                }
            ]
        })


    }

    function getLaporanKontrakKegiatan(id) {

        document.getElementById("tbl-kt").style.display = "block";

        var settingTable = {
            dom: 'Bfrtip',
            order: [
                [0, 'desc']
            ],
            buttons: [{
                text: 'Export to Excel',
                extend: 'excelHtml5',
                title: 'Daftar Kontrak Kegiatan',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5]
                }
            }],
            serverSide: true,
            ajax: {
                data: {
                    id: id
                },
                url: "<?= base_url("laporan/get_detail_laporan_kegiatan_kontrak") ?>",
                type: 'POST'
            },
            drawCallback: (res) => {
                // res.json.kegiatan.nama_kegiatan

                $("h5#nama-detail-kegiatan").html('<span class="badge" style="background-color: #241373; font-size:16px; color:#fff;">Daftar Kontrak : </span> ' + res.json.kegiatan_detail.detail_kegiatan)
                $("h5#nama-detail-kegiatan-tagihan").html('<span class="badge badge-danger" style=" font-size:16px;">Daftar Tagihan : </span> ' + res.json.kegiatan_detail.detail_kegiatan)
            },
            columns: [{
                    "data": "no"

                }, {
                    "data": "nomor_kontrak"

                },
                {
                    "data": "tanggal_kontrak"

                },
                {
                    "data": "uraian"

                },
                {
                    "data": "nilai_kontrak"

                },
                {
                    "data": "jenis_kontrak"

                },
                {
                    data: "File"

                }
            ],
            columnDefs: [{
                    targets: 4,
                    render: function(data, type, row) {
                        return `Rp. ${new Intl.NumberFormat().format(row.nilai_kontrak)}`
                    }
                },
                {
                    targets: 5,
                    render: function(data, type, row) {
                        if (row.jenis_kontrak == 1) {
                            return "Singel Year"
                        } else if (row.jenis_kontrak == 2) {
                            return "Multi Year"
                        } else {
                            return ""
                        }
                    }
                },
                {
                    "sorting": true,
                    "orderable": true,
                    "type": "html",
                    "targets": 6,
                    // "data": id,
                    "render": function(data, type, row) {

                        var file_kontrak = ""
                        var file_myx = ""
                        var file_addendum = ""


                        if (row.file_kontrak != "") {
                            file_kontrak = `<a href='<?= base_url("kontrak/download/") ?>${row.id}/kontrak'> Download File Kontrak </a>`
                        }

                        if (row.file_myx != "") {
                            file_myx = `<a href='<?= base_url("kontrak/download/") ?>${row.id}/myx'> Download File Myc </a>`
                        }

                        if (row.file_addendum != "") {
                            file_konfile_addendumtrak = `<a href='<?= base_url("kontrak/download/") ?>${row.id}/addendum'> Download File Addendum </a>`
                        }

                        return `<span>${file_kontrak} | ${file_myx} | ${file_addendum}</span>`
                    }
                }
            ]
        }

        $('#table-laporan-kegiatan-kontrak').DataTable().destroy()
        var table = $('#table-laporan-kegiatan-kontrak').DataTable(settingTable)
        table.draw()

    }

    // id_detail_kegiatan
    function getLaporanTagihanKegiatan(id) {

        document.getElementById("tbl-tag").style.display = "block"

        var settingTable = {
            dom: 'Bfrtip',
            order: [
                [0, 'desc']
            ],
            buttons: [{
                text: 'Export to Excel',
                extend: 'excelHtml5',
                title: 'Daftar Tagihan Kegiatan',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4]
                }
            }],
            serverSide: true,
            ajax: {
                data: {
                    id: id
                },
                url: "<?= base_url("laporan/get_detail_laporan_kegiatan_tagihan") ?>",
                type: 'POST'
            },
            drawCallback: (res) => {
                // res.json.kegiatan.nama_kegiatan

                $("h5#nama-detail-kegiatan-tagihan").html('<span class="badge badge-danger" style=" font-size:16px;">Daftar Tagihan : </span> ' + res.json.kegiatan_detail.detail_kegiatan)
            },
            columns: [{
                    "data": "no"

                }, {
                    "data": "nomor_tagihan"

                },
                {
                    "data": "tanggal_tagihan"

                },
                {
                    "data": "uraian_tagihan"

                },
                {
                    "data": "nilai_tagihan"

                },

            ],
            columnDefs: [{
                    targets: 4,
                    render: function(data, type, row) {
                        return `Rp. ${new Intl.NumberFormat().format(row.nilai_tagihan)}`
                    }
                },

                {
                    "sorting": true,
                    "orderable": true,
                    "type": "html",
                    "targets": 5,
                    // "data": id,
                    "render": function(data, type, row) {

                        var base_url = "<?= base_url("tagihan/download") ?>"

                        var file_surat_permohonan = row.file_surat_permohonan ? "" : "disabled"
                        var file_invoice = row.file_invoice ? "" : "disabled"
                        var file_kwitansi = row.file_kwitansi ? "" : "disabled"
                        var file_faktur_pajak = row.file_faktur_pajak ? "" : "disabled"
                        var file_bap = row.file_bap ? "" : "disabled"
                        var file_bapp = row.file_bapp ? "" : "disabled"
                        var file_bast = row.file_bast ? "" : "disabled"
                        var file_laporan = row.file_laporan ? "" : "disabled"
                        var file_spm = row.file_spm ? "" : "disabled"
                        var file_foto_kegiatan = row.file_foto_kegiatan ? "" : "disabled"

                        var lampiran_khusus = row.lampiran.map(item => {
                            return (`<a class="dropdown-item" href="${base_url}_lampiran/${item.id}">${item.file_name}</a>`)
                        }).join(" ")

                        return `<div>
                            <span class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Lampiran Umum
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item ${file_surat_permohonan}" href="${base_url}/${row.id}/file_surat_permohonan"> Surat permohonan </a>
                                    <a class="dropdown-item ${file_invoice}"  href="${base_url}/${row.id}/file_invoice"> Invoice </a>
                                    <a class="dropdown-item ${file_kwitansi}" href="${base_url}/${row.id}/file_kwitansi"> Kwitansi </a>
                                    <a class="dropdown-item ${file_faktur_pajak}" href="${base_url}/${row.id}/file_faktur_pajak"> Faktur Pajak </a>
                                    <a class="dropdown-item ${file_bap}"  href="${base_url}/${row.id}/file_bap"> BAP </a>
                                    <a class="dropdown-item ${file_bapp}" href="${base_url}/${row.id}/file_bapp"> BAPP </a>
                                    <a class="dropdown-item ${file_bast}" href="${base_url}/${row.id}/file_bast"> BAST </a>
                                    <a class="dropdown-item ${file_laporan}"  href="${base_url}/${row.id}/file_laporan"> Laporan </a>
                                    <a class="dropdown-item ${file_spm}"  href="${base_url}/${row.id}/file_spm"> SPM </a>
                                    <a class="dropdown-item ${file_foto_kegiatan}" href="${base_url}/${row.id}/file_foto_kegiatan"> Foto Kegiatan </a>
                                </div>
                            </span>
                            <span class="dropdown">
                                <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Lampiran Khusus
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    ${lampiran_khusus}
                                </div>
                            </span>
                        </div>`
                    }
                }
            ]
        }

        $('#table-laporan-kegiatan-tagihan').DataTable().destroy()
        var table = $('#table-laporan-kegiatan-tagihan').DataTable(settingTable)
        table.draw()
    }

    $(document).ready(function(e) {

    })

    $(function() {
        getLaporanKegiatan()
    })

    $("#modalDetailLaporanKegiatan").on("hidden.bs.modal", function() {

        $('#table-laporan-kegiatan-detail').DataTable().destroy()
        $('#table-laporan-kegiatan-kontrak').DataTable().destroy()
        $('#table-laporan-kegiatan-tagihan').DataTable().destroy()

        document.getElementById("tbl-kt").style.display = "none";
        document.getElementById("tbl-tag").style.display = "none"
    })



    // $(document).on('click', '#showTableKegiatan', function(e) {
    //     showTable()
    // })
</script>