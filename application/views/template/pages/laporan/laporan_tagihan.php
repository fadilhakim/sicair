<div class="page-content">
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Laporan</li>
            <li class="breadcrumb-item active" aria-current="page">Tagihan</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">

                    <h6 style="font-size:15px !important; color: #241373 !important;">Pilih Laporan</h6>
                    <br>
                    <form id="form-laporan-kegiatan">
                        <div class="row">

                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="">Tahun Anggaran :</label>
                                    <select class="js-example-basic-single w-100" name="filter_tahun_anggaran" class="form-control" id="filter_tahun_anggaran">
                                        <option value="">Semua Tahun</option>
                                        <option value="1">2021</option>
                                        <option value="2">2022</option>
                                        <option value="3">2023</option>
                                        <option value="4">2024</option>
                                        <option value="5">2025</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="">Nama PPK :</label>
                                    <select class="js-example-basic-single w-100" data-width="100%" id="filter_nama_ppk" name="filter_nama_ppk">
                                        <option value="">Semua PPK</option>
                                        <?php foreach ($all_user as $rowuser => $value) { ?>
                                            <option value="<?= $value['id'] ?>"><?= $value['fullname'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="">Kegiatan Prioritas :</label>
                                    <select class="js-example-basic-single w-100" name="filter_prioritas" class="form-control" id="filter_prioritas">
                                        <option value="">Semua</option>
                                        <option value="1">Ya</option>
                                        <option value="0">Tidak</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="">&nbsp;<br></label>
                                    <br>
                                    <input type="button" value="Tampilkan" style="display: block !important; width:100%; height:35px" id="showTableKegiatan" class="btn btn-primary">
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <br>
    <div id="tableLaporanKegiatan" class="row">

        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <h4>Total Jumlah Pagu: <span class="badge badge-primary" id="total-pagu-kegiatan"></span></h4>
                        </div>
                        <div class="col-lg-6">
                            <h4>Total Daya Serap: <span class="badge badge-primary" id="total-kontrak"></span></h4>
                        </div>

                    </div>
                    <br>
                    <br>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table id="table-laporan-kegiatan" class="table table-hover table-striped mb-0" style="width:100%">
                                <thead>
                                    <tr>
                                        <th class="pt-0">No</th>
                                        <th style="width:120px !important;" class="pt-0">Kode Aktifitas/Kro/Ro/Komponent</th>
                                        <th class="pt-0">Nama Kegiatan</th>
                                        <th class="pt-0">Pagu</th>
                                        <th class="pt-0">Daya Serap</th>
                                        <th class="pt-0">Sisa Pagu</th>
                                        <th class="pt-0">Nama PPK</th>
                                        <th class="pt-0">Rincian</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalDetailLaporanTagihan" tabindex="-1" role="dialog" aria-labelledby="modalDetailLaporanTagihan" aria-hidden="true">
    <div style="max-width:100%" class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Tagihan : Kegiatan {nama_kegiatan}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-lampiran">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="">Jumlah Pagu :</label>
                                <input type="text" readonly disabled id='id_kontrak' class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="">Daya Serap :</label>
                                <input type="text" readonly disabled id="total_nilai_kontrak" class="form-control">
                            </div>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-lg-8">
                            <div class="table-responsive">
                                <table id="table-laporan-kegiatan-detail" class="table table-hover table-striped mb-0" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th class="pt-0">No</th>
                                            <th style="width:120px !important;" class="pt-0">Nomor SPTB</th>
                                            <th class="pt-0">TGL Tagihan</th>
                                            <th class="pt-0">Uraian</th>
                                            <th class="pt-0">Nilai Tagihan</th>
                                            <th class="pt-0">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <a href="#" style="color:#fff;" class="btn btn-primary" onclick="showLampiran(1)">Lihat Lampiran</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div id="listLampiran" class="col-lg-4">
                            <ul class="list-group">
                                <li class="list-group-item active">Lampiran Umum</li>
                                <li class="list-group-item">Dapibus ac facilisis in</li>
                                <li class="list-group-item">Morbi leo risus</li>
                                <li class="list-group-item">Porta ac consectetur ac</li>
                                <li class="list-group-item">Vestibulum at eros</li>
                            </ul>
                            <br>
                            <ul class="list-group">
                                <li class="list-group-item active">Lampiran Tambahan</li>
                                <li class="list-group-item">Dapibus ac facilisis in</li>
                                <li class="list-group-item">Morbi leo risus</li>
                                <li class="list-group-item">Porta ac consectetur ac</li>
                                <li class="list-group-item">Vestibulum at eros</li>
                            </ul>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>