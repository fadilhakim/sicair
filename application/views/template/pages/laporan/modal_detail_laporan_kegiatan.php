<!-- modal laporan detail kegiatan -->
<style>
    #table-laporan-kegiatan-kontrak_info {
        display: none !important;
    }

    #table-laporan-kegiatan-tagihan_info {
        display: none !important;
    }

    #table-laporan-kegiatan-tagihan_paginate {
        display: none !important;
    }

    #table-laporan-kegiatan-kontrak_paginate {
        display: none !important;
    }
</style>
<div class="modal fade" id="modalDetailLaporanKegiatan" tabindex="-1" role="dialog" aria-labelledby="modalDetailLaporanKegiatan" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document" style="max-width: 100%;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Kontrak : Kegiatan <span id="nama-kegiatan">{nama_kegiatan}</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Total Pagu Kegiatan:</label>
                                <input type="text" readonly disabled id='total_pagu' class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Total Nilai Kontrak :</label>
                                <input type="text" readonly disabled id="total_nilai_kontrak" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Total Daya Serap :</label>
                                <input type="text" readonly disabled id="total_nilai_tagihan" class="form-control">
                            </div>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="table-laporan-kegiatan-detail" class="table table-hover table-striped mb-0" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th class="pt-0">No</th>
                                            <th style="width:120px !important;" class="pt-0">Detail Kegiatan</th>
                                            <th class="pt-0">Pagu Kegiatan</th>
                                            <th class="pt-0">Kode MAK</th>
                                            <th class="pt-0">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>

                        </div>

                        <div style="display: none; margin-top:20px" class="col-lg-12" id="tbl-kt">
                            <h5 id="nama-detail-kegiatan"> { Nama Detail Kegiatan }</h5><br>
                            <div class="table-responsive">
                                <table id="table-laporan-kegiatan-kontrak" class="table table-hover table-striped mb-0" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th class="pt-0">No</th>
                                            <th style="width:120px !important;" class="pt-0">Nomor Kontrak</th>
                                            <th class="pt-0">Tanggal Kontrak</th>
                                            <th class="pt-0">Uraian</th>
                                            <th class="pt-0">Nilai Kontrak</th>
                                            <th class="pt-0">Jenis Kontrak</th>
                                            <th class="pt-0">File</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>

                        <div style="display: none; margin-top:20px" class="col-lg-12" id="tbl-tag">
                            <h5 id="nama-detail-kegiatan-tagihan"> { Nama Detail Kegiatan }</h5><br>
                            <div class="table-responsive">
                                <table id="table-laporan-kegiatan-tagihan" class="table table-hover table-striped mb-0" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th class="pt-0">No</th>
                                            <th style="width:120px !important;" class="pt-0">Nomor SPTB</th>
                                            <th class="pt-0">TGL Tagihan</th>
                                            <th class="pt-0">Uraian</th>
                                            <th class="pt-0">Nilai Tagihan</th>
                                            <th class="pt-0">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <a href="#" style="color:#fff;" class="btn btn-primary" onclick="showLampiran(1)">Lihat Lampiran</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                    <br><br><br>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>