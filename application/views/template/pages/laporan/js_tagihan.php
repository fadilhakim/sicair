<!-- plugin js for this page -->
<script src="<?= base_url("assets/vendors/datatables.net/jquery.dataTables.js") ?>"></script>
<script src="<?= base_url("assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js") ?>"></script>
<script src="<?= base_url("assets/vendors/sweetalert2/sweetalert2.min.js") ?>"></script>
<script src="<?= base_url("assets/vendors/select2/select2.min.js") ?>"></script>
<script src="<?= base_url("assets/js/select2.js") ?>"></script>
<!-- end plugin js for this page -->



<script src=" https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>

<script>
    var displayTable = document.getElementById("tableLaporanKegiatan");
    displayTable.style.display = 'none';
    var displayLampiran = document.getElementById("listLampiran");
    displayLampiran.style.display = 'none';

    function showTable() {

        // table.ajax.reload()

        if (displayTable.style.display === "none") {
            displayTable.style.display = "block";
        }
    }


    function getLaporanKegiatan() {

        table = $("#table-laporan-kegiatan").DataTable({
            dom: 'Bfrtip',
            paging: true,
            order: [
                [1, 'desc']
            ],
            buttons: [{
                text: 'Export to Excel',
                extend: 'excelHtml5',
                title: 'Daftar Laporan Kegiatan',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4]
                }
            }],
            serverSide: true,
            ajax: {
                data: function(data) {
                    data.filter_tahun_anggaran = $("#filter_tahun_anggaran").val()
                    data.filter_nama_ppk = $("#filter_nama_ppk").val()
                    data.filter_prioritas = $("#filter_prioritas").val()
                },
                url: "<?= base_url("laporan/get_laporan_kegiatan") ?>",
                type: 'POST',
                dataType: "json",
                //success:function(res) { 
                //var total_pagu_kegiatan = `Rp. ${new Intl.NumberFormat().format(res.total_pagu_anggaran.pagu_kegiatan)}`
                //$("#total-pagu-kegiatan").html(total_pagu_kegiatan)
                //}
                // 

            },
            drawCallback: (res) => {
                var total_pagu_kegiatan = `Rp. ${new Intl.NumberFormat().format(res.json.total_pagu_anggaran.pagu_kegiatan)}`
                $("#total-pagu-kegiatan").html(total_pagu_kegiatan)

                var total_kontrak = `Rp. ${new Intl.NumberFormat().format(res.json.total_kontrak)}`
                $("#total-kontrak").html(total_kontrak)
            },
            columns: [{
                    "data": "no"
                },
                {
                    "data": "kode_aktivitas"
                },
                {
                    "data": "nama_kegiatan"
                },
                {
                    "data": "pagu_kegiatan"
                },
                {
                    "data": "nilai_kontrak"
                },
                {
                    "data": "sisa_pagu"
                },
                {
                    "data": "nama_ppk"
                },
                {
                    "data": "Rincian"
                }

            ],
            columnDefs: [{
                    targets: 3,
                    render: function(data, type, row) {
                        return `Rp. ${new Intl.NumberFormat().format(row.pagu_kegiatan)}`
                    }
                },
                {
                    targets: 4,
                    render: function(data, type, row) {
                        return `Rp. ${new Intl.NumberFormat().format(row.nilai_kontrak)}`
                    }
                },
                {
                    targets: 5,
                    render: function(data, type, row) {
                        return `Rp. ${new Intl.NumberFormat().format(row.sisa_pagu)}`
                    }
                },

                {
                    "sorting": false,
                    "orderable": false,
                    "type": "html",
                    "targets": 7,
                    "render": function(data, type, row) {
                        //console.log("data => ", data, type, row)

                        const newRow = row

                        return `<a data-toggle="modal" data-target="#modalDetailLaporanTagihan" onClick="getLaporanDetailKegiatan(${row.id})" class="btn btn-info">Rincian Kegiatan</a>`

                    }
                }
            ]
        })

        $("#showTableKegiatan").click(function(e) {
            showTable()
            table.draw()
        })


    }

    function showLampiran(id) {

        // table.ajax.reload()

        if (displayLampiran.style.display === "none") {
            displayLampiran.style.display = "block";
        }
    }


    $(function() {
        getLaporanKegiatan()
    })
</script>