<div class="page-content">
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Laporan</li>
            <li class="breadcrumb-item active" aria-current="page">Kontrak Kegiatan</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">

                    <h6 style="font-size:15px !important; color: #241373 !important;">Pilih Laporan</h6>
                    <br>
                    <form id="form-laporan-kegiatan">
                        <div class="row">

                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="">Tahun Anggaran :</label>
                                    <select class="js-example-basic-single w-100" name="filter_tahun_anggaran" class="form-control" id="filter_tahun_anggaran">
                                        <option value="">Semua Tahun</option>
                                        <option value="1">2021</option>
                                        <option value="2">2022</option>
                                        <option value="3">2023</option>
                                        <option value="4">2024</option>
                                        <option value="5">2025</option>
                                    </select>
                                </div>
                            </div>
                            <?php if ($this->session->userdata("group_id") != 2) { ?>

                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="">Nama PPK :</label>
                                        <select class="js-example-basic-single w-100" data-width="100%" id="filter_nama_ppk" name="filter_nama_ppk">
                                            <option value="">Semua PPK</option>
                                            <?php foreach ($all_user as $rowuser => $value) { ?>
                                                <option value="<?= $value['id'] ?>"><?= $value['fullname'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                            <?php } else { ?>
                                <input style="display: none;" id="filter_nama_ppk" value="<?= $this->session->userdata("user_id") ?>" name="filter_nama_ppk" type="text">
                            <?php } ?>

                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="">Kegiatan Prioritas :</label>
                                    <select class="js-example-basic-single w-100" name="filter_prioritas" class="form-control" id="filter_prioritas">
                                        <option value="">Semua</option>
                                        <option value="1">Ya</option>
                                        <option value="0">Tidak</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="">&nbsp;<br></label>
                                    <br>
                                    <input type="button" value="Tampilkan" style="display: block !important; width:100%; height:35px" id="showTableKegiatan" class="btn btn-primary">
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <br>
    <div id="tableLaporanKegiatan" class="row">

        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <h4>Total Pagu: <span class="badge badge-primary" id="total-pagu-kegiatan">Rp. 0</span></h4>
                        </div>
                        <div class="col-lg-6">
                            <h4>Total Kontrak: <span class="badge badge-primary" id="total-kontrak">Rp. 0</h4>
                        </div>

                    </div>
                    <br>
                    <br>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table id="table-laporan-kegiatan" class="table table-hover table-striped mb-0" style="width:100%">
                                <thead>
                                    <tr>
                                        <th class="pt-0">No</th>
                                        <th style="width:120px !important;" class="pt-0">Kode Aktifitas/Kro/Ro/Komponent</th>
                                        <th class="pt-0">Nama Kegiatan</th>
                                        <th class="pt-0">Pagu Anggaran</th>
                                        <th class="pt-0">Nilai Kontrak</th>
                                        <th class="pt-0">Sisa Pagu</th>
                                        <th class="pt-0">Nama PPK</th>
                                        <th class="pt-0">Rincian</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view("template/pages/laporan/modal_detail_laporan_kegiatan") ?>