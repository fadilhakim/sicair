<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
/* All List Projects Pages */
$route['all-projects/update/:num'] = 'project/update_project';
$route['all-projects/details'] = 'project/view_detail_projects';
$route['all-projects'] = 'project/show_all_projects';

/* History Pages */
$route['history/project-update'] = 'history/show_all_project_update';
$route['history/project-update/detail'] = 'history/show_project_update_detail';
$route['history/users'] = 'history/show_history_users';
$route['history/users/detail'] = 'history/show_history_users_detail';

$route["user/detail/:num"]    = "user/user_detail";
$route["user/update/process"] = "user/user_update_process";
$route["user/add/process"]    = "user/user_add_process";

$route["group/roles/update"] = "group/group_roles_update_process";
$route["group/roles/:num"]    = "group/group_roles";
$route["group/detail/:num"]    = "group/group_detail";
$route["group/update/process"] = "group/group_update_process";
$route["group/add/process"]    = "group/group_add_process";
$route["group/list"]       = "group/group_list";
$route["group/add"]         = "group/group_add";
$route["group/delete"]      = "group/group_delete_process";

$route["user/list"]        = "user/user_list";
$route["user/add"]         = "user/user_add";
$route["user/delete"]      = "user/user_delete_process";

$route["login/process"]    = "auth/login_process";
$route["login"] = "auth";
$route["login/pass"] = "auth/pass";
$route["logout"] = "auth/logout";
$route["user"] = "user";
$route["group"] = "group";
$route["project"] = "project";

$route["unauthorized"] = "page/unauthorized";
$route['default_controller'] = 'dashboard';
$route['404_override'] = 'page/notfound';
$route['translate_uri_dashes'] = FALSE;

// dashboard
$route['dashboard_chart'] = 'dashboard';

//Anggaran
$route['anggaran_list'] = 'anggaran';
$route['tambah_anggaran'] = 'anggaran/tambah_anggaran';
$route['detail_anggaran/:num'] = 'anggaran/detail_anggaran';
$route['anggaran/tambah_anggaran_proses'] = 'anggaran/tambah_anggaran_proses';
$route['anggaran/get_anggaran'] = 'anggaran/get_anggaran';

$route['anggaran/tambah_kegiatan_process'] = 'anggaran/tambah_kegiatan_process';

// Kegiatan
$route['kegiatan'] = 'kegiatan';
$route['tambah_kegiatan/:num'] = 'kegiatan/tambah_kegiatan';
$route['kegiatan/detail_kegiatan/:num'] = 'kegiatan/view_detail_kegiatan';


//Kontrak
$route['kontrak']                           = 'kontrak';
$route['kontrak/download/:num/:any']        = 'kontrak/download_file';
$route['detail_kontrak/:num']               = 'kontrak/detail_kontrak';

$route['tambah_kontrak']        = 'kontrak/tambah_kontrak';
$route['anggaran/get_kontrak']  = 'kontrak/get_kontrak';


// Pencairan Anggaran Atau Tagihan
$route['tagihan/pencairan_anggaran']     = 'tagihan';
$route['tagihan/:num']                   = 'tagihan/detail_pencairan';
$route['tagihan/pengajuan_tagihan']      = 'tagihan/pengajuan_tagihan';
$route['tagihan/pengajuan_tagihan/:num'] = 'tagihan/detail_tagihan';
$route['tagihan/download/:num/:any']     = 'tagihan/download';
$route['tagihan/download_lampiran/:num'] = 'tagihan/download_lampiran';

// Laporan
$route['laporan'] = 'laporan/kegiatan';
$route['laporan/tagihan'] = 'laporan/tagihan';
