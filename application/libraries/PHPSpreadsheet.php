<?php 

    include "./vendor/autoload.php";

    // use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;

    class PHPSpreadsheet {

        function __construct() {
            $this->ci =& get_instance();
        }

        function uploadFileSpreadsheet($dt) {
            
            $this->ci->load->model("projects_model");
            $this->ci->load->model("project_timeline_model");

            if($_FILES["select_excel"]["name"] != '') {

                $allowed_extension = array('xls', 'xlsx', "csv");
                $file_array = explode(".", $_FILES['select_excel']['name']);
                // print_r($file_array);
                $file_extension = end($file_array);

                if(in_array($file_extension, $allowed_extension)) {

                    if($file_extension == 'csv'){
                        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
                    } else if($file_extension == 'xlsx') {
                        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                    } else {
                        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                    }

                    // file path
                    $spreadsheet = $reader->load($_FILES['select_excel']['tmp_name']);
                    $allDataInSheet = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
                    
                    $isError = 0;
                    $errCollection = array(); 

                    foreach($allDataInSheet as $key => $val) {
                        if($key > 1) {
                            // check SID 
                            $project_update_location_detail = $this->ci->projects_model->check_sid($val["A"], $dt["project_update_id"]);
                            
                            
                            if(!empty($project_update_location_detail)){
                                 /*
                                    [A] => 100005640001
                                    [B] => Pengadaan
                                    [C] => 24/10/2020
                                    [D] => 24/10/2020
                                    [E] => 24/11/2020
                                    [F] => teknisinya harus ada tambahan mcu. Teknisi sedang MCU.
                                */

                                $project_status = $this->ci->projects_model->get_detail_status($val["B"]);
                                $status_date    = date("Y-m-d", strtotime(str_replace("/","-",$val["C"])));
                                $start_progress = date("Y-m-d", strtotime(str_replace("/","-",$val["D"]))); 
                                $tol            = date("Y-m-d", strtotime(str_replace("/","-",$val["E"])));

                                //print($start_progress); exit;

                                $data = array(                           
                                    "project_status_id"  => $project_status["id"],
                                    "project_status_date"=> $status_date,
                                    "tgl_terima_proses"  => $start_progress,
                                    "tol"                => $tol,
                                    "keterangan"         => $val["F"]
                                );

                                //$this->ci->projects_model->project_update_location_insert($data);
                                $this->ci->projects_model->project_update_location_update($data, $project_update_location_detail["id"]);

                                
                                $pars["sid"] = $val["A"];
                                // print_r( $pars);
                                $project_update_location_detail_dt = $this->ci->projects_model->project_update_location_detail($pars);
                                
                                $params["workorder_id"] = $project_update_location_detail_dt['h_workorder_id'];
                                $project_update_detail =$this->ci->projects_model->get_detail_data_project_update($params);

                                $project_update_location_id = $project_update_location_detail_dt["id"];
                                $last_timeline = $this->ci->project_timeline_model->get_last_timeline_nodelink($project_update_location_id);

                                // print_r($last_timeline);

                                if(!empty($last_timeline)) {
                                    if($last_timeline["status"] != $project_status["id"]) {
                                        $this->ci->project_timeline_model->insert_timeline([
                                            "project_update_location_id" => $project_update_location_id,
                                            "status"                     => $project_status["id"],
                                            "status_date"                => $status_date,
                                            "status_time"                => date("H:i:s"),
                                            "keterangan"                 => $val["F"],
                                        ]);
                                    }

                                } else {
                                    $this->ci->project_timeline_model->insert_timeline([
                                        "project_update_location_id" => $project_update_location_id,
                                        "status"                     => $project_status["id"],
                                        "status_date"                => $status_date,
                                        "status_time"                => date("H:i:s"),
                                        "keterangan"                 => $val["F"],
                                    ]);
                                }

                                $this->ci->projects_model->pul_record_insert([
                                    "user_id" 					 		=> $this->ci->session->userdata("user_id"),
                                    "username" 					 		=> $this->ci->session->userdata("username"),
                                    "project_update_location_id" 		=> $project_update_location_id,
                                    "project_update_id"					=> $project_update_detail["project_update_id"],
                                    // "location" 							=> $project_update_location_detail["lokasi_instalasi"],
                                    "nama_project" 						=> $project_update_detail["project_nama"],
                                    "pelanggan"							=> $project_update_detail["nama_pelanggan"],
                                    "no_wo"								=> $project_update_detail["no_wo"],
                                    "pic"								=> $project_update_location_detail_dt["pic"],
                                    "project_status"					=> $project_update_location_detail_dt["status_name"],
                                    "keterangan"						=> $val["F"],
                                ]);

                            } else {

                                $isError = 1;
                                $errCollection[] = " SID ".$val["A"]." in row ".($key - 1)." is not valid. <br>";
                                $errCollection["project_update_detail"] = $project_update_detail;
                                $errCollection["sid"] = $val["A"];
                                $errCollection["project_update_id"] =$dt["project_update_id"]; 

                            }
                        }
                    }

                    $message = array(
                        "status" => "success",
                        "message" => "You successfully updated project node by the csv data. ",
                        "errCollection" => $errCollection
                    );
                    
                    
                }
                else {
                    $message = array(
                        "status" => "error",
                        "message" => '<div class="alert alert-danger">Only .xls or .xlsx file allowed</div>'
                    );
                    
                }
            }
            else {

                $message = array(
                    "status" => "error",
                    "message" => '<div class="alert alert-danger">Please Select File</div>'
                );
               
            }

            return json_encode($message);
        }

    }