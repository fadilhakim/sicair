<?php 

    // ini adalah function yg mau kita test 
    function myfunction($params) {
        return $params * $params;
    }

    //
    function myfunction_unit_test($thisfunction,$expected_output){
        return $thisfunction === $expected_output;
    }

    // run unit test
    echo myfunction_unit_test(myfunction(4),16);
    echo "<br>";
    echo myfunction_unit_test(myfunction(5),25);
    echo "<br>";
    echo myfunction_unit_test(myfunction(6),36);


