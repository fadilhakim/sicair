<?php 

    class MyUpload  {

        function __construct() { 
             $this->ci =& get_instance();
        }

        /*
            $configuration = [
                "filename"    => "",
                "prefix_name" => "", 
                "nomor"       => 
            ]
        */

        function format_filename($configuration) { 
            $filename   = explode(".", $configuration["filename"]);
            $extension  = $filename[count($filename) - 1];
            $newfilename   = strtolower($configuration["prefix_name"] ."-". str_replace("/","",$configuration["nomor"]) . "-" . date("YmdHis") . "." . $extension);
        
            return $newfilename;
        }

         /*
            $configuration = [
                "file"        => "test",
                "upload_path" => "./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]),
                "allowed_types" => "",
                "max_size" => 5000,
                "prefix" => "",
                "success_message" => "Berhasil menambahkan tagihan. "
            ]

        */

        function upload($configuration) { 

            if (!empty($_FILES[$configuration["file"]]["name"])) {

                if (!is_dir( $configuration["upload_path"] )) {
                    mkdir($configuration["upload_path"],0777, true);
                }

                $this->ci->load->library('upload',[
                    "upload_path"   => $configuration["upload_path"] . "/",
                    "allowed_types" => $configuration["allowed_types"],
                    "max_size"      => $configuration["max_size"],
                    "file_name"     => $configuration["file_name"],
                ]);
                
                // IF FILE UPLOADED NOT VALIDATE
                if (!$this->ci->upload->do_upload($configuration["file"])) {
                    // $this->mom_model->add_mom($data);
                    $error_file = $this->ci->upload->display_errors();

                    return json_encode([
                        "message" => "<div>".$configuration["file"]." : ". $error_file."</div>",
                    ]);

                } else {
                    return json_encode([ "message" => "<div>".$configuration["success_message"]."</div>" ]);
                }
            } else {
                return json_encode([ "message" => "<div>".$configuration["file"]." NOT uploaded"."</div>" ]);
            }
        }

    }