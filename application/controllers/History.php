<?php 

    class History extends CI_Controller {
        function __construct(){
            parent::__construct();
            validSessionIsOut();
            $this->authorization->redirect_menu("project_history");
        }

        function index() {
            
        }

        function show_all_project_update(){
            
            $data["title"] = "Project Update";

            $data["css"] = "/template/pages/history/css"; // path 
            $data["js"] = "/template/pages/history/js"; // path

            $dt = array();
            $data["content"] = $this->load->view("template/pages/history/history_project_update",$dt,true);

            $this->load->view("template/index",$data);
        }

        function show_history_users(){
            $data["title"] = "Users";

            $data["css"] = "/template/pages/history/css"; // path 
            $data["js"] = "/template/pages/history/js"; // path

            $dt = array();
            $data["content"] = $this->load->view("template/pages/history/history_users",$dt,true);

            $this->load->view("template/index",$data);
        }

        function show_project_update_detail(){

            $data["title"] = "Detail";

            $data["css"] = "/template/pages/history/css"; // path 
            $data["js"] = "/template/pages/history/js"; // path

            $dt = array();
            $data["content"] = $this->load->view("template/pages/history/project_update_detail",$dt,true);

            $this->load->view("template/index",$data);
        }

        function show_history_users_detail(){

            $this->load->model("user_model");

            $session_id = $this->uri->segment(3);
            $data["title"] = "Detail";

            $data["css"] = "/template/pages/history/css"; // path 
            $data["js"] = "/template/pages/history/js"; // path

            $project_update_history  = $this->user_model->get_user_project_nodelink_history($session_id);
            $user_history_detail = $this->user_model->user_history_detail($session_id);
            $user_detail = $this->user_model->get_user_detail($user_history_detail["user_id"]);

            $dt = array(
                "user_detail"            => $user_detail,
                "project_update_history" => $project_update_history
            );
            $data["content"] = $this->load->view("template/pages/history/users_detail",$dt,true);

            $this->load->view("template/index",$data);
        }

        function login_record_list() {

            $this->load->model("user_model");
            $this->load->model("projects_model");

            $draw   = intval($this->input->post("draw"));
            $start  = intval($this->input->post("start"));
            $length = intval($this->input->post("length"));
            $order  = $this->input->post("order");
            $search = $this->input->post("search");
            $search = $search['value'];
            $col    = 0;
            $dir    = "";

            $params = array(
                "limit"     =>$length,
                "offset"    =>$start,
                "order"     =>$order,
                "col"       =>$col,
                "dir"       =>$dir,
                "search"    =>$search,
            );

            $login_records = $this->user_model->get_login_records($params);
            

            $data = array();
            foreach($login_records as $rows)
            {
                $last_project_update_location = $this->user_model->get_last_user_project_nodelink_history($rows["session_id"]);

                // print_r($last_project_update_location);

                $data[] = array(
                    "id"         => $rows["id"],
                    "user_id"    => $rows["user_id"],
                    "username"   => $rows["username"],
                    "session_id" => $rows["session_id"],
                    "ip_address" => $rows["ip_address"],
                    "location"   => $rows["location"],
                    "created_at" => $rows["created_at"],
                    "last_update" => $last_project_update_location["nama_project"] ?? ""
                    // action tak usah di definisikan
                );
            }

            print json_encode([
                "draw"=>$draw,
                "recordsTotal" => count($login_records),
                "recordsFiltered" => count($login_records),
                "data" => $data
            ]);
        } 

        function project_update_location_record() {
            $this->load->model("projects_model");

            $draw   = intval($this->input->post("draw"));
            $start  = intval($this->input->post("start"));
            $length = intval($this->input->post("length"));
            $order  = $this->input->post("order");
            $search = $this->input->post("search");
            $search = $search['value'];
            $col    = 0;
            $dir    = "";

            $params = array(
                "limit"     =>$length,
                "offset"    =>$start,
                "order"     =>$order,
                "col"       =>$col,
                "dir"       =>$dir,
                "search"    =>$search,
            );

            $pul_records = $this->projects_model->get_project_update_location_history($params);
            

            $data = array();
            foreach($pul_records as $rows)
            {
                $status_detail          = $this->projects_model->get_detail_status($rows["project_status"]);
                $project_update_detail  = $this->projects_model->project_update_detail($rows["project_update_id"]);

                $data[] = array(
                    "id"                => $rows["id"],
                    "project_update_id" => $rows["project_update_id"],
                    "project_update_location_id" => $rows["project_update_location_id"],
                    "workorder_id"      => $project_update_detail["h_workorder_id"],
                    "user_id"           => $rows["user_id"],
                    "username"          => $rows["username"],
                    "nama_project"      => $rows["nama_project"],
                    "pelanggan"         => $rows["pelanggan"],
                    "no_wo"             => $rows["no_wo"],
                    "pic"               => $rows["pic"],
                    "project_status"    => $rows["project_status"],
                    "status_color"      => $status_detail["status_color"],
                    "created_at"        => $rows["created_at"],
                    // action tak usah di definisikan
                );
            }

            print json_encode([
                "draw"=>$draw,
                "recordsTotal" => count($pul_records),
                "recordsFiltered" => count($pul_records),
                "data" => $data
            ]);
        } 

    }