<?php

class Laporan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        validSessionIsOut();
        $this->authorization->redirect_menu("dashboard");
        $this->load->model("laporan_model");
        $this->load->model("user_model");
    }

    function kegiatan()
    {

        validSessionIsOut();
        $data["title"] = "Laporan Kegiatan";
        $data["css"] = "/template/pages/laporan/css"; // path
        $data["js"] = "/template/pages/laporan/js"; // path
        $this->load->model('user_model');
        $all_user =  $this->user_model->get_all_user_ppk();
        $dt = array(
            'all_user' => $all_user
        );

        $data["content"] = $this->load->view("template/pages/laporan/laporan_kegiatan", $dt, true);
        $this->load->view("template/index", $data);
    }

    function get_laporan_kegiatan()
    {

        $draw   = intval($this->input->post("draw"));
        $start  = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order  = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col    = 0;
        $dir    = "";

        $userID = $this->session->userdata("user_id");

        $filter_tahun_anggaran     = $this->input->post("filter_tahun_anggaran");
        $filter_nama_ppk           = $this->input->post("filter_nama_ppk");
        $filter_prioritas          = $this->input->post("filter_prioritas");

        $params = array(
            "limit"             => $length,
            "offset"            => $start,
            "order"             => $order,
            "col"               => $col,
            "dir"               => $dir,
            "search"            => $search,

            "filter_tahun_anggaran" => $filter_tahun_anggaran,
            "filter_nama_ppk"       => $filter_nama_ppk,
            "filter_prioritas"      => $filter_prioritas,

        );

        // if ($this->session->userdata("group_id") == 2) {
        //     $list_laporan_kegiatan  = $this->laporan_model->get_all_kegiatan_by_ppk($params, $userID);
        //     $params["count"]        = true;
        //     $filter_laporan_count   = $this->laporan_model->get_all_kegiatan_by_ppk($params, $userID);

        //     $params2["count"]       = true;
        //     $total_laporan_count    = $this->laporan_model->get_all_kegiatan_by_ppk($params2, $userID);
        // } else {
        //     $list_laporan_kegiatan  = $this->laporan_model->get_all_kegiatan($params);
        //     $params["count"]        = true;
        //     $filter_laporan_count   = $this->laporan_model->get_all_kegiatan($params);

        //     $params2["count"]       = true;
        //     $total_laporan_count    = $this->laporan_model->get_all_kegiatan($params2);
        // }

        $list_laporan_kegiatan  = $this->laporan_model->get_all_kegiatan($params);
        $params["count"]        = true;
        $filter_laporan_count   = $this->laporan_model->get_all_kegiatan($params);

        $params2["count"]       = true;
        $total_laporan_count    = $this->laporan_model->get_all_kegiatan($params2);

        $params3["filter_tahun_anggaran"] = $filter_tahun_anggaran;
        $params3["filter_nama_ppk"]       = $filter_nama_ppk;
        $params3["filter_prioritas"]      = $filter_prioritas;
        $params3["sum"]                   = true;
        $total_laporan_sum                = $this->laporan_model->get_all_kegiatan($params3);

        $total_kontrak                    = 0;

        $data = array();
        $no = 1;

        foreach ($list_laporan_kegiatan as $rows) {
            $nama_ppk =  $this->user_model->get_specific_user_ppk($rows['id_user']);
            foreach ($nama_ppk as $rowuser) {
                $datauser[] = array(
                    'fullname' => $rowuser['fullname']
                );
            }

            $nilai_kontrak = $this->laporan_model->kontrak_kegiatan($rows["id"])["total_kontrak"];
            $total_kontrak = $total_kontrak + $nilai_kontrak;
            $sisa_pagu     = $rows["pagu_kegiatan"] - $nilai_kontrak;

            $data[] = array(
                "no"                => $no,
                "id"                => $rows["id"],
                "kode_aktivitas"    => $rows["kode_aktivitas"] . "/" . $rows["kro"] . "/" . $rows["ro"] . "/" . $rows["komponen"] . "-" . $rows["subkomponen"],
                "nama_kegiatan"     => $rows['nama_kegiatan'],
                "pagu_kegiatan"     => $rows["pagu_kegiatan"],
                "nilai_kontrak"     => $nilai_kontrak,
                "sisa_pagu"         => $sisa_pagu,
                "nama_ppk"          => $rowuser['fullname']
            );

            $no++;
        }
        // echo '<pre>';
        // var_dump($data);
        // echo '</pre>';
        print json_encode([
            "draw"                  => $draw,
            "recordsTotal"          => $filter_laporan_count,
            "recordsFiltered"       => $total_laporan_count,
            "total_pagu_anggaran"   => $total_laporan_sum,
            "total_kontrak"         => $total_kontrak,
            "data"                  => $data
        ]);
    }

    function get_detail_laporan_kegiatan()
    {
        $this->load->model("kegiatan_model");

        error_reporting(0);
        $id     = $this->input->post("id", true);

        $draw   = intval($this->input->post("draw"));
        $start  = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order  = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col    = 0;
        $dir    = "";

        $kegiatan_detail = $this->kegiatan_model->get_kegiatan($id);

        $params = array(
            "limit"     => $length,
            "offset"    => $start,
            "order"     => $order,
            "col"       => $col,
            "dir"       => $dir,
            "search"    => $search,
        );
        $list_kontrak_by_id_kegiatan  = $this->laporan_model->get_all_kegiatan_by_id_detail_kegiatan($params, $id);
        $params["count"] = true;
        $params2["count"] = true;
        $total_detail_kegiatan_count = $this->laporan_model->get_all_kegiatan_by_id_detail_kegiatan($params2, $id);
        $data = array();
        $no = 1;

        $total_pagu = 0;
        $total_nilai_kontrak = $this->laporan_model->kontrak_kegiatan($id)["total_kontrak"];
        $total_nilai_tagihan = $this->laporan_model->tagihan_kegiatan($id)["total_tagihan"];;

        foreach ($list_kontrak_by_id_kegiatan as $rowKontrak) {

            $total_pagu = $total_pagu + $rowKontrak["pagu_kegiatan"];

            $data[] = array(
                "no"                           => $no,
                "id"                           => $rowKontrak["id"],
                "kode_mak"                     => $rowKontrak["kode_mak"],
                "detail_kegiatan"              => $rowKontrak["detail_kegiatan"],
                "pagu_kegiatan"                => $rowKontrak["pagu_kegiatan"]
            );

            $no++;
        }

        // echo '<pre>';
        // var_dump($data);
        // echo '</pre>';

        print json_encode([
            "draw" => $draw,
            "recordsTotal"        => $total_detail_kegiatan_count,
            "recordsFiltered"     => $total_detail_kegiatan_count,
            "total_nilai_kontrak" => $total_nilai_kontrak,
            "total_nilai_tagihan" => $total_nilai_tagihan,
            "total_pagu"          => $total_pagu,

            "kegiatan"            => $kegiatan_detail,
            "data"                => $data
        ]);
    }

    function get_detail_laporan_kegiatan_kontrak()
    {
        $this->load->model("kegiatan_model");

        error_reporting(0);
        $id     = $this->input->post("id", true);
        $draw   = intval($this->input->post("draw"));
        $start  = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order  = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col    = 0;
        $dir    = "";

        $kegiatan_detail_detail = $this->kegiatan_model->detail_detail_kegiatan($id);
        $kegiatan_detail = $this->kegiatan_model->detail_detail_kegiatan($kegiatan_detail_detail["id_kegiatan"]);

        $params = array(
            "limit"     => $length,
            "offset"    => $start,
            "order"     => $order,
            "col"       => $col,
            "dir"       => $dir,
            "search"    => $search,
        );
        $list_kontrak_by_id_detail_kegiatan  = $this->laporan_model->list_kontrak_by_id_detail_kegiatan($params, $id);
        $params["count"] = true;
        $params2["count"] = true;
        $list_kontrak_by_id_detail_kegiatan_count = $this->laporan_model->list_kontrak_by_id_detail_kegiatan($params2, $id);
        $data = array();
        $no = 1;



        foreach ($list_kontrak_by_id_detail_kegiatan as $rowKontrak) {



            $data[] = array(
                "id"                           => $rowKontrak["id"],
                "no"                           => $no,
                "id_detail_kegiatan"           => $rowKontrak["id_detail_kegiatan"],
                "nomor_kontrak"                => $rowKontrak["no_kontrak"],
                "tanggal_kontrak"              => $rowKontrak["tanggal_kontrak"],
                "uraian"                       => $rowKontrak["uraian"],
                "nilai_kontrak"                => $rowKontrak["nilai_kontrak"],
                "jenis_kontrak"                => $rowKontrak["jenis_kontrak"]
            );

            $no++;
        }

        // echo '<pre>';
        // var_dump($data);
        // echo '</pre>';

        print json_encode([
            "draw" => $draw,
            "recordsTotal" => $list_kontrak_by_id_detail_kegiatan_count,
            "recordsFiltered" => $list_kontrak_by_id_detail_kegiatan_count,

            "data" => $data,
            "kegiatan_detail" => $kegiatan_detail_detail,
            "kegiatan"  => $kegiatan_detail
        ]);
    }

    function get_detail_laporan_kegiatan_tagihan()
    {
        $this->load->model("kegiatan_model");
        $this->load->model("tagihan_model");

        error_reporting(0);
        $id     = $this->input->post("id", true);
        $draw   = intval($this->input->post("draw"));
        $start  = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order  = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col    = 0;
        $dir    = "";

        $kegiatan_detail_detail = $this->kegiatan_model->detail_detail_kegiatan($id);
        $kegiatan_detail = $this->kegiatan_model->detail_detail_kegiatan($kegiatan_detail_detail["id_kegiatan"]);

        $params = array(
            "limit"     => $length,
            "offset"    => $start,
            "order"     => $order,
            "col"       => $col,
            "dir"       => $dir,
            "search"    => $search,
        );
        $list_tagihan_by_id_detail_kegiatan  = $this->laporan_model->list_tagihan_by_id_detail_kegiatan($params, $id);
        $params["count"] = true;
        $params2["count"] = true;
        $list_tagihan_by_id_detail_kegiatan_count = $this->laporan_model->list_tagihan_by_id_detail_kegiatan($params2, $id);
        $data = array();
        $no = 1;

        foreach ($list_tagihan_by_id_detail_kegiatan as $rowTagihan) {

            $date_tagihan = date_create($rows["created_at"]);
            $date_tagihan =  date_format($date_tagihan, "d/m/Y");

            $data[] = array(
                "id"                           => $rowTagihan["id"],
                "no"                           => $no,
                "id_detail_kegiatan"           => $rowTagihan["id_detail_kegiatan"],
                "nomor_tagihan"                => $rowTagihan["nomor_tagihan"],
                "tanggal_tagihan"              => $date_tagihan,
                "uraian_tagihan"               => $rowTagihan["uraian_tagihan"],
                "nilai_tagihan"                => $rowTagihan["nilai_tagihan"],
                "lampiran"                     => $this->tagihan_model->lampiran_list($rowTagihan["id"]),
                "file_surat_permohonan"        => $rowTagihan["file_surat_permohonan"],
                "file_invoice"                 => $rowTagihan["file_invoice"],
                "file_kwitansi"                => $rowTagihan["file_kwitansi"],
                "file_faktur_pajak"            => $rowTagihan["file_faktur_pajak"],
                "file_bap"                     => $rowTagihan["file_bap"],
                "file_bapp"                    => $rowTagihan["file_bapp"],
                "file_bast"                    => $rowTagihan["file_bast"],
                "file_laporan"                 => $rowTagihan["file_laporan"],
                "file_spm"                     => $rowTagihan["file_spm"],
                "file_foto_kegiatan"           => $rowTagihan["file_foto_kegiatan"],
            );

            $no++;
        }

        // echo '<pre>';
        // var_dump($data);
        // echo '</pre>';

        print json_encode([
            "draw" => $draw,
            "recordsTotal" => $list_tagihan_by_id_detail_kegiatan_count,
            "recordsFiltered" => $list_tagihan_by_id_detail_kegiatan_count,
            "data" => $data,
            "kegiatan_detail" => $kegiatan_detail_detail,
            "kegiatan"  => $kegiatan_detail
        ]);
    }

    function tagihan()
    {
        validSessionIsOut();
        $data["title"] = "Laporan Tagihan";
        $data["css"] = "/template/pages/laporan/css"; // path
        $data["js"] = "/template/pages/laporan/js_tagihan"; // path
        $this->load->model('user_model');
        $all_user =  $this->user_model->get_all_user_ppk();
        $dt = array(
            'all_user' => $all_user
        );

        $data["content"] = $this->load->view("template/pages/laporan/laporan_tagihan", $dt, true);
        $this->load->view("template/index", $data);
    }

    function get_laporan_tagihan()
    {
        $draw   = intval($this->input->post("draw"));
        $start  = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order  = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col    = 0;
        $dir    = "";

        $filter_tahun_anggaran     = $this->input->post("filter_tahun_anggaran");
        $filter_nama_ppk           = $this->input->post("filter_nama_ppk");
        $filter_prioritas          = $this->input->post("filter_prioritas");

        $params = array(
            "limit"             => $length,
            "offset"            => $start,
            "order"             => $order,
            "col"               => $col,
            "dir"               => $dir,
            "search"            => $search,

            "filter_tahun_anggaran" => $filter_tahun_anggaran,
            "filter_nama_ppk"       => $filter_nama_ppk,
            "filter_prioritas"      => $filter_prioritas,

        );

        $list_laporan_kegiatan  = $this->laporan_model->get_all_kegiatan($params);
        $params["count"]        = true;
        $filter_laporan_count   = $this->laporan_model->get_all_kegiatan($params);

        $params2["count"]       = true;
        $total_laporan_count    = $this->laporan_model->get_all_kegiatan($params2);

        $params3["filter_tahun_anggaran"] = $filter_tahun_anggaran;
        $params3["filter_nama_ppk"]       = $filter_nama_ppk;
        $params3["filter_prioritas"]      = $filter_prioritas;
        $params3["sum"]                   = true;
        $total_laporan_sum                = $this->laporan_model->get_all_kegiatan($params3);

        $total_kontrak                    = 0;

        $data = array();
        $no = 1;

        foreach ($list_laporan_kegiatan as $rows) {
            $nama_ppk =  $this->user_model->get_specific_user_ppk($rows['id_user']);
            foreach ($nama_ppk as $rowuser) {
                $datauser[] = array(
                    'fullname' => $rowuser['fullname']
                );
            }

            $nilai_kontrak = $this->laporan_model->kontrak_kegiatan($rows["id"])["total_kontrak"];
            $total_kontrak = $total_kontrak + $nilai_kontrak;
            $sisa_pagu     = $rows["pagu_kegiatan"] - $nilai_kontrak;

            $data[] = array(
                "no"                => $no,
                "id"                => $rows["id"],
                "kode_aktivitas"    => $rows["kode_aktivitas"] . "/" . $rows["kro"] . "/" . $rows["ro"] . "/" . $rows["komponen"] . "-" . $rows["subkomponen"],
                "nama_kegiatan"     => $rows['nama_kegiatan'],
                "pagu_kegiatan"     => $rows["pagu_kegiatan"],
                "nilai_kontrak"     => $nilai_kontrak,
                "sisa_pagu"         => $sisa_pagu,
                "nama_ppk"          => $rowuser['fullname']
            );

            $no++;
        }
        // echo '<pre>';
        // var_dump($data);
        // echo '</pre>';
        print json_encode([
            "draw"                  => $draw,
            "recordsTotal"          => $filter_laporan_count,
            "recordsFiltered"       => $total_laporan_count,
            "total_pagu_anggaran"   => $total_laporan_sum,
            "total_kontrak"         => $total_kontrak,
            "data"                  => $data
        ]);
    }
}
