<?php

class Tagihan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        validSessionIsOut();
        $this->authorization->redirect_menu("dashboard");
        $this->db2 = $this->load->database();
    }

    function index()
    {

        validSessionIsOut();

        $data["title"] = "List Pencairan";
        $data["css"] = "/template/pages/tagihan/css"; // path
        $data["js"] = "/template/pages/tagihan/js"; // path

        $this->load->model("user_model");
        $all_user =  $this->user_model->get_all_user_ppk();
        $dt = array(
            'all_user' => $all_user
        );
        $data["content"] = $this->load->view("template/pages/tagihan/list_pencairan", $dt, true);
        $this->load->view("template/index", $data);
    }


    function pengajuan_tagihan()
    {

        validSessionIsOut();

        $this->load->model("kontrak_model");
        $this->load->model("kegiatan_model");

        $data["title"] = "Pengajuan Tagihan";
        $data["css"] = "/template/pages/tagihan/css"; // path
        $data["js"] = "/template/pages/tagihan/tagihan_add_js"; // path

        $dt = array(
            "kontrak_list"          => $this->kontrak_model->kontrak_list_verified(),
            "kegiatan_list"         => $this->kegiatan_model->list_kegiatan(),
            "kegiatan_detail_list"  => $this->kegiatan_model->list_detail_kegiatan()
        );

        // print json_encode([
        //     "dt" => $dt['kegiatan_list']
        // ]);
        // exit;

        $data["content"] = $this->load->view("template/pages/tagihan/pengajuan_tagihan", $dt, true);
        $this->load->view("template/index", $data);
    }

    function detail_tagihan()
    {
        validSessionIsOut();

        $this->load->model("kontrak_model");
        $this->load->model("kegiatan_model");
        $this->load->model("tagihan_model");

        $data["title"] = "Detail Tagihan";
        $data["css"] = "/template/pages/tagihan/css"; // path
        $data["js"] = "/template/pages/tagihan/tagihan_update_js"; // path

        $tagihan_id = $this->uri->segment(3);

        $tagihan_detail     = $this->tagihan_model->tagihan_detail($tagihan_id);
        $kegiatan_detail_dt = $this->kegiatan_model->detail_detail_kegiatan($tagihan_detail["id_detail_kegiatan"]);

        $params["id_kegiatan"] = $kegiatan_detail_dt["id_kegiatan"];

        $dt = array(
            "tagihan_detail"        => $tagihan_detail,
            "kegiatan_detail_dt"    => $kegiatan_detail_dt,
            "kontrak_list"          => $this->kontrak_model->kontrak_list(),
            "kegiatan_detail_list"  => $this->kegiatan_model->list_detail_kegiatan($params),
            "kegiatan_list"         => $this->kegiatan_model->list_kegiatan(),
        );

        if (empty($dt["tagihan_detail"])) {
            show_404(base_url("page/notfound"), true);
        }

        $data["content"] = $this->load->view("template/pages/tagihan/pengajuan_tagihan", $dt, true);
        $this->load->view("template/index", $data);
    }

    function detail_pencairan()
    {

        validSessionIsOut();

        $data["title"] = "Detail Tagihan";
        $data["css"] = "/template/pages/tagihan/css"; // path
        $data["js"] = "/template/pages/tagihan/js"; // path

        $dt = array();
        $data["content"] = $this->load->view("template/pages/tagihan/detail_pencairan", $dt, true);
        $this->load->view("template/index", $data);
    }

    function get_all_tagihan()
    {

        error_reporting(0);

        $this->load->model("tagihan_model");
        $this->load->model("user_model");
        $this->load->model("kontrak_model");
        $this->load->model("kegiatan_model");
        $userID = $this->session->userdata("user_id");


        $draw   = intval($this->input->post("draw"));
        $start  = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order  = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col    = 0;
        $dir    = "";

        $filter_status_tagihan     = $this->input->post("filter_status_tagihan");
        $filter_jenis_tagihan      = $this->input->post("filter_jenis_tagihan");
        $filter_subdit             = $this->input->post("filter_subdit");
        $filter_tanggal_tagihan    = $this->input->post("filter_tanggal_tagihan");
        $filter_nama_ppk           = $this->input->post("filter_nama_ppk");

        $params = array(
            "limit"             => $length,
            "offset"            => $start,
            "order"             => $order,
            "col"               => $col,
            "dir"               => $dir,
            "search"            => $search,

            "filter_status_tagihan"   => $filter_status_tagihan,
            "filter_jenis_tagihan"    => $filter_jenis_tagihan,
            "filter_subdit"           => $filter_subdit,
            "filter_tanggal_tagihan"  => $filter_tanggal_tagihan,
            "filter_nama_ppk"         => $filter_nama_ppk
        );

        if ($this->session->userdata("group_id") == 2) {

            $list_tagihan           = $this->tagihan_model->get_all_tagihan_by_ppk($params, $userID);
            $params["count"]        = true;
            $filter_tagihan_count   = $this->tagihan_model->get_all_tagihan_by_ppk($params, $userID);
            $params2["count"]       = true;
            $total_tagihan_count    = $this->tagihan_model->get_all_tagihan_by_ppk($params2, $userID);
        } else {
            $list_tagihan           = $this->tagihan_model->get_all_tagihan($params);
            $params["count"]        = true;
            $filter_tagihan_count   = $this->tagihan_model->get_all_tagihan($params);
            $params2["count"]       = true;
            $total_tagihan_count    = $this->tagihan_model->get_all_tagihan($params2);
        }

        $data = array();
        $no = 1;
        foreach ($list_tagihan as $rows) {

            $date_tagihan = date_create($rows["created_at"]);
            $date_tagihan =  date_format($date_tagihan, "d/m/Y");

            $nama_ppk =  $this->user_model->get_specific_user_ppk($rows['id_user']);
            $detail_kegiatan = $this->kegiatan_model->detail_detail_kegiatan($rows['id_detail_kegiatan']);
            $kegiatan =  $this->kegiatan_model->get_kegiatan_by_id($detail_kegiatan['id_kegiatan']);

            foreach ($nama_ppk as $rowuser) {
                $datauser[] = array(
                    'fullname' => $rowuser['fullname']
                );
            }

            $nama_kontraktor =  $this->kontrak_model->kontrak_detail2($rows['id_kontrak']);

            foreach ($nama_kontraktor as $rowKontraktor) {
                $datakontraktor[] = array(
                    'nama_kontraktor' => $rowKontraktor['nama_kontraktor'],
                    'subdit' => $rowKontraktor['subdit']
                );
            }

            $data[] = array(
                "no"                => $no,
                "id"                => $rows["id"],
                "nomor_tagihan"     => $rows["nomor_tagihan"],
                "tanggal_tagihan"   => $date_tagihan,
                "uraian_tagihan"    => $rows["uraian_tagihan"],
                "status_tagihan"    => $rows["status_tagihan"],
                "jenis_tagihan"     => $rows["jenis_tagihan"],
                "nilai_tagihan"     => number_format($rows["nilai_tagihan"]),
                "jenis_tup"           => $rows["jenis_tup"],
                "potongan_pajak_ppn"  => number_format($rows["potongan_pajak_ppn"]),
                "jenis_pajak_pph"     => $rows["jenis_pajak_pph"],
                "potongan_pajak_pph"  => number_format($rows["potongan_pajak_pph"]),
                "id_user"           => $rowuser['fullname'],
                "nama_kontraktor"   => $rowKontraktor['nama_kontraktor'],
                "nama_penerima"     => $rows["nama_penerima"],
                "subditKontraktor"  => $rowKontraktor['subdit'],
                "subdit"            => $rows['subdit'],
                'kode_mak'          => $detail_kegiatan['kode_mak'],
                'uraian_mak'        => $detail_kegiatan['uraian_mak'],
                'detail_kegiatan'   => $detail_kegiatan['detail_kegiatan'],
                'nama_kegiatan'     => $kegiatan['nama_kegiatan']
            );

            $no++;
        }

        // echo '<pre>';
        // var_dump($data);
        // echo '</pre>';

        print json_encode([
            "draw"              => $draw,
            "recordsTotal"      => $total_tagihan_count,
            "recordsFiltered"   => $filter_tagihan_count,
            "data"              => $data
        ]);
    }

    function get_detail_tagihan()
    {
    }

    function add_tagihan()
    {

        $this->load->model("tagihan_model");
        $this->load->library("form_validation");
        $this->load->library("MyUpload");
        $this->load->library("NumberFormat");

        $data = [];


        // $this->db->order_by("id", "DESC");
        // $this->db->limit(1);
        // $last_tagihan = $this->db->get("tr_tagihan")->row_array();
        $qaa = $this->db->query("SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES WHERE table_name = 'tr_tagihan'");
        $raa = $qaa->row_array();
        $last_tagihan = $raa["auto_increment"];

        $month_roman = $this->numberformat->ConverToRoman(date("m"));
        $year        = date("Y");


        $data["id_kontrak"]             = $this->input->post("real_id_kontrak", true);
        $data["id_detail_kegiatan"]     = $this->input->post("real_id_detail_kegiatan", true);

        $data["nomor_tagihan"]          = "$last_tagihan/PPK/SPTB/$month_roman/$year";
        $data["jenis_tagihan"]          = $this->input->post("jenis_tagihan", true);
        $data["jenis_tup"]              = $this->input->post("jenis_tup", true);
        $data["subdit"]                 = $this->input->post("subdit", true);

        $data["nilai_tagihan"]          = $this->input->post("nilai_tagihan", true);
        $data["nilai_tagihan"]          = str_replace("Rp. ", "", $data["nilai_tagihan"]);
        $data["nilai_tagihan"]          = str_replace(".", "", $data["nilai_tagihan"]);
        $data["nilai_tagihan"]          = str_replace(",00", "", $data["nilai_tagihan"]);

        $data["nama_penerima"]            = $this->input->post("nama_penerima", true);
        $data["alamat_penerima"]          = $this->input->post("alamat_penerima", true);
        $data["nomor_kuitansi"]           = $this->input->post("nomor_kuitansi", true);
        $data["nama_bank_penerima"]       = $this->input->post("nama_bank_penerima", true);
        $data["nomor_rekening_penerima"]  = $this->input->post("nomor_rekening_penerima", true);

        $data["potongan_pajak_ppn"]     = $this->input->post("potongan_pajak_ppn", true);
        $data["potongan_pajak_ppn"]     = str_replace("Rp. ", "", $data["potongan_pajak_ppn"]);
        $data["potongan_pajak_ppn"]     = str_replace(".", "", $data["potongan_pajak_ppn"]);
        $data["potongan_pajak_ppn"]     = str_replace(",00", "", $data["potongan_pajak_ppn"]);

        $data["potongan_pajak_pph"]     = $this->input->post("potongan_pajak_pph", true);
        $data["potongan_pajak_pph"]     = str_replace("Rp. ", "", $data["potongan_pajak_pph"]);
        $data["potongan_pajak_pph"]     = str_replace(".", "", $data["potongan_pajak_pph"]);
        $data["potongan_pajak_pph"]     = str_replace(",00", "", $data["potongan_pajak_pph"]);

        $data["status_tagihan"]         = 'upload';
        $data["jenis_pajak_pph"]        = $this->input->post("jenis_pajak_pph", true);
        $data["uraian_tagihan"]         = $this->input->post("uraian_tagihan", true);
        $data["created_at"]             = date("Y-m-d H:i:s");

        $data["id_user"]                = $this->input->post("id_user", true);

        $this->form_validation->set_rules("jenis_tagihan", "Jenis Tagihan", "required");
        $this->form_validation->set_rules("jenis_tup", "Jenis Pembayaran", "required");

        if ($data["jenis_tagihan"] == "kontrak") {
            $this->form_validation->set_rules("real_id_kontrak", "ID Kontrak", "required|integer");
        }

        if ($data["jenis_tagihan"] == "non_kontrak") {
            $this->form_validation->set_rules("real_id_detail_kegiatan", "ID detail kegiatan", "required|integer");
        }

        $this->form_validation->set_rules("nilai_tagihan", "Nilai tagihan", "required");

        if ($this->form_validation->run()) {

            if (!empty($_FILES["file_surat_permohonan"]["name"])) {
                $data["file_surat_permohonan"]  = $this->myupload->format_filename([
                    "filename"      => $_FILES["file_surat_permohonan"]["name"],
                    "prefix_name"   => "surat-permohonan",
                    "nomor"         => $data["nomor_tagihan"],
                ]);
            }

            if ($_FILES["file_invoice"]["name"]) {
                $data["file_invoice"]  = $this->myupload->format_filename([
                    "filename"      => $_FILES["file_invoice"]["name"],
                    "prefix_name"   => "invoice",
                    "nomor"         => $data["nomor_tagihan"],
                ]);
            }
            if ($_FILES["file_spp_ppn"]["name"]) {
                $data["file_spp_ppn"]  = $this->myupload->format_filename([
                    "filename"      => $_FILES["file_spp_ppn"]["name"],
                    "prefix_name"   => "File-SPP-PPN",
                    "nomor"         => $data["nomor_tagihan"],
                ]);
            }
            if ($_FILES["file_spp_pph"]["name"]) {
                $data["file_spp_pph"]  = $this->myupload->format_filename([
                    "filename"      => $_FILES["file_spp_pph"]["name"],
                    "prefix_name"   => "File-SPP-PPH",
                    "nomor"         => $data["nomor_tagihan"],
                ]);
            }

            if ($_FILES["file_kwitansi"]["name"]) {
                $data["file_kwitansi"]  = $this->myupload->format_filename([
                    "filename"      => $_FILES["file_kwitansi"]["name"],
                    "prefix_name"   => "kwitansi",
                    "nomor"         => $data["nomor_tagihan"],
                ]);
            }

            if ($_FILES["file_faktur_pajak"]["name"]) {
                $data["file_faktur_pajak"]  = $this->myupload->format_filename([
                    "filename"      => $_FILES["file_faktur_pajak"]["name"],
                    "prefix_name"   => "faktur-pajak",
                    "nomor"         => $data["nomor_tagihan"],
                ]);
            }

            if ($_FILES["file_bap"]["name"]) {
                $data["file_bap"]  = $this->myupload->format_filename([
                    "filename"      => $_FILES["file_bap"]["name"],
                    "prefix_name"   => "bap",
                    "nomor"         => $data["nomor_tagihan"],
                ]);
            }

            if ($_FILES["file_bapp"]["name"]) {
                $data["file_bapp"]  = $this->myupload->format_filename([
                    "filename"      => $_FILES["file_bapp"]["name"],
                    "prefix_name"   => "bapp",
                    "nomor"         => $data["nomor_tagihan"],
                ]);
            }

            if ($_FILES["file_bast"]["name"]) {
                $data["file_bast"]  = $this->myupload->format_filename([
                    "filename"      => $_FILES["file_bast"]["name"],
                    "prefix_name"   => "bast",
                    "nomor"         => $data["nomor_tagihan"],
                ]);
            }

            if ($_FILES["file_laporan"]["name"]) {
                $data["file_laporan"]  = $this->myupload->format_filename([
                    "filename"      => $_FILES["file_laporan"]["name"],
                    "prefix_name"   => "laporan",
                    "nomor"         => $data["nomor_tagihan"],
                ]);
            }

            if ($_FILES["file_foto_kegiatan"]["name"]) {
                $data["file_foto_kegiatan"]  = $this->myupload->format_filename([
                    "filename"      => $_FILES["file_foto_kegiatan"]["name"],
                    "prefix_name"   => "foto-kegiatan",
                    "nomor"         => $data["nomor_tagihan"],
                ]);
            }

            if ($this->tagihan_model->add_tagihan($data)) {

                // upload file surat permohonan 
                if (!empty($_FILES["file_surat_permohonan"]["name"])) {

                    if (!is_dir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]))) {
                        mkdir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]), 0777, true);
                    }

                    $config1 = [
                        "upload_path"   => "./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]) . "/",
                        "allowed_types" => "*",
                        "max_size"      => 5000,
                        "file_name"     => $data["file_surat_permohonan"],
                    ];

                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_surat_permohonan"]["tmp_name"], $config1["upload_path"] . $config1["file_name"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = "error";

                        $res_file_surat_permohonan = json_encode([
                            "message" => "<div> file surat permohonan" . " : " . $error_file . "</div>",
                        ]);
                    } else {
                        $res_file_surat_permohonan = json_encode(["message" => "<div> upload surat permohonan sukses </div>"]);
                    }
                } else {
                    $res_file_surat_permohonan = json_encode(["message" => "<div> file surat permohonan NOT uploaded" . "</div>"]);
                }

                // invoice
                if (!empty($_FILES["file_invoice"]["name"])) {

                    if (!is_dir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]))) {
                        mkdir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]), 0777, true);
                    }

                    $config2 = [
                        "upload_path"   => "./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]) . "/",
                        "allowed_types" => "*",
                        "max_size"      => 5000,
                        "file_name"     => $data["file_invoice"],
                    ];

                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_invoice"]["tmp_name"], $config2["upload_path"] . $config2["file_name"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = "";

                        $res_file_invoice = json_encode([
                            "message" => "<div> file invoice" . " : " . $error_file . "</div>",
                        ]);
                    } else {
                        $res_file_invoice = json_encode(["message" => "<div> upload invoice sukses </div>"]);
                    }
                } else {
                    $res_file_invoice = json_encode(["message" => "<div> file invoice NOT uploaded" . "</div>"]);
                }
                // file spp ppn
                if (!empty($_FILES["file_spp_ppn"]["name"])) {

                    if (!is_dir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]))) {
                        mkdir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]), 0777, true);
                    }

                    $config2 = [
                        "upload_path"   => "./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]) . "/",
                        "allowed_types" => "*",
                        "max_size"      => 5000,
                        "file_name"     => $data["file_spp_ppn"],
                    ];

                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_spp_ppn"]["tmp_name"], $config2["upload_path"] . $config2["file_name"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = "";

                        $res_file_invoice = json_encode([
                            "message" => "<div> file spp ppn" . " : " . $error_file . "</div>",
                        ]);
                    } else {
                        $res_file_invoice = json_encode(["message" => "<div> upload file spp ppn sukses </div>"]);
                    }
                } else {
                    $res_file_invoice = json_encode(["message" => "<div> file spp ppn can not uploaded" . "</div>"]);
                }

                // file spp pph
                if (!empty($_FILES["file_spp_pph"]["name"])) {

                    if (!is_dir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]))) {
                        mkdir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]), 0777, true);
                    }

                    $config2 = [
                        "upload_path"   => "./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]) . "/",
                        "allowed_types" => "*",
                        "max_size"      => 5000,
                        "file_name"     => $data["file_spp_pph"],
                    ];

                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_spp_pph"]["tmp_name"], $config2["upload_path"] . $config2["file_name"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = "";

                        $res_file_invoice = json_encode([
                            "message" => "<div> file spp ppn" . " : " . $error_file . "</div>",
                        ]);
                    } else {
                        $res_file_invoice = json_encode(["message" => "<div> upload file_spp_pph sukses </div>"]);
                    }
                } else {
                    $res_file_invoice = json_encode(["message" => "<div> file_spp_pph can not uploaded" . "</div>"]);
                }

                // kwitansi
                if (!empty($_FILES["file_kwitansi"]["name"])) {

                    if (!is_dir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]))) {
                        mkdir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]), 0777, true);
                    }

                    $config9 = [
                        "upload_path"   => "./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]) . "/",
                        "allowed_types" => "*",
                        "max_size"      => 5000,
                        "file_name"     => $data["file_kwitansi"],
                    ];

                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_kwitansi"]["tmp_name"], $config9["upload_path"] . $config9["file_name"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = " error ";

                        $res_file_invoice = json_encode([
                            "message" => "<div> file invoice" . " : " . $error_file . "</div>",
                        ]);
                    } else {
                        $res_file_invoice = json_encode(["message" => "<div> upload invoice sukses </div>"]);
                    }
                } else {
                    $res_file_invoice = json_encode(["message" => "<div> file invoice NOT uploaded" . "</div>"]);
                }

                // faktur pajak 
                if (!empty($_FILES["file_faktur_pajak"]["name"])) {

                    if (!is_dir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]))) {
                        mkdir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]), 0777, true);
                    }

                    $config3 = [
                        "upload_path"   => "./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]) . "/",
                        "allowed_types" => "*",
                        "max_size"      => 5000,
                        "file_name"     => $data["file_faktur_pajak"],
                    ];

                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_faktur_pajak"]["tmp_name"], $config3["upload_path"] . $config3["file_name"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = "error";

                        $res_file_faktur_pajak = json_encode([
                            "message" => "<div> file surat permohonan" . " : " . $error_file . "</div>",
                        ]);
                    } else {
                        $res_file_faktur_pajak = json_encode(["message" => "<div> upload surat permohonan sukses </div>"]);
                    }
                } else {
                    $res_file_faktur_pajak = json_encode(["message" => "<div> file surat permohonan NOT uploaded" . "</div>"]);
                }

                // bap
                if (!empty($_FILES["file_bap"]["name"])) {

                    if (!is_dir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]))) {
                        mkdir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]), 0777, true);
                    }

                    $config4 = [
                        "upload_path"   => "./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]) . "/",
                        "allowed_types" => "*",
                        "max_size"      => 5000,
                        "file_name"     => $data["file_bap"],
                    ];

                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_bap"]["tmp_name"], $config4["upload_path"] . $config4["file_name"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = "error";

                        $res_file_bap = json_encode([
                            "message" => "<div> file surat permohonan" . " : " . $error_file . "</div>",
                        ]);
                    } else {
                        $res_file_bap = json_encode(["message" => "<div> upload surat permohonan sukses </div>"]);
                    }
                } else {
                    $res_file_bap = json_encode(["message" => "<div> file surat permohonan NOT uploaded" . "</div>"]);
                }

                // bapp
                if (!empty($_FILES["file_bapp"]["name"])) {

                    if (!is_dir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]))) {
                        mkdir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]), 0777, true);
                    }

                    $config5 = [
                        "upload_path"   => "./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]) . "/",
                        "allowed_types" => "*",
                        "max_size"      => 5000,
                        "file_name"     => $data["file_bapp"],
                    ];

                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_bapp"]["tmp_name"], $config5["upload_path"] . $config5["file_name"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = "error";

                        $res_file_bapp = json_encode([
                            "message" => "<div> file surat permohonan" . " : " . $error_file . "</div>",
                        ]);
                    } else {
                        $res_file_bapp = json_encode(["message" => "<div> upload surat permohonan sukses </div>"]);
                    }
                } else {
                    $res_file_bapp = json_encode(["message" => "<div> file surat permohonan NOT uploaded" . "</div>"]);
                }

                // bast
                if (!empty($_FILES["file_bast"]["name"])) {

                    if (!is_dir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]))) {
                        mkdir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]), 0777, true);
                    }

                    $config6 = [
                        "upload_path"   => "./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]) . "/",
                        "allowed_types" => "*",
                        "max_size"      => 5000,
                        "file_name"     => $data["file_bast"],
                    ];

                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_bast"]["tmp_name"], $config6["upload_path"] . $config6["file_name"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = "";

                        $res_file_bast = json_encode([
                            "message" => "<div> file surat permohonan" . " : " . $error_file . "</div>",
                        ]);
                    } else {
                        $res_file_bast = json_encode(["message" => "<div> upload surat permohonan sukses </div>"]);
                    }
                } else {
                    $res_file_bast = json_encode(["message" => "<div> file surat permohonan NOT uploaded" . "</div>"]);
                }

                // laporan
                if (!empty($_FILES["file_laporan"]["name"])) {

                    if (!is_dir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]))) {
                        mkdir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]), 0777, true);
                    }

                    $config7 = [
                        "upload_path"   => "./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]) . "/",
                        "allowed_types" => "*",
                        "max_size"      => 5000,
                        "file_name"     => $data["file_laporan"],
                    ];

                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_laporan"]["tmp_name"], $config7["upload_path"] . $config7["file_name"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = "error";

                        $res_file_laporan = json_encode([
                            "message" => "<div> file surat permohonan" . " : " . $error_file . "</div>",
                        ]);
                    } else {
                        $res_file_laporan = json_encode(["message" => "<div> upload surat permohonan sukses </div>"]);
                    }
                } else {
                    $res_file_laporan = json_encode(["message" => "<div> file surat permohonan NOT uploaded" . "</div>"]);
                }

                // foto kegiatan
                if (!empty($_FILES["file_foto_kegiatan"]["name"])) {

                    if (!is_dir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]))) {
                        mkdir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]), 0777, true);
                    }

                    $config8 = [
                        "upload_path"   => "./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]) . "/",
                        "allowed_types" => "*",
                        "max_size"      => 5000,
                        "file_name"     => $data["file_foto_kegiatan"],
                    ];


                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_foto_kegiatan"]["tmp_name"], $config8["upload_path"] . $config8["file_name"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = "error";

                        $res_file_foto_kegiatan = json_encode([
                            "message" => "<div> file surat permohonan" . " : " . $error_file . "</div>",
                        ]);
                    } else {
                        $res_file_foto_kegiatan = json_encode(["message" => "<div> upload surat permohonan sukses </div>"]);
                    }
                } else {
                    $res_file_foto_kegiatan = json_encode(["message" => "<div> file surat permohonan NOT uploaded" . "</div>"]);
                }

                print(json_encode([
                    "success" => true,
                    "message" => "<div> Berhasil menambahkan tagihan </div>",
                    "last_id" => $this->db->insert_id()
                ]));
            } else {

                print(json_encode([
                    "success" => false,
                    "message" => "gagal menambahkan tagihan, terdapat error query database",
                ]));
            }
        } else {

            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }

    function update_tagihan()
    {

        $this->load->model("tagihan_model");
        $this->load->library("form_validation");
        $this->load->library("MyUpload");

        $data = [];

        $data["id"]                     = $this->input->post("id_tagihan", true);

        $data["id_kontrak"]             = $this->input->post("real_id_kontrak", true);
        $data["id_detail_kegiatan"]     = $this->input->post("real_id_detail_kegiatan", true);

        $data["nomor_tagihan"]          = $this->input->post("nomor_tagihan", true);
        $data["jenis_tagihan"]          = $this->input->post("jenis_tagihan", true);

        $data["nilai_tagihan"]          = $this->input->post("nilai_tagihan", true);
        $data["nilai_tagihan"]          = str_replace("Rp. ", "", $data["nilai_tagihan"]);
        $data["nilai_tagihan"]          = str_replace(".", "", $data["nilai_tagihan"]);
        $data["nilai_tagihan"]          = str_replace(",00", "", $data["nilai_tagihan"]);

        $data["nama_penerima"]          = $this->input->post("nama_penerima", true);
        $data["alamat_penerima"]          = $this->input->post("alamat_penerima", true);
        $data["nomor_kuitansi"]           = $this->input->post("nomor_kuitansi", true);
        $data["nama_bank_penerima"]       = $this->input->post("nama_bank_penerima", true);
        $data["nomor_rekening_penerima"]  = $this->input->post("nomor_rekening_penerima", true);

        $data["jenis_tup"]              = $this->input->post("jenis_tup", true);
        $data["subdit"]                 = $this->input->post("subdit", true);

        $data["potongan_pajak_ppn"]     = $this->input->post("potongan_pajak_ppn", true);
        $data["potongan_pajak_ppn"]     = str_replace("Rp. ", "", $data["potongan_pajak_ppn"]);
        $data["potongan_pajak_ppn"]     = str_replace(".", "", $data["potongan_pajak_ppn"]);
        $data["potongan_pajak_ppn"]     = str_replace(",00", "", $data["potongan_pajak_ppn"]);

        $data["potongan_pajak_pph"]     = $this->input->post("potongan_pajak_pph", true);
        $data["potongan_pajak_pph"]     = str_replace("Rp. ", "", $data["potongan_pajak_pph"]);
        $data["potongan_pajak_pph"]     = str_replace(".", "", $data["potongan_pajak_pph"]);
        $data["potongan_pajak_pph"]     = str_replace(",00", "", $data["potongan_pajak_pph"]);

        $data["status_tagihan"]         = $this->input->post("status_tagihan", true);
        $data["jenis_pajak_pph"]        = $this->input->post("jenis_pajak_pph", true);
        $data["uraian_tagihan"]         = $this->input->post("uraian_tagihan", true);

        $data["id_user"]                = $this->input->post("id_user", true);

        // print_r( array($data["potongan_pajak_ppn"], $data["potongan_pajak_pph"], $data["nilai_tagihan"] ) );
        // exit;

        $this->form_validation->set_rules("id_tagihan", "ID tagihan", "required|integer");

        // updates on 12 juli karena file spm jadi ga bisa diupload kalau status sudah verifikasi bendhara
        // $this->form_validation->set_rules("jenis_tagihan", "Jenis Tagihan", "required");
        // $this->form_validation->set_rules("nomor_tagihan", "NO Tagihan", "required");

        if ($data["jenis_tagihan"] == "kontrak") {
            $this->form_validation->set_rules("real_id_kontrak", "ID Kontrak", "required|integer");
        }

        if ($data["jenis_tagihan"] == "non_kontrak") {
            $this->form_validation->set_rules("real_id_detail_kegiatan", "ID detail kegiatan", "required|integer");
            $data["id_kontrak"] = NULL;
        }

        // updates on 12 juli karena file spm jadi ga bisa diupload kalau status sudah verifikasi bendhara
        // $this->form_validation->set_rules("nilai_tagihan", "Nilai tagihan", "required");

        if ($this->form_validation->run()) {

            if (!empty($_FILES["file_surat_permohonan"]["name"])) {
                $data["file_surat_permohonan"]  = $this->myupload->format_filename([
                    "filename"      => $_FILES["file_surat_permohonan"]["name"],
                    "prefix_name"   => "surat-permohonan",
                    "nomor"         => $data["nomor_tagihan"],
                ]);
            }

            if ($_FILES["file_invoice"]["name"]) {
                $data["file_invoice"]  = $this->myupload->format_filename([
                    "filename"      => $_FILES["file_invoice"]["name"],
                    "prefix_name"   => "invoice",
                    "nomor"         => $data["nomor_tagihan"],
                ]);
            }

            if ($_FILES["file_spp_ppn"]["name"]) {
                $data["file_spp_ppn"]  = $this->myupload->format_filename([
                    "filename"      => $_FILES["file_spp_ppn"]["name"],
                    "prefix_name"   => "File-SPP-PPN",
                    "nomor"         => $data["nomor_tagihan"],
                ]);
            }
            if ($_FILES["file_spp_pph"]["name"]) {
                $data["file_spp_pph"]  = $this->myupload->format_filename([
                    "filename"      => $_FILES["file_spp_pph"]["name"],
                    "prefix_name"   => "File-SPP-PPH",
                    "nomor"         => $data["nomor_tagihan"],
                ]);
            }

            if ($_FILES["file_kwitansi"]["name"]) {
                $data["file_kwitansi"]  = $this->myupload->format_filename([
                    "filename"      => $_FILES["file_kwitansi"]["name"],
                    "prefix_name"   => "kwitansi",
                    "nomor"         => $data["nomor_tagihan"],
                ]);
            }

            if ($_FILES["file_faktur_pajak"]["name"]) {
                $data["file_faktur_pajak"]  = $this->myupload->format_filename([
                    "filename"      => $_FILES["file_faktur_pajak"]["name"],
                    "prefix_name"   => "faktur-pajak",
                    "nomor"         => $data["nomor_tagihan"],
                ]);
            }

            if ($_FILES["file_bap"]["name"]) {
                $data["file_bap"]  = $this->myupload->format_filename([
                    "filename"      => $_FILES["file_bap"]["name"],
                    "prefix_name"   => "bap",
                    "nomor"         => $data["nomor_tagihan"],
                ]);
            }

            if ($_FILES["file_bapp"]["name"]) {
                $data["file_bapp"]  = $this->myupload->format_filename([
                    "filename"      => $_FILES["file_bapp"]["name"],
                    "prefix_name"   => "bapp",
                    "nomor"         => $data["nomor_tagihan"],
                ]);
            }

            if ($_FILES["file_bast"]["name"]) {
                $data["file_bast"]  = $this->myupload->format_filename([
                    "filename"      => $_FILES["file_bast"]["name"],
                    "prefix_name"   => "bast",
                    "nomor"         => $data["nomor_tagihan"],
                ]);
            }

            if ($_FILES["file_laporan"]["name"]) {
                $data["file_laporan"]  = $this->myupload->format_filename([
                    "filename"      => $_FILES["file_laporan"]["name"],
                    "prefix_name"   => "laporan",
                    "nomor"         => $data["nomor_tagihan"],
                ]);
            }

            if ($_FILES["file_foto_kegiatan"]["name"]) {
                $data["file_foto_kegiatan"]  = $this->myupload->format_filename([
                    "filename"      => $_FILES["file_foto_kegiatan"]["name"],
                    "prefix_name"   => "foto-kegiatan",
                    "nomor"         => $data["nomor_tagihan"],
                ]);
            }
            if ($_FILES["file_spm"]["name"]) {
                $data["status_tagihan"]         = 'terbit spm';
                $data["file_spm"]  = $this->myupload->format_filename([
                    "filename"      => $_FILES["file_spm"]["name"],
                    "prefix_name"   => "file_spm",
                    "nomor"         => $data["nomor_tagihan"],
                ]);
            }

            if ($this->tagihan_model->update_tagihan($data)) {

                // upload file surat permohonan 
                if (!empty($_FILES["file_surat_permohonan"]["name"])) {

                    if (!is_dir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]))) {
                        mkdir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]), 0777, true);
                    }

                    $config1 = [
                        "upload_path"   => "./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]) . "/",
                        "allowed_types" => "*",
                        "max_size"      => 5000,
                        "file_name"     => $data["file_surat_permohonan"],
                    ];

                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_surat_permohonan"]["tmp_name"], $config1["upload_path"] . $config1["file_name"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = "error";

                        $res_file_surat_permohonan = json_encode([
                            "message" => "<div> file surat permohonan" . " : " . $error_file . "</div>",
                        ]);
                    } else {
                        $res_file_surat_permohonan = json_encode(["message" => "<div> upload surat permohonan sukses </div>"]);
                    }
                } else {
                    $res_file_surat_permohonan = json_encode(["message" => "<div> file surat permohonan NOT uploaded" . "</div>"]);
                }

                // invoice
                if (!empty($_FILES["file_invoice"]["name"])) {

                    if (!is_dir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]))) {
                        mkdir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]), 0777, true);
                    }

                    $config2 = [
                        "upload_path"   => "./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]) . "/",
                        "allowed_types" => "*",
                        "max_size"      => 5000,
                        "file_name"     => $data["file_invoice"],
                    ];

                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_invoice"]["tmp_name"], $config2["upload_path"] . $config2["file_name"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = "";

                        $res_file_invoice = json_encode([
                            "message" => "<div> file invoice" . " : " . $error_file . "</div>",
                        ]);
                    } else {
                        $res_file_invoice = json_encode(["message" => "<div> upload invoice sukses </div>"]);
                    }
                } else {
                    $res_file_invoice = json_encode(["message" => "<div> file invoice NOT uploaded" . "</div>"]);
                }

                // file spp ppn
                if (!empty($_FILES["file_spp_ppn"]["name"])) {

                    if (!is_dir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]))) {
                        mkdir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]), 0777, true);
                    }

                    $config2 = [
                        "upload_path"   => "./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]) . "/",
                        "allowed_types" => "*",
                        "max_size"      => 5000,
                        "file_name"     => $data["file_spp_ppn"],
                    ];

                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_spp_ppn"]["tmp_name"], $config2["upload_path"] . $config2["file_name"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = "";

                        $res_file_invoice = json_encode([
                            "message" => "<div> file spp ppn" . " : " . $error_file . "</div>",
                        ]);
                    } else {
                        $res_file_invoice = json_encode(["message" => "<div> upload file spp ppn sukses </div>"]);
                    }
                } else {
                    $res_file_invoice = json_encode(["message" => "<div> file spp ppn can not uploaded" . "</div>"]);
                }

                // file spp pph
                if (!empty($_FILES["file_spp_pph"]["name"])) {

                    if (!is_dir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]))) {
                        mkdir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]), 0777, true);
                    }

                    $config2 = [
                        "upload_path"   => "./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]) . "/",
                        "allowed_types" => "*",
                        "max_size"      => 5000,
                        "file_name"     => $data["file_spp_pph"],
                    ];

                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_spp_pph"]["tmp_name"], $config2["upload_path"] . $config2["file_name"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = "";

                        $res_file_invoice = json_encode([
                            "message" => "<div> file spp ppn" . " : " . $error_file . "</div>",
                        ]);
                    } else {
                        $res_file_invoice = json_encode(["message" => "<div> upload file_spp_pph sukses </div>"]);
                    }
                } else {
                    $res_file_invoice = json_encode(["message" => "<div> file_spp_pph can not uploaded" . "</div>"]);
                }

                // kwitansi
                if (!empty($_FILES["file_kwitansi"]["name"])) {

                    if (!is_dir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]))) {
                        mkdir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]), 0777, true);
                    }

                    $config9 = [
                        "upload_path"   => "./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]) . "/",
                        "allowed_types" => "*",
                        "max_size"      => 5000,
                        "file_name"     => $data["file_kwitansi"],
                    ];

                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_kwitansi"]["tmp_name"], $config9["upload_path"] . $config9["file_name"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = " error ";

                        $res_file_invoice = json_encode([
                            "message" => "<div> file invoice" . " : " . $error_file . "</div>",
                        ]);
                    } else {
                        $res_file_invoice = json_encode(["message" => "<div> upload invoice sukses </div>"]);
                    }
                } else {
                    $res_file_invoice = json_encode(["message" => "<div> file invoice NOT uploaded" . "</div>"]);
                }

                // faktur pajak 
                if (!empty($_FILES["file_faktur_pajak"]["name"])) {

                    if (!is_dir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]))) {
                        mkdir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]), 0777, true);
                    }

                    $config3 = [
                        "upload_path"   => "./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]) . "/",
                        "allowed_types" => "*",
                        "max_size"      => 5000,
                        "file_name"     => $data["file_faktur_pajak"],
                    ];

                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_faktur_pajak"]["tmp_name"], $config3["upload_path"] . $config3["file_name"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = "error";

                        $res_file_faktur_pajak = json_encode([
                            "message" => "<div> file surat permohonan" . " : " . $error_file . "</div>",
                        ]);
                    } else {
                        $res_file_faktur_pajak = json_encode(["message" => "<div> upload surat permohonan sukses </div>"]);
                    }
                } else {
                    $res_file_faktur_pajak = json_encode(["message" => "<div> file surat permohonan NOT uploaded" . "</div>"]);
                }

                // bap
                if (!empty($_FILES["file_bap"]["name"])) {

                    if (!is_dir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]))) {
                        mkdir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]), 0777, true);
                    }

                    $config4 = [
                        "upload_path"   => "./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]) . "/",
                        "allowed_types" => "*",
                        "max_size"      => 5000,
                        "file_name"     => $data["file_bap"],
                    ];

                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_bap"]["tmp_name"], $config4["upload_path"] . $config4["file_name"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = "error";

                        $res_file_bap = json_encode([
                            "message" => "<div> file surat permohonan" . " : " . $error_file . "</div>",
                        ]);
                    } else {
                        $res_file_bap = json_encode(["message" => "<div> upload surat permohonan sukses </div>"]);
                    }
                } else {
                    $res_file_bap = json_encode(["message" => "<div> file surat permohonan NOT uploaded" . "</div>"]);
                }

                // bapp
                if (!empty($_FILES["file_bapp"]["name"])) {

                    if (!is_dir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]))) {
                        mkdir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]), 0777, true);
                    }

                    $config5 = [
                        "upload_path"   => "./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]) . "/",
                        "allowed_types" => "*",
                        "max_size"      => 5000,
                        "file_name"     => $data["file_bapp"],
                    ];

                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_bapp"]["tmp_name"], $config5["upload_path"] . $config5["file_name"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = "error";

                        $res_file_bapp = json_encode([
                            "message" => "<div> file surat permohonan" . " : " . $error_file . "</div>",
                        ]);
                    } else {
                        $res_file_bapp = json_encode(["message" => "<div> upload surat permohonan sukses </div>"]);
                    }
                } else {
                    $res_file_bapp = json_encode(["message" => "<div> file surat permohonan NOT uploaded" . "</div>"]);
                }

                // bast
                if (!empty($_FILES["file_bast"]["name"])) {

                    if (!is_dir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]))) {
                        mkdir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]), 0777, true);
                    }

                    $config6 = [
                        "upload_path"   => "./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]) . "/",
                        "allowed_types" => "*",
                        "max_size"      => 5000,
                        "file_name"     => $data["file_bast"],
                    ];

                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_bast"]["tmp_name"], $config6["upload_path"] . $config6["file_name"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = "";

                        $res_file_bast = json_encode([
                            "message" => "<div> file surat permohonan" . " : " . $error_file . "</div>",
                        ]);
                    } else {
                        $res_file_bast = json_encode(["message" => "<div> upload surat permohonan sukses </div>"]);
                    }
                } else {
                    $res_file_bast = json_encode(["message" => "<div> file surat permohonan NOT uploaded" . "</div>"]);
                }

                // laporan
                if (!empty($_FILES["file_laporan"]["name"])) {

                    if (!is_dir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]))) {
                        mkdir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]), 0777, true);
                    }

                    $config7 = [
                        "upload_path"   => "./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]) . "/",
                        "allowed_types" => "*",
                        "max_size"      => 5000,
                        "file_name"     => $data["file_laporan"],
                    ];

                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_laporan"]["tmp_name"], $config7["upload_path"] . $config7["file_name"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = "error";

                        $res_file_laporan = json_encode([
                            "message" => "<div> file surat permohonan" . " : " . $error_file . "</div>",
                        ]);
                    } else {
                        $res_file_laporan = json_encode(["message" => "<div> upload surat permohonan sukses </div>"]);
                    }
                } else {
                    $res_file_laporan = json_encode(["message" => "<div> file surat permohonan NOT uploaded" . "</div>"]);
                }

                // foto kegiatan
                if (!empty($_FILES["file_foto_kegiatan"]["name"])) {

                    if (!is_dir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]))) {
                        mkdir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]), 0777, true);
                    }

                    $config8 = [
                        "upload_path"   => "./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]) . "/",
                        "allowed_types" => "*",
                        "max_size"      => 5000,
                        "file_name"     => $data["file_foto_kegiatan"],
                    ];


                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_foto_kegiatan"]["tmp_name"], $config8["upload_path"] . $config8["file_name"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = "error";

                        $res_file_foto_kegiatan = json_encode([
                            "message" => "<div> file surat permohonan" . " : " . $error_file . "</div>",
                        ]);
                    } else {
                        $res_file_foto_kegiatan = json_encode(["message" => "<div> upload surat permohonan sukses </div>"]);
                    }
                } else {
                    $res_file_foto_kegiatan = json_encode(["message" => "<div> file surat permohonan NOT uploaded" . "</div>"]);
                }

                // file spm
                if (!empty($_FILES["file_spm"]["name"])) {

                    if (!is_dir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]))) {
                        mkdir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]), 0777, true);
                    }

                    $config8 = [
                        "upload_path"   => "./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]) . "/",
                        "allowed_types" => "*",
                        "max_size"      => 5000,
                        "file_name"     => $data["file_spm"],
                    ];


                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_spm"]["tmp_name"], $config8["upload_path"] . $config8["file_name"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = "error";

                        $res_file_foto_kegiatan = json_encode([
                            "message" => "<div> file surat spm" . " : " . $error_file . "</div>",
                        ]);
                    } else {
                        $res_file_foto_kegiatan = json_encode(["message" => "<div> upload surat spm sukses </div>"]);
                    }
                } else {
                    $res_file_foto_kegiatan = json_encode(["message" => "<div> file surat spm NOT uploaded" . "</div>"]);
                }

                print(json_encode([
                    "success" => true,
                    "message" => "<div> Berhasil mengubah tagihan </div>",
                ]));
            } else {

                print(json_encode([
                    "success" => false,
                    "message" => "gagal mengubah tagihan, terdapat error query database",
                ]));
            }
        } else {

            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }

    function update_file_spm()
    {
        $this->load->model("tagihan_model");
        $this->load->library("form_validation");
        $this->load->library("MyUpload");

        $data = [];

        $data["id"]                     = $this->input->post("id_tagihan", true);
        $data["nomor_tagihan"]          = $this->input->post("nomor_tagihan", true);
        $data["status_tagihan"]         = 'terbit spm';

        // echo '<pre>';
        // var_dump($data);
        // echo '</pre>';
        // exit();

        if ($_FILES["file_spm"]["name"]) {
            $data["file_spm"]  = $this->myupload->format_filename([
                "filename"      => $_FILES["file_spm"]["name"],
                "prefix_name"   => "file-spm",
                "nomor"         => $data["nomor_tagihan"],
            ]);
        }

        $this->tagihan_model->update_tagihan($data);

        if (!empty($_FILES["file_spm"]["name"])) {

            if (!is_dir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]))) {
                mkdir("./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]), 0777, true);
            }

            $config8 = [
                "upload_path"   => "./files/tagihan/" . str_replace("/", "", $data["nomor_tagihan"]) . "/",
                "allowed_types" => "*",
                "max_size"      => 5000,
                "file_name"     => $data["file_spm"],
            ];


            print(json_encode([
                "success" => true,
                "message" => "<div> Berhasil upload spm </div>",
            ]));

            // IF FILE UPLOADED NOT VALIDATE
            if (!move_uploaded_file($_FILES["file_spm"]["tmp_name"], $config8["upload_path"] . $config8["file_name"])) {
                // $this->mom_model->add_mom($data);
                $error_file = "error";

                $res_file_foto_kegiatan = json_encode([
                    "message" => "<div> file surat permohonan" . " : " . $error_file . "</div>",
                ]);
            } else {
                $res_file_foto_kegiatan = json_encode(["message" => "<div> upload file sukses </div>"]);
            }
        } else {
            $res_file_foto_kegiatan = json_encode(["message" => "<div> file spm tidak ter-upload" . "</div>"]);
        }

        print(json_encode([
            "success" => true,
            "message" => "<div> Berhasil upload spm </div>",
        ]));
    }

    function delete_tagihan()
    {
        $this->load->model("tagihan_model");
        $this->load->library("form_validation");

        $id_tagihan = $this->input->post("id_tagihan");

        $this->form_validation->set_rules("id_tagihan", "ID Tagihan", "required|integer");

        if ($this->form_validation->run()) {

            // delete files kontrak

            if ($this->tagihan_model->delete_tagihan($id_tagihan)) {
                print(json_encode([
                    "success" => true,
                    "message" => "sukses menghapus tagihan",
                ]));
            } else {
                // 
                print(json_encode([
                    "success" => false,
                    "message" => "gagal menghapus tagihan, terdapat error query database",
                ]));
            }
        } else {
            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }

    function download()
    {

        $this->load->model("tagihan_model");
        $this->load->helper(array("download", "url"));

        $tagihan_id = $this->uri->segment(3);
        $type       = $this->uri->segment(4);

        if (!empty($tagihan_id) && !empty($type)) {
            $tagihan_detail = $this->tagihan_model->tagihan_detail($tagihan_id);

            if ($tagihan_detail) {
                $no_tagihan = str_replace("/", "", $tagihan_detail["nomor_tagihan"]);

                $url_add = "files/tagihan/$no_tagihan/$tagihan_detail[$type]";
                force_download($url_add, NULL);
            }
        }

        show_404();
        // echo "aaa";
    }

    // lampiran 
    function get_all_lampiran()
    {

        error_reporting(0);

        $this->load->model("tagihan_model");

        $draw   = intval($this->input->post("draw"));
        $start  = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order  = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col    = 0;
        $dir    = "";

        $id_tagihan = $this->input->post("id_tagihan", true);


        $params = array(
            "limit"             => $length,
            "offset"            => $start,
            "order"             => $order,
            "col"               => $col,
            "dir"               => $dir,
            "search"            => $search,

            "id_tagihan"        => $id_tagihan
        );

        $list_lampiran            = $this->tagihan_model->get_all_lampiran($params);
        $params["count"]          = true;
        $filter_lampiran_count    = $this->tagihan_model->get_all_lampiran($params);
        $params2["count"]         = true;
        $params2["id_lampiran"]   = $id_lampiran;
        $total_lampiran_count     = $this->tagihan_model->get_all_lampiran($params2);

        $data = array();
        $no = 1;
        foreach ($list_lampiran as $rows) {

            $data[] = array(
                "no"                => $no,
                "id"                => $rows["id"],
                "id_tagihan"        => $rows["id_tagihan"],
                "file_name"         => $rows["file_name"],
                "file"              => $rows["file"],
            );

            $no++;
        }

        // echo '<pre>';
        // var_dump($data);
        // echo '</pre>';

        print json_encode([
            "draw"              => $draw,
            "recordsTotal"      => $total_lampiran_count,
            "recordsFiltered"   => $filter_lampiran_count,
            "data"              => $data
        ]);
    }

    function add_lampiran()
    {

        $this->load->model("tagihan_model");
        $this->load->library("form_validation");

        $data["id_tagihan"]     = $this->input->post("id_tagihan", true);
        $data["file_name"]      = $this->input->post("file_name", true);
        $no_tagihan             = $this->input->post("no_tagihan", true);

        if ($_FILES['file']['size'] != 0 && $_FILES['file']['error'] == 0) {

            $data["file"]       = $_FILES["file"]["name"];
        } else {
            print(json_encode([
                "success" => false,
                "message" => "harus menyertakan lampiran"
            ]));
            exit();
        }

        $this->form_validation->set_rules("id_tagihan", "ID Tagihan", "required|integer");
        $this->form_validation->set_rules("file_name", "File Name", "required");
        $this->form_validation->set_rules("no_tagihan", "No Tagihan", "required");

        $upload_path   = "./files/tagihan/" . str_replace("/", "", $no_tagihan) . "/";

        if ($this->form_validation->run()) {

            if (!is_dir("./files/tagihan/" . str_replace("/", "", $no_tagihan))) {
                mkdir("./files/tagihan/" . str_replace("/", "", $no_tagihan), 0777, true);
            }

            // IF FILE UPLOADED NOT VALIDATE
            if (move_uploaded_file($_FILES["file"]["tmp_name"], $upload_path . $data["file"])) {
                $this->tagihan_model->add_lampiran($data);

                print(json_encode([
                    "success" => true,
                    "message" => "Sukses menambah lampiran"
                ]));
            } else {
                echo json_encode([
                    "success" => false,
                    "message" => "Upload file lampiran gagal"
                ]);
            }
        } else {
            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }

    function update_lampiran()
    {

        $this->load->model("tagihan_model");
        $this->load->library("form_validation");

        $data["id"]             = $this->input->post("id_lampiran", true);
        $data["file_name"]      = $this->input->post("file_name", true);
        $no_tagihan             = $this->input->post("no_tagihan", true);

        if ($_FILES['file']['size'] != 0) {
            $data["file"]           = $_FILES["file"]["name"];
        }

        $this->form_validation->set_rules("id_tagihan", "ID Tagihan", "required|integer");
        $this->form_validation->set_rules("file_name", "File Name", "required");
        $this->form_validation->set_rules("no_tagihan", "No Tagihan", "required");

        $upload_path   = "./files/tagihan/" . str_replace("/", "", $no_tagihan) . "/";

        if ($this->form_validation->run()) {

            if (!is_dir("./files/tagihan/" . str_replace("/", "", $no_tagihan))) {
                mkdir("./files/tagihan/" . str_replace("/", "", $no_tagihan), 0777, true);
            }

            $this->tagihan_model->update_lampiran($data);

            if (!empty($_FILES["file"]["name"])) {
                move_uploaded_file($_FILES["file"]["tmp_name"], $upload_path . $data["file"]);
            }

            print(json_encode([
                "success" => true,
                "message" => "Sukses mengubah lampiran"
            ]));
        } else {
            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }

    function detail_lampiran()
    {

        $this->load->model("tagihan_model");
        $this->load->library("form_validation");

        $lampiran_id = $this->input->post("lampiran_id");

        $this->form_validation->set_rules("lampiran_id", "ID lampiran", "required|integer");

        if ($this->form_validation->run()) {

            $result = $this->tagihan_model->detail_lampiran($lampiran_id);

            print(json_encode([
                "success" => true,
                "data" => $result
            ]));
        } else {
            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }

    function delete_lampiran()
    {
        $this->load->model("tagihan_model");
        $this->load->library("form_validation");

        $lampiran_id = $this->input->post("id", true);

        $this->form_validation->set_rules("id", "ID lampiran", "required|integer");

        if ($this->form_validation->run()) {

            $this->tagihan_model->delete_lampiran($lampiran_id);

            print(json_encode([
                "success" => true,
                "message" => "Sukses menghapus lampiran"
            ]));
        } else {
            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }

    function download_lampiran()
    {

        $this->load->model("tagihan_model");
        $this->load->helper(array("download", "url"));

        $lampiran_id = $this->uri->segment(3);

        if (!empty($lampiran_id)) {
            $lampiran_detail = $this->tagihan_model->detail_lampiran($lampiran_id);
            $tagihan_detail = $this->tagihan_model->tagihan_detail($lampiran_detail["id_tagihan"]);

            if (!empty($lampiran_detail) && !empty($tagihan_detail)) {
                $no_tagihan = str_replace("/", "", $tagihan_detail["nomor_tagihan"]);

                $url_add = "files/tagihan/$no_tagihan/$lampiran_detail[file]";
                force_download($url_add, NULL);
            }
        }

        show_404();
        // echo "aaa";
    }

    function verifikasi_tagihan()
    {
        $this->load->model("tagihan_model");
        $this->load->library("form_validation");

        $data["id"]                 = $this->input->post("id", true);
        $data["status_tagihan"]             = 'verifikasi bendahara';

        $this->form_validation->set_rules("id", "ID Kontrak", "required|integer");
        // $this->form_validation->set_rules("status", "Status Kontrak", "required");

        if ($this->form_validation->run()) {



            $this->tagihan_model->update_tagihan($data);

            print(json_encode([
                "success" => true,
                "message" => "Sukses Verifikasi Tagihan",
            ]));
        } else {
            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }

    function reject_tagihan()
    {
        $this->load->model("tagihan_model");
        $this->load->library("form_validation");

        $data["id"]                 = $this->input->post("id", true);
        $data["status_tagihan"]     = 'rejected';
        $data["reject_note"]        = $this->input->post("reject_note", true);

        $this->form_validation->set_rules("id", "ID Kontrak", "required|integer");
        // $this->form_validation->set_rules("status", "Status Kontrak", "required");

        if ($this->form_validation->run()) {



            $this->tagihan_model->update_tagihan($data);

            print(json_encode([
                "success" => true,
                "message" => "Tagihan telah di reject",
            ]));
        } else {
            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }
}
