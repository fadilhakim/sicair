<?php

class Anggaran extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        validSessionIsOut();
        $this->authorization->redirect_menu("dashboard");
        $this->load->model("anggaran_model");
    }

    function index()
    {

        validSessionIsOut();

        // $tahun_anggaran        = $this->input->post("tahun_anggaran");

        // $param = array(
        //     "tahun_anggaran"            => $tahun_anggaran
        // );

        $data["title"] = "Kelola Kegiatan Anggaran";
        $data["css"] = "/template/pages/kelola_anggaran/css"; // path
        $data["js"] = "/template/pages/kelola_anggaran/js"; // path
        $dt = array();

        $data["content"] = $this->load->view("template/pages/kelola_anggaran/list_anggaran", $dt, true);
        $this->load->view("template/index", $data);
    }

    function get_anggaran()
    {
        $this->load->model("anggaran_model");

        error_reporting(0);

        $draw   = intval($this->input->post("draw"));
        $start  = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order  = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col    = 0;
        $dir    = "";

        $tahun_anggaran     = $this->input->post("tahun_anggaran");

        $params = array(
            "limit"     => $length,
            "offset"    => $start,
            "order"     => $order,
            "col"       => $col,
            "dir"       => $dir,
            "search"    => $search,

            "tahun_anggaran"    => $tahun_anggaran
        );

        $list_anggaran = $this->anggaran_model->get_all_anggaran($params);
        $params["count"] = true;
        $filter_anggaran_count = $this->anggaran_model->get_all_anggaran($params);
        $params2["count"] = true;
        $total_anggaran_count = $this->anggaran_model->get_all_anggaran($params2);

        $data = array();
        foreach ($list_anggaran as $rows) {

            $data[] = array(
                "id"                => $rows["id"],
                "nomor_dipa"        => $rows["nomor_dipa"],
                "tanggal_dipa"      => $rows["tanggal_dipa"],
                "pagu_anggaran"     => number_format($rows["pagu_anggaran"], 2, ",", "."),
                "tahun_anggaran"    => $rows["tahun_anggaran"],
                "kode_satker"       => $rows["kode_satker"],
                "nama_satker"       => $rows["nama_satker"],
            );
        }

        // echo '<pre>';
        // var_dump($data);
        // echo '</pre>';

        print json_encode([
            "draw" => $draw,
            "recordsTotal" => $total_anggaran_count,
            "recordsFiltered" => $filter_anggaran_count,
            "data" => $data
        ]);
    }

    function tambah_anggaran()
    {

        validSessionIsOut();

        $data["title"] = "Tambah Kegiatan Anggaran";
        $data["css"] = "/template/pages/kelola_anggaran/css"; // path
        $data["js"] = "/template/pages/kelola_anggaran/js"; // path

        $dt = array();
        $data["content"] = $this->load->view("template/pages/kelola_anggaran/form_tambah_anggaran", $dt, true);
        $this->load->view("template/index", $data);
    }

    function edit_anggaran()
    {
        $this->load->library("form_validation");

        $id_anggaran         = $this->input->post("id_anggaran", true);

        $nomor_dipa          = $this->input->post("nomor_dipa", true);
        $tanggal_dipa        = $this->input->post("tanggal_dipa", true);
        $pagu_anggaran       = $this->input->post("pagu_anggaran", true);
        $pagu_anggaran       = str_replace("Rp. ", "", $pagu_anggaran);
        $pagu_anggaran       = str_replace(".", "", $pagu_anggaran);
        $pagu_anggaran       = str_replace(",00", "", $pagu_anggaran);
        $tahun_anggaran      = $this->input->post("tahun_anggaran");
        $kode_satker         = $this->input->post("kode_satker");
        $nama_satker         = $this->input->post("nama_satker");

        $this->form_validation->set_rules("id_anggaran", "id anggaran", "required|integer");

        if ($this->form_validation->run()) {

            $data = [
                "nomor_dipa"          => $nomor_dipa,
                "tanggal_dipa"        => $tanggal_dipa,
                "pagu_anggaran"       => $pagu_anggaran,
                "tahun_anggaran"      => $tahun_anggaran,
                "kode_satker"         => $kode_satker,
                "nama_satker"         => $nama_satker,

            ];

            $this->anggaran_model->edit_anggaran($id_anggaran, $data);

            echo json_encode([
                "success" => true,
                "message" => "Berhasil Meng-update Anggaran"
            ]);
        }
    }

    function tambah_anggaran_proses()
    {

        $this->load->library("form_validation");

        $nomor_dipa          = $this->input->post("nomor_dipa", true);
        $tanggal_dipa        = $this->input->post("tanggal_dipa", true);
        $pagu_anggaran       = $this->input->post("pagu_anggaran", true);
        $pagu_anggaran           = str_replace("Rp. ", "", $pagu_anggaran);
        $pagu_anggaran           = str_replace(".", "", $pagu_anggaran);
        $pagu_anggaran           = str_replace(",00", "", $pagu_anggaran);
        $tahun_anggaran      = $this->input->post("tahun_anggaran", true);
        $kode_satker         = $this->input->post("kode_satker", true);
        $nama_satker         = $this->input->post("nama_satker", true);

        $this->form_validation->set_rules("nomor_dipa", "nomor dipa", "required");
        $this->form_validation->set_rules("tanggal_dipa", "tanggal dipa", "required");
        $this->form_validation->set_rules("pagu_anggaran", "pagu anggaran", "required");
        $this->form_validation->set_rules("tahun_anggaran", "tahun anggaran", "required");
        $this->form_validation->set_rules("kode_satker", "kode satker", "required");
        $this->form_validation->set_rules("nama_satker", "nama satker", "required");

        if ($this->form_validation->run()) {
            $data = [
                "nomor_dipa"        => $nomor_dipa,
                "tanggal_dipa"      => $tanggal_dipa,
                "pagu_anggaran"     => $pagu_anggaran,
                "tahun_anggaran"    => $tahun_anggaran,
                "kode_satker"       => $kode_satker,
                "nama_satker"       => $nama_satker,
            ];

            $this->anggaran_model->tambah_anggaran($data);
            exit(json_encode([
                "success" => true,
                "message" => "Berhasil menambahkan anggaran",
            ]));
        } else {
            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }

    function detail_anggaran()
    {

        validSessionIsOut();

        $data["title"] = "Detail Anggaran Kegiatan";
        $data["css"] = "/template/pages/kelola_anggaran/css"; // path
        $data["js"] = "/template/pages/kelola_anggaran/js"; // path
        $this->load->model('user_model');
        $this->load->model('anggaran_model');
        $segmentUri = $this->uri->segment(2);
        $anggaran_detail = $this->anggaran_model->get_anggaran_detail($segmentUri);
        $all_user =  $this->user_model->get_all_user_ppk();
        $dt = array(
            'anggaran_detail' => $anggaran_detail,
            'all_user' => $all_user

        );
        $data["content"] = $this->load->view("template/pages/kelola_anggaran/detail_anggaran", $dt, true);
        $this->load->view("template/index", $data);
    }

    function get_kegiatan()
    {
        validSessionIsOut();
        $this->load->model("anggaran_model");
        $this->load->model("user_model");
        $userID = $this->session->userdata("user_id");

        error_reporting(0);
        $id_anggaran    = $this->input->post("id_anggaran");
        $draw   = intval($this->input->post("draw"));
        $start  = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order  = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col    = 0;
        $dir    = "";

        $nama_ppk     = $this->input->post("id_user");
        $filter_nama_ppk     = $this->input->post("filter_nama_ppk");
        $filter_anggaran_id  = $this->input->post("filter_anggaran_id");
        $filter_prioritas    = $this->input->post("filter_prioritas");

        $params = array(
            "limit"     => $length,
            "offset"    => $start,
            "order"     => $order,
            "col"       => $col,
            "dir"       => $dir,
            "search"    => $search,

            //"nama_ppk"    => $nama_ppk,
            "filter_nama_ppk"    => $filter_nama_ppk,
            "filter_anggaran_id" => $filter_anggaran_id,
            "filter_prioritas"   => $filter_prioritas
        );
        if ($this->session->userdata("group_id") == 2) {

            $list_kegiatan = $this->anggaran_model->getMainKegiatan_by_ppk($params, $id_anggaran, $userID);
            $params["count"] = true;
            $filter_kegiatan_count = $this->anggaran_model->getMainKegiatan_by_ppk($params, $id_anggaran, $userID);
            $params2["count"] = true;
            $total_kegiatan_count = $this->anggaran_model->getMainKegiatan_by_ppk($params2, $id_anggaran, $userID);
        } else {
            $list_kegiatan = $this->anggaran_model->getMainKegiatan($params, $id_anggaran);
            $params["count"] = true;
            $filter_kegiatan_count = $this->anggaran_model->getMainKegiatan($params, $id_anggaran);
            $params2["count"] = true;
            $total_kegiatan_count = $this->anggaran_model->getMainKegiatan($params2, $id_anggaran);
        }
        $data = array();
        foreach ($list_kegiatan as $rows) {
            $nama_ppk =  $this->user_model->get_specific_user_ppk($rows['id_user']);

            foreach ($nama_ppk as $rowuser) {
                $datauser[] = array(
                    'fullname' => $rowuser['fullname']
                );
            }

            $data[] = array(
                "id"                  => $rows["id"],
                "kode_aktivitas"      => $rows["kode_aktivitas"],
                "komponen"            => $rows["komponen"],
                "subkomponen"         => $rows["subkomponen"],
                "nama_kegiatan"       => $rows["nama_kegiatan"],
                "pagu_kegiatan"       => number_format($rows["pagu_kegiatan"]),
                "id_user"             => $rowuser['fullname'],
                "kro"                 => $rows["kro"],
                "ro"                  => $rows["ro"],
                "prioritas"           => $rows["prioritas"],
            );
        }

        // echo '<pre>';
        // var_dump($datauser);
        // echo '</pre>';

        print json_encode([
            "draw" => $draw,
            "recordsTotal" => $total_kegiatan_count,
            "recordsFiltered" => $filter_kegiatan_count,
            "data" => $data
        ]);
    }

    function tambah_kegiatan_process()
    {
        $this->load->library("form_validation");

        $kode_aktivitas          = $this->input->post("kode_aktivitas", true);
        $kro                     = $this->input->post("kro", true);
        $ro                      = $this->input->post("ro", true);
        $komponen                = $this->input->post("komponen", true);
        $subkomponen             = $this->input->post("subkomponen", true);
        $pagu_kegiatan           = $this->input->post("pagu_kegiatan", true);
        $pagu_kegiatan           = str_replace("Rp. ", "", $pagu_kegiatan);
        $pagu_kegiatan           = str_replace(".", "", $pagu_kegiatan);
        $pagu_kegiatan           = str_replace(",00", "", $pagu_kegiatan);
        $id_user                 = $this->input->post("id_user", true);
        $nama_kegiatan           = $this->input->post("nama_kegiatan", true);
        $prioritas               = $this->input->post("prioritas", true);
        $anggaran_id             = $this->input->post("anggaran_id", true);

        $this->form_validation->set_rules("kode_aktivitas", "Kode Aktivitas", "required");

        if ($this->form_validation->run()) {
            $data = [
                "kode_aktivitas"    => strtoupper($kode_aktivitas),
                "kro"               => strtoupper($kro),
                "ro"                => strtoupper($ro),
                "komponen"          => strtoupper($komponen),
                "subkomponen"       => strtoupper($subkomponen),
                "pagu_kegiatan"     => $pagu_kegiatan,
                "id_user"           => $id_user,
                "nama_kegiatan"     => strtoupper($nama_kegiatan),
                "prioritas"         => $prioritas,
                "anggaran_id"       => $anggaran_id
            ];

            $this->anggaran_model->tambah_kegiatan($data);
            exit(json_encode([
                "success" => true,
                "message" => "Berhasil menambahkan kegiatan",
            ]));
        } else {
            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }


    function get_tambah_kegiatan_detail()
    {

        $this->load->library("form_validation");

        $id_kegiatan = $this->input->post("id_kegiatan");

        $this->form_validation->set_rules("id_kegiatan", "ID Kegiatan", "required|integer");

        if ($this->form_validation->run()) {

            $result = $this->anggaran_model->get_kegiatan_by_id($id_kegiatan);

            echo json_encode([
                "status" => 200,
                "message" => "you successfully retrieve action plan detail",
                "data"   => $result
            ]);
        } else {
            echo json_encode([
                "status" => 500,
                "message" => validation_errors(),
                "data"   => []
            ]);
        }
    }

    function update_kegiatan()
    {
        $this->load->library("form_validation");

        $id_kegiatan  = $this->input->post("id_kegiatan");
        $kro  = $this->input->post("kro");
        $ro  = $this->input->post("ro");
        $nama_kegiatan  = $this->input->post("nama_kegiatan");
        $kode_aktivitas  = $this->input->post("kode_aktivitas");
        $detail_kegiatan  = $this->input->post("detail_kegiatan");
        $nama_ppk  = $this->input->post("nama_ppk");
        $pagu_kegiatan  = $this->input->post("pagu_kegiatan");
        $pagu_kegiatan       = str_replace("Rp. ", "", $pagu_kegiatan);
        $pagu_kegiatan       = str_replace(".", "", $pagu_kegiatan);
        $pagu_kegiatan       = str_replace(",00", "", $pagu_kegiatan);
        $komponen  = $this->input->post("komponen");
        $subkomponen  = $this->input->post("subkomponen");
        // $kode_mak  = $this->input->post("kode_mak");
        $prioritas  = $this->input->post("prioritas");
        $this->form_validation->set_rules("id_kegiatan", "ID Kegiatan", "required|integer");

        if ($this->form_validation->run()) {

            $this->anggaran_model->update_kegiatan($id_kegiatan, [
                "kode_aktivitas"    => $kode_aktivitas,
                "kro"               => $kro,
                "ro"                => $ro,
                "nama_kegiatan"     => $nama_kegiatan,
                "detail_kegiatan"   => $detail_kegiatan,
                "id_user"           => $nama_ppk,
                "pagu_kegiatan"     => $pagu_kegiatan,
                "komponen"          => $komponen,
                "subkomponen"       => $subkomponen,
                "prioritas"         => $prioritas
            ]);

            echo json_encode([
                "success" => true,
                "status" => 200,
                "message" => "Berhasi Edit Kegiatan"
            ]);
        } else {
            echo json_encode([
                "success" => false,
                "status" => 500,
                "message" => validation_errors()
            ]);
        }
    }

    function delete_anggaran()
    {
        $this->load->library("form_validation");
        $anggaran_id = $this->input->post("anggaran_id");
        $this->form_validation->set_rules("anggaran_id", "kegiatan id", "required|integer");
        if ($this->form_validation->run()) {;
            $this->anggaran_model->delete_anggaran($anggaran_id);

            echo json_encode([
                "success" => true,
                "message" => "Berhasil Delete Anggaran"
            ]);
        } else {
            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }
}
