<?php

	class Test extends CI_Controller {

		function __construct(){
			parent::__construct();

			
			$this->load->model("group_model");
		}

		function index() {

			$this->load->library("table");


			$data["title"] = "Test";

            $data["css"] = ""; // path
            $data["js"] = "/template/pages/test/js"; // path

			$template = array(
				'table_open' => '<table border="1" cellpadding="2" cellspacing="1" class="mytable">'
			);

			$this->table->set_template($template);
			$this->table->set_heading('id', 'nama', 'email',"phone","status","is_aktif","m_customer_id");
			$dt = $this->test_model->get_cp_customer();
			$dtable = $this->table->generate($dt);

			$dt["cp_customer"] = $dtable;

			$this->table->clear();

			$template2 = array(
				'table_open' => '<table border="1" cellpadding="2" cellspacing="1" class="mytable">'
			);

			$this->table->set_template($template2);
			$this->table->set_heading('id', 'group_name', 'created_at',"updated_at");
			$dt2 = $this->group_model->get_group();
			$dtable2 = $this->table->generate($dt2);

			$dt["group"] = $dtable2;
			$dt["group_menu"] = $this->group_model->detail_group_menu_authorized(1);


            $data["content"] = $this->load->view("template/pages/test/test_list",$dt,true);

            $this->load->view("template/index",$data);
		}

		function group_menu() {
			$group_menu = $this->group_model->detail_group_menu_authorized($this->session->userdata("group_id"));
			print_r($group_menu);
		}

		function sess() { 
			print_r($this->session->userdata("group_id"));
		}

		function user_nik_list() {
			$this->load->model("user_model");

			$result = $this->user_model->get_user_nik();
			// $result = $this->user_model->get_user_nik_detail(104);

			print_r($result);
		}

		function dashboard() {

			$this->load->model("projects_model");

			// $result = $this->projects_model->get_total_node_bycustomer();
			// print_r($result);
			// echo "<hr>";

			// $result2 = $this->projects_model->get_total_node_bywoi();
			// print_r($result2);

			// $result3 = $this->projects_model->get_total_node_bylayanan();
			// print_r($result3);

			//$tol_chart = $this->projects_model->chart_tol_periode();
			//print_r($tol_chart);

			$tto_chart = $this->projects_model->chart_tto_periode();
			print_r($tto_chart);
			echo "<br>";
			// print_r($this->db->last_query());

		}

		function timeline() { 
			$this->load->model("project_timeline_model");

			$dt = $this->project_timeline_model->get_timeline_nodelink(1);

			print_r($dt);
		}

		function search_project() {

			$this->load->model("projects_model");

			// $search = $this->input->post("search") ;
            $search = "telkom";
            $result = [];

            if($search) {
                $dt = $this->projects_model->get_all_data_projects([
                    "search" => $search 
                ]);

                foreach($dt as $row){
					
					$row["list_layanan"] = [];
					$kontrak_layanan = $this->projects_model->get_layanan_bykontrak($row["h_kontrak_id"]);
					
					foreach($kontrak_layanan as $row2) {
						$layanan = $this->projects_model->get_layanan($row2["m_layanan_id"]);

						$row["list_layanan"][] = $layanan["nama"]; 
					}

					$result[] = $row;

	
                }
            }

            echo json_encode(array(
                "data" => $result,
                "status" => "OK"
            ));
		}
	}
