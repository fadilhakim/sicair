<?php

class Dashboard extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    // validSessionIsOut();
    // $this->authorization->redirect_menu("dashboard");
    $this->db2 = $this->load->database();
  }

  function index()
  {

    validSessionIsOut();


    $data["title"] = "Dashboard";

    $data["css"] = "/template/pages/dashboard/css"; // path
    $data["js"] = "/template/pages/dashboard/js"; // path

    $dt = array();
    $data["content"] = $this->load->view("template/pages/dashboard/dashboard", $dt, true);
    $this->load->view("template/index", $data);
  }

  function count_kegiatan()
  {
    validSessionIsOut();
    $this->load->model("kegiatan_model");

    $result = $this->kegiatan_model->count_kegiatan();
    echo json_encode([
      "status" => 200,
      "message" => "success",
      "data" => $result
    ]);
  }

  function count_detail_kegiatan()
  {
    validSessionIsOut();
    $this->load->model("kegiatan_model");

    $result = $this->kegiatan_model->count_detail_kegiatan();
    echo json_encode([
      "status" => 200,
      "message" => "success",
      "data" => $result
    ]);
  }

  function count_anggaran()
  {
    validSessionIsOut();
    $this->load->model("anggaran_model");
    $this->load->library("form_validation");

    $year = $this->input->post("year", true);

    $this->form_validation->set_rules("year", "Year", "required|integer");

    if ($this->form_validation->run()) {

      $result = $this->anggaran_model->count_anggaran($year);
      // echo '<pre>';
      // var_dump($result);
      // echo '</pre>';
      echo json_encode([
        "status" => 200,
        "message" => "success",
        "data" => $result
      ]);
    } else {
      echo json_encode([
        "status" => 500,
        "message" => "error",
        "data" => validation_errors()
      ]);
    }
  }

  function count_tagihan()
  {
    validSessionIsOut();
    $this->load->model("tagihan_model");
    $result = $this->tagihan_model->all_tagihan_count();
    echo json_encode([
      "status" => 200,
      "message" => "success",
      "data" => $result
    ]);
  }
  function count_tagihan_verified()
  {
    validSessionIsOut();
    $this->load->model("tagihan_model");
    $result = $this->tagihan_model->all_tagihan_verified_count();
    echo json_encode([
      "status" => 200,
      "message" => "success",
      "data" => $result
    ]);
  }

  function updatedKontrak()
  {
    validSessionIsOut();
    $this->load->model("kontrak_model");

    $result = $this->kontrak_model->get_updated_kontrak();
    echo json_encode([
      "status" => 200,
      "message" => "success",
      "data" => $result
    ]);
  }

  function pie_chart_daya_serap_all()
  {

    $this->load->library("form_validation");

    $year = $this->input->post("year", true) ?: date("Y");

    $this->form_validation->set_rules("year", "Year", "required|integer");

    if ($this->form_validation->run()) {

      $ds_str = "SELECT SUM(tr_tagihan.nilai_tagihan) as total_tagihan,tr_tagihan.status_tagihan,m_anggaran.tahun_anggaran
            FROM
          m_anggaran 
            JOIN tr_kegiatan ON m_anggaran.id = tr_kegiatan.id_anggaran
            JOIN tr_detail_kegiatan ON tr_kegiatan.id = tr_detail_kegiatan.id_kegiatan
            JOIN tr_tagihan ON tr_detail_kegiatan.id = tr_tagihan.id_detail_kegiatan
          
          WHERE 
            tr_tagihan.status_tagihan = 'verifikasi bendahara' OR tr_tagihan.status_tagihan = 'terbit spm'  
            AND
            m_anggaran.tahun_anggaran = '$year'
            
        ";
      $ds_sql = $this->db->query($ds_str);
      $dsf = $ds_sql->row_array();

      $this->db->where("tahun_anggaran", $year);
      $res_anggaran = $this->db->get("m_anggaran");
      $ta_result = $res_anggaran->row_array();

      $daya_serap     = $dsf["total_tagihan"];
      $total_anggaran = $ta_result["pagu_anggaran"];

      print(json_encode([
        "success" => true,
        "data"    => [
          "daya_serap" => $daya_serap ?: 0,
          "total_anggaran" => $total_anggaran ?: 0
        ]
      ]));
    } else {
      print(json_encode([
        "success" => false,
        "message" => validation_errors()
      ]));
    }
  }

  function bar_chart_daya_serap_pkk()
  {

    $this->load->library("form_validation");

    $year = $this->input->post("year", true);

    $this->form_validation->set_rules("year", "Year", "required|integer");

    if ($this->form_validation->run()) {

      /*
          $str = "SELECT DISTINCT
            m_users.id, 
            m_users.fullname,
						
            (SELECT SUM(tr_kegiatan.pagu_kegiatan) FROM tr_kegiatan WHERE tr_kegiatan.id_user = m_users.id) as total_anggaran,
            (SELECT SUM(tr_tagihan.nilai_tagihan) FROM tr_tagihan WHERE tr_tagihan.id_user = m_users.id AND (tr_tagihan.status_tagihan = 'verifikasi bendahara' OR tr_tagihan.status_tagihan = 'terbit spm') ) as daya_serap
            -- (SELECT SUM(tr_tagihan.nilai_tagihan) FROM tr_tagihan WHERE tr_tagihan.id_user = m_users.id 
            --   --AND 
            --   --(tr_tagihan.status_tagihan = 'verifikasi bendahara' OR tr_tagihan.status_tagihan = 'terbit spm') 
            -- ) as daya_serap
          FROM
            m_users
          JOIN 
            tr_kegiatan ON m_users.id = tr_kegiatan.id_user
          JOIN
            m_anggaran ON m_anggaran.id = tr_kegiatan.id_anggaran
          JOIN
            tr_detail_kegiatan ON tr_kegiatan.id = tr_detail_kegiatan.id_kegiatan
          JOIN 
            tr_tagihan ON tr_detail_kegiatan.id = tr_tagihan.id_detail_kegiatan
            
          WHERE
            -- (tr_tagihan.status_tagihan = 'verifikasi bendahara' OR tr_tagihan.status_tagihan = 'terbit spm') 
            -- AND
            m_anggaran.tahun_anggaran = '$year' 
        ";
        
        */

      $str = "SELECT DISTINCT
            m_users.id, 
            m_users.fullname,
            m_users.group_id,
          
            (SELECT SUM(pagu_kegiatan) 
              FROM tr_kegiatan JOIN m_anggaran ON m_anggaran.id = tr_kegiatan.id_anggaran
              WHERE 
              tahun_anggaran = '$year' AND
              tr_kegiatan.id_user = m_users.id
            ) as total_anggaran,
            (
              SELECT SUM(nilai_tagihan)
                FROM tr_tagihan 
                JOIN tr_detail_kegiatan ON tr_tagihan.id_detail_kegiatan = tr_detail_kegiatan.id
                JOIN tr_kegiatan ON tr_detail_kegiatan.id_kegiatan = tr_kegiatan.id 
                JOIN m_anggaran ON m_anggaran.id = tr_kegiatan.id_anggaran
              WHERE
                (tr_tagihan.status_tagihan = 'verifikasi bendahara' OR tr_tagihan.status_tagihan = 'terbit spm')
                AND
                (tahun_anggaran = '$year' AND tr_tagihan.id_user =  m_users.id)
              
            ) as daya_serap
          FROM
            m_users
            JOIN tr_kegiatan ON tr_kegiatan.id_user = m_users.id 
           
          WHERE 
            
          
            group_id = 2
        ";

      $q = $this->db->query($str);
      $result = $q->result_array();

      echo json_encode([
        "success" => true,

        "data" => $result ?: []
      ]);
    } else {
      echo json_encode([
        "success" => false,

        "data" => []
      ]);
    }
  }

  function bar_chart_daya_serap_kegiatan()
  {
    $this->load->library("form_validation");

    $year = $this->input->post("year", true);

    $this->form_validation->set_rules("year", "Year", "required|integer");

    if ($this->form_validation->run()) {

      // $str = "SELECT DISTINCT
      //       tr_kegiatan.id as kegiatan_utama_id,
      //       tr_kegiatan.nama_kegiatan,
      //       tr_kegiatan.pagu_kegiatan as total_anggaran,
      //       (SELECT 
      //           SUM(tr_tagihan.nilai_tagihan) 
      //         FROM 
      //           tr_tagihan 
      //         JOIN
      //           tr_detail_kegiatan ON tr_detail_kegiatan.id = tr_tagihan.id_detail_kegiatan
      //         JOIN 
      //           tr_kegiatan ON tr_kegiatan.id = tr_detail_kegiatan.id_kegiatan
      //         WHERE 
      //         --tr_kegiatan.id = kegiatan_utama_id AND
      //         tr_tagihan.status_tagihan = 'verifikasi bendahara' OR tr_tagihan.status_tagihan = 'terbit spm'
      //         --   tr_kegiatan.prioritas = 1 AND
      //         --   m_anggaran.tahun_anggaran = '$year'

      //       ) as daya_serap

      //     FROM 
      //       tr_kegiatan
      //     JOIN
      //       m_anggaran ON m_anggaran.id = tr_kegiatan.id_anggaran
      //     JOIN
      //       tr_detail_kegiatan ON tr_kegiatan.id = tr_detail_kegiatan.id_kegiatan
      //     JOIN 
      //       tr_tagihan ON tr_detail_kegiatan.id = tr_tagihan.id_detail_kegiatan
      //     JOIN
      //       m_users ON tr_tagihan.id_user = m_users.id
      //     WHERE
      //       -- (tr_tagihan.status_tagihan = 'verifikasi bendahara' OR tr_tagihan.status_tagihan = 'terbit spm') 
      //       -- OR
      //       tr_kegiatan.prioritas = 1 AND
      //       m_anggaran.tahun_anggaran = '$year'

      //   ";

      $str = "SELECT DISTINCT
      tr_kegiatan.id as kegiatan_utama_id,
      tr_kegiatan.nama_kegiatan,
      (SELECT SUM(sub_tr_kegiatan.pagu_kegiatan)
        FROM tr_kegiatan as sub_tr_kegiatan JOIN m_anggaran ON m_anggaran.id = sub_tr_kegiatan.id_anggaran
        WHERE 
        tahun_anggaran = '$year' AND sub_tr_kegiatan.prioritas = 1 AND sub_tr_kegiatan.id = kegiatan_utama_id
      ) as total_anggaran,
      (
        SELECT SUM(nilai_tagihan)
          FROM tr_tagihan 
          JOIN tr_detail_kegiatan ON tr_tagihan.id_detail_kegiatan = tr_detail_kegiatan.id
          JOIN tr_kegiatan ON tr_detail_kegiatan.id_kegiatan = tr_kegiatan.id 
          JOIN m_anggaran ON m_anggaran.id = tr_kegiatan.id_anggaran
        WHERE
          (tr_tagihan.status_tagihan = 'verifikasi bendahara' OR tr_tagihan.status_tagihan = 'terbit spm')
          AND
          tahun_anggaran = '$year' AND tr_kegiatan.id = kegiatan_utama_id  
        
      ) as daya_serap
    FROM
      tr_kegiatan
      WHERE
      tr_kegiatan.prioritas = 1
        ";

      $q = $this->db->query($str);
      $result = $q->result_array();

      echo json_encode([
        "success" => true,

        "data" => $result ?: []
      ]);
    } else {
      echo json_encode([
        "success" => false,

        "data" => []
      ]);
    }
  }
}
