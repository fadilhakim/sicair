<?php

class Kegiatan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        validSessionIsOut();
        $this->authorization->redirect_menu("dashboard");
        $this->load->model("kegiatan_model");
        $this->load->model("anggaran_model");
    }

    function index()
    {

        validSessionIsOut();

        $data["title"] = "Kelola Kegiatan";
        $data["css"] = "/template/pages/kegiatan/css";
        $data["js"] = "/template/pages/kegiatan/js";
        $this->load->model("user_model");
        $all_user =  $this->user_model->get_all_user_ppk();
        $dt = array(
            'all_user' => $all_user
        );
        $data["content"] = $this->load->view("template/pages/kegiatan/list_kegiatan", $dt, true);
        $this->load->view("template/index", $data);
    }

    function get_all_kegiatan()
    {
        $this->load->model("user_model");
        $userID = $this->session->userdata("user_id");

        error_reporting(0);

        $draw   = intval($this->input->post("draw"));
        $start  = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order  = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col    = 0;
        $dir    = "";

        $filter_nama_ppk     = $this->input->post("filter_nama_ppk");
        $filter_anggaran_id  = $this->input->post("filter_anggaran_id");
        $filter_prioritas    = $this->input->post("filter_prioritas");

        $params = array(
            "limit"     => $length,
            "offset"    => $start,
            "order"     => $order,
            "col"       => $col,
            "dir"       => $dir,
            "search"    => $search,

            "filter_nama_ppk"    => $filter_nama_ppk,
            "filter_anggaran_id" => $filter_anggaran_id,
            "filter_prioritas"   => $filter_prioritas
        );

        if ($this->session->userdata("group_id") == 2) {

            $list_kegiatan = $this->kegiatan_model->getMainKegiatan_by_ppk($params, $userID);

            $params["count"] = true;
            $filter_kegiatan_count = $this->kegiatan_model->getMainKegiatan_by_ppk($params, $userID);

            $params2["count"] = true;
            $total_kegiatan_count = $this->kegiatan_model->getMainKegiatan_by_ppk($params2, $userID);
        } else {

            $list_kegiatan = $this->kegiatan_model->getMainKegiatan($params);

            $params["count"] = true;
            $filter_kegiatan_count = $this->kegiatan_model->getMainKegiatan($params);

            $params2["count"] = true;
            $total_kegiatan_count = $this->kegiatan_model->getMainKegiatan($params2);
        }

        $data = array();
        $no = 1;
        foreach ($list_kegiatan as $rows) {
            $nama_ppk =  $this->user_model->get_specific_user_ppk($rows['id_user']);

            foreach ($nama_ppk as $rowuser) {
                $datauser[] = array(
                    'fullname' => $rowuser['fullname']
                );
            }

            // $text = substr($rows["nama_kegiatan"], 0, 35);
            $data[] = array(
                "no"                  => $no,
                "id"                  => $rows["id"],
                "kode_aktivitas"      => $rows["kode_aktivitas"],
                "komponen"            => $rows["komponen"],
                "subkomponen"         => $rows["subkomponen"],
                "nama_kegiatan"       => $rows["nama_kegiatan"],
                "pagu_kegiatan"       => number_format($rows["pagu_kegiatan"], 0, ",", "."),
                "id_user"             => $rowuser['fullname'],
                "kro"                 => $rows["kro"],
                "ro"                  => $rows["ro"],
                "prioritas"           => $rows["prioritas"],
            );
            $no++;
        }

        // echo '<pre>';
        // var_dump($data);
        // echo '</pre>';

        print json_encode([
            "draw" => $draw,
            "recordsTotal" => $total_kegiatan_count,
            "recordsFiltered" => $filter_kegiatan_count,
            "data" => $data
        ]);
    }

    function tambah_kegiatan()
    {

        validSessionIsOut();

        $data["title"] = "Tambah Kegiatan Anggaran";
        $data["css"] = "/template/pages/kelola_anggaran/css"; // path
        $data["js"] = "/template/pages/kelola_anggaran/js"; // path

        $dt = array();
        $data["content"] = $this->load->view("template/pages/kelola_anggaran/form_tambah_anggaran", $dt, true);
        $this->load->view("template/index", $data);
    }

    function edit_kegiatan()
    {
        $this->load->library("form_validation");

        $id_kegiatan         = $this->input->post("id_kegiatan", true);
        $id_anggaran         = $this->input->post("id_anggaran", true);
        $kode_aktivitas      = $this->input->post("kode_aktivitas", true);
        $kro                 = $this->input->post("kro", true);
        $ro                  = $this->input->post("ro", true);
        $nama_kegiatan       = $this->input->post("nama_kegiatan");
        $pagu_kegiatan       = $this->input->post("pagu_kegiatan");
        $pagu_kegiatan       = str_replace("Rp. ", "", $pagu_kegiatan);
        $pagu_kegiatan       = str_replace(".", "", $pagu_kegiatan);
        $pagu_kegiatan       = str_replace(",00", "", $pagu_kegiatan);
        $id_user             = $this->input->post("id_user");
        $komponen            = $this->input->post("komponen");
        $subkomponen         = $this->input->post("subkomponen");

        $this->form_validation->set_rules("id_anggaran", "id anggaran", "required|integer");

        if ($this->form_validation->run()) {

            $data = [
                "id_anggaran"       => $id_anggaran,
                "kode_aktivitas"    => $kode_aktivitas,
                "kro"               => $kro,
                "ro"                => $ro,
                "nama_kegiatan"     => $nama_kegiatan,
                "pagu_kegiatan"     => $pagu_kegiatan,
                "komponen"          => $komponen,
                "subkomponen"       => $subkomponen,
                "id_user"           => $id_user
            ];

            $this->kegiatan_model->edit_kegiatan($id_kegiatan, $data);

            echo json_encode([
                "success" => true,
                "message" => "Berhasil Meng-update Kegiatan"
            ]);
        }
    }

    function view_detail_kegiatan()
    {

        validSessionIsOut();
        $uri = $this->uri->segment(3);
        $data["title"] = "Detail Kegiatan";
        $data["css"] = "/template/pages/kegiatan/css"; // path
        $data["js"] = "/template/pages/kegiatan/js"; // path
        $this->load->model("user_model");
        $this->load->model("kegiatan_model");
        $all_mak =  $this->kegiatan_model->list_mak();
        $dt = array(
            'data_kegiatan' => $this->kegiatan_model->get_kegiatan($uri),
            'all_mak' => $all_mak
        );

        // echo '<pre>';
        // var_dump($nama_ppk);
        // echo '</pre>';

        $data["content"] = $this->load->view("template/pages/kegiatan/detail_kegiatan", $dt, true);
        $this->load->view("template/index", $data);
    }

    function get_detail_list_kegiatan()
    {
        error_reporting(0);
        $id_kegiatan = $this->input->post("id_kegiatan", true);
        $draw   = intval($this->input->post("draw"));
        $start  = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order  = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col    = 0;
        $dir    = "";

        $kode_mak     = $this->input->post("kode_mak");

        $params = array(
            "limit"     => $length,
            "offset"    => $start,
            "order"     => $order,
            "col"       => $col,
            "dir"       => $dir,
            "search"    => $search,

            "kode_mak"    => $kode_mak
        );

        $list_detail_kegiatan = $this->kegiatan_model->get_detail_kegiatan($params, $id_kegiatan);
        $params["count"] = true;
        $filter_detail_kegiatan_count = $this->kegiatan_model->get_detail_kegiatan($params, $id_kegiatan);
        $params2["count"] = true;
        $total_detail_kegiatan_count = $this->kegiatan_model->get_detail_kegiatan($params2, $id_kegiatan);

        $data = array();
        foreach ($list_detail_kegiatan as $rows) {

            $data[] = array(
                "id"                  => $rows["id"],
                "detail_kegiatan"     => $rows["detail_kegiatan"],
                "pagu_kegiatan"       => number_format($rows["pagu_kegiatan"]),
                "volume"              => $rows["volume"],
                "kode_mak"            => $rows["kode_mak"],
                "uraian_mak"          => $rows["uraian_mak"],
            );
        }

        // echo '<pre>';
        // var_dump($id_kegiatan);
        // echo '</pre>';

        print json_encode([
            "draw" => $draw,
            "recordsTotal" => $total_detail_kegiatan_count,
            "recordsFiltered" => $total_detail_kegiatan_count,
            "data" => $data
        ]);
    }

    function delete_kegiatan()
    {
        $this->load->library("form_validation");
        $kegiatan_id = $this->input->post("kegiatan_id");
        $this->form_validation->set_rules("kegiatan_id", "kegiatan id", "required|integer");
        if ($this->form_validation->run()) {;
            $this->kegiatan_model->delete_kegiatan($kegiatan_id);

            echo json_encode([
                "success" => true,
                "message" => "Berhasil Delete Kegiatan"
            ]);
        } else {
            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }

    function tambah_detail_kegiatan_process()
    {
        $this->load->library("form_validation");

        $kode_mak          = $this->input->post("kode_mak", true);
        $uraian_mak        = $this->input->post("uraian_mak", true);
        $detail_kegiatan   = $this->input->post("detail_kegiatan", true);
        $volume            = $this->input->post("volume", true);
        $pagu_detail_kegiatan     = $this->input->post("pagu_detail_kegiatan", true);
        $pagu_detail_kegiatan       = str_replace("Rp. ", "", $pagu_detail_kegiatan);
        $pagu_detail_kegiatan       = str_replace(".", "", $pagu_detail_kegiatan);
        $pagu_detail_kegiatan       = str_replace(",00", "", $pagu_detail_kegiatan);
        $id_kegiatan       = $this->input->post("id_kegiatan", true);
        $id_user           = $this->input->post("id_user", true);

        $this->form_validation->set_rules("id_kegiatan", "Id Kegiatan", "required");
        $this->form_validation->set_rules("kode_mak", "kode mak", "required");
        $this->form_validation->set_rules("uraian_mak", "uraian mak", "required");
        $this->form_validation->set_rules("detail_kegiatan", "detail kegiatan", "required");
        $this->form_validation->set_rules("volume", "volume", "required");
        $this->form_validation->set_rules("pagu_detail_kegiatan", "pagu detail kegiatan", "required");

        if ($this->form_validation->run()) {
            $data = [
                "kode_mak"              => $kode_mak,
                "uraian_mak"            => $uraian_mak,
                "detail_kegiatan"       => $detail_kegiatan,
                "id_kegiatan"           => $id_kegiatan,
                "pagu_detail_kegiatan"  => $pagu_detail_kegiatan,
                "volume"                => $volume,
                "id_user"               => $id_user
            ];

            $this->kegiatan_model->tambah_detail_kegiatan($data);
            exit(json_encode([
                "success" => true,
                "message" => "Berhasil menambahkan Detail Kegiatan",
            ]));
        } else {
            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }

    function get_kegiatan()
    {

        $result = $this->kegiatan_model->list_kegiatan();

        echo json_encode([
            "success" => true,
            "message" => "you successfully retrieve kegiatan",
            "data"   => $result
        ]);
    }

    function get_kegiatan_detail_list()
    {

        $this->load->library("form_validation");

        $id_kegiatan = $this->input->post("id_kegiatan");
        $params["id_kegiatan"] = $id_kegiatan;

        $this->form_validation->set_rules("id_kegiatan", "ID Kegiatan", "required|integer");

        if ($this->form_validation->run()) {

            $result = $this->kegiatan_model->list_detail_kegiatan($params);

            echo json_encode([
                "success" => true,
                "data"   => $result
            ]);
        } else {
            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }

    function get_kegiatan_detail()
    {
        $this->load->library("form_validation");

        $id = $this->input->post("id");

        $this->form_validation->set_rules("id", "id", "required|integer");

        if ($this->form_validation->run()) {

            $result = $this->kegiatan_model->detail_detail_kegiatan($id);

            echo json_encode([
                "status" => 200,
                "message" => "you successfully retrieve detail kegiatan",
                "data"   => $result
            ]);
        } else {
            echo json_encode([
                "status" => 500,
                "message" => validation_errors(),
                "data"   => []
            ]);
        }
    }


    function update_process_detail_kegiatan()
    {
        $this->load->library("form_validation");

        $id_kegiatan_detail  = $this->input->post("id_kegiatan_detail");
        $kode_mak  = $this->input->post("kode_mak");
        $uraian_mak  = $this->input->post("uraian_mak");
        $detail_kegiatan  = $this->input->post("detail_kegiatan");
        $volume  = $this->input->post("volume");
        $pagu_detail_kegiatan  = $this->input->post("pagu_detail_kegiatan");
        $pagu_detail_kegiatan       = str_replace("Rp. ", "", $pagu_detail_kegiatan);
        $pagu_detail_kegiatan       = str_replace(".", "", $pagu_detail_kegiatan);
        $pagu_detail_kegiatan       = str_replace(",00", "", $pagu_detail_kegiatan);
        $id_kegiatan  = $this->input->post("id_kegiatan");


        $this->form_validation->set_rules("id_kegiatan_detail", "id_kegiatan_detail", "required");
        $this->form_validation->set_rules("kode_mak", "kode mak", "required");
        $this->form_validation->set_rules("uraian_mak", "uraian mak", "required");
        $this->form_validation->set_rules("detail_kegiatan", "detail kegiatan", "required");
        $this->form_validation->set_rules("volume", "volume", "required");
        $this->form_validation->set_rules("pagu_detail_kegiatan", "pagu detail kegiatan", "required");


        if ($this->form_validation->run()) {

            $this->kegiatan_model->update_detail_kegiatan($id_kegiatan_detail, [
                "kode_mak"          => $kode_mak,
                "uraian_mak"        => $uraian_mak,
                "detail_kegiatan"   => $detail_kegiatan,
                "pagu_detail_kegiatan"     => $pagu_detail_kegiatan,
                "volume"            => $volume,
                "id_kegiatan"       => $id_kegiatan,
            ]);


            echo json_encode([
                "success" => true,
                "status" => 200,
                "message" => "Berhasi Edit Detail Kegiatan",
                "Query" => $this->db->last_query()
            ]);
        } else {
            echo json_encode([
                "success" => false,
                "status" => 500,
                "message" => validation_errors()
            ]);
        }
    }

    function delete_detail_kegiatan()
    {
        $this->load->library("form_validation");
        $detail_kegiatan_id = $this->input->post("detail_kegiatan_id");
        $this->form_validation->set_rules("detail_kegiatan_id", "Detail kegiatan id", "required|integer");
        if ($this->form_validation->run()) {;
            $this->kegiatan_model->delete_detail_kegiatan($detail_kegiatan_id);

            echo json_encode([
                "success" => true,
                "message" => "Berhasil Delete Kegiatan"
            ]);
        } else {
            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }
    function getUraianMak()
    {
        $kode_mak = $this->input->post('kode_mak', true);
        $getMak = $this->kegiatan_model->get_uraian_by_mak($kode_mak);
        echo json_encode([
            "status" => "OK",
            "results" => $getMak
        ]);
    }

    function kegiatan_search()
    {

        $search = $this->input->get("search");

        $params = ["search" => $search];

        $all_kegiatan = $this->kegiatan_model->get_all_data_kegiatan($params);

        // echo '<pre>';
        // var_dump($all_kegiatan);
        // echo '</pre>';
        echo json_encode([
            "status" => "OK",
            "results" => $all_kegiatan
        ]);
    }
}
