<?php

class Kontrak extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        validSessionIsOut();
        $this->authorization->redirect_menu("dashboard");
    }

    function index()
    {

        validSessionIsOut();
        $data["title"] = "Kelola Kontrak";

        $data["css"] = "/template/pages/kontrak/css"; // path
        $data["js"] = "/template/pages/kontrak/js"; // path
        $this->load->model("user_model");
        $all_user =  $this->user_model->get_all_user_ppk();
        $dt = array(
            'all_user' => $all_user
        );
        $data["content"] = $this->load->view("template/pages/kontrak/list_kontrak", $dt, true);
        $this->load->view("template/index", $data);
    }
    function detail_kontrak()
    {

        $this->load->model("kegiatan_model");
        $this->load->model("kontrak_model");

        validSessionIsOut();
        $data["title"] = "Detail Kontrak";

        $data["css"] = "/template/pages/kontrak/css"; // path
        // $data["js"] = "/template/pages/kontrak/js"; // path
        $data["js"] = "/template/pages/kontrak/js_detail_kontrak"; // path


        $id_kontrak = $this->uri->segment(2);

        $dt_kontrak = $this->kontrak_model->kontrak_detail($id_kontrak);

        $kegiatan_detail_dt = $this->kegiatan_model->detail_detail_kegiatan($dt_kontrak["id_detail_kegiatan"]);

        $params["id_kegiatan"] = $kegiatan_detail_dt["id_kegiatan"];
        $dt = array(
            "title"                => "Detail Kontrak",
            'kegiatan_detail_dt'   => $kegiatan_detail_dt,
            "list_kegiatan"        => $this->kegiatan_model->list_kegiatan(),
            "list_detail_kegiatan" => $this->kegiatan_model->list_detail_kegiatan($params),
            "dt_kontrak"           => $dt_kontrak
        );
        $data["content"] = $this->load->view("template/pages/kontrak/tambah_kontrak", $dt, true);
        $this->load->view("template/index", $data);
    }

    function tambah_kontrak()
    {
        $this->load->model("kegiatan_model");

        validSessionIsOut();
        $data["title"] = "Tambah Kontrak";
        $data["css"] = "/template/pages/kontrak/css"; // path
        $data["js"] = "/template/pages/kontrak/js_add_kontrak"; // path
        $id_kegiatan  = $this->input->post("id_kegiatan");
        //$id_kegiatan  = 21;
        $dt = array(
            "title"                => "Tambah Kontrak",
            "list_kegiatan"        => $this->kegiatan_model->list_kegiatan(),
            "list_detail_kegiatan" => $this->kegiatan_model->list_detail_kegiatan()
        );

        $data["content"] = $this->load->view("template/pages/kontrak/tambah_kontrak", $dt, true);
        $this->load->view("template/index", $data);
    }

    function get_kontrak()
    {

        error_reporting(0);

        $this->load->model("kontrak_model");
        $this->load->model("user_model");
        $this->load->model("kegiatan_model");
        $userID = $this->session->userdata("user_id");


        $draw   = intval($this->input->post("draw"));
        $start  = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order  = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col    = 0;
        $dir    = "";

        $filter_status_kontrak     = $this->input->post("filter_status_kontrak");
        $filter_jenis_kontrak      = $this->input->post("filter_jenis_kontrak");
        $filter_subdit             = $this->input->post("filter_subdit");
        // $filter_tanggal_mulai      = $this->input->post("filter_tanggal_mulai");
        // $filter_tanggal_selesai    = $this->input->post("filter_tanggal_selesai");
        $filter_tanggal_kontrak    = $this->input->post("filter_tanggal_kontrak");
        $filter_nama_ppk           = $this->input->post("filter_nama_ppk");

        $params = array(
            "limit"             => $length,
            "offset"            => $start,
            "order"             => $order,
            "col"               => $col,
            "dir"               => $dir,
            "search"            => $search,
            "count"             => false,

            "filter_status_kontrak" => $filter_status_kontrak,
            "filter_jenis_kontrak"  => $filter_jenis_kontrak,
            "filter_subdit"         => $filter_subdit,
            "filter_tanggal_kontrak"  => $filter_tanggal_kontrak,
            "filter_nama_ppk"         => $filter_nama_ppk,
            // "filter_tanggal_mulai"  => $filter_tanggal_mulai,
            // "filter_tanggal_selesai" => $filter_tanggal_selesai
        );


        if ($this->session->userdata("group_id") == 2) {
            $list_kontrak           = $this->kontrak_model->get_all_kontrak_by_ppk($params, $userID);

            $params["count"]        = true;
            $filter_kontrak_count   = $this->kontrak_model->get_all_kontrak_by_ppk($params, $userID);

            // ALL but COUNT 
            $params2["count"]       = true;
            $total_kontrak_count    = $this->kontrak_model->get_all_kontrak_by_ppk($params2, $userID);
        } else {
            $list_kontrak           = $this->kontrak_model->get_all_kontrak($params);

            $params["count"]        = true;
            $filter_kontrak_count   = $this->kontrak_model->get_all_kontrak($params);

            // ALL but COUNT 
            $params2["count"]       = true;
            $total_kontrak_count    = $this->kontrak_model->get_all_kontrak($params2);
        }

        $data = array();
        $no = 1;
        foreach ($list_kontrak as $rows) {

            $date_start = date_create($rows["tanggal_mulai"]);
            $tanggal_mulai =  date_format($date_start, "d/m/Y");

            $date_end = date_create($rows["tanggal_selesai"]);
            $tanggal_selesai =  date_format($date_end, "d/m/Y");

            $tanggal_kontrak = date_create($rows["tanggal_kontrak"]);
            $tanggal_kontrak =  date_format($tanggal_kontrak, "d/m/Y");

            $nama_ppk =  $this->user_model->get_specific_user_ppk($rows['id_user']);
            $detail_kegiatan = $this->kegiatan_model->detail_detail_kegiatan($rows['id_detail_kegiatan']);
            $kegiatan =  $this->kegiatan_model->get_kegiatan_by_id($detail_kegiatan['id_kegiatan']);

            foreach ($nama_ppk as $rowuser) {
                $datauser[] = array(
                    'fullname' => $rowuser['fullname']
                );
            }

            $data[] = array(
                "no"                => $no,
                "id"                => $rows["id"],
                "no_kontrak"        => $rows["no_kontrak"],
                "tanggal_kontrak"   => $tanggal_kontrak,
                "uraian_kontrak"    => $rows["uraian"],
                "masa_kontrak"      => $tanggal_mulai . " - " . $tanggal_selesai,
                "nilai_kontrak"     => number_format($rows["nilai_kontrak"]),
                "jenis_kontrak"     => $rows["jenis_kontrak"],
                "nama_kontraktor"   => $rows["nama_kontraktor"],
                "subdit"            => $rows["subdit"],
                "status"            => $rows["status"],
                "tanggal_addendum"  => $rows["tanggal_addendum"],
                "nomor_addendum"    => $rows["nomor_addendum"],
                "id_user"           => $rowuser['fullname'],
                'kode_mak'          => $detail_kegiatan['kode_mak'],
                'uraian_mak'        => $detail_kegiatan['uraian_mak'],
                'detail_kegiatan'   => $detail_kegiatan['detail_kegiatan'],
                'nama_kegiatan'     => $kegiatan['nama_kegiatan']

            );

            $no++;
        }

        // echo '<pre>';
        // var_dump($data);
        // echo '</pre>';

        print json_encode([
            "draw"              => $draw,
            "recordsTotal"      => $total_kontrak_count,
            "recordsFiltered"   => $filter_kontrak_count,
            "data"              => $data
        ]);
    }

    function add_kontrak()
    {

        $this->load->model("kontrak_model");
        $this->load->library("form_validation");

        $termin                         = $this->input->post("termin", true);

        $data = [];

        $data["id_detail_kegiatan"]     = $this->input->post("id_detail_kegiatan", true);
        $data["no_kontrak"]             = $this->input->post("no_kontrak", true);
        $data["tanggal_kontrak"]        = $this->input->post("tanggal_kontrak", true);
        $data["uraian"]                 = $this->input->post("uraian", true);
        $data["status"]                 = 'Terdaftar Belum di Verifikasi';
        $data["jenis_kontrak"]          = $this->input->post("jenis_kontrak", true);
        $data["nomor_addendum"]         = $this->input->post("no_addendum", true);
        $data["tanggal_addendum"]       = $this->input->post("tanggal_addendum", true);
        $data["nama_kontraktor"]        = $this->input->post("nama_kontraktor", true);
        $data["alamat_kontraktor"]      = $this->input->post("alamat_kontraktor", true);
        $data["npwp_kontraktor"]        = $this->input->post("npwp_kontraktor", true);
        $data["rekening_kontraktor"]    = $this->input->post("rekening_kontraktor", true);
        $data["nama_rekening"]          = $this->input->post("nama_rekening", true);
        $data["nilai_kontrak"]          = $this->input->post("nilai_kontrak", true);
        $data["nilai_kontrak"]            = str_replace("Rp. ", "", $data["nilai_kontrak"]);
        $data["nilai_kontrak"]            = str_replace(".", "", $data["nilai_kontrak"]);
        $data["nilai_kontrak"]            = str_replace(",00", "", $data["nilai_kontrak"]);
        $data["cara_pembayaran"]        = $this->input->post("cara_pembayaran", true);
        $data["waktu_penyelesaian"]     = $this->input->post("waktu_penyelesaian", true);
        $data["waktu_pemeliharaan"]     = $this->input->post("waktu_pemeliharaan", true);
        $data["tanggal_mulai"]          = $this->input->post("tanggal_mulai", true);
        $data["tanggal_selesai"]        = $this->input->post("tanggal_selesai", true);

        $data["jumlah_termin"]          = $this->input->post("jumlah_termin", true);
        $data["uang_muka"]              = $this->input->post("uang_muka", true);
        $data["uang_muka"]              = str_replace("Rp. ", "", $data["uang_muka"]);
        $data["uang_muka"]              = str_replace(".", "", $data["uang_muka"]);
        $data["uang_muka"]              = str_replace(",00", "", $data["uang_muka"]);
        $data["subdit"]                 = $this->input->post("subdit", true);

        $data["id_user"]                = $this->input->post("id_user", true);

        $this->form_validation->set_rules("id_detail_kegiatan", "ID Detail Kegiatan", "required|integer");
        $this->form_validation->set_rules("no_kontrak", "No Kontrak", "required");
        $this->form_validation->set_rules("tanggal_kontrak", "Tanggal Kontrak", "required");
        $this->form_validation->set_rules("uraian", "Uraian Kontrak", "required");
        $this->form_validation->set_rules("nilai_kontrak", "Nilai Kontrak", "required");
        // $this->form_validation->set_rules("rekening_kontraktor", "Rekening Kontraktor", "numeric");

        $this->form_validation->set_rules("waktu_penyelesaian", "Waktu Penyelesaian", "integer");
        $this->form_validation->set_rules("waktu_pemeliharaan", "Waktu Pemeliharaan", "integer");
        $this->form_validation->set_rules("uang_muka", "Uang Muka", "");

        if ($this->form_validation->run()) {


            // nama file masuk ke database
            if ($_FILES["file_kontrak"]) {
                // $file_kontrak       = explode(".", $_FILES['file_kontrak']['name']);
                // $ext_file_kontrak   = end($file_kontrak);
                // $file_kontrak_name  = strtolower("" . str_replace("/", "", $data["no_kontrak"]) . "-" . "." . $ext_file_kontrak);

                // $data["file_kontrak"]            = $file_kontrak_name; // untuk data ke database
                $data["file_kontrak"]            =  $_FILES['file_kontrak']['name']; // untuk data ke database
            }

            // if($_FILES["file_addendum"]) {
            //     $data["file_addendum"]          = $_FILES["file_addendum"]["name"];
            // }

            if ($_FILES["file_myx"]) {
                // $file_myx           = explode(".", $_FILES['file_myx']['name']);
                // $ext_file_myx       = end($file_myx);
                // $file_myx_name      = strtolower("myx-" . str_replace("/", "", $data["no_kontrak"]) . "-" . date("YmdHis") . "." . $ext_file_myx);

                //$data["file_myx"]                = $file_myx_name;
                $data["file_myx"]            =  $_FILES['file_myx']['name']; // untuk data ke database
            }

            if ($this->kontrak_model->add_kontrak($data)) {


                // if (count($termin) > 0) {
                //     $termin_phase = 1;
                //     $last_id = $this->db->insert_id();
                //     foreach ($termin as $dt) {

                //         $dt_termin["termin_phase"] = $termin_phase;
                //         $dt_termin["harga_termin"] = $dt;
                //         $dt_termin["id_kontrak"] = $last_id;

                //         $this->kontrak_model->add_termin($dt_termin);

                //         $termin_phase++;
                //     }
                // }

                // upload file
                if ($_FILES["file_kontrak"]) {

                    if (!is_dir("./files/kontrak/" . str_replace("/", "", $data["no_kontrak"]))) {
                        mkdir("./files/kontrak/" . str_replace("/", "", $data["no_kontrak"]), 0777, true);
                    }

                    $config1['upload_path']          = "./files/kontrak/" . str_replace("/", "", $data["no_kontrak"]) . "/";
                    $config1['allowed_types']        = '*';
                    $config1['max_size']             = 5000;
                    $config1['file_name']            = $data["file_kontrak"];

                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_kontrak"]["tmp_name"], $config1["upload_path"] . $config1["file_name"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = "";

                        // exit(json_encode([
                        //     "success" => true,
                        //     "message" => "Berhasil menambahkan kontrak. " . $error_file,
                        // ]));
                    } else {
                        // echo "upload berhasil";
                    }
                }

                // if($_FILES["file_addendum"]) {

                // }

                if ($_FILES["file_myx"]) {

                    if (!is_dir("./files/kontrak/" . str_replace("/", "", $data["no_kontrak"]))) {
                        mkdir("./files/kontrak/" . str_replace("/", "", $data["no_kontrak"]), 0777, TRUE);
                    }

                    $config2['upload_path']          = "./files/kontrak/" . str_replace("/", "", $data["no_kontrak"]) . "/";
                    $config2['allowed_types']        = '*';
                    $config2['max_size']             = 5000;
                    $config2['file_name']            =  $data["file_myx"];

                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_myx"]["tmp_name"], $config2["upload_path"] . $config2["file_name"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = "";

                        // exit(json_encode([
                        //     "success" => true,
                        //     "message" => "Berhasil menambahkan kontrak. " . $error_file,
                        // ]));
                    }
                }

                print(json_encode([
                    "success" => true,
                    "message" => "Berhasil menambahkan kontrak",
                    "id_kontrak" => $this->db->insert_id()
                ]));
            } else {

                print(json_encode([
                    "success" => false,
                    "message" => "gagal menambahkan kontrak, terdapat error query database",
                ]));
            }
        } else {

            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }

    function update_kontrak()
    {

        $this->load->model("kontrak_model");
        $this->load->library("form_validation");

        $data = [];

        $data["id"]                     = $this->input->post("kontrak_id", true);

        $data["id_detail_kegiatan"]     = $this->input->post("id_detail_kegiatan", true);
        $data["no_kontrak"]             = $this->input->post("no_kontrak", true);
        $data["tanggal_kontrak"]        = $this->input->post("tanggal_kontrak", true);
        $data["uraian"]                 = $this->input->post("uraian", true);
        $data["status"]                 = 'Terdaftar Belum di Verifikasi';
        $data["jenis_kontrak"]          = $this->input->post("jenis_kontrak", true);
        $data["nomor_addendum"]         = $this->input->post("no_addendum", true);
        $data["tanggal_addendum"]       = $this->input->post("tanggal_addendum", true);
        $data["nama_kontraktor"]        = $this->input->post("nama_kontraktor", true);
        $data["alamat_kontraktor"]      = $this->input->post("alamat_kontraktor", true);
        $data["npwp_kontraktor"]        = $this->input->post("npwp_kontraktor", true);
        $data["rekening_kontraktor"]    = $this->input->post("rekening_kontraktor", true);
        $data["nama_rekening"]          = $this->input->post("nama_rekening", true);
        $data["nilai_kontrak"]          = $this->input->post("nilai_kontrak", true);
        $data["nilai_kontrak"]          = str_replace("Rp. ", "", $data["nilai_kontrak"]);
        $data["nilai_kontrak"]          = str_replace(".", "", $data["nilai_kontrak"]);
        $data["nilai_kontrak"]          = str_replace(",00", "", $data["nilai_kontrak"]);
        $data["cara_pembayaran"]        = $this->input->post("cara_pembayaran", true);
        $data["waktu_penyelesaian"]     = $this->input->post("waktu_penyelesaian", true);
        $data["waktu_pemeliharaan"]     = $this->input->post("waktu_pemeliharaan", true);
        $data["tanggal_mulai"]          = $this->input->post("tanggal_mulai", true);
        $data["tanggal_selesai"]        = $this->input->post("tanggal_selesai", true);

        $data["jumlah_termin"]          = $this->input->post("jumlah_termin", true);
        $data["uang_muka"]              = $this->input->post("uang_muka", true);
        $data["uang_muka"]              = str_replace("Rp. ", "", $data["uang_muka"]);
        $data["uang_muka"]              = str_replace(".", "", $data["uang_muka"]);
        $data["uang_muka"]              = str_replace(",00", "", $data["uang_muka"]);
        $data["subdit"]                 = $this->input->post("subdit", true);

        $data["id_user"]                = $this->input->post("id_user", true);

        $this->form_validation->set_rules("kontrak_id", "ID Kontrak", "required|integer");

        $this->form_validation->set_rules("id_detail_kegiatan", "ID Detail Kegiatan", "required|integer");
        $this->form_validation->set_rules("no_kontrak", "No Kontrak", "required");
        $this->form_validation->set_rules("tanggal_kontrak", "Tanggal Kontrak", "required");
        $this->form_validation->set_rules("uraian", "Uraian Kontrak", "required");
        $this->form_validation->set_rules("nilai_kontrak", "Nilai Kontrak", "required");
        // $this->form_validation->set_rules("rekening_kontraktor", "Rekening Kontraktor", "");

        $this->form_validation->set_rules("waktu_penyelesaian", "Waktu Penyelesaian", "integer");
        $this->form_validation->set_rules("waktu_pemeliharaan", "Waktu Pemeliharaan", "integer");
        $this->form_validation->set_rules("uang_muka", "Uang Muka", "");

        if ($this->form_validation->run()) {

            // nama file masuk ke database
            if ($_FILES["file_kontrak"]["error"] == 0) {
                // $file_kontrak       = explode(".", $_FILES['file_kontrak']['name']);
                // $ext_file_kontrak   = end($file_kontrak);
                // $file_kontrak_name  = strtolower("" . str_replace("/", "", $data["no_kontrak"]) . "-" . "." . $ext_file_kontrak);
                //$data["file_kontrak"]            =  $file_kontrak_name; // untuk data ke database
                $data["file_kontrak"]            =  $_FILES['file_kontrak']['name']; // untuk data ke database
            }

            // if($_FILES["file_addendum"]) {
            //     $data["file_addendum"]          = $_FILES["file_addendum"]["name"];
            // }

            if ($_FILES["file_myx"]["error"] == 0) {
                // $file_myx           = explode(".", $_FILES['file_myx']['name']);
                // $ext_file_myx       = end($file_myx);
                // $file_myx_name      = strtolower("myx-" . str_replace("/", "", $data["no_kontrak"]) . "-" . date("YmdHis") . "." . $ext_file_myx);

                // $data["file_myx"]                = $file_myx_name;
                $data["file_myx"]                = $_FILES["file_myx"]["name"];
            }


            if ($this->kontrak_model->update_kontrak($data)) {

                // upload file
                if ($_FILES["file_kontrak"]) {

                    if (!is_dir("./files/kontrak/" . str_replace("/", "", $data["no_kontrak"]))) {
                        mkdir("./files/kontrak/" . str_replace("/", "", $data["no_kontrak"]), 0777, true);
                    }

                    $config1['upload_path']          = "./files/kontrak/" . str_replace("/", "", $data["no_kontrak"]) . "/";
                    $config1['allowed_types']        = '*';
                    $config1['max_size']             = 5000;

                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_kontrak"]["tmp_name"], $config1["upload_path"] . $data["file_kontrak"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = "";

                        // exit(json_encode([
                        //     "success" => true,
                        //     "message" => "Berhasil mengubah kontrak. " . $error_file,
                        // ]));
                    } else {
                        // echo "upload berhasil";
                    }
                }

                // if($_FILES["file_addendum"]) {

                // }

                if ($_FILES["file_myx"]) {

                    if (!is_dir("./files/kontrak/" . str_replace("/", "", $data["no_kontrak"]))) {
                        mkdir("./files/kontrak/" . str_replace("/", "", $data["no_kontrak"]), 0777, TRUE);
                    }

                    $config2['upload_path']          = "./files/kontrak/" . str_replace("/", "", $data["no_kontrak"]) . "/";
                    $config2['allowed_types']        = '*';
                    $config2['max_size']             = 5000;

                    // IF FILE UPLOADED NOT VALIDATE
                    if (!move_uploaded_file($_FILES["file_myx"]["tmp_name"], $config2["upload_path"] . $data["file_myx"])) {
                        // $this->mom_model->add_mom($data);
                        $error_file = "";

                        // exit(json_encode([
                        //     "success" => true,
                        //     "message" => "Berhasil mengubah kontrak. " . $error_file,
                        // ]));
                    }
                }

                print(json_encode([
                    "success" => true,
                    "message" => "Berhasil mengubah kontrak "
                ]));
            } else {

                print(json_encode([
                    "success" => false,
                    "message" => "gagal mengubah kontrak, terdapat error query database",
                ]));
            }
        } else {

            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }

    function delete_kontrak()
    {

        $this->load->model("kontrak_model");
        $this->load->library("form_validation");

        $id_kontrak = $this->input->post("id_kontrak");

        $this->form_validation->set_rules("id_kontrak", "ID Kontrak", "required|integer");

        if ($this->form_validation->run()) {

            // delete files kontrak

            if ($this->kontrak_model->delete_kontrak($id_kontrak)) {
                print(json_encode([
                    "success" => true,
                    "message" => "sukses menghapus kontrak",
                ]));
            } else {
                // 
                print(json_encode([
                    "success" => false,
                    "message" => "gagal menghapus kontrak, terdapat error query database",
                ]));
            }
        } else {
            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }

    function get_detail_kontrak()
    {

        error_reporting(0);

        $this->load->model("kontrak_model");
        //$this->load->model("kegiatan_model");

        $id_kontrak = $this->input->post("id_kontrak");

        $result = $this->kontrak_model->kontrak_detail($id_kontrak);
        //$kegiatan = $this->kegiatan_model->detail_detail_kegiatan($result["id_detail_kegiatan"])

        echo json_encode([
            "success" => true,
            "data" => $result,
            //"kegiatan_utama" => $kegiatan_utama
        ]);
    }

    function update_addendum()
    {

        $this->load->model("kontrak_model");
        $this->load->library("form_validation");

        $data["id"]                 = $this->input->post("id_kontrak", true);
        $data["nomor_addendum"]     = $this->input->post("nomor_addendum", true);
        $data["tanggal_addendum"]   = $this->input->post("tanggal_addendum", true);

        $this->form_validation->set_rules("id_kontrak", "ID Kontrak", "required|integer");
        $this->form_validation->set_rules("nomor_addendum", "NO Addendum", "required");
        $this->form_validation->set_rules("tanggal_addendum", "Tanggal Addendum", "required");

        if ($this->form_validation->run()) {

            //kontrak detail
            $kontrak_detail = $this->kontrak_model->kontrak_detail($data["id"]);

            if ($_FILES["file_addendum"]) {

                $file_addendum        = explode(".", $_FILES['file_addendum']['name']);
                $ext_file_addendum    = end($file_addendum);
                $file_addendum_name   = strtolower("addendum-" . str_replace("/", "", $kontrak_detail["no_kontrak"]) . "-" . date("YmdHis") . "." . $ext_file_addendum);

                $data["file_addendum"] = $file_addendum_name; // untuk data ke database
            }

            if ($this->kontrak_model->update_kontrak($data)) {
                if ($_FILES["file_addendum"]) {

                    if (!is_dir("./files/kontrak/" . str_replace("/", "", $kontrak_detail["no_kontrak"]))) {
                        mkdir("./files/kontrak/" . str_replace("/", "", $kontrak_detail["no_kontrak"]), 0777, true);
                    }

                    $config1['upload_path']          = "./files/kontrak/" . str_replace("/", "", $kontrak_detail["no_kontrak"]) . "/";
                    $config1['allowed_types']        = '*';
                    $config1['max_size']             = 5000;
                    $config1['file_name']            = $data["file_addendum"];

                    $this->load->library('upload', $config1);

                    // IF FILE UPLOADED NOT VALIDATE
                    if (!$this->upload->do_upload('file_addendum')) {
                        // $this->mom_model->add_mom($data);
                        $error_file = $this->upload->display_errors();

                        exit(json_encode([
                            "success" => true,
                            "message" => "Berhasil menambahkan addendum. " . $error_file,
                        ]));
                    } else {
                        // echo "upload berhasil";
                    }
                }
            }

            print(json_encode([
                "success" => true,
                "message" => "Sukses menyimpan Addendum",
            ]));
        } else {
            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }

    function update_status()
    {

        $this->load->model("kontrak_model");
        $this->load->library("form_validation");

        $data["id"]                 = $this->input->post("id", true);
        $data["status"]             = 'Verifikasi Bendahara';

        $this->form_validation->set_rules("id", "ID Kontrak", "required|integer");
        // $this->form_validation->set_rules("status", "Status Kontrak", "required");

        if ($this->form_validation->run()) {

            //kontrak detail
            $kontrak_detail = $this->kontrak_model->kontrak_detail($data["id"]);

            $this->kontrak_model->update_kontrak($data);

            print(json_encode([
                "success" => true,
                "message" => "Sukses menyimpan Status",
            ]));
        } else {
            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }

    function update_status_reject()
    {

        $this->load->model("kontrak_model");
        $this->load->library("form_validation");

        $data["id"]                 = $this->input->post("id", true);
        $data["status"]             = 'Rejected';
        $data["catatan_reject"]     = $this->input->post("catatan_reject", true);;

        $this->form_validation->set_rules("id", "ID Kontrak", "required|integer");
        // $this->form_validation->set_rules("status", "Status Kontrak", "required");

        if ($this->form_validation->run()) {

            //kontrak detail
            $kontrak_detail = $this->kontrak_model->kontrak_detail($data["id"]);

            $this->kontrak_model->update_kontrak($data);

            print(json_encode([
                "success" => true,
                "message" => "Sukses menyimpan Status",
            ]));
        } else {
            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }

    function download_file()
    {

        $this->load->model("kontrak_model");
        $this->load->helper(array("download", "url"));

        $id_kontrak  = $this->uri->segment(3);
        $type        = $this->uri->segment(4);

        $kontrak_detail = $this->kontrak_model->kontrak_detail($id_kontrak);

        if (!empty($kontrak_detail)) {
            $no_kontrak_format = str_replace("/", "", $kontrak_detail["no_kontrak"]);

            if ($type == "addendum") {
                $url_add = "files/kontrak/$no_kontrak_format/$kontrak_detail[file_addendum]";
                force_download($url_add, NULL);
                echo "aaa";
                exit();
            } else if ($type == "kontrak") {
                $url_add = "files/kontrak/$no_kontrak_format/$kontrak_detail[file_kontrak]";
                force_download($url_add, NULL);
            } else {
                $url_add = "files/kontrak/$no_kontrak_format/$kontrak_detail[file_myx]";
                force_download($url_add, NULL);
            }
        }


        show_404(base_url("page/notfound"), true);
    }

    function get_all_termin()
    {
        error_reporting(0);

        $this->load->model("kontrak_model");

        $draw   = intval($this->input->post("draw"));
        $start  = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order  = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col    = 0;
        $dir    = "";

        $id_kontrak = $this->input->post("id_kontrak", true);


        $params = array(
            "limit"             => $length,
            "offset"            => $start,
            "order"             => $order,
            "col"               => $col,
            "dir"               => $dir,
            "search"            => $search,

            "id_kontrak"        => $id_kontrak
        );

        $list_termin            = $this->kontrak_model->get_all_termin($params);
        $params["count"]        = true;
        $filter_termin_count    = $this->kontrak_model->get_all_termin($params);
        $params2["count"]       = true;
        $params2["id_kontrak"]  = $id_kontrak;
        $total_termin_count     = $this->kontrak_model->get_all_termin($params2);

        $data = array();
        $no = 1;
        foreach ($list_termin as $rows) {

            $data[] = array(
                "no"                => $no,
                "id"                => $rows["id"],
                "termin_phase"      => $rows["termin_phase"],
                "harga_termin"      => number_format($rows["harga_termin"]),

            );

            $no++;
        }

        // echo '<pre>';
        // var_dump($data);
        // echo '</pre>';

        print json_encode([
            "draw"              => $draw,
            "recordsTotal"      => $total_termin_count,
            "recordsFiltered"   => $filter_termin_count,
            "data"              => $data
        ]);
    }

    function add_termin()
    {
        $this->load->model("kontrak_model");
        $this->load->library("form_validation");

        $data["id_kontrak"]   = $this->input->post("id_kontrak", true);
        $data["termin_phase"] = $this->input->post("termin_phase", true);
        $data["harga_termin"] = $this->input->post("harga_termin", true);
        $data["harga_termin"]              = str_replace("Rp. ", "", $data["harga_termin"]);
        $data["harga_termin"]              = str_replace(".", "", $data["harga_termin"]);
        $data["harga_termin"]              = str_replace(",00", "", $data["harga_termin"]);

        $this->form_validation->set_rules("id_kontrak", "ID Kontrak", "required|integer");
        $this->form_validation->set_rules("termin_phase", "Termin Phase", "required|integer");
        $this->form_validation->set_rules("harga_termin", "Harga Termin", "required");

        if ($this->form_validation->run()) {

            $this->kontrak_model->add_termin($data);

            print(json_encode([
                "success" => true,
                "message" => "Sukses menambah termin"
            ]));
        } else {
            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }

    function update_termin()
    {
        $this->load->model("kontrak_model");
        $this->load->library("form_validation");

        $data["id"]           = $this->input->post("id", true);
        $data["termin_phase"] = $this->input->post("termin_phase", true);
        $data["harga_termin"] = $this->input->post("harga_termin", true);
        $data["harga_termin"]              = str_replace("Rp. ", "", $data["harga_termin"]);
        $data["harga_termin"]              = str_replace(".", "", $data["harga_termin"]);
        $data["harga_termin"]              = str_replace(",00", "", $data["harga_termin"]);

        $this->form_validation->set_rules("id", "ID Termin", "required|integer");
        $this->form_validation->set_rules("termin_phase", "Termin Phase", "required|integer");
        $this->form_validation->set_rules("harga_termin", "Harga Termin", "required");

        if ($this->form_validation->run()) {

            $this->kontrak_model->update_termin($data);

            print(json_encode([
                "success" => true,
                "message" => "Sukses mengubah termin"
            ]));
        } else {
            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }

    function detail_termin()
    {

        $this->load->model("kontrak_model");
        $this->load->library("form_validation");

        $termin_id = $this->input->post("termin_id");

        $this->form_validation->set_rules("termin_id", "ID Termin", "required|integer");

        if ($this->form_validation->run()) {

            $result = $this->kontrak_model->detail_termin($termin_id);

            print(json_encode([
                "success" => true,
                "data" => $result
            ]));
        } else {
            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }

    function delete_termin()
    {
        $this->load->model("kontrak_model");
        $this->load->library("form_validation");

        $termin_id = $this->input->post("id", true);

        $this->form_validation->set_rules("id", "ID Termin", "required|integer");

        if ($this->form_validation->run()) {

            $this->kontrak_model->delete_termin($termin_id);

            print(json_encode([
                "success" => true,
                "message" => "Sukses menghapus termin"
            ]));
        } else {
            echo json_encode([
                "success" => false,
                "message" => validation_errors()
            ]);
        }
    }
}
