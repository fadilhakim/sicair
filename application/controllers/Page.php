<?php

    class Page extends CI_Controller {

        function __construct(){
            parent::__construct();
        }

        function unauthorized(){

            $data["title"] = "Unauthorized Page";

            $data["css"] = "template/pages/login/css"; // path
            $data["js"] = "template/pages/login/js"; // path

            $dt = array();
            $data["content"] = $this->load->view("template/pages/unauthorized/youhavenopowerhere",$dt,true);

            $this->load->view("template/authentication",$data);
		}
		
		function notfound(){
            $this->output->set_status_header('404'); 
            
            $data["title"] = "Page Not Found";

            $data["css"] = "template/pages/login/css"; // path
            $data["js"] = "template/pages/login/js"; // path

            $dt = array();
            $data["content"] = $this->load->view("template/pages/notfound/pagenotfound",$dt,true);

            $this->load->view("template/authentication",$data);
        }

    }
