<?php

class Printpdf extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        validSessionIsOut();
        $this->authorization->redirect_menu("dashboard");
        // $this->load->model("anggaran_model");
        // $this->load->model("kontrak_model");
        // $this->load->model("kegiatan_model"); 
    }

    function cetakKontrak()
    {
        validSessionIsOut();
        $this->load->library('dompdf_gen');
        $this->load->model("kontrak_model");


        $id_kontrak = $this->uri->segment(3);

        $str2 = "SELECT tr_kontrak.* , 
                m_anggaran.nomor_dipa , 
                m_anggaran.tanggal_dipa , 
                m_anggaran.kode_satker ,
                m_anggaran.nama_satker ,
                tr_kegiatan.*,
                tr_detail_kegiatan.*,
                m_users.*          
            FROM 
                m_anggaran  
            JOIN 
                tr_kegiatan ON m_anggaran.id = tr_kegiatan.id_anggaran
            JOIN 
                m_users ON tr_kegiatan.id_user = m_users.id
            JOIN 
                tr_detail_kegiatan ON tr_kegiatan.id = tr_detail_kegiatan.id_kegiatan
            JOIN 
                tr_kontrak ON tr_detail_kegiatan.id = tr_kontrak.id_detail_kegiatan 
            JOIN 
                tr_kontrak_termin ON tr_kontrak.id = tr_kontrak_termin.id_kontrak
            WHERE 
                tr_kontrak.id = '$id_kontrak'
        ";

        $q = $this->db->query($str2);
        $result = $q->row_array();
        $kontrakList =  $this->kontrak_model->get_termin_by_kontrak_id($id_kontrak);
        // echo '<pre>';
        // var_dump($kontrakList);
        // echo '</pre>';
        // exit();
        $data = array(
            'kontrak' => $result,
            'kontrakList' => $kontrakList
        );
        $this->load->view("template/pages/kontrak/resume", $data);
        $paper_size = 'A4';
        $orientation = 'potrait';
        $html =  $this->output->get_output();
        $this->dompdf->set_paper($paper_size, $orientation);

        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream('RESUME KONTRAK.pdf', array('Attachment' => 0));
    }

    function cetakSPTBKontrak()
    {
        validSessionIsOut();
        $this->load->library('dompdf_gen');
        $id_tagihan = $this->uri->segment(3);
        $str2 = "SELECT tr_tagihan.* , 
                    m_anggaran.nomor_dipa , 
                    m_anggaran.tanggal_dipa , 
                    m_anggaran.kode_satker ,
                    m_anggaran.nama_satker ,
                    tr_kegiatan.kode_aktivitas, tr_kegiatan.kro, tr_kegiatan.ro, tr_kegiatan.nama_kegiatan,
                    tr_detail_kegiatan.kode_mak,
                    tr_kontrak.nama_kontraktor,tr_kontrak.alamat_kontraktor,
                    m_users.fullname, m_users.id, m_users.nip
                    
            FROM 
                m_anggaran  
            JOIN 
                tr_kegiatan ON m_anggaran.id = tr_kegiatan.id_anggaran
            JOIN 
                tr_detail_kegiatan ON tr_kegiatan.id = tr_detail_kegiatan.id_kegiatan
            JOIN 
                tr_tagihan ON tr_detail_kegiatan.id = tr_tagihan.id_detail_kegiatan
            JOIN
                tr_kontrak ON tr_tagihan.id_kontrak = tr_kontrak.id
            JOIN 
                m_users ON tr_kontrak.id_user = m_users.id
            WHERE 
                tr_tagihan.id = '$id_tagihan'
        ";

        $q = $this->db->query($str2);
        $result = $q->row_array();

        $data = array(
            'sptb' => $result,
            'nomor' =>  $id_tagihan
        );

        // echo '<pre>';
        // var_dump($data);
        // echo '</pre>';
        // exit();
        $this->load->view("template/pages/tagihan/sptb", $data);
        $paper_size = 'A4';
        $orientation = 'potrait';
        $html =  $this->output->get_output();
        $this->dompdf->set_paper($paper_size, $orientation);

        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream('SPTB.pdf', array('Attachment' => 0));
    }

    function cetakSPTBNonKontrak()
    {
        validSessionIsOut();
        $this->load->library('dompdf_gen');
        $id_tagihan = $this->uri->segment(3);
        $str2 = "SELECT tr_tagihan.* , 
                    m_anggaran.nomor_dipa , 
                    m_anggaran.tanggal_dipa , 
                    m_anggaran.kode_satker ,
                    m_anggaran.nama_satker ,
                    tr_kegiatan.*,
                    tr_detail_kegiatan.*,
                    m_users.fullname, m_users.id, m_users.nip
                    
            FROM 
                m_anggaran  
            JOIN 
                tr_kegiatan ON m_anggaran.id = tr_kegiatan.id_anggaran
            JOIN 
                tr_detail_kegiatan ON tr_kegiatan.id = tr_detail_kegiatan.id_kegiatan
            JOIN 
                tr_tagihan ON tr_detail_kegiatan.id = tr_tagihan.id_detail_kegiatan
            JOIN 
                m_users ON tr_tagihan.id_user = m_users.id
            WHERE 
                tr_tagihan.id = '$id_tagihan'
        ";

        $q = $this->db->query($str2);
        $result = $q->row_array();

        $data = array(
            'sptb' => $result,
            'nomor' =>  $id_tagihan
        );

        // echo '<pre>';
        // var_dump($data);
        // echo '</pre>';
        // exit();
        $this->load->view("template/pages/tagihan/sptb", $data);
        $paper_size = 'A4';
        $orientation = 'potrait';
        $html =  $this->output->get_output();
        $this->dompdf->set_paper($paper_size, $orientation);

        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream('SPTB.pdf', array('Attachment' => 0));
    }
}
