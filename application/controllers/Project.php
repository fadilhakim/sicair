<?php

    class Project extends CI_Controller {
        function __construct(){
          parent::__construct();
          validSessionIsOut();
          $this->authorization->redirect_menu("project_update");

          $this->db2 = $this->load->database("db2", true);
          $this->load->model("projects_model");
		  $this->load->model("general_model");

        }

    }
