<?php

class Kegiatan_model extends CI_model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function count_kegiatan()
    {
        $query = $this->db->query('SELECT * FROM tr_kegiatan');
        return $query->num_rows();
    }

    function count_detail_kegiatan()
    {
        $query = $this->db->query('SELECT * FROM tr_detail_kegiatan');
        return $query->num_rows();
    }

    function count_anggaran()
    {
        $query = $this->db->query('SELECT * FROM tr_user_login_records');
        return $query->num_rows();
    }

    function count_tagihan()
    {
        $query = $this->db->query('SELECT * FROM tr_user_login_records');
        return $query->num_rows();
    }

    function get_kegiatan($id)
    {

        $this->db->select('*');
        $this->db->from('tr_kegiatan');
        $this->db->where("id", $id);
        $query = $this->db->get();
        return $query->row_array();
    }

    function list_kegiatan()
    {
        $this->db->select('*');
        $this->db->from('tr_kegiatan');
        $query = $this->db->get();
        return $query->result_array();
    }

    function list_detail_kegiatan_by_id_kegiatan($id_kegiatan)
    {
        $this->db->select('*');
        $this->db->from('tr_detail_kegiatan');
        $this->db->where('id_kegiatan', $id_kegiatan);
        $query = $this->db->get();
        return $query->result_array();
    }



    function list_mak()
    {
        $this->db->select('*');
        $this->db->from('m_mak');
        $query = $this->db->get();
        return $query->result_array();
    }

    function getMainKegiatan($params)
    {
        error_reporting(0);

        $limit          = isset($params["limit"]) ? $params["limit"] : 10;
        $offset         = isset($params["offset"]) ? $params["offset"] : 0;
        $order          = isset($params["order"]) ? $params["order"] : null;
        $col            = isset($params["col"]) ? $params["col"] : null;
        $dir            = isset($params["dir"]) ? $params["dir"] : null;
        $search         = isset($params["search"]) ? $params["search"] : "";
        $valid_columns  = array(
            0 => 'nama_kegiatan',
            1 => 'id_user',
            2 => 'prioritas'
        );

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }

        // $this->db->select("
        //     tr_detail_kegiatan.id as detail_kegiatan_id,

        // ");
        $this->db->select('*');
        $this->db->order_by('created_at', 'DESC');
        $result = $this->db;

        if ($order != null) {
            $result->order_by($order, $dir);
        }

        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $result->like($sterm, $search);
                } else {
                    $result->or_like($sterm, $search);
                }
                $x++;
            }
        }


        if ($params["filter_nama_ppk"]) {
            $result->where("id_user", $params["filter_nama_ppk"]);
        }
        if ($params["filter_anggaran_id"]) {
            $result->where("id_anggaran", $params["filter_anggaran_id"]);
        }
        if ($params["filter_prioritas"]) {
            $result->where("prioritas", $params["filter_prioritas"]);
        }

        if ($params["count"]) {
            $result = $result->get(DB_DATABASE . ".tr_kegiatan");
            $result = $result->num_rows();
        } else {
            if (!empty($limit) || !empty($offset)) {

                $result = $result->get(DB_DATABASE . ".tr_kegiatan", $params["limit"], $params["offset"]);
            } else {

                $result = $result->get(DB_DATABASE . ".tr_kegiatan");
            }
            $result = $result->result_array();
        }

        return $result;
    }

    function getMainKegiatan_by_ppk($params, $userID)
    {
        error_reporting(0);

        $limit          = isset($params["limit"]) ? $params["limit"] : 10;
        $offset         = isset($params["offset"]) ? $params["offset"] : 0;
        $order          = isset($params["order"]) ? $params["order"] : null;
        $col            = isset($params["col"]) ? $params["col"] : null;
        $dir            = isset($params["dir"]) ? $params["dir"] : null;
        $search         = isset($params["search"]) ? $params["search"] : "";
        $valid_columns  = array(
            0 => 'nama_kegiatan',
            1 => 'id_user',
            2 => 'prioritas'
        );

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }

        // $this->db->select("
        //     tr_detail_kegiatan.id as detail_kegiatan_id,

        // ");
        $this->db->select('*');
        $this->db->order_by('created_at', 'DESC');
        $result = $this->db;

        if ($order != null) {
            $result->order_by($order, $dir);
        }

        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $result->like($sterm, $search);
                } else {
                    $result->or_like($sterm, $search);
                }
                $x++;
            }
        }


        if ($params["filter_nama_ppk"]) {
            $result->where("id_user", $params["filter_nama_ppk"]);
        }
        if ($params["filter_anggaran_id"]) {
            $result->where("id_anggaran", $params["filter_anggaran_id"]);
        }
        if ($params["filter_prioritas"]) {
            $result->where("prioritas", $params["filter_prioritas"]);
        }
        $result->where("id_user", $userID);

        if ($params["count"]) {
            $result = $result->get(DB_DATABASE . ".tr_kegiatan");
            $result = $result->num_rows();
        } else {
            if (!empty($limit) || !empty($offset)) {

                $result = $result->get(DB_DATABASE . ".tr_kegiatan", $params["limit"], $params["offset"]);
            } else {

                $result = $result->get(DB_DATABASE . ".tr_kegiatan");
            }
            $result = $result->result_array();
        }

        return $result;
    }

    function get_detail_kegiatan($params, $id_kegiatan)
    {
        error_reporting(0);

        $limit          = isset($params["limit"]) ? $params["limit"] : 0;
        $offset         = isset($params["offset"]) ? $params["offset"] : 0;
        $order          = isset($params["order"]) ? $params["order"] : null;
        $col            = isset($params["col"]) ? $params["col"] : null;
        $dir            = isset($params["dir"]) ? $params["dir"] : null;
        $search         = isset($params["search"]) ? $params["search"] : "";
        $valid_columns  = array(
            0 => 'detail_kegiatan',
            1 => 'pagu_kegiatan',
            2 => 'kode_mak',
            3 => 'volume'
        );

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }

        $this->db->select("*");
        $this->db->where('id_kegiatan', $id_kegiatan);
        $this->db->order_by('created_at', 'DESC');
        $result = $this->db;

        if ($order != null) {
            $result->order_by($order, $dir);
        }

        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $result->like($sterm, $search);
                } else {
                    $result->or_like($sterm, $search);
                }
                $x++;
            }
        }

        if ($params["count"]) {
            $result = $result->get(DB_DATABASE . ".tr_detail_kegiatan");
            $result = $result->num_rows();
        } else {
            if (!empty($limit) || !empty($offset)) {

                $result = $result->get(DB_DATABASE . ".tr_detail_kegiatan", $params["limit"], $params["offset"]);
            } else {

                $result = $result->get(DB_DATABASE . ".tr_detail_kegiatan");
            }
            $result = $result->result_array();
        }

        return $result;

        // $this->db->select("nomor_dipa,tanggal_dipa,pagu_anggaran,tahun_anggaran,kode_satker,nama_satker");
        // $this->db->from('m_anggaran');
        // $query = $this->db->get();
        // return $query->result_array();
    }

    function edit_kegiatan($id_kegiatan, $data)
    {
        error_reporting(0);

        if (isset($data["kode_aktivitas"])) {
            $newdata["kode_aktivitas"] = $data["kode_aktivitas"];
        }
        if (isset($data["kro"])) {
            $newdata["kro"] = $data["kro"];
        }
        if (isset($data["ro"])) {
            $newdata["ro"]  = $data["ro"];
        }
        if (isset($data["nama_kegiatan"])) {
            $newdata["nama_kegiatan"] = $data["nama_kegiatan"];
        }
        if (isset($data["pagu_kegiatan"])) {
            $newdata["pagu_kegiatan"] = $data["pagu_kegiatan"];
        }
        if (isset($data["id_user"])) {
            $newdata["id_user"] = $data["id_user"];
        }
        if (isset($data["komponen"])) {
            $newdata["komponen"] = $data["komponen"];
        }
        if (isset($data["subkomponen"])) {
            $newdata["subkomponen"] = $data["subkomponen"];
        }

        $this->db->where('id', $id_kegiatan);
        return $this->db->update("tr_kegiatan", $newdata);
    }

    function list_detail_kegiatan($params = [])
    {

        $this->db->select("*");
        $this->db->from("tr_detail_kegiatan");
        $this->db->order_by("created_at", 'DESC');

        if ($params["id_kegiatan"]) {
            $this->db->where("id_kegiatan", $params["id_kegiatan"]);
        }

        return $this->db->get()->result_array();
    }

    function detail_detail_kegiatan($id_detail_kegiatan)
    {
        $this->db->select("*");
        $this->db->from("tr_detail_kegiatan");
        $this->db->order_by("created_at", 'DESC');
        $this->db->where("id", $id_detail_kegiatan);
        return $this->db->get()->row_array();
    }

    function detail_kegiatan_by_id_kegiatan($id_kegiatan)
    {
        $this->db->select("*");
        $this->db->from("tr_detail_kegiatan");
        $this->db->order_by("created_at", 'DESC');
        $this->db->where("id_kegiatan", $id_kegiatan);
        return $this->db->get()->row_array();
    }
    function detail_detail_kegiatan_by_mak($kode_mak)
    {
        $this->db->select("*");
        $this->db->from("tr_detail_kegiatan");
        $this->db->order_by("created_at", 'DESC');
        $this->db->where("kode_mak", $kode_mak);
        return $this->db->get()->row_array();
    }

    function get_uraian_by_mak($kode_mak)
    {
        $this->db->select("*");
        $this->db->from("m_mak");
        $this->db->where("kode_mak", $kode_mak);
        return $this->db->get()->row_array();
    }

    function delete_kegiatan($kegiatan_id)
    {

        $this->db->where("id", $kegiatan_id);
        return $this->db->delete("tr_kegiatan");
    }

    function tambah_detail_kegiatan($data)
    {
        error_reporting(0);
        $newdata = array(
            "id_kegiatan"    => $data['id_kegiatan'],
            "kode_mak"       => $data['kode_mak'],
            "uraian_mak"     => $data['uraian_mak'],
            "pagu_kegiatan"  => $data['pagu_detail_kegiatan'],
            "detail_kegiatan"  => $data['detail_kegiatan'],
            "volume"           => $data['volume'],
            "id_user"          => $data['id_user'],
        );

        return $this->db->insert("tr_detail_kegiatan", $newdata);
    }

    function update_detail_kegiatan($id_detail_kegiatan, $data)
    {
        error_reporting(0);

        if (isset($data["kode_mak"])) {
            $newdata["kode_mak"] = $data["kode_mak"];
        }
        if (isset($data["uraian_mak"])) {
            $newdata["uraian_mak"] = $data["uraian_mak"];
        }
        if (isset($data["detail_kegiatan"])) {
            $newdata["detail_kegiatan"]  = $data["detail_kegiatan"];
        }
        if (isset($data["pagu_detail_kegiatan"])) {
            $newdata["pagu_kegiatan"] = $data["pagu_detail_kegiatan"];
        }
        if (isset($data["volume"])) {
            $newdata["volume"] = $data["volume"];
        }
        if (isset($data["id_kegiatan"])) {
            $newdata["id_kegiatan"] = $data["id_kegiatan"];
        }

        $this->db->where('id', $id_detail_kegiatan);
        return $this->db->update("tr_detail_kegiatan", $newdata);
    }

    function delete_detail_kegiatan($detail_kegiatan_id)
    {
        $this->db->where("id", $detail_kegiatan_id);
        return $this->db->delete("tr_detail_kegiatan");
    }
    function get_all_data_kegiatan($params = [])
    {

        error_reporting(0);
        $qry = "SELECT * FROM tr_kegiatan ";

        //INI DARI HEADER
        if (isset($params["search"])) {
            $qry .= "WHERE kode_aktivitas LIKE '%$params[search]%' OR ";
            $qry .= "nama_kegiatan LIKE '%$params[search]%' ";
            $qry .= "LIMIT 0,10 ";
        }
        $result = $this->db->query($qry);
        return $result = $result->result_array();
    }

    function get_kegiatan_by_id($id)
    {
        $this->db->select("*");
        $this->db->from("tr_kegiatan");
        $this->db->where("id", $id);
        return $this->db->get()->row_array();
    }
}
