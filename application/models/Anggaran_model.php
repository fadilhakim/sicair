<?php

class Anggaran_model extends CI_model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function get_all_anggaran($params)
    {
        error_reporting(0);

        $limit          = isset($params["limit"]) ? $params["limit"] : 0;
        $offset         = isset($params["offset"]) ? $params["offset"] : 0;
        $order          = isset($params["order"]) ? $params["order"] : null;
        $col            = isset($params["col"]) ? $params["col"] : null;
        $dir            = isset($params["dir"]) ? $params["dir"] : null;
        $search         = isset($params["search"]) ? $params["search"] : "";
        $valid_columns  = array(
            0 => 'id',
            2 => 'nomor_dipa',
            3 => 'tanggal_dipa',
            4 => 'pagu_anggaran',
            5 => 'tahun_anggaran',
            6 => 'kode_satker',
            7 => "nama_satker",

        );

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }

        $this->db->select("*");
        $result = $this->db;

        if ($order != null) {
            $result->order_by($order, $dir);
        }

        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $result->like($sterm, $search);
                } else {
                    $result->or_like($sterm, $search);
                }
                $x++;
            }
        }

        // ==================== TAMBAHAN WHERE ======================

        if ($params["tahun_anggaran"]) {
            $result->where("status_progress", $params["progress_status"]);
        }

        if ($params["count"]) {
            $result = $result->get(DB_DATABASE . ".m_anggaran");
            $result = $result->num_rows();
        } else {
            if (!empty($limit) && !empty($offset)) {
                $result = $result->get(DB_DATABASE . ".m_anggaran", $params["limit"], $params["offset"]);
            } else {
                $result = $result->get(DB_DATABASE . ".m_anggaran");
            }
            $result = $result->result_array();
        }

        return $result;

        // $this->db->select("nomor_dipa,tanggal_dipa,pagu_anggaran,tahun_anggaran,kode_satker,nama_satker");
        // $this->db->from('m_anggaran');
        // $query = $this->db->get();
        // return $query->result_array();
    }

    function count_anggaran($year)
    {
        $query = $this->db->query("SELECT SUM(pagu_anggaran) as totalPagu from m_anggaran where tahun_anggaran = $year");
        return $query->row_array();
    }

    function count_tagihan()
    {
        $query = $this->db->query('SELECT pagu_anggaran FROM m_anggaran where tahun_anggaran = 2020 ');
        return $query->num_rows();
    }

    function tambah_anggaran($data)
    {

        error_reporting(0);
        $newdata = array(
            'nomor_dipa'    => $data["nomor_dipa"],
            'tanggal_dipa'  => $data["tanggal_dipa"],
            'pagu_anggaran' => $data["pagu_anggaran"],
            'tahun_anggaran' => $data["tahun_anggaran"],
            'kode_satker'   => $data["kode_satker"],
            'nama_satker'   => $data["nama_satker"],
            'created_at'    => $data["created_at"]
        );

        return $this->db->insert("m_anggaran", $newdata);
        // exit();
    }

    function get_anggaran_detail($anggaran_id)
    {
        $this->db->select('*');
        $this->db->from('m_anggaran');
        $this->db->where("id", $anggaran_id);
        $query = $this->db->get();
        return $query->row_array();
    }

    function get_anggaran_list()
    {
        $this->db->select('*');
        $this->db->from('m_anggaran');

        $query = $this->db->get();
        return $query->result_array();
    }

    function tambah_kegiatan($data)
    {

        error_reporting(0);
        $newdata = array(
            "kode_aktivitas"    => $data['kode_aktivitas'],
            "kro"               => $data['kro'],
            "ro"                => $data['ro'],
            "komponen"          => $data['komponen'],
            "subkomponen"       => $data['subkomponen'],
            "pagu_kegiatan"     => $data['pagu_kegiatan'],
            "id_user"           => $data['id_user'],
            "nama_kegiatan"     => $data['nama_kegiatan'],
            "prioritas"         => $data['prioritas'],
            "id_anggaran"       => $data['anggaran_id']
        );

        return $this->db->insert("tr_kegiatan", $newdata);
    }

    function get_all_kegiatan($params, $id_anggaran)
    {
        error_reporting(0);

        $limit          = isset($params["limit"]) ? $params["limit"] : 0;
        $offset         = isset($params["offset"]) ? $params["offset"] : 0;
        $order          = isset($params["order"]) ? $params["order"] : null;
        $col            = isset($params["col"]) ? $params["col"] : null;
        $dir            = isset($params["dir"]) ? $params["dir"] : null;
        $search         = isset($params["search"]) ? $params["search"] : "";
        $valid_columns  = array(
            0 => 'kode_aktivitas',
            1 => 'komponen',
            2 => 'nama_kegiatan',
            3 => 'pagu_kegiatan',
            4 => 'id_user',
            5 => 'prioritas'

        );

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }

        $this->db->select("*");
        $this->db->where('id_anggaran', $id_anggaran);
        $result = $this->db;

        if ($order != null) {
            $result->order_by($order, $dir);
        }

        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $result->like($sterm, $search);
                } else {
                    $result->or_like($sterm, $search);
                }
                $x++;
            }
        }

        // ==================== TAMBAHAN WHERE ======================

        if ($params["nama_ppk"]) {
            $result->where("id_user", $params["nama_ppk"]);
        }



        if ($params["count"]) {
            $result = $result->get(DB_DATABASE . ".tr_kegiatan");
            $result = $result->num_rows();
        } else {
            if (!empty($limit) && !empty($offset)) {

                $result = $result->get(DB_DATABASE . ".tr_kegiatan", $params["limit"], $params["offset"]);
            } else {

                $result = $result->get(DB_DATABASE . ".tr_kegiatan");
            }
            $result = $result->result_array();
        }

        return $result;

        // $this->db->select("nomor_dipa,tanggal_dipa,pagu_anggaran,tahun_anggaran,kode_satker,nama_satker");
        // $this->db->from('m_anggaran');
        // $query = $this->db->get();
        // return $query->result_array();
    }

    function getMainKegiatan($params, $id_anggaran)
    {
        error_reporting(0);

        $limit          = isset($params["limit"]) ? $params["limit"] : 10;
        $offset         = isset($params["offset"]) ? $params["offset"] : 0;
        $order          = isset($params["order"]) ? $params["order"] : null;
        $col            = isset($params["col"]) ? $params["col"] : null;
        $dir            = isset($params["dir"]) ? $params["dir"] : null;
        $search         = isset($params["search"]) ? $params["search"] : "";
        $valid_columns  = array(
            0 => 'nama_kegiatan',
            1 => 'id_user',
            2 => 'prioritas'
        );

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }

        // $this->db->select("
        //     tr_detail_kegiatan.id as detail_kegiatan_id,

        // ");
        $this->db->select('*');
        $this->db->where('id_anggaran', $id_anggaran);
        $this->db->order_by('created_at', 'DESC');
        $result = $this->db;

        if ($order != null) {
            $result->order_by($order, $dir);
        }

        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $result->like($sterm, $search);
                } else {
                    $result->or_like($sterm, $search);
                }
                $x++;
            }
        }


        if ($params["filter_nama_ppk"]) {
            $result->where("id_user", $params["filter_nama_ppk"]);
        }
        if ($params["filter_anggaran_id"]) {
            $result->where("id_anggaran", $params["filter_anggaran_id"]);
        }
        if ($params["filter_prioritas"]) {
            $result->where("prioritas", $params["filter_prioritas"]);
        }

        if ($params["count"]) {
            $result = $result->get(DB_DATABASE . ".tr_kegiatan");
            $result = $result->num_rows();
        } else {
            if (!empty($limit) || !empty($offset)) {

                $result = $result->get(DB_DATABASE . ".tr_kegiatan", $params["limit"], $params["offset"]);
            } else {

                $result = $result->get(DB_DATABASE . ".tr_kegiatan");
            }
            $result = $result->result_array();
        }

        return $result;
    }

    function getMainKegiatan_by_ppk($params, $id_anggaran, $userID)
    {
        error_reporting(0);

        $limit          = isset($params["limit"]) ? $params["limit"] : 10;
        $offset         = isset($params["offset"]) ? $params["offset"] : 0;
        $order          = isset($params["order"]) ? $params["order"] : null;
        $col            = isset($params["col"]) ? $params["col"] : null;
        $dir            = isset($params["dir"]) ? $params["dir"] : null;
        $search         = isset($params["search"]) ? $params["search"] : "";
        $valid_columns  = array(
            0 => 'nama_kegiatan',
            1 => 'id_user',
            2 => 'prioritas'
        );

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }

        // $this->db->select("
        //     tr_detail_kegiatan.id as detail_kegiatan_id,

        // ");
        $this->db->select('*');
        $this->db->where('id_anggaran', $id_anggaran);
        $this->db->order_by('created_at', 'DESC');
        $result = $this->db;

        if ($order != null) {
            $result->order_by($order, $dir);
        }

        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $result->like($sterm, $search);
                } else {
                    $result->or_like($sterm, $search);
                }
                $x++;
            }
        }


        if ($params["filter_nama_ppk"]) {
            $result->where("id_user", $params["filter_nama_ppk"]);
        }
        if ($params["filter_anggaran_id"]) {
            $result->where("id_anggaran", $params["filter_anggaran_id"]);
        }
        if ($params["filter_prioritas"]) {
            $result->where("prioritas", $params["filter_prioritas"]);
        }
        $result->where("id_user", $userID);

        if ($params["count"]) {
            $result = $result->get(DB_DATABASE . ".tr_kegiatan");
            $result = $result->num_rows();
        } else {
            if (!empty($limit) || !empty($offset)) {

                $result = $result->get(DB_DATABASE . ".tr_kegiatan", $params["limit"], $params["offset"]);
            } else {

                $result = $result->get(DB_DATABASE . ".tr_kegiatan");
            }
            $result = $result->result_array();
        }

        return $result;
    }

    function get_kegiatan_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tr_kegiatan');
        $this->db->where("id", $id);
        $query = $this->db->get();
        return $query->row_array();
    }

    function update_kegiatan($id_kegiatan, $data)
    {
        error_reporting(0);

        if (isset($data["kode_aktivitas"])) {
            $newdata["kode_aktivitas"] = $data["kode_aktivitas"];
        }
        if (isset($data["kro"])) {
            $newdata["kro"] = $data["kro"];
        }
        if (isset($data["ro"])) {
            $newdata["ro"]  = $data["ro"];
        }
        if (isset($data["nama_kegiatan"])) {
            $newdata["nama_kegiatan"] = $data["nama_kegiatan"];
        }
        if (isset($data["detail_kegiatan"])) {
            $newdata["detail_kegiatan"] = $data["detail_kegiatan"];
        }
        if (isset($data["prioritas"])) {
            $newdata["prioritas"] = $data["prioritas"];
        }
        if (isset($data["id_user"])) {
            $newdata["id_user"] = $data["id_user"];
        }
        if (isset($data["pagu_kegiatan"])) {
            $newdata["pagu_kegiatan"] = $data["pagu_kegiatan"];
        }
        if (isset($data["komponen"])) {
            $newdata["komponen"] = $data["komponen"];
        }
        if (isset($data["subkomponen"])) {
            $newdata["subkomponen"] = $data["subkomponen"];
        }
        if (isset($data["kode_mak"])) {
            $newdata["kode_mak"] = $data["kode_mak"];
        }

        $this->db->where('id', $id_kegiatan);

        return $this->db->update("tr_kegiatan", $newdata);
    }

    function edit_anggaran($id_anggaran, $data)
    {
        error_reporting(0);

        if (isset($data["nomor_dipa"])) {
            $newdata["nomor_dipa"] = $data["nomor_dipa"];
        }
        if (isset($data["tanggal_dipa"])) {
            $newdata["tanggal_dipa"] = $data["tanggal_dipa"];
        }
        if (isset($data["pagu_anggaran"])) {
            $newdata["pagu_anggaran"]  = $data["pagu_anggaran"];
        }
        if (isset($data["tahun_anggaran"])) {
            $newdata["tahun_anggaran"] = $data["tahun_anggaran"];
        }
        if (isset($data["kode_satker"])) {
            $newdata["kode_satker"] = $data["kode_satker"];
        }
        if (isset($data["nama_satker"])) {
            $newdata["nama_satker"] = $data["nama_satker"];
        }

        $this->db->where('id', $id_anggaran);
        return $this->db->update("m_anggaran", $newdata);
    }

    function delete_anggaran($anggaran_id)
    {

        $this->db->where("id", $anggaran_id);
        return $this->db->delete("m_anggaran");
    }
}
