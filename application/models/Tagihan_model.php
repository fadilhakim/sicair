<?php

class Tagihan_model extends CI_Model
{

    function get_all_tagihan($params)
    {

        error_reporting(0);

        $limit          = isset($params["limit"]) ? $params["limit"] : 10;
        $offset         = isset($params["offset"]) ? $params["offset"] : 0;
        $order          = isset($params["order"]) ? $params["order"] : null;
        $col            = isset($params["col"]) ? $params["col"] : null;
        $dir            = isset($params["dir"]) ? $params["dir"] : null;
        $search         = isset($params["search"]) ? $params["search"] : "";

        $valid_columns  = array(
            0 => 'tr_tagihan.id',
            1 => 'nomor_tagihan',

            2 => 'created_at',
            3 => 'status_tagihan',
            4 => 'nilai_tagihan',
            5 => "uraian_tagihan"
        );

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }


        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }

        $this->db->select("tr_tagihan.*"); // tak boleh semua
        //$this->db->select("tr_detail_kegiatan.id as tr_detail_kegiatan_id, tr_tagihan.*");
        //$this->db->join(DB_DATABASE .'.tr_detail_kegiatan', 'tr_detail_kegiatan.id = tr_tagihan.id_detail_kegiatan'); 
        $this->db->order_by('created_at', 'DESC');
        $result = $this->db;

        if ($order != null) {
            $result->order_by($order, $dir);
        }

        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $result->like($sterm, $search);
                } else {
                    $result->or_like($sterm, $search);
                }
                $x++;
            }
        }

        // ==================== TAMBAHAN WHERE ======================

        if ($params["filter_status_tagihan"]) {
            $result->where("status_tagihan", $params["filter_status_tagihan"]);
        }

        if ($params["filter_jenis_tagihan"]) {
            $result->where("jenis_tagihan", $params["filter_jenis_tagihan"]);
        }

        if ($params["filter_subdit"]) {
            $result->where("subdit", $params["filter_subdit"]);
        }


        if ($params["filter_tanggal_tagihan"]) {
            $result->where("created_at >= '$params[filter_tanggal_tagihan]'", null, false);
        }
        if ($params["filter_nama_ppk"]) {
            $result->where("id_user", $params["filter_nama_ppk"]);
        }

        if ($params["count"]) {
            $result = $result->get(DB_DATABASE . ".tr_tagihan");
            $result = $result->num_rows();
        } else {
            if (!empty($limit) || !empty($offset)) {

                $result = $result->get(DB_DATABASE . ".tr_tagihan", $params["limit"], $params["offset"]);
            } else {

                $result = $result->get(DB_DATABASE . ".tr_tagihan");
            }
            $result = $result->result_array();
        }

        return $result;
    }

    function get_all_tagihan_by_ppk($params, $userID)
    {

        error_reporting(0);

        $limit          = isset($params["limit"]) ? $params["limit"] : 10;
        $offset         = isset($params["offset"]) ? $params["offset"] : 0;
        $order          = isset($params["order"]) ? $params["order"] : null;
        $col            = isset($params["col"]) ? $params["col"] : null;
        $dir            = isset($params["dir"]) ? $params["dir"] : null;
        $search         = isset($params["search"]) ? $params["search"] : "";

        $valid_columns  = array(
            0 => 'tr_tagihan.id',
            1 => 'nomor_tagihan',

            2 => 'created_at',
            3 => 'status_tagihan',
            4 => 'nilai_tagihan',
            5 => "uraian_tagihan"
        );

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }


        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }

        $this->db->select("tr_tagihan.*"); // tak boleh semua
        //$this->db->select("tr_detail_kegiatan.id as tr_detail_kegiatan_id, tr_tagihan.*");
        //$this->db->join(DB_DATABASE .'.tr_detail_kegiatan', 'tr_detail_kegiatan.id = tr_tagihan.id_detail_kegiatan'); 
        $this->db->order_by('created_at', 'DESC');
        $result = $this->db;

        if ($order != null) {
            $result->order_by($order, $dir);
        }

        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $result->like($sterm, $search);
                } else {
                    $result->or_like($sterm, $search);
                }
                $x++;
            }
        }

        // ==================== TAMBAHAN WHERE ======================

        if ($params["filter_status_tagihan"]) {
            $result->where("status_tagihan", $params["filter_status_tagihan"]);
        }

        if ($params["filter_jenis_tagihan"]) {
            $result->where("jenis_tagihan", $params["filter_jenis_tagihan"]);
        }

        if ($params["filter_subdit"]) {
            $result->where("subdit", $params["filter_subdit"]);
        }


        if ($params["filter_tanggal_tagihan"]) {
            $result->where("created_at >= '$params[filter_tanggal_tagihan]'", null, false);
        }
        if ($params["filter_nama_ppk"]) {
            $result->where("id_user", $params["filter_nama_ppk"]);
        }
        $result->where("id_user", $userID);
        if ($params["count"]) {
            $result = $result->get(DB_DATABASE . ".tr_tagihan");
            $result = $result->num_rows();
        } else {
            if (!empty($limit) || !empty($offset)) {

                $result = $result->get(DB_DATABASE . ".tr_tagihan", $params["limit"], $params["offset"]);
            } else {

                $result = $result->get(DB_DATABASE . ".tr_tagihan");
            }
            $result = $result->result_array();
        }

        return $result;
    }

    function tagihan_detail($id_tagihan)
    {

        $this->db->select("*");
        // $this->db->from("tr_tagihan");
        $this->db->where("id", $id_tagihan);
        return $this->db->get("tr_tagihan")->row_array();
    }

    function add_tagihan($data)
    {

        return $this->db->insert("tr_tagihan", $data);
    }

    function update_tagihan($data)
    {
        //print_r($data);
        // exit;

        $this->db->where('id', $data["id"]);
        return $this->db->update("tr_tagihan", $data);
    }

    function delete_tagihan($tagihan_id)
    {

        $this->db->where('id', $tagihan_id);
        return $this->db->delete("tr_tagihan");
    }

    function all_tagihan_count()
    {
        $query = $this->db->query('SELECT SUM(nilai_tagihan) as TotalTagihan from tr_tagihan');
        return $query->row_array();
    }

    function all_tagihan_verified_count()
    {
        $query = $this->db->query('SELECT SUM(nilai_tagihan) as TotalTagihan from tr_tagihan where status_tagihan = "verifikasi bendahara" OR status_tagihan = "terbit spm"');
        return $query->row_array();
    }

    function add_lampiran($data)
    {
        return $this->db->insert("tr_detail_lampiran", $data);
    }

    function update_lampiran($data)
    {
        $this->db->where("id", $data["id"]);
        return $this->db->update("tr_detail_lampiran", $data);
    }

    function lampiran_list($id_tagihan)
    {
        $this->db->where('id_tagihan', $id_tagihan);
        return $this->db->get("tr_detail_lampiran")->result_array();
    }

    function detail_lampiran($lampiran_id)
    {
        $this->db->where('id', $lampiran_id);
        return $this->db->get("tr_detail_lampiran")->row_array();
    }

    function get_all_lampiran($params)
    {

        error_reporting(0);

        $limit          = isset($params["limit"]) ? $params["limit"] : 0;
        $offset         = isset($params["offset"]) ? $params["offset"] : 0;
        $order          = isset($params["order"]) ? $params["order"] : null;
        $col            = isset($params["col"]) ? $params["col"] : null;
        $dir            = isset($params["dir"]) ? $params["dir"] : null;
        $search         = isset($params["search"]) ? $params["search"] : "";

        $valid_columns  = array(
            0 => 'id',
            1 => "id_tagihan",
            2 => 'file_name',
            3 => 'file',

        );

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        }

        if (!isset($valid_columns[$col])) {
            $order = "file_name";
        } else {
            $order = $valid_columns[$col];
        }

        $this->db->select("*"); // tak boleh semua
        //$this->db->select("tr_detail_kegiatan.id as tr_detail_kegiatan_id, tr_tagihan.*");
        //$this->db->join(DB_DATABASE .'.tr_detail_kegiatan', 'tr_detail_kegiatan.id = tr_tagihan.id_detail_kegiatan'); 
        $result = $this->db;

        if ($order != null) {
            $result->order_by($order, $dir);
        }

        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $result->like($sterm, $search);
                } else {
                    $result->or_like($sterm, $search);
                }
                $x++;
            }
        }

        // ==================== TAMBAHAN WHERE ======================

        $result->where("id_tagihan", $params["id_tagihan"]);

        if ($params["count"]) {
            $result = $result->get(DB_DATABASE . ".tr_detail_lampiran");
            $result = $result->num_rows();
        } else {
            if (!empty($limit) && !empty($offset)) {

                $result = $result->get(DB_DATABASE . ".tr_detail_lampiran", $params["limit"], $params["offset"]);
            } else {

                $result = $result->get(DB_DATABASE . ".tr_detail_lampiran");
            }
            $result = $result->result_array();
        }

        return $result;
    }

    function delete_lampiran($lampiran_id)
    {
        $this->db->where('id', $lampiran_id);
        return $this->db->delete("tr_detail_lampiran");
    }
}
