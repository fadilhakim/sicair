<?php 

    class Layanan_model extends CI_model {

        function __construct() {
            parent::__construct();

            $this->db2 = $this->load->database("db2",true);
        }

        function get_layanan() {
            $q = $this->db2->query("SELECT * FROM  ".DB_DATABASE2.".m_layanan ");
            return $q->result_array();
        }

        function get_layanan_join() {
            $str  = "SELECT 
                l.id,
                kl.id as h_kontrak_layanan_id,
                kl.m_layanan_id,
                l.nama 
            FROM 
                ".DB_DATABASE2.".m_layanan l 
            JOIN 
                ".DB_DATABASE2.".h_kontrak_layanan kl ON l.id = kl.m_layanan_id  

            GROUP BY kl.m_layanan_id";
          
            $q = $this->db2->query($str);
            return $q->result_array();
        }

        function get_detail_layanan($layanan_id) {
        
            $q = $this->db2->query("SELECT * FROM  ".DB_DATABASE2.".m_layanan WHERE id = '$layanan_id' ");
            return $q->row_array();
        }

    }