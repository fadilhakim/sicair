<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Auth_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database();
    }

    function login($email, $password)
    {

        $result = $this->db
            ->where("email", $email)
            ->where("password", $password)
            ->where("status_id", 1) // status active
            ->get('m_users')
            ->row_array();

        // print_r($result);
        // exit();
        return $result;
    }
}
