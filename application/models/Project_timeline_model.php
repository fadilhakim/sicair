<?php

    class Project_timeline_model extends CI_Model {

        function get_timeline_nodelink($project_update_location_id) {

            // $sql = "SELECT * FROM (SELECT *,MAX(created_at) FROM tr_project_status_timeline
            //     WHERE project_update_location_id = $project_update_location_id
            //     ORDER BY created_at) as timeline_tbl";
            // $this->db->where();
            // $this->db->order_by("created_at", "DESC");
            //$this->db->group_by("status");


            // $sql = "SELECT * FROM  ";

            $this->db->select("a.*");
            $this->db->select("b.status_name");
            $this->db->where("project_update_location_id",$project_update_location_id);
            $this->db->from(DB_DATABASE.".tr_project_status_timeline a");
            $this->db->join(DB_DATABASE.".m_nodelink_status b","a.status = b.id");
            //$this->db->join(DB_DATABASE.".tr_project_update_location b","b.project_update_location_id = b.id");
            // $this->db->join(DB_DATABASE.".tr_project_update c","b.project_update_id = c.id");
            // $this->db->group_by("status");
            // $this->db->order_by("status");


            //$q = $this->db->query($sql);
            // print $this->db->last_query();

            $result = $this->db->get();

            return $result->result_array();
            //return $q->result_array();
        }

        function get_last_timeline_nodelink($project_update_location_id) {

            $this->db->select("*");
            $this->db->from(DB_DATABASE.".tr_project_status_timeline");
            $this->db->where("project_update_location_id",$project_update_location_id);
            $this->db->limit(1,0);
            $this->db->order_by("created_at", "DESC");

            $result = $this->db->get();

            // print $this->db->last_query();

            return $result->row_array();
        }

        function insert_timeline($data) {
            $data = [
                "project_update_location_id" => $data["project_update_location_id"],
                "status"                     => $data["status"],
                "status_date"                => $data["status_date"]." ".$data["status_time"],
                "keterangan"                 => $data["keterangan"],
                "created_at"                 => date("Y-m-d H:i:s")
            ];

            $this->db->insert("tr_project_status_timeline",$data);
        }

    }
