<?php

    class Test_model extends CI_Model {
        function __construct(){
            parent::__construct();

            $this->db2 = $this->load->database("db2", true);
        }

        function get_all_tests(){

            $result = $this->db->get("test")->result_array();

            return $result;

        }

        function get_cp_customer() {
            $result = $this->db2->limit(5)->get("h_cp_customer")->result_array();

            return $result;
        }
    }