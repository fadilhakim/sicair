<?php

class User_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();

        $this->db = $this->load->database("default", true);
    }

    function get_user_detail($id)
    {
        $user = $this->db
            ->where('id', $id)
            ->get(USER_TABLE)
            ->row_array();

        return $user;
    }


    function get_all_user($params)
    {

        $limit          = isset($params["limit"]) ? $params["limit"] : 0;
        $offset         = isset($params["offset"]) ? $params["offset"] : 0;
        $order          = isset($params["order"]) ? $params["order"] : null;
        $col            = isset($params["col"]) ? $params["col"] : null;
        $dir            = isset($params["dir"]) ? $params["dir"] : null;
        $search         = isset($params["search"]) ? $params["search"] : "";
        $valid_columns  = array(
            0 => 'id',
            1 => 'username',
            2 => 'email',
            3 => 'fullname',
            4 => 'group_id',

        );

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }

        $this->db->select("id,username,fullname,email,group_id,status_id");
        $result = $this->db;

        if ($order != null) {
            $result->order_by($order, $dir);
        }

        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $result->like($sterm, $search);
                } else {
                    $result->or_like($sterm, $search);
                }
                $x++;
            }
        }

        if (!empty($limit) && !empty($offset)) {
            $result = $result->get(USER_TABLE, $params["limit"], $params["offset"]);
        } else {
            $result = $result->get(USER_TABLE);
        }

        $result = $result->result_array();

        return $result;
    }

    function get_all_user_ppk()
    {

        $this->db->select("*");
        $this->db->from("m_users");
        $this->db->where("group_id", 2);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_specific_user_ppk($user_id)
    {

        $this->db->select("fullname");
        $this->db->from("m_users");
        $this->db->where("group_id", 2);
        $this->db->where("id", $user_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_role($group_id)
    {
        $this->db->select("group_name");
        $this->db->from("m_groups");
        $this->db->where("id", $group_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    function user_insert($dt)
    {
        $data = array(
            'username'  => $dt["username"],
            'fullname'  => $dt["fullname"],
            'group_id'  => $dt["group_id"],
            "status_id" => $dt["status_id"],
            "email"     => $dt["email"],
            "password"  => $dt["password"],
            "nip"       => $dt["nip"]
        );

        return $this->db->insert('m_users', $data);  // Produces: INSERT INTO mytable (title, name, date) VALUES ('{$title}', '{$name}', '{$date}')
    }

    function user_update($dt)
    {

        $user_id = $dt["user_id"];
        // $passwordEnc = md5($dt['password']);

        $data = array(
            'username'  => $dt["username"],
            'fullname'  => $dt["fullname"],
            'group_id'  => $dt["group_id"],
            "status_id" => $dt["status_id"],
            "email"     => $dt["email"],
            "password"  => $dt["password"]

        );

        // if(!empty($password)) {
        //     $data["password"] = $dt["password"];
        // }

        $this->db->where('id', $user_id);
        return $this->db->update('m_users', $data);  // Produces: INSERT INTO mytable (title, name, date) VALUES ('{$title}', '{$name}', '{$date}')
    }

    function user_delete($user_id)
    {
        return $this->db->delete(USER_TABLE, array(
            "id" => $user_id
        ));
    }

    function get_user_nik()
    {

        $this->db3->select("users.id as user_id");
        $this->db3->select("m_karyawan.id as karyawan_id");
        $this->db3->select("users.m_karyawan_id as m_karyawan_id");
        $this->db3->select("m_karyawan.nik");
        $this->db3->select("users.username");
        $this->db3->select("users.email");
        $this->db3->select("m_karyawan.nama");
        $this->db3->from("users");
        $this->db3->join("m_karyawan", "m_karyawan.id = users.m_karyawan_id");
        // $this->db3->limit(10);
        $query = $this->db3->get();

        return $query->result_array();
    }

    function get_user_nik_detail($m_karyawan_id)
    {

        $this->db3->select("users.id as user_id");
        $this->db3->select("m_karyawan.id as karyawan_id");
        $this->db3->select("users.m_karyawan_id as m_karyawan_id");
        $this->db3->select("m_karyawan.nik");
        $this->db3->select("users.username");
        $this->db3->select("users.email");
        $this->db3->select("m_karyawan.nama");
        $this->db3->from("users");
        $this->db3->join("m_karyawan", "m_karyawan.id = users.m_karyawan_id");
        $this->db3->where("m_karyawan.id", $m_karyawan_id);
        $query = $this->db3->get();

        return $query->row_array();
    }

    function login_ldap($username, $password)
    {
        $domain_name = 'telkomsat.co.id';
        $username_domain = $username . '@' . $domain_name;
        $ldap_server = 'ldap://ldap.telkomsat.co.id';
        $dn = 'uid=' . $username . ',cn=users,dc=telkomsat,dc=co,dc=id';

        if ($connect = @ldap_connect($ldap_server)) {
            ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);
            ldap_start_tls($connect);

            if ($bind = @ldap_bind($connect, $dn, $password)) {
                @ldap_close($connect);
                return TRUE;
            } else {
                @ldap_close($connect);
                return FALSE;
            }
        }
        return FALSE;
    }

    function login_record($data)
    {

        $loc = json_decode(file_get_contents("http://ipinfo.io/"));

        return $this->db->insert(LOGIN_RECORD, [
            "user_id" => $data["user_id"],
            "username" => $data["username"],
            "session_id" => $this->session->my_session_id,
            "ip_address" => $this->input->ip_address(),
            "location" => $loc->city,
            "created_at" => date("Y-m-d H:i:s")
        ]);
    }

    function get_login_records($params)
    {

        $limit         = isset($params["limit"]) ? $params["limit"] : 0;
        $offset         = isset($params["offset"]) ? $params["offset"] : 0;
        $order          = isset($params["order"]) ? $params["order"] : null;
        $col            = isset($params["col"]) ? $params["col"] : null;
        $dir            = isset($params["dir"]) ? $params["dir"] : null;
        $search         = isset($params["search"]) ? $params["search"] : "";
        $valid_columns  = array(
            0 => "id",
            1 => 'user_id',
            2 => 'username',
            3 => 'ip_address',
            4 => 'location',
            5 => "created_at"

        );

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }

        $this->db->select("id,user_id,username,session_id,ip_address,location,created_at");
        $result = $this->db;

        if ($order != null) {
            $result->order_by($order, $dir);
        }

        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $result->like($sterm, $search);
                } else {
                    $result->or_like($sterm, $search);
                }
                $x++;
            }
        }

        if (!empty($limit) && !empty($offset)) {
            $result = $result->get(LOGIN_RECORD, $params["limit"], $params["offset"]);
        } else {
            $result = $result->get(LOGIN_RECORD);
        }

        $result = $result->result_array();

        return $result;

        // $q = $this->db->get(LOGIN_RECORD);
        // $result = $q->result_array();

        // return $result;
    }

    // pul = project_update_location
    function get_user_project_nodelink_history($session_id)
    {

        $this->db->select("*");
        $this->db->from(PROJECT_UPDATE_LOCATION_RECORD);
        $this->db->where("session_id", $session_id);

        $result = $this->db->get();
        return $result->result_array();
    }

    function get_last_user_project_nodelink_history($session_id)
    {
        $this->db->select("*");
        $this->db->from(PROJECT_UPDATE_LOCATION_RECORD);
        $this->db->where("session_id", $session_id);

        $result = $this->db->get();
        return $result->row_array();
    }

    function user_history_detail($session_id)
    {
        $this->db->select("*");
        $this->db->from(LOGIN_RECORD);
        $this->db->where("session_id", $session_id);

        $result = $this->db->get();
        return $result->row_array();
    }

    function get_freelance()
    {
        $db3 = $this->load->database("db3", true);
        $q = $db3->query("SELECT * FROM " . DB_DATABASE3 . ".m_freelance ");
        return $q->result_array();
    }

    function get_karyawan()
    {
        $db3 = $this->load->database("db3", true);
        $q = $db3->query("SELECT * FROM " . DB_DATABASE3 . ".m_karyawan ");
        return $q->result_array();
    }

    function get_freelance_detail($freelance_id)
    {
        // $db3 = $this->load->database("db3", true);
        $q = $this->db->query("SELECT * FROM " . DB_DATABASE3 . ".m_freelance WHERE id = '$freelance_id' ");
        return $q->row_array();
    }

    function get_karyawan_detail($karyawan_id)
    {
        // $db3 = $this->load->database("db3", true);
        $str = "SELECT * FROM " . DB_DATABASE3 . ".m_karyawan WHERE id = '$karyawan_id' ";

        $q = $this->db->query($str);
        return $q->row_array();
    }
}
