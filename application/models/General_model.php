<?php 

	class General_model extends CI_Model { 

		function __construct(){
			parent::__construct();

			$this->db2 = $this->load->database("db2", true);
		}

		function get_cities() {
			$result = $this->db2->get("r_kabupaten")->result_array();
			return $result;
		}

		function get_cities_byprov($prov_id) { 
			$this->db2->where("r_provinsi_id",$prov_id);
			$result = $this->db2->get("r_kabupaten")->result_array();
			return $result;
		}

		function get_provinsi() {
			$result = $this->db2->get("r_provinsi")->result_array();
			return $result;
		}

	}
