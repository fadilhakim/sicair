<?php

class Kontrak_model extends CI_Model
{

    function get_all_kontrak($params)
    {

        error_reporting(0);

        $limit          = isset($params["limit"]) ? $params["limit"] : 0;
        $offset         = isset($params["offset"]) ? $params["offset"] : 0;
        $order          = isset($params["order"]) ? $params["order"] : null;
        $col            = isset($params["col"]) ? $params["col"] : null;
        $dir            = isset($params["dir"]) ? $params["dir"] : null;
        $search         = isset($params["search"]) ? $params["search"] : "";

        $valid_columns  = array(
            0 => 'tr_kontrak.id',
            1 => 'no_kontrak',
            2 => 'tanggal_kontrak',
            3 => 'uraian',
            4 => "tanggal_mulai",
            5 => "tanggal_selesai",
            6 => 'nilai_kontrak',
            7 => 'jenis_kontrak',
            8 => 'nama_kontraktor',
            9 => 'subdit',
            10 => 'id_user',
        );

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }

        $this->db->select("
                              tr_kontrak.id,
                              tr_kontrak.id_detail_kegiatan,
                              no_kontrak,
                              tanggal_kontrak,
                              uraian,
                              tanggal_mulai,
                              tanggal_selesai,
                              nilai_kontrak,
                              jenis_kontrak,
                              nama_kontraktor,
                              subdit,
                              tanggal_addendum,
                              nomor_addendum,
                              subdit,
                              status,
                              id_user
                              "); // tak boleh semua
        //$this->db->select("tr_detail_kegiatan.id as tr_detail_kegiatan_id, tr_kontrak.*");
        //$this->db->from();
        //$this->db->join(DB_DATABASE .'.tr_detail_kegiatan', 'tr_detail_kegiatan.id = tr_kontrak.id_detail_kegiatan'); 
        $this->db->order_by('created_at', 'DESC');
        $result = $this->db;

        if ($order != null) {
            $result->order_by($order, $dir);
        }

        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $result->like($sterm, $search);
                } else {
                    $result->or_like($sterm, $search);
                }
                $x++;
            }
        }

        // ==================== TAMBAHAN WHERE ======================

        if ($params["filter_status_kontrak"]) {
            $result->where("status", $params["filter_status_kontrak"]);
        }

        if ($params["filter_jenis_kontrak"]) {
            $result->where("jenis_kontrak", $params["filter_jenis_kontrak"]);
        }

        if ($params["filter_subdit"]) {
            $result->where("subdit", $params["filter_subdit"]);
        }

        // if ($params["filter_tanggal_mulai"]) {
        //     $result->where("tanggal_mulai >= '$params[filter_tanggal_mulai]'", null, false);
        // }

        if ($params["filter_tanggal_kontrak"]) {
            $result->where("tanggal_kontrak >= '$params[filter_tanggal_kontrak]'", null, false);
        }
        if ($params["filter_nama_ppk"]) {
            $result->where("id_user", $params["filter_nama_ppk"]);
        }

        // if ($params["filter_tanggal_selesai"]) {
        //     $result->where("tanggal_selesai <= '$params[filter_tanggal_selesai]'", null, false);
        // }

        // if ($params["tahun_anggaran"]) {
        //     $result->where("status_progress", $params["progress_status"]);
        // }

        if ($params["count"] == true) {
            $result = $result->get(DB_DATABASE . ".tr_kontrak");
            $result = $result->num_rows();
        } else {

            if (!empty($limit) || !empty($offset)) {

                $result = $result->get(DB_DATABASE . ".tr_kontrak", $params["limit"], $params["offset"]); // 10 

            } else {

                $result = $result->get(DB_DATABASE . ".tr_kontrak");
            }

            $result = $result->result_array();
        }

        return $result;
    }

    function get_all_kontrak_by_ppk($params, $userID)
    {

        error_reporting(0);

        $limit          = isset($params["limit"]) ? $params["limit"] : 0;
        $offset         = isset($params["offset"]) ? $params["offset"] : 0;
        $order          = isset($params["order"]) ? $params["order"] : null;
        $col            = isset($params["col"]) ? $params["col"] : null;
        $dir            = isset($params["dir"]) ? $params["dir"] : null;
        $search         = isset($params["search"]) ? $params["search"] : "";

        $valid_columns  = array(
            0 => 'tr_kontrak.id',
            1 => 'no_kontrak',
            2 => 'tanggal_kontrak',
            3 => 'uraian',
            4 => "tanggal_mulai",
            5 => "tanggal_selesai",
            6 => 'nilai_kontrak',
            7 => 'jenis_kontrak',
            8 => 'nama_kontraktor',
            9 => 'subdit',
            10 => 'id_user',
        );

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }

        $this->db->select("
                              tr_kontrak.id,
                              tr_kontrak.id_detail_kegiatan,
                              no_kontrak,
                              tanggal_kontrak,
                              uraian,
                              tanggal_mulai,
                              tanggal_selesai,
                              nilai_kontrak,
                              jenis_kontrak,
                              nama_kontraktor,
                              subdit,
                              tanggal_addendum,
                              nomor_addendum,
                              subdit,
                              status,
                              id_user
                              "); // tak boleh semua
        //$this->db->select("tr_detail_kegiatan.id as tr_detail_kegiatan_id, tr_kontrak.*");
        //$this->db->from();
        //$this->db->join(DB_DATABASE .'.tr_detail_kegiatan', 'tr_detail_kegiatan.id = tr_kontrak.id_detail_kegiatan'); 
        $this->db->order_by('created_at', 'DESC');
        $result = $this->db;

        if ($order != null) {
            $result->order_by($order, $dir);
        }

        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $result->like($sterm, $search);
                } else {
                    $result->or_like($sterm, $search);
                }
                $x++;
            }
        }

        // ==================== TAMBAHAN WHERE ======================

        if ($params["filter_status_kontrak"]) {
            $result->where("status", $params["filter_status_kontrak"]);
        }

        if ($params["filter_jenis_kontrak"]) {
            $result->where("jenis_kontrak", $params["filter_jenis_kontrak"]);
        }

        if ($params["filter_subdit"]) {
            $result->where("subdit", $params["filter_subdit"]);
        }

        // if ($params["filter_tanggal_mulai"]) {
        //     $result->where("tanggal_mulai >= '$params[filter_tanggal_mulai]'", null, false);
        // }

        if ($params["filter_tanggal_kontrak"]) {
            $result->where("tanggal_kontrak >= '$params[filter_tanggal_kontrak]'", null, false);
        }
        if ($params["filter_nama_ppk"]) {
            $result->where("id_user", $params["filter_nama_ppk"]);
        }

        // if ($params["filter_tanggal_selesai"]) {
        //     $result->where("tanggal_selesai <= '$params[filter_tanggal_selesai]'", null, false);
        // }

        // if ($params["tahun_anggaran"]) {
        //     $result->where("status_progress", $params["progress_status"]);
        // }
        $result->where("id_user", $userID);
        if ($params["count"] == true) {
            $result = $result->get(DB_DATABASE . ".tr_kontrak");
            $result = $result->num_rows();
        } else {

            if (!empty($limit) || !empty($offset)) {

                $result = $result->get(DB_DATABASE . ".tr_kontrak", $params["limit"], $params["offset"]); // 10 

            } else {

                $result = $result->get(DB_DATABASE . ".tr_kontrak");
            }

            $result = $result->result_array();
        }

        return $result;
    }

    function kontrak_list()
    {
        $this->db->select("*");
        // $this->db->from("tr_kontrak");

        return $this->db->get("tr_kontrak")->result_array();
    }

    function kontrak_list_verified()
    {
        $this->db->select("*");
        $this->db->from("tr_kontrak");
        $this->db->where('status', 'Verifikasi Bendahara');
        $this->db->order_by('created_at', 'DESC');
        return $this->db->get()->result_array();
    }

    function kontrak_detail($id_kontrak)
    {

        $this->db->select("*");
        // $this->db->from("tr_kontrak");
        $this->db->where("id", $id_kontrak);
        return $this->db->get("tr_kontrak")->row_array();
    }
    function kontrak_detail2($id_kontrak)
    {

        $this->db->select("*");
        // $this->db->from("tr_kontrak");
        $this->db->where("id", $id_kontrak);
        return $this->db->get("tr_kontrak")->result_array();
    }

    function add_kontrak($data)
    {

        return $this->db->insert("tr_kontrak", $data);
    }

    function update_kontrak($data)
    {

        $this->db->where('id', $data["id"]);
        return $this->db->update("tr_kontrak", $data);
    }

    function delete_kontrak($kontrak_id)
    {

        $this->db->where('id', $kontrak_id);
        return $this->db->delete("tr_kontrak");
    }

    function get_updated_kontrak()
    {
        $this->db->select("*");
        $this->db->from("tr_kontrak");
        $this->db->order_by("updated_at");
        $this->db->limit("5");
        return $this->db->get()->result_array();
    }

    function add_termin($data)
    {
        return $this->db->insert("tr_kontrak_termin", $data);
    }

    function update_termin($data)
    {
        $this->db->where("id", $data["id"]);
        return $this->db->update("tr_kontrak_termin", $data);
    }

    function termin_list($id_kontrak)
    {
        $this->db->where('id_kontrak', $id_kontrak);
        return $this->db->get("tr_kontrak_termin")->result_array();
    }

    function detail_termin($termin_id)
    {
        $this->db->where('id', $termin_id);
        return $this->db->get("tr_kontrak_termin")->row_array();
    }

    function get_all_termin($params)
    {

        error_reporting(0);

        $limit          = isset($params["limit"]) ? $params["limit"] : 0;
        $offset         = isset($params["offset"]) ? $params["offset"] : 0;
        $order          = isset($params["order"]) ? $params["order"] : null;
        $col            = isset($params["col"]) ? $params["col"] : null;
        $dir            = isset($params["dir"]) ? $params["dir"] : null;
        $search         = isset($params["search"]) ? $params["search"] : "";

        $valid_columns  = array(
            0 => 'id',
            1 => 'termin_phase',
            2 => 'harga_termin',

        );

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        }

        if (!isset($valid_columns[$col])) {
            $order = "termin_phase";
        } else {
            $order = $valid_columns[$col];
        }

        $this->db->select("*"); // tak boleh semua
        //$this->db->select("tr_detail_kegiatan.id as tr_detail_kegiatan_id, tr_kontrak.*");
        //$this->db->join(DB_DATABASE .'.tr_detail_kegiatan', 'tr_detail_kegiatan.id = tr_kontrak.id_detail_kegiatan'); 
        $result = $this->db;

        if ($order != null) {
            $result->order_by($order, $dir);
        } else {
            $result->order_by("termin_phase", "asc");
        }

        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $result->like($sterm, $search);
                } else {
                    $result->or_like($sterm, $search);
                }
                $x++;
            }
        }

        // ==================== TAMBAHAN WHERE ======================

        $result->where("id_kontrak", $params["id_kontrak"]);

        if ($params["count"]) {
            $result = $result->get(DB_DATABASE . ".tr_kontrak_termin");
            $result = $result->num_rows();
        } else {
            if (!empty($limit) && !empty($offset)) {

                $result = $result->get(DB_DATABASE . ".tr_kontrak_termin", $params["limit"], $params["offset"]);
            } else {

                $result = $result->get(DB_DATABASE . ".tr_kontrak_termin");
            }
            $result = $result->result_array();
        }

        return $result;
    }

    function delete_termin($termin_id)
    {
        $this->db->where('id', $termin_id);
        return $this->db->delete("tr_kontrak_termin");
    }

    function get_termin_by_kontrak_id($id)
    {
        $this->db->select("*");
        $this->db->from("tr_kontrak_termin");
        $this->db->where('id_kontrak', $id);
        return $this->db->get()->result_array();
    }
}
