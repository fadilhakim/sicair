<?php

class Laporan_model extends CI_model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function kontrak_kegiatan($id_kegiatan)
    {
        $str = "SELECT 
                SUM(tr_kontrak.nilai_kontrak) as total_kontrak
            FROM
                tr_kontrak
            JOIN 
                tr_detail_kegiatan ON tr_kontrak.id_detail_kegiatan = tr_detail_kegiatan.id 
            JOIN 
                tr_kegiatan ON tr_kegiatan.id = tr_detail_kegiatan.id_kegiatan
            WHERE  
                tr_kegiatan.id = $id_kegiatan
            ";

        $q = $this->db->query($str);
        $res = $q->row_array($q);

        return $res;
    }

    function tagihan_kegiatan($id_kegiatan)
    {
        $str = "SELECT 
                SUM(tr_tagihan.nilai_tagihan) as total_tagihan
            FROM
                tr_tagihan
            JOIN 
                tr_detail_kegiatan ON tr_tagihan.id_detail_kegiatan = tr_detail_kegiatan.id 
            JOIN 
                tr_kegiatan ON tr_kegiatan.id = tr_detail_kegiatan.id_kegiatan
            WHERE  
                tr_kegiatan.id = $id_kegiatan
            ";

        $q = $this->db->query($str);
        $res = $q->row_array($q);

        return $res;
    }

    function kontrak_detail_kegiatan($id_detail_kegiatan)
    {
        $str = "SELECT 
                SUM(tr_kontrak.nilai_kontrak) as total_kontrak
            FROM
                tr_kontrak
            JOIN 
                tr_detail_kegiatan ON tr_kontrak.id_detail_kegiatan = tr_detail_kegiatan.id 
            WHERE  
                tr_detail_kegiatan.id = $id_detail_kegiatan
            ";

        $q = $this->db->query($str);
        $res = $q->row_array($q);

        return $res;
    }

    function get_all_kegiatan($params)
    {

        error_reporting(0);

        $limit          = isset($params["limit"]) ? $params["limit"] : 0;
        $offset         = isset($params["offset"]) ? $params["offset"] : 0;
        $order          = isset($params["order"]) ? $params["order"] : null;
        $col            = isset($params["col"]) ? $params["col"] : null;
        $dir            = isset($params["dir"]) ? $params["dir"] : null;
        $search         = isset($params["search"]) ? $params["search"] : "";

        $valid_columns  = array(
            0 => 'nama_kegiatan',
            1 => 'pagu_kegiatan',
            2 => 'id_user'
        );

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }


        if ($params["sum"]) {

            $this->db->select_sum("pagu_kegiatan");
        } else {
            $this->db->select("
            id,
            kode_aktivitas,
            kro,
            ro,
            komponen,
            subkomponen,
            id_user,
            nama_kegiatan,
            pagu_kegiatan");
        }

        $this->db->from("tr_kegiatan");

        $result = $this->db;

        if ($order != null) {
            $result->order_by($order, $dir);
        }

        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $result->like($sterm, $search);
                } else {
                    $result->or_like($sterm, $search);
                }
                $x++;
            }
        }


        if ($params["filter_tahun_anggaran"]) {
            $result->where("id_anggaran", $params["filter_tahun_anggaran"]);
        }

        if ($params["filter_nama_ppk"]) {
            $result->where("id_user", $params["filter_nama_ppk"]);
        }

        if ($params["filter_prioritas"]) {
            $result->where("prioritas", $params["filter_prioritas"]);
        }

        if (!empty($limit) && !empty($offset)) {


            $result->db->limit($params["limit"], $params["offset"]);
            $result = $result->get();
        } else {

            $result = $result->get();
        }

        if ($params["count"]) {
            $result = $result->num_rows();
        } else if ($params["sum"]) {
            $result = $result->row_array();
        } else {
            $result = $result->result_array();
        }

        return $result;
    }

    function get_all_kegiatan_by_ppk($params, $userID)
    {

        error_reporting(0);

        $limit          = isset($params["limit"]) ? $params["limit"] : 0;
        $offset         = isset($params["offset"]) ? $params["offset"] : 0;
        $order          = isset($params["order"]) ? $params["order"] : null;
        $col            = isset($params["col"]) ? $params["col"] : null;
        $dir            = isset($params["dir"]) ? $params["dir"] : null;
        $search         = isset($params["search"]) ? $params["search"] : "";

        $valid_columns  = array(
            0 => 'nama_kegiatan',
            1 => 'pagu_kegiatan',
            2 => 'nama_ppk'
        );

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }


        if ($params["sum"]) {

            $this->db->select_sum("pagu_kegiatan");
        } else {
            $this->db->select("
            id,
            kode_aktivitas,
            kro,
            ro,
            komponen,
            subkomponen,
            id_user,
            nama_kegiatan,
            pagu_kegiatan");
        }

        $this->db->from("tr_kegiatan");

        $result = $this->db;

        if ($order != null) {
            $result->order_by($order, $dir);
        }

        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $result->like($sterm, $search);
                } else {
                    $result->or_like($sterm, $search);
                }
                $x++;
            }
        }


        $result->where("id_user", $userID);

        if ($params["filter_tahun_anggaran"]) {
            $result->where("id_anggaran", $params["filter_tahun_anggaran"]);
        }

        if ($params["filter_nama_ppk"]) {
            $result->where("id_user", $params["filter_nama_ppk"]);
        }

        if ($params["filter_prioritas"]) {
            $result->where("prioritas", $params["filter_prioritas"]);
        }

        if (!empty($limit) && !empty($offset)) {


            $result->db->limit($params["limit"], $params["offset"]);
            $result = $result->get();
        } else {

            $result = $result->get();
        }

        if ($params["count"]) {
            $result = $result->num_rows();
        } else if ($params["sum"]) {
            $result = $result->row_array();
        } else {
            $result = $result->result_array();
        }

        return $result;
    }

    function get_all_kegiatan_by_id_detail_kegiatan($params, $id)
    {

        error_reporting(0);

        $limit          = isset($params["limit"]) ? $params["limit"] : 0;
        $offset         = isset($params["offset"]) ? $params["offset"] : 0;
        $order          = isset($params["order"]) ? $params["order"] : null;
        $col            = isset($params["col"]) ? $params["col"] : null;
        $dir            = isset($params["dir"]) ? $params["dir"] : null;
        $search         = isset($params["search"]) ? $params["search"] : "";
        $valid_columns  = array(
            0 => 'detail_kegiatan',
            1 => 'kode_mak',
            2 => 'pagu_kegiatan'
        );

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }

        $this->db->select("*");
        $this->db->where('id_kegiatan', $id);
        $this->db->order_by('created_at', 'DESC');
        $result = $this->db;

        if ($order != null) {
            $result->order_by($order, $dir);
        }

        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $result->like($sterm, $search);
                } else {
                    $result->or_like($sterm, $search);
                }
                $x++;
            }
        }

        if ($params["count"]) {
            $result = $result->get(DB_DATABASE . ".tr_detail_kegiatan");
            $result = $result->num_rows();
        } else {
            if (!empty($limit) && !empty($offset)) {

                $result = $result->get(DB_DATABASE . ".tr_detail_kegiatan", $params["limit"], $params["offset"]);
            } else {

                $result = $result->get(DB_DATABASE . ".tr_detail_kegiatan");
            }
            $result = $result->result_array();
        }

        return $result;
    }

    function list_kontrak_by_id_detail_kegiatan($params, $id)
    {
        error_reporting(0);

        $limit          = isset($params["limit"]) ? $params["limit"] : 0;
        $offset         = isset($params["offset"]) ? $params["offset"] : 0;
        $order          = isset($params["order"]) ? $params["order"] : null;
        $col            = isset($params["col"]) ? $params["col"] : null;
        $dir            = isset($params["dir"]) ? $params["dir"] : null;
        $search         = isset($params["search"]) ? $params["search"] : "";
        $valid_columns  = array(
            0 => 'no_kontrak',
            1 => 'tanggal_kontrak',
            2 => 'uraian'
        );

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }

        $this->db->select("*");
        $this->db->where('id_detail_kegiatan', $id);
        $this->db->order_by('created_at', 'DESC');
        $result = $this->db;

        if ($order != null) {
            $result->order_by($order, $dir);
        }

        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $result->like($sterm, $search);
                } else {
                    $result->or_like($sterm, $search);
                }
                $x++;
            }
        }

        if ($params["count"]) {
            $result = $result->get(DB_DATABASE . ".tr_kontrak");
            $result = $result->num_rows();
        } else {
            if (!empty($limit) && !empty($offset)) {

                $result = $result->get(DB_DATABASE . ".tr_kontrak", $params["limit"], $params["offset"]);
            } else {

                $result = $result->get(DB_DATABASE . ".tr_kontrak");
            }
            $result = $result->result_array();
        }

        return $result;
    }

    function list_tagihan_by_id_detail_kegiatan($params, $id)
    {
        error_reporting(0);

        $limit          = isset($params["limit"]) ? $params["limit"] : 0;
        $offset         = isset($params["offset"]) ? $params["offset"] : 0;
        $order          = isset($params["order"]) ? $params["order"] : null;
        $col            = isset($params["col"]) ? $params["col"] : null;
        $dir            = isset($params["dir"]) ? $params["dir"] : null;
        $search         = isset($params["search"]) ? $params["search"] : "";
        $valid_columns  = array(
            0 => 'nomor_tagihan',
            1 => 'tanggal_tagihan',
            2 => 'uraian',
            3 => "nilai_tagihan"
        );

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }

        $this->db->select("*");
        $this->db->where('id_detail_kegiatan', $id);
        $this->db->order_by('created_at', 'DESC');
        $result = $this->db;

        if ($order != null) {
            $result->order_by($order, $dir);
        }

        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $result->like($sterm, $search);
                } else {
                    $result->or_like($sterm, $search);
                }
                $x++;
            }
        }

        if ($params["count"]) {
            $result = $result->get(DB_DATABASE . ".tr_tagihan");
            $result = $result->num_rows();
        } else {
            if (!empty($limit) && !empty($offset)) {

                $result = $result->get(DB_DATABASE . ".tr_tagihan", $params["limit"], $params["offset"]);
            } else {

                $result = $result->get(DB_DATABASE . ".tr_tagihan");
            }
            $result = $result->result_array();
        }

        return $result;
    }
}
