<?php

class Projects_model extends CI_Model {

	function __construct(){
		parent::__construct();

		$this->db2 = $this->load->database("db2", true);
	}

	// function get_all_data_projects(){

	//     $qry = "SELECT DISTINCT
	//               w.nomor as no_wo,
	//               c.nama as nama_pelanggan,
	//               w.rfs,
	//               w.tgl as tanggal_terima_order,
	//               k.project_id,
	//               k.project_nama,
	//               kl.jumlah_node
	//             FROM
	//               h_workorder w
	//             LEFT JOIN
	//               h_kontrak k ON w.h_kontrak_id = k.id
	//             LEFT JOIN
	//               m_customer c ON c.id = k.m_customer_id
	//             LEFT JOIN
	//               h_kontrak_layanan kl ON kl.h_kontrak_id = k.id ";

	//     // $this->db2->select("nama, nomor, rfs, sid, no_provisioning, project_id, project_nama, h_kontrak_nodelink.nama, tgl");

	//     $query = $this->db2->query($qry);
	//     $result = $query->result_array();
	//     return $result;
			// }

	function project_status_list() {
		$result = $this->db->get(PROJECT_STATUS_TABLE)->result_array();
		return $result;
	}

	function get_all_data_projects($params = []) {

		error_reporting(0);

		$qry = " SELECT DISTINCT
				w.id as h_workorder_id,
				w.nomor as no_wo,
				c.nama as nama_pelanggan,
				c.id as pelanggan_id,
				w.rfs,
				w.tgl as tanggal_terima_order,
				k.project_id,
				k.project_nama,
				w.r_jenis_workorder_id as jenis_layanan,
				l.id,
				l.nama as layanan_nama,
				k.id as h_kontrak_id,
				knl.sid,

				(".$this->get_count_pul_bywoi().") as jumlah_node
			FROM
				".DB_DATABASE2.".h_workorder w
			LEFT JOIN
				".DB_DATABASE2.".h_kontrak k ON w.h_kontrak_id = k.id
			LEFT JOIN
				".DB_DATABASE2.".m_customer c ON c.id = k.m_customer_id
			LEFT JOIN
				".DB_DATABASE2.".h_kontrak_layanan kl ON kl.h_kontrak_id = k.id
			LEFT JOIN
				".DB_DATABASE2.".h_kontrak_nodelink knl ON kl.h_kontrak_id = knl.id
			LEFT JOIN
				".DB_DATABASE2.".h_provisioning p ON kl.h_kontrak_id = p.id
			LEFT JOIN
				".DB_DATABASE2.".m_layanan l ON kl.m_layanan_id = l.id
		";

		// INI DARI HEADER
		if(isset($params["search"])) {
			$qry .= "WHERE k.project_nama LIKE '%$params[search]%' OR ";
			$qry .= "k.project_id LIKE '%$params[search]%' ";
			$qry .= "LIMIT 0,10";
		}
		else {

			$qry .= "WHERE ";

			if(isset($params["rfs_diff7"])) {
				$qry .= "r_jenis_workorder_id NOT LIKE 8 AND ";
				$qry .= "DATEDIFF(w.rfs,'".date("Y-m-d")."') BETWEEN 0 AND 7 AND ";
			}

			if(isset($params["customer_id"])) {
				$qry .= "r_jenis_workorder_id NOT LIKE 8 AND ";
				$qry .= "c.id = '$params[customer_id]' AND ";
			}

			if(isset($params["jenis_layanan"])) {
				$qry .= "r_jenis_workorder_id NOT LIKE 8 AND ";
				$qry .= "kl.m_layanan_id = ".$this->db->escape($params["jenis_layanan"])." AND ";
			}

			if(isset($params["nama_pelanggan"])){
				$qry .= "r_jenis_workorder_id NOT LIKE 8 AND ";
				$qry .= "c.id = ".$this->db->escape($params["nama_pelanggan"])." AND ";
			}

			if(isset($params["jumlah_node"])) {
				$qry .= "r_jenis_workorder_id NOT LIKE 8 AND ";
				if($params["jumlah_node"] == "A") {
					$qry .= "jumlah_node BETWEEN 1 AND 10 AND ";
				} elseif ($params["jumlah_node"] == "B") {
					$qry .= "jumlah_node BETWEEN 11 AND 50 AND ";
				} elseif ($params["jumlah_node"] == "C") {
					$qry .= "jumlah_node BETWEEN 51 AND 100 AND ";
				} elseif ($params["jumlah_node"] == "D") {
					$qry .= "jumlah_node > 100 AND ";
				}
			}

			// untuk jumlah layanan bar chart
			if($params["layanan_id"] && $params["month"]) {
				
				$qry .= "l.id = '$params[layanan_id]' AND MONTH(w.tgl) = '$params[month]' AND YEAR(w.tgl) = ".date("Y")." AND ";
			
				if($params["is_dismantle"]) {
					$qry .= "r_jenis_workorder_id = 8 AND ";
				} else {
					$qry .= "r_jenis_workorder_id NOT LIKE 8 AND ";
				}
			}

			

			$qry .= "1 = 1  ";

			$qry .= " GROUP BY project_id ORDER BY h_workorder_id DESC";
		}

		

		// $this->db2->select("nama, nomor, rfs, sid, no_provisioning, project_id, project_nama, h_kontrak_nodelink.nama, tgl");
		$query = $this->db2->query($qry);
		
		if($params["count"]) {
			return $result = $query->num_rows();
		}
		return $result = $query->result_array();
	}

    function get_detail_data_project($workorder_id) {
            $qry = "SELECT DISTINCT
              w.id as h_workorder_id,
              w.nomor as no_wo,
              c.nama as nama_pelanggan,
              c.id as pelanggan_id,
              w.rfs,
			        w.rfs_fix,
              w.tgl as tanggal_terima_order,
              k.project_id,
              k.project_nama,
              w.r_jenis_workorder_id as jenis_layanan,
              kl.jumlah_node


            FROM
              h_workorder w
            LEFT JOIN
              h_kontrak k ON w.h_kontrak_id = k.id
            LEFT JOIN
              m_customer c ON c.id = k.m_customer_id
            LEFT JOIN
              h_kontrak_layanan kl ON kl.h_kontrak_id = k.id
            LEFT JOIN
              h_kontrak_nodelink knl ON kl.h_kontrak_id = knl.id
            LEFT JOIN
              h_provisioning p ON kl.h_kontrak_id = p.id

            WHERE
              w.id = $workorder_id

          ";
            // $this->db2->select("nama, nomor, rfs, sid, no_provisioning, project_id, project_nama, h_kontrak_nodelink.nama, tgl");
            $query = $this->db2->query($qry);
            return $result = $query->row_array();
    }

    function get_detail_data_project_update($params) {
		$qry = "SELECT DISTINCT
			pu.id as project_update_id,
			pu.h_workorder_id as h_workorder_id,
			pu.no_wo as no_wo,
			c.nama as nama_pelanggan,
			c.id as pelanggan_id,
			pu.rfs,
				w.rfs_fix,
			pu.tgl_terima_order,
			k.project_id,
			k.project_nama,
			pu.project_code,
			pu.teknologi,
			l.nama as teknologi_nama,
			rjw.id as jenis_layanan,
			rjw.nama as jenis_layanan_nama,
			highlighted,
			highlighted_desc
		FROM
			".DB_DATABASE2.".h_workorder w
		LEFT JOIN
			".DB_DATABASE2.".r_jenis_workorder rjw ON w.r_jenis_workorder_id = rjw.id
		LEFT JOIN
			".DB_DATABASE2.".h_kontrak k ON w.h_kontrak_id = k.id
		LEFT JOIN
			".DB_DATABASE2.".m_customer c ON c.id = k.m_customer_id
		LEFT JOIN
			".DB_DATABASE2.".h_kontrak_layanan kl ON kl.h_kontrak_id = k.id
		LEFT JOIN
			".DB_DATABASE2.".m_layanan l ON kl.m_layanan_id = l.id
		LEFT JOIN
			".DB_DATABASE.".tr_project_update pu ON w.id = pu.h_workorder_id
		WHERE

		";
	
		if($params["workorder_id"]) {
			$qry .= " w.id = '$params[workorder_id]' AND ";
		}

		$qry .= " 1 = 1 ";
		// $this->db2->select("nama, nomor, rfs, sid, no_provisioning, project_id, project_nama, h_kontrak_nodelink.nama, tgl");
		$query = $this->db2->query($qry);
		return $result = $query->row_array();
    }

	function check_project_update($workorder_id) {

      $this->db->where("h_workorder_id",$workorder_id);
      $result = $this->db->get(PROJECT_UPDATE_TABLE)->row_array();
      return $result;
    }

    function check_project_update_location($project_update_id) {
      $this->db->where("project_update_id",$project_update_id);
      $result = $this->db->get(PROJECT_UPDATE_LOCATION_TABLE)->result_array();
      return $result;
	}

	function project_update_detail($project_update_id) {
		$this->db->where("id",$project_update_id);
		$result = $this->db->get(PROJECT_UPDATE_TABLE)->row_array();
		return $result;
	}

	function insert_project_update($data) {
		$data = array(
			"pelanggan"         => $data["pelanggan_id"],
			"h_workorder_id"    => $data["h_workorder_id"],
			"no_wo"             => $data["no_wo"],
			"project_code"      => $data["project_id"]."-".$data["project_nama"],
			"tgl_terima_order"  => $data["tanggal_terima_order"],
			"RFS"               => $data["rfs"],
			"jenis_layanan"     => $data["jenis_layanan"],
			"teknologi"         => isset($data["teknologi"]) ? $data["teknologi"] : "",
			"created_at"		=> date("Y-m-d H:i:s"),
		);

		return $this->db->insert(PROJECT_UPDATE_TABLE,$data);
	}

    function project_update_location_unknown(){


    }

    function project_update_location_list_source($params = []) {

		error_reporting(0);

		$comment = "LEFT JOIN
						".DB_DATABASE2.".h_provisioning_nodelink hpv ON  hpv.h_workorder_nodelink_id = wn.id
					LEFT JOIN
						".DB_DATABASE2.".h_provisioning hp ON hpv.h_provisioning_id = hp.id";

		$qry = "SELECT DISTINCT

		kn.id as id,
		c.nama as nama_pelanggan,
		k.id as kontrak_id,
		kn.h_kontrak_layanan_id as h_kontrak_layanan_id,
		wn.h_kontrak_nodelink_id,
		k.project_nama,
			w.id as h_workorder_id,
			tgl,
			r_jenis_workorder_id ,
			-- hp.no_provisioning as no_provisioning,
			kn.sid as sid,
			kn.r_bandwidth_down_id as bw,

			kn.nama as lokasi_instalasi,
			kn.alamat as alamat,
			kab.id as kab,
			kab.nama as kab_nama,
			prov.id as provinsi,
			prov.nama as provinsi_nama,
			wn.nama_pic as nama_pic,
			wn.hp_pic as hp_pic,
			pu.id as project_update_id,
			pul.status

		FROM
			".DB_DATABASE2.".h_kontrak_nodelink kn
			LEFT JOIN
			".DB_DATABASE2.".h_workorder_nodelink wn ON kn.id = wn.h_kontrak_nodelink_id
			LEFT JOIN
			".DB_DATABASE2.".h_kontrak_layanan kl ON kl.id = kn.h_kontrak_layanan_id

			LEFT JOIN
			".DB_DATABASE2.".h_kontrak k ON kl.h_kontrak_id = k.id
			LEFT JOIN
			".DB_DATABASE2.".m_customer c ON c.id = k.m_customer_id
			LEFT JOIN
			".DB_DATABASE2.".h_workorder w ON k.id = w.h_kontrak_id
			LEFT JOIN
			".DB_DATABASE2.".r_kabupaten kab ON kab.id = kn.r_kabupaten_id
			LEFT JOIN
			".DB_DATABASE2.".r_provinsi prov ON kab.r_provinsi_id = prov.id
			LEFT JOIN
				".DB_DATABASE.".tr_project_update pu ON pu.h_workorder_id =  w.id
			LEFT JOIN
				".DB_DATABASE.".tr_project_update_location pul ON pu.id =  pul.project_update_id

			WHERE
		";

		if($params["no_status"]) {
			$qry .= "(pul.status is null OR pul.status = 0) AND ";
		}

		if($params["workorder_id"]) {
			$qry .= "w.id = $params[workorder_id] AND ";
		}


		if($params["year"]) {
			$qry .= "YEAR(tgl) = $params[year] AND ";
		}

		if($params["no_dismantle"]) {
			$qry .= "r_jenis_workorder_id != 8 AND ";
		}

		$qry .= "1=1 GROUP BY sid";

		// $this->db2->select("nama, nomor, rfs, sid, no_provisioning, project_id, project_nama, h_kontrak_nodelink.nama, tgl");
		$query = $this->db2->query($qry);

		if($params["count"]) {
			return $result = $query->num_rows();
		}
		return $result = $query->result_array();
    }

    function project_update_location_list($params = []) {

		error_reporting(0);

			  $qry = "SELECT DISTINCT
						    pul.id,

							w.id as h_workorder_id,
							-- hp.no_provisioning as no_provisioning,

							pul.bw as bw,
							pul.alamat as lokasi_instalasi,

							pu.id as project_update_id,
							pul.sid,
              pul.h_kontrak_nodelink_id,
              pul.alamat,
              pul.kota,
							pul.provinsi,
							pul.teknisi_type,
							pul.freelance_id,
							pul.teknisi_id,
							pul.teknisi_phone_number,
              pul.pic,
							pul.pic_phone_number,
							pul.tgl_mulai_progress,
							pul.TOL,
							pul.status,
              ps.status_name,
			  				ps.status_color,
							pul.keterangan

			FROM
				".DB_DATABASE.".tr_project_update_location pul
			LEFT JOIN
				".DB_DATABASE.".tr_project_update pu ON pul.project_update_id = pu.id
			LEFT JOIN
				".DB_DATABASE2.".h_workorder w ON  pu.h_workorder_id = w.id
            LEFT JOIN
              	".DB_DATABASE.".".PROJECT_STATUS_TABLE." ps ON ps.id = pul.status
			WHERE
			";

			if($params["project_status_id"]) {
				$qry .= " pul.status = '$params[project_status_id]' AND ";
			}

			if($params["workorder_id"]) {
				$qry .= "w.id = $params[workorder_id] AND ";
			}

			if($params["year"]) {
				$qry .= "YEAR(tgl) = $params[year] AND ";
			}

			if($params["is_status"]) {
				$qry .= " pul.status != 0 AND pul.status != 8 AND  ";
			}

			if($params["is_online_status"]) {
				$qry .= " pul.status = 5 AND  ";
			}

			if($params["is_baa_status"]) {
				$qry .= " pul.status = 8 AND  ";
			}

			$qry .= " 1=1  GROUP BY sid ORDER BY sid DESC";

			// echo $qry;

			// $this->db2->select("nama, nomor, rfs, sid, no_provisioning, project_id, project_nama, h_kontrak_nodelink.nama, tgl");
			$query = $this->db2->query($qry);
			if($params["count"]) {
				return $result = $query->num_rows();
			}

			return $result = $query->result_array();
		}

		function project_update_location_detail_source($project_update_location_id) {
			$qry = "SELECT DISTINCT
					kn.id as id,
					w.id as h_workorder_id,
					hp.no_provisioning as no_provisioning,
					kn.sid as sid,
					kn.r_bandwidth_down_id as bw,
					kn.nama as lokasi_instalasi,
					kn.alamat as alamat,
					kn.ao,
					kab.id as kab,
					prov.id as provinsi,
					wn.nama_pic as nama_pic,
					wn.hp_pic as hp_pic,
					pu.id as project_update_id,
					pul.teknisi_id,
					pul.teknisi_phone_number,
					pul.tgl_mulai_progress,
					pul.TOL,
					pul.status,
					pul.keterangan

				FROM
					".DB_DATABASE2.".h_kontrak_nodelink kn
				LEFT JOIN
					".DB_DATABASE2.".h_workorder_nodelink wn ON kn.id = wn.h_kontrak_nodelink_id
				LEFT JOIN
					".DB_DATABASE2.".h_kontrak_layanan kl ON kl.id = kn.h_kontrak_layanan_id
				LEFT JOIN
					".DB_DATABASE2.".h_kontrak k ON kl.h_kontrak_id = k.id
				LEFT JOIN
					".DB_DATABASE2.".h_workorder w ON k.id = w.h_kontrak_id
				LEFT JOIN
					".DB_DATABASE2.".r_kabupaten kab ON kab.id = kn.r_kabupaten_id
				LEFT JOIN
					".DB_DATABASE2.".r_provinsi prov ON kab.r_provinsi_id = prov.id
				LEFT JOIN
					".DB_DATABASE2.".h_provisioning_nodelink hpv ON  hpv.h_workorder_nodelink_id = wn.id
				LEFT JOIN
					".DB_DATABASE2.".h_provisioning hp ON hpv.h_provisioning_id = hp.id
				LEFT JOIN
					".DB_DATABASE.".tr_project_update pu ON pu.h_workorder_id =  w.id
				LEFT JOIN
					".DB_DATABASE.".tr_project_update_location pul ON pu.id = pul.project_update_id
				WHERE
					kn.id = $project_update_location_id

			";
			// $this->db2->select("nama, nomor, rfs, sid, no_provisioning, project_id, project_nama, h_kontrak_nodelink.nama, tgl");
			$query = $this->db2->query($qry);
			return $result = $query->row_array();
        }

        function project_update_location_detail($params) {

			error_reporting(0);

			$qry = "SELECT DISTINCT

				pul.id,
					w.id as h_workorder_id,
					hp.no_provisioning as no_provisioning,
					kn.sid,
					kn.r_bandwidth_down_id as bw,
					kn.nama as lokasi_instalasi,
					kn.ao,
					pu.id as project_update_id,

				pul.h_kontrak_nodelink_id,
					pul.alamat,
					pul.kota,
					pul.provinsi,
					pul.teknisi_type,
					pul.teknisi_id,
					pul.freelance_id,
					pul.teknisi_phone_number,
					pul.pic,
					pul.pic_phone_number,
					pul.tgl_mulai_progress,
					pul.TOL,
					pul.status,
					pul.status_date,
					pul.keterangan,
				ps.status_name


				FROM
					".DB_DATABASE2.".h_kontrak_nodelink kn
				LEFT JOIN
					".DB_DATABASE.".tr_project_update_location pul ON pul.h_kontrak_nodelink_id = kn.id
				LEFT JOIN
					".DB_DATABASE2.".h_workorder_nodelink wn ON kn.id = wn.h_kontrak_nodelink_id
				LEFT JOIN
					".DB_DATABASE2.".h_kontrak_layanan kl ON kl.id = kn.h_kontrak_layanan_id
				LEFT JOIN
					".DB_DATABASE2.".h_kontrak k ON kl.h_kontrak_id = k.id
				LEFT JOIN
					".DB_DATABASE2.".h_workorder w ON k.id = w.h_kontrak_id
				-- LEFT JOIN
				-- 	".DB_DATABASE2.".r_kabupaten kab ON kab.id = kn.r_kabupaten_id
				-- LEFT JOIN
				-- 	".DB_DATABASE2.".r_provinsi prov ON kab.r_provinsi_id = prov.id
				LEFT JOIN
					".DB_DATABASE2.".h_provisioning_nodelink hpv ON  hpv.h_workorder_nodelink_id = wn.id
				LEFT JOIN
					".DB_DATABASE2.".h_provisioning hp ON hpv.h_provisioning_id = hp.id
				LEFT JOIN
					".DB_DATABASE.".tr_project_update pu ON pu.h_workorder_id =  w.id
				LEFT JOIN
					".DB_DATABASE.".".PROJECT_STATUS_TABLE." ps ON ps.id = pul.status
				WHERE
					

			";

			if($params["project_update_location_id"]) {
				$qry .= "pul.id = '$params[project_update_location_id]' AND ";
			}

			if($params["sid"]) {
				$qry .= "kn.sid = '$params[sid]' AND ";
			}

			$qry .= "1=1";

			// $this->db2->select("nama, nomor, rfs, sid, no_provisioning, project_id, project_nama, h_kontrak_nodelink.nama, tgl");
			$query = $this->db2->query($qry);
			return $result = $query->row_array();
		}

		function project_update_location_insert($data) {

			if($data["jenis_teknisi"] == "teknisi") {
				$key = "teknisi_id";
			} else if($data["jenis_teknisi"] == "freelance") {
				$key = "freelance_id";
			}

			$field = array(
				"h_kontrak_nodelink_id" => isset($data["h_kontrak_nodelink_id"]) ? $data["h_kontrak_nodelink_id"] : "",
				"no_provisioning" 		=> !is_null($data["no_provisioning"]) ? $data["no_provisioning"] : "",
				"sid"					=> !is_null($data["sid"]) ? $data["sid"] : "" ,
				"project_update_id"		=> $data["project_update_id"],
				"BW"					=> !is_null($data["bw"]) ? $data["bw"] : "",
				"alamat"				=> !is_null($data["alamat"]) ? $data["alamat"] : "",
				"kota"					=> !is_null($data["kota"]) ? $data["kota"] : "",
				"provinsi"				=> !is_null($data["provinsi"]) ? $data["provinsi"] : "",
				"PIC"					=> !is_null($data["pic"]) ? $data["pic"] : "",
				"pic_phone_number"		=> !is_null($data["pic_phone_number"]) ? $data["pic_phone_number"] : "",
				"teknisi_type"			=> isset($data["jenis_teknisi"]) ? $data["jenis_teknisi"] : "" ,

				"teknisi_phone_number"	=> isset($data["teknisi_phone_number"]) ? $data["teknisi_phone_number"] : "" ,
				"tgl_mulai_progress"	=> isset($data["tgl_mulai_proses"]) ? $data["tgl_mulai_proses"] : null,
				"TOL"					=> isset($data["TOL"]) ? $data["TOL"] : null,
				"status"				=> isset($data["status"]) ? $data["status"] : "" ,
				"keterangan"			=> isset($data["keterangan"]) ? $data["keterangan"] : "" ,
				"created_at"			=> date("Y-m-d H:i:s")
			);

			if($data["jenis_teknisi"]) {
				$field[$key]		    = isset($data["nama_teknisi"]) ? $data["nama_teknisi"] : "" ;
			}

			return $this->db->insert(PROJECT_UPDATE_LOCATION_TABLE,$field);
		}

		function project_update_location_update($data, $project_update_location_id){

			if($data["jenis_teknisi"] == "teknisi") {
				$key = "teknisi_id";
				$this->db->where('id', $project_update_location_id);
				$this->db->update(PROJECT_UPDATE_LOCATION_TABLE,[
					"freelance_id" => 0
				]);
			} else if($data["jenis_teknisi"] == "freelance") {
				$key = "freelance_id";
				$this->db->where('id', $project_update_location_id);
				$this->db->update(PROJECT_UPDATE_LOCATION_TABLE,[
					"teknisi_id" => 0
				]);
			} 

			$field = [
				"teknisi_type"			=> isset($data["jenis_teknisi"]) ? $data["jenis_teknisi"] : "",

				"teknisi_phone_number"	=> isset($data["hp_teknisi"]) ? $data["hp_teknisi"] : "",
				"tgl_mulai_progress"	=> $data["tgl_terima_proses"],
				"tol"					=> $data["tol"],
				"status"				=> $data["project_status_id"],
				"status_date"			=> isset($data["project_status_date"]) ? $data["project_status_date"]." ".$data["project_status_time"] : null,
				"keterangan"			=> $data["keterangan"]
			];

			if($data["jenis_teknisi"]) {
				$field[$key]			= isset($data["nama_teknisi"]) ? $data["nama_teknisi"] : "" ;
			}

			$this->db->where('id', $project_update_location_id);
			return $this->db->update(PROJECT_UPDATE_LOCATION_TABLE,$field);
		}

		function get_count_pul_bywoi() {
			$str = "SELECT count(kna.id)
			 FROM
				 ".DB_DATABASE2.".h_kontrak_nodelink kna
			 JOIN
				 ".DB_DATABASE2.".h_kontrak_layanan kla
				 ON kna.h_kontrak_layanan_id = kla.id
			 JOIN
				".DB_DATABASE2.".h_kontrak ka
				ON kla.h_kontrak_id = ka.id
			JOIN
				".DB_DATABASE2.".h_workorder wa
				ON wa.h_kontrak_id = ka.id
			WHERE
				wa.id = w.id";

			return $str;

		}

		function get_count_pulstatus_bykl($status) {
			$str = "SELECT count(pula.id)
			  FROM
				   ".DB_DATABASE.".tr_project_update_location pula
			   JOIN
				   ".DB_DATABASE.".tr_project_update pua
				   ON pula.project_update_id = pua.id
			   JOIN
				   ".DB_DATABASE2.".h_workorder wa
				   ON pua.h_workorder_id = wa.id
			   JOIN
				   ".DB_DATABASE2.".h_kontrak ka
				   ON wa.h_kontrak_id = ka.id
			   JOIN
				   ".DB_DATABASE2.".h_kontrak_layanan kla
				  ON kla.h_kontrak_id = ka.id
			   JOIN
				  ".DB_DATABASE2.".m_layanan mla
				  ON mla.id = kla.m_layanan_id
			   WHERE
			   	  `status` = $status AND
				   mla.id = ml.id
		   ";
		   return $str;
		}

		function get_count_pulstatus_bywoi($status) {
			$str = "SELECT count(pul.id)
			   FROM
				   ".DB_DATABASE.".tr_project_update_location pul
			   JOIN
				   ".DB_DATABASE.".tr_project_update pu
				   ON pul.project_update_id = pu.id
			   JOIN
				   ".DB_DATABASE2.".h_workorder wa
				   ON pu.h_workorder_id = wa.id
			   WHERE
			   	  `status` = $status AND
				   wa.id = w.id
		   ";
		   return $str;
	   }

		function get_count_pul_bystatus($status) {
			$str = "SELECT count(pul.id)
				FROM
					".DB_DATABASE.".tr_project_update_location pul
				JOIN
					".DB_DATABASE.".tr_project_update pu
					ON pul.project_update_id = pu.id
				JOIN
					".DB_DATABASE2.".h_workorder w
					ON pu.h_workorder_id = w.id
				JOIN
					".DB_DATABASE2.".h_kontrak k
					ON w.h_kontrak_id = k.id
				JOIN
					".DB_DATABASE2.".m_customer ca
					ON k.m_customer_id = ca.id
				WHERE
					`status` = $status AND
					ca.id = c.id
			";
			return $str;
		}

		function get_count_pul() {
		     $str = "SELECT count(kna.id)
			 FROM
				 ".DB_DATABASE2.".h_kontrak_nodelink kna
			 JOIN
				 ".DB_DATABASE2.".h_kontrak_layanan kla
				 ON kna.h_kontrak_layanan_id = kla.id
			 JOIN
				".DB_DATABASE2.".h_kontrak ka
				ON kla.h_kontrak_id = ka.id
			JOIN
				".DB_DATABASE2.".m_customer ca
				ON ka.m_customer_id = ca.id
			WHERE
				ca.id = c.id";

			return $str;
		}

		function get_count_pul_bylayanan(){
			$str = "SELECT count(kna.id)
			FROM
				".DB_DATABASE2.".h_kontrak_nodelink kna
			JOIN
				".DB_DATABASE2.".h_kontrak_layanan kla
				ON kna.h_kontrak_layanan_id = kla.id
			JOIN
				".DB_DATABASE2.".m_layanan mla
				ON kla.m_layanan_id = mla.id

			WHERE
				mla.id = ml.id";
	      return $str;
		}

		function get_total_node_bywoi($params = []) {

			$this->db->distinct();
			//$this->db->select("h_workorder_id");
			$this->db->select("w.id as `woi`");
			$this->db->select("rfs");
			$this->db->select("w.nomor");
			$this->db->select("w.nomor as no_wo");
			$this->db->select("(".$this->get_count_pul_bywoi().") as `jml_node`");
			$this->db->select("(".$this->get_count_pulstatus_bywoi(1).") as `pengadaan`");
			$this->db->select("(".$this->get_count_pulstatus_bywoi(2).") as `mod`");
			$this->db->select("(".$this->get_count_pulstatus_bywoi(3).") as `mos`");
			$this->db->select("(".$this->get_count_pulstatus_bywoi(4).") as `instalasi`");
			$this->db->select("(".$this->get_count_pulstatus_bywoi(5).") as `online`");
			$this->db->select("(".$this->get_count_pulstatus_bywoi(6).") as `pending`");
			$this->db->select("(".$this->get_count_pulstatus_bywoi(7).") as `cancel`");
			$this->db->select("(".$this->get_count_pulstatus_bywoi(8).") as `baa diterima`");

			if($params["top10"]) {
				$this->db->order_by("jml_node", "DESC");
				$this->db->limit(10);
			}

			$this->db->from(DB_DATABASE2.".h_workorder w");

			$result = $this->db->get();
			return $result->result_array();


		}

		function get_total_node_bycustomer(){

			$this->db->distinct();
			$this->db->select("c.id as 'customer_id'");
			$this->db->select("c.nama");
			$this->db->select("( SELECT count(wa.id)
					FROM
				".DB_DATABASE2.".h_workorder wa
					LEFT JOIN
				".DB_DATABASE2.".h_kontrak ka
				ON
					wa.h_kontrak_id = ka.id
				WHERE
					ka.m_customer_id = c.id
			) as `jml_wo`");
			$this->db->select("(".$this->get_count_pul().") as `jml_node`");
			$this->db->select("(".$this->get_count_pul_bystatus(1).") as `pengadaan`");
			$this->db->select("(".$this->get_count_pul_bystatus(2).") as `mod`");
			$this->db->select("(".$this->get_count_pul_bystatus(3).") as `mos`");
			$this->db->select("(".$this->get_count_pul_bystatus(4).") as `instalasi`");
			$this->db->select("(".$this->get_count_pul_bystatus(5).") as `online`");
			$this->db->select("(".$this->get_count_pul_bystatus(6).") as `pending`");
			$this->db->select("(".$this->get_count_pul_bystatus(7).") as `cancel`");
			$this->db->select("(".$this->get_count_pul_bystatus(8).") as `baa diterima`");
			$this->db->from(DB_DATABASE2.".m_customer c");

			$result = $this->db->get();

			return $result->result_array();

		}

		function get_total_node_bylayanan() {
			$this->db->distinct();
			$this->db->select("ml.id as 'layanan_id'");
			$this->db->select("ml.nama as nama_layanan");
			$this->db->select("( SELECT count(wa.id)
					FROM
				".DB_DATABASE2.".h_workorder wa
					LEFT JOIN
				".DB_DATABASE2.".h_kontrak ka
					ON wa.h_kontrak_id = ka.id
					LEFT JOIN
				".DB_DATABASE2.".h_kontrak_layanan kl
					ON ka.id = kl.h_kontrak_id
					LEFT JOIN
				".DB_DATABASE2.".m_layanan mlb
					ON kl.m_layanan_id = mlb.id
				WHERE
					mlb.id = ml.id
			) as `jml_wo`");
			$this->db->select("(".$this->get_count_pul_bylayanan().") as `jml_node`");
			$this->db->select("(".$this->get_count_pulstatus_bykl(1).") as `pengadaan`");
			$this->db->select("(".$this->get_count_pulstatus_bykl(2).") as `mod`");
			$this->db->select("(".$this->get_count_pulstatus_bykl(3).") as `mos`");
			$this->db->select("(".$this->get_count_pulstatus_bykl(4).") as `instalasi`");
			$this->db->select("(".$this->get_count_pulstatus_bykl(5).") as `online`");
			$this->db->select("(".$this->get_count_pulstatus_bykl(6).") as `pending`");
			$this->db->select("(".$this->get_count_pulstatus_bykl(7).") as `cancel`");
			$this->db->select("(".$this->get_count_pulstatus_bykl(8).") as `baa diterima`");
			$this->db->from(DB_DATABASE2.".h_kontrak_layanan kl");
			$this->db->join(DB_DATABASE2.".m_layanan ml","kl.m_layanan_id = ml.id");

			$result = $this->db->get();

			return $result->result_array();
		}

		function chart_tol_periode() {

			// by default 30 hari kebelakang
			$tol_start_date = !empty($this->input->post("tol_start_date")) ? date("Y-m-d",strtotime($this->input->post("tol_start_date"))) : date('Y-m-d', strtotime('-30 days')) ;
			$tol_end_date = !empty($this->input->post("tol_end_date")) ? date("Y-m-d",strtotime($this->input->post("tol_end_date"))) : date('Y-m-d') ;

			// print_r($tol_start_date, $tol_end_date);

			// echo $tol_start_date;
			// echo "<br>";
			// echo $tol_end_date;
			// echo "<br>";

			$this->db->distinct();
			$this->db->select("TOL");
			$this->db->select("count(pul.id) as jml_node");
			$this->db->from(DB_DATABASE.".tr_project_update_location pul") ;
			$this->db->where('TOL >=', $tol_start_date);
			$this->db->where('TOL <=', $tol_end_date);
			$this->db->group_by("TOL");

			$result = $this->db->get();

			return $result->result_array();
		}

		function chart_tto_periode(){  // BY PROJECT UPDATE
			// by default 30 hari kebelakang
			$tto_start_date = !empty($this->input->post("tto_start_date")) ? date("Y-m-d",strtotime($this->input->post("tto_start_date"))) : date('Y-m-d', strtotime('-30 days')) ;
			$tto_end_date = !empty($this->input->post("tto_end_date")) ? date("Y-m-d",strtotime($this->input->post("tto_end_date"))) : date('Y-m-d') ;

			// $tto_start_date = "2020-08-01";
			// $tto_end_date = "2020-09-30";

			$this->db->distinct();
			$this->db->select("w.tgl");
			$this->db->select(" count(kn.sid) as `jml_node`");

			$this->db->from(DB_DATABASE2.".h_workorder w");
			$this->db->join(DB_DATABASE2.".h_kontrak k","w.h_kontrak_id = k.id");
			$this->db->join(DB_DATABASE2.".h_kontrak_layanan kl","kl.h_kontrak_id = k.id");
			$this->db->join(DB_DATABASE2.".h_kontrak_nodelink kn","kn.h_kontrak_layanan_id = kl.id");

			$this->db->where('w.tgl >=', $tto_start_date);
			$this->db->where('w.tgl <=', $tto_end_date);
			$this->db->group_by("w.tgl");

			$result = $this->db->get();

			return $result->result_array();
		}

		// function chart_tto_periode(){  // BY PROJECT UPDATE
		// 	// by default 30 hari kebelakang
		// 	$tto_start_date = !empty($this->input->post("tto_start_date")) ? date("Y-m-d",strtotime($this->input->post("tto_start_date"))) : date('Y-m-d', strtotime('-30 days')) ;
		// 	$tto_end_date = !empty($this->input->post("tto_end_date")) ? date("Y-m-d",strtotime($this->input->post("tto_end_date"))) : date('Y-m-d') ;

		// 	$this->db->distinct();
		// 	$this->db->select("w.tgl");
		// 	$this->db->select("count(kn.id) as `jml_node`");

		// 	$this->db->from(DB_DATABASE2.".h_kontrak_nodelink kn");
		// 	$this->db->join(DB_DATABASE2.".h_kontrak_layanan kl","kn.h_kontrak_layanan_id = kl.id");
		// 	$this->db->join(DB_DATABASE2.".h_kontrak k","kl.h_kontrak_id = k.id");
		// 	$this->db->join(DB_DATABASE2.".h_workorder w","w.h_kontrak_id = k.id");

		// 	$this->db->where('w.tgl >=', $tto_start_date);
		// 	$this->db->where('w.tgl <=', $tto_end_date);
		// 	$this->db->group_by("w.tgl");

		// 	$result = $this->db->get();

		// 	return $result->result_array();
		// }

		function get_detail_status($status) {

			$this->db->where("status_name",$status);
			$q = $this->db->get(PROJECT_STATUS_TABLE);
			return $q->row_array();
		}

		function get_detail_status_id($status) {

			$this->db->where("id",$status);
			$q = $this->db->get(PROJECT_STATUS_TABLE);
			return $q->row_array();
		}

		function check_sid($sid, $project_update_id){
			$this->db->where("sid",$sid);
			$this->db->where("project_update_id",$project_update_id);
			$q = $this->db->get(PROJECT_UPDATE_LOCATION_TABLE);
			return $q->row_array();
		}

		function get_project_update_location_history($params) {

			$limit         = isset($params["limit"]) ? $params["limit"] : 0;
            $offset         = isset($params["offset"]) ? $params["offset"] : 0;
            $order          = isset($params["order"]) ? $params["order"] : null;
            $col            = isset($params["col"]) ? $params["col"] : null;
            $dir            = isset($params["dir"]) ? $params["dir"] : null;
			$search         = isset($params["search"]) ? $params["search"] : "";

            $valid_columns  = array(
                0=>"id",
				1=>'user_id',
				2=>"username",
                3=>'nama_project',
                4=>'pelanggan',
                5=>'no_wo',
				6=>"pic",
				7=>"project_status",
            );

            if(!empty($order))
            {
                foreach($order as $o)
                {
                    $col = $o['column'];
                    $dir= $o['dir'];
                }
            }

            if($dir != "asc" && $dir != "desc")
            {
                $dir = "desc";
            }

            if(!isset($valid_columns[$col]))
            {
                $order = null;
            }
            else
            {
                $order = $valid_columns[$col];
            }

			$this->db->select("id,project_update_location_id,project_update_id,user_id,username,nama_project,pelanggan,no_wo,pic,project_status, keterangan,created_at");

			$result = $this->db;

            if($order != null)
            {
                $result->order_by($order, $dir);
            }

            if(!empty($search))
            {
                $x=0;
                foreach($valid_columns as $sterm)
                {
                    if($x==0)
                    {
                        $result->like($sterm,$search);
                    }
                    else
                    {
                        $result->or_like($sterm,$search);
                    }
                    $x++;
                }
            }

            if(!empty($limit) && !empty($offset)){
                $result = $result->get(PROJECT_UPDATE_LOCATION_RECORD,$params["limit"],$params["offset"]);
            }else{
                $result = $result->get(PROJECT_UPDATE_LOCATION_RECORD);
            }

            $result = $result->result_array();

            return $result;
		}

		function pul_record_insert($data) {

			return $this->db->insert(PROJECT_UPDATE_LOCATION_RECORD,[
                "user_id" 					 		=> $data["user_id"],
				"username" 					 		=> $data["username"],
				"session_id"						=> $this->session->my_session_id,
				"project_update_location_id" 		=> $data["project_update_location_id"],
				"project_update_id"					=> $data["project_update_id"],
				"nama_project" 						=> $data["nama_project"],
				"pelanggan"							=> $data["pelanggan"],
				"no_wo"								=> $data["no_wo"],
				"pic"								=> $this->session->userdata("username"),
				"project_status"					=> $data["project_status"],
				"keterangan"						=> $data["keterangan"],
				"created_at"						=> date("Y-m-d H:i:s")
            ]);
		}

		function get_layanan_bykontrak($kontrak_id = "") {

			$str = "SELECT * FROM ".DB_DATABASE2.".h_kontrak_layanan WHERE h_kontrak_id = $kontrak_id";
			$q = $this->db->query($str);
			$f = $q->result_array();

			return $f;
		}

		function get_layanan_kontrak($kontrak_layanan_id = "") {

			$str = "SELECT * FROM ".DB_DATABASE2.".h_kontrak_layanan WHERE id = '$kontrak_layanan_id'";
			$q = $this->db->query($str);
			$f = $q->result_array();

			return $f;
		}

		function get_all_layanan() {
			$str = "SELECT * FROM ".DB_DATABASE2.".m_layanan";
			$q = $this->db->query($str);
			$f = $q->result_array();

			return $f;
		}

		function get_layanan($layanan_id = "") {
			$str = "SELECT * FROM ".DB_DATABASE2.".m_layanan WHERE id = '$layanan_id'";
			$q = $this->db->query($str);
			$f = $q->row_array();

			return $f;
		}

		function get_kontrak_nodelink($h_kontrak_nodelink_id = "") {
			$str = "SELECT * FROM ".DB_DATABASE2.".h_kontrak_nodelink WHERE id = '$h_kontrak_nodelink_id'";
			$q = $this->db->query($str);
			$f = $q->row_array();

			return $f;
		}

		function get_all_customer(){
			$str = "SELECT * FROM ".DB_DATABASE2.".m_customer";
			$q = $this->db->query($str);
			$f = $q->result_array();

			return $f;
		}

		function pul_count_status($status) {

			$qry  = "SELECT count(id) as count_status FROM ".DB_DATABASE.".".PROJECT_UPDATE_LOCATION_TABLE."
				WHERE ";

			$qry .= " status = $status ";

			$q = $this->db->query($qry);
			return $q->row_array();
		}

		function project_woi_count() {
			$q = $this->db->query("SELECT count as count_woi FROM ");
			return $q->row_array();
		}

    	function project_sid_count() {
			$q = $this->db->query("SELECT count(DISTINCT sid) as count_sid FROM ".DB_DATABASE2.".h_kontrak_nodelink kn
			 LEFT JOIN ".DB_DATABASE2.".h_workorder_nodelink wn ON wn.h_kontrak_nodelink_id = kn.id
			 RIGHT JOIN ".DB_DATABASE2.".h_workorder w ON wn.h_workorder_id = w.id
			 WHERE YEAR(w.tgl) = ".date("Y")." ");
			return $q->row_array();
		}

		function project_update_highlighted() {
			$q = $this->db->query("SELECT * FROM ".DB_DATABASE.".".PROJECT_UPDATE_TABLE." WHERE highlighted = 1");
			return $q->result_array();
		}

		function kontrak_nodelink_all() {
			$q = $this->db->query("SELECT count(DISTINCT sid) as count_sid FROM ".DB_DATABASE2.".h_kontrak_nodelink kn
			 LEFT JOIN ".DB_DATABASE2.".h_workorder_nodelink wn ON wn.h_kontrak_nodelink_id = kn.id
			 RIGHT JOIN ".DB_DATABASE2.".h_workorder w ON wn.h_workorder_id = w.id  ");
			return $q->result_array();
		}

		function get_workorder_nodelink($params) {
			$str  = "SELECT * FROM ".DB_DATABASE2.".h_workorder_nodelink wn 
				JOIN ".DB_DATABASE2.".h_workorder w 
					ON wn.h_workorder_id = w.id WHERE ";

			if($params["month"]) {

				if(!$params["year"]) {
					$params["year"] = date("Y");
				}

				$str .= " MONTH(w.tgl) = '$params[month]' AND YEAR(w.tgl) = '$params[year]' AND ";
			}

			$str .= " 1=1 ";
			$q = $this->db->query($str);

			if($params["count"]) {
				return $q->num_rows();
			}

			return $q->result_array();
		}

		function project_update_highlight_update($project_update_id, $data) {

			$field = [
				"highlighted" 		=> $data["highlighted"], // 1 OR 0
				"highlighted_desc" 	=> $data["highlighted_desc"],
			];

			$this->db->where('id', $project_update_id);
			return $this->db->update(PROJECT_UPDATE_TABLE,$field);

		}
 
	}
	
	
